
-- SUMMARY --


The Drupal module Magellan is designed to add a curriculum mapping interface to the Daedalus module.


-- REQUIREMENTS


Depends on: Menu, Daedalus


-- INSTALLATION --


Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --


1) User roles are created during the installation of the required module Daedalus.
2) Permissisions by role are pre-configured with the installation of the required module Daedalus.


-- CUSTOMIZATION --


Customize various site settings here: /daedalus/manage/settings


-- TROUBLESHOOTING --


None.


-- FAQ --


None.


-- CONTACT --


Current maintainers:
	Christian Blouin, Assoc. Prof., Dalhousie University (cblouin@cs.dal.ca)
	Justin Joyce, BCS., Dalhousie Univeristy (joyce@cs.dal.ca)