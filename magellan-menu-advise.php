<?php

// $ID$

/**
 * @file magellan-menu-advise.php
 * Created by Dr. Blouin and Justin Joyce
 */

//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Advise Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_advise_page() {
	return drupal_get_form('daedalus_advise_form');
}

/**
* Menu Location: Daedalus -> Advise
* URL Location:  daedalus/daedalus/advise
*
* Displays the Program menu.
* Links to:
* 	* Advise Student
*	* Session Log
*	* Session History
*
* @global <type> $base_url
* @param  <type> $form
* @return string
*/
function daedalus_advise_form( $form ) {

    // This gives the base site url.
    // This is also a variable that may be
    // configured in the settings for drupal.
    global $base_url;

    // Set current page url.
    $page_url = daedalus_get_setting("advise");

    // Set a blank title
    drupal_set_title('Magellan Advise');

    $form = array();

    // Displays the program menu items. The menu item URLs
    // are stored in the correct order in the database.
    $result = db_query("SELECT * FROM {dae_settings} WHERE type = 'url' ORDER BY id");

    while($row = db_fetch_array($result)) {
        $setting = $row['value'];
        $title = ucwords($row['setting']);

        $pos = strpos($setting,$page_url);

        if( $pos !== FALSE && $setting != $page_url) {
            $program_list .= "<li><a href='".$base_url."/".$setting."'>".$title."</a></li>";
        }
    }

    $form[] = array(
        '#type'  => 'item',
        '#value' => t('<ul>'.$program_list.'</ul>'),
    );

    return $form;
} // function daedalus_program_form($form)


//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Program->Advise_Student Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_advise_student_page() {
    return drupal_get_form("daedalus_advise_student_form");
};

/**
* Menu Location: Daedalus -> Advise -> Advise Student
* URL Location:  daedalus/daedalus/advise/advise_student
*
* Displays
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_advise_student_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Get current page url.
    $page_url = $help_url = daedalus_get_setting("advise student");

    // Store URL Parameters in $param array.
    $page_url_length = sizeof(explode('/',$page_url));

    $param = array();
    $param[0] = arg(0+$page_url_length);
    $param[1] = arg(1+$page_url_length);

    // Determine the advisor id
    global $user;
    $advisor_id = $user->uid;

    // Get the images.
    $question_img_src = $base_url."/".daedalus_get_setting("question mark");

// USEFUL TO REMOVE FILES WHERE YOU DON"T HAVE PERMISSIONS
//exec('rm -r /var/www/html/daedalus_dev/daedalus_test/sites/default');

    $form = array();

    // Add the hidden help form. Paramaters are
    // (page url, show border, show break).
    $form = display_hidden_form($help_url, 1, 1);

    if( $param[0] == 'submit' ) {

        drupal_set_title(t("Submit Student Information <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));

        $form['first-name'] = array(
            '#type'  => 'textfield',
            '#title' => t('Enter First Name'),
        );

        $form['last-name'] = array(
            '#type'  => 'textfield',
            '#title' => t('Enter Last Name'),
        );

        $form['submit-student'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );
        
        $form['student-user-name'] = array( '#type' => 'value', '#value' => $param[1], );

    }
    
    else {

        $result = db_query("SELECT id, student_id FROM {mag_advisor_session} WHERE advisor_id=%d", $advisor_id);
        while( $row = db_fetch_array($result) ) {
            $session_id  = $row['id'];
            $session_sid = $row['student_id'];
        }

        // Get the session name.
        $current_session = $_COOKIE[session_name()];
    //echo "DB Session > ".$session_id."<br />";
    //echo "Cur Session > ".$current_session."<br />";
    //echo "Adv Identif > ".$advisor_id."<br />";

        // Reload the $_COOKIE[session_name()] if it happens to have been
        // created previously. Only reload if there is a session saved in
        // the session log and this session does not appear in the current
        // advisor session table.
        if( !db_result(db_query("SELECT COUNT(*) FROM {mag_advisor_session} WHERE id='%s' AND advisor_id=%d", $session_id, $advisor_id)) && db_result(db_query("SELECT COUNT(*) FROM {mag_session_log} WHERE session_id='%s'", $current_session)) ) {

            // This generates a new session id.
            $_COOKIE[session_regenerate_id()];

            drupal_goto($base_url."/".$page_url."/reload");

        }

        // Before beginning a session ensure that the session is not in use by another user.
        if( db_result(db_query("SELECT COUNT(*) FROM {mag_advisor_session} WHERE id='%s' AND advisor_id != %d", $current_session, $advisor_id)) ) {

            // If it is in use, reload the session and try again.
            // This generates a new session id.
            $_COOKIE[session_regenerate_id()];

            drupal_goto($base_url."/".$page_url."/reload");

        }

        // If current advisor has a session stored in {mag_advisor_session},
        // remove this session if the session id does not match the current
        // session id.  This indicates that the advisor did not end a previous session.
        if( $session_id && $session_id != $current_session ) {

            // Display the title
            drupal_set_title(t("Advise Students <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));

            $message = "<b>Warning</b>:<br />You are currently advising under a different session id.
                        <br /><br />This is possible if you logged into Daedalus:
                        <br /><ul><li>with another web browser.</li>
                                  <li>with another computer.</li></ul>
                        <br />This is also possible if you began a session and:
                        <br /><ul><li>turned off your computer.</li>
                                  <li>logged out of Daedalus.</li></ul>";

            $form[] = array(
                '#type' => 'item',
                '#value' => t($message),
            );

            $form['end-previous-session'] = array(
                '#type' => 'submit',
                '#value' => t('End session'),
            );

            $form['adopt-session'] = array(
                '#type' => 'submit',
                '#value' => t('Adopt session'),
            );

            // Pass the hidden advisor id.
            $form['advisor-id'] = array( '#type' => 'value', '#value' => $advisor_id );
            $form['previous-session-id'] = array( '#type' => 'value', '#value' => $session_id );
            $form['current-session-id'] = array( '#type' => 'value', '#value' => $current_session );

        }

        else {

            // Current session, ask the user if
            // they would like to end the session.
            if( $session_id == $current_session ) {

                $student_id = db_result(db_query("SELECT student_id FROM {mag_advisor_session} WHERE id=%d", $session_id));

                $student_info = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE id=%d", $student_id));

                $first_name = $student_info['first_name'];
                $last_name = $student_info['last_name'];

                // Display the title
                drupal_set_title(t("Advising Student: " . $first_name . " " . $last_name . " <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));

                $session_time = db_result(db_query("SELECT session_time FROM {mag_session_log} WHERE advisor_id=%d AND student_id=%d AND session_id='%s'", $advisor_id, $student_id, $session_id));

                $current_time = date("Y-m-d H:i:s", time());

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // Date difference calculation thanks to http://stackoverflow.com/questions/676824/how-to-calculate-the-difference-between-two-dates-using-php;
                $date1 = $current_time;
                $date2 = $session_time;

                $diff = abs(strtotime($date2) - strtotime($date1));

                $years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
                $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
                $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

                // Format the diffence to
                // a user friendly output.
                if( $hours ) {
                    $open_string = $hours . ' hours ';
                }

                if( $minutes ) {
                    $open_string .= $minutes . ' minutes ';
                }

                if( $seconds ) {

                    if( $hours || $minutes ) {
                        $open_string .= 'and ' . $seconds . ' seconds';
                    }

                    else {
                        $open_string .= $seconds . ' seconds';
                    }

                }

                if( $hours >= 24 ) {
                    drupal_set_message(t('&nbsp Current session created more than one day ago.'), 'warning');
                }

                $form['end-session'] = array(
                    '#type' => 'submit',
                    '#value' => t('End current session'),
                    '#prefix' => t('<b>Current session active for: ' . $open_string. '</b><br />'),
                );

                // Pass the hidden advisor id.
                $form['advisor-id'] = array( '#type' => 'value', '#value' => $advisor_id );
                $form['session-id'] = array( '#type' => 'value', '#value' => $session_id );

            }

            // Have the advisor select a student
            // to create a session with.
            else {

                // Display the title
                drupal_set_title(t("Advise Students <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));

                $form['student-select'] = array(
                    '#type' => 'textfield',
                    '#title' => t('Enter username'),
                    '#size' => 50,
                    '#default_value' => $param[0],
                    '#autocomplete_path' => 'autocomp/student',
                    '#description' => t('Select student by their Faculty of Computer Science username.')
                );

                $form['student-submit'] = array(
                    '#type' => 'submit',
                    '#value' => t('Select student'),
                );

                // Pass the hidden advisor id.
                $form['advisor-id'] = array( '#type' => 'value', '#value' => $advisor_id );

            }

        }

    }

    return $form;

}

/**
 * Implementation of hook_submit().
 */
function daedalus_advise_student_form_submit( $form, &$form_state ) {

    global $base_url;

    switch( $form_state['values']['op'] ) {

        case $form_state['values']['student-submit']:

            // Get the advisors user id.
            $advisor_id = $form_state['values']['advisor-id'];

            // Determine the student's user id.
            $student_id = db_result(db_query("SELECT id FROM {mag_student_identification} WHERE user_name='%s'", $form_state['values']['student-select']));

            if( $student_id  ) {

                $page_url = daedalus_get_setting("session log");

                // Session id
                $session = $_COOKIE[session_name()];

                db_query("INSERT INTO {mag_advisor_session} (id, advisor_id, student_id) VALUES ('%s', %d, %d)", $session, $advisor_id, $student_id);

                $session_time = DATE("Y-m-d H:i:s");

                // The date one day ago
                $yesterday = date("Y-m-d H:i:s", time()-86400);

                // This value indicates the first instance of an empty session in the database. An empty session is a
                // session that does not have a session note or a session file. These values are set by a flag whenever
                // a session is updated or saved again. This id will indicate wether to overwrite a session in the database.
                $overwrite = db_result(db_query("SELECT id FROM {mag_session_log} WHERE note=0 AND file=0 AND entry=0 AND session_time < '%s' ORDER BY id", $yesterday));

                if( $overwrite ) {

                    drupal_set_message(t('&nbsp Session created by overwriting an empty session.'));

                    db_query("UPDATE {mag_session_log} SET advisor_id=%d, student_id=%d, session_id='%s', session_time='%s' WHERE id=%d", $advisor_id, $student_id, $session, $session_time, $overwrite);

                }

                else {

                    drupal_set_message(t('&nbsp Session created.'));

                    db_query("INSERT INTO {mag_session_log} (advisor_id, student_id, session_id, session_time) VALUES (%d, %d, '%s', '%s')", $advisor_id, $student_id, $session, $session_time);

                }

                drupal_goto($base_url."/".$page_url);

            }

            else {

                $page_url = daedalus_get_setting("advise student");

                drupal_goto($base_url."/".$page_url."/submit/".$form_state['values']['student-select']);

            }

            break;

        case $form_state['values']['submit-student']:

            $page_url = daedalus_get_setting("advise student");

            $user_name = $form_state['values']['student-user-name'];
            $first_name = $form_state['values']['first-name'];
            $last_name = $form_state['values']['last-name'];

            db_query("INSERT INTO {mag_student_identification} (user_name, first_name, last_name)
                               VALUES ('%s','%s','%s')", $user_name, $first_name, $last_name );

            drupal_goto($base_url."/".$page_url."/".$user_name);

            break;

        case $form_state['values']['end-session']:

            $page_url = daedalus_get_setting("advise student");

            db_query("DELETE FROM {mag_advisor_session} WHERE id=%d", $form_state['values']['session-id']);

            drupal_set_message(t('&nbsp The current session has been closed.'));

            // This generates a new session id.
            $_COOKIE[session_regenerate_id()];

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['end-previous-session']:

            $page_url = daedalus_get_setting("advise student");

            db_query("DELETE FROM {mag_advisor_session} WHERE id=%d", $form_state['values']['previous-session-id']);

            drupal_set_message(t('&nbsp The other session has been closed.'));

            drupal_goto($base_url."/".$page_url);

            break;
        
        case $form_state['values']['adopt-session']:

            $previous_session = $form_state['values']['previous-session-id'];
            $current_session = $form_state['values']['current-session-id'];
            $advisor_id = $form_state['values']['advisor-id'];
            
            $page_url = daedalus_get_setting("advise student");
            
            db_query("UPDATE {mag_advisor_session} SET id='%s' WHERE id='%s' AND advisor_id=%d", $current_session, $previous_session, $advisor_id );
            
            db_query("UPDATE {mag_session_log} SET session_id='%s' WHERE session_id='%s' AND advisor_id=%d", $current_session, $previous_session, $advisor_id );

            drupal_set_message("&nbsp; Session adopted.");

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['dae-help-submit']:

            $page_url = daedalus_get_setting("advise student");

            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message("&nbsp; Help information saved.");


                drupal_goto($base_url."/".$page_url);

            }

            break;
    }

}


//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Program->Session_Log Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_session_log_page() {
    return drupal_get_form("daedalus_session_log_form");
};

/**
* Menu Location: Daedalus -> Advise -> Session Log
* URL Location:  daedalus/daedalus/advise/session_log
*
* Displays
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_session_log_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Get current page url.
    $page_url = $help_url = daedalus_get_setting("session log");

    // Store URL Parameters in $param array.
    $page_url_length = sizeof(explode('/',$page_url));

    $param = array();
    $param[0] = arg(0+$page_url_length);
    $param[1] = arg(1+$page_url_length);
    $param[2] = arg(2+$page_url_length);
    $param[3] = arg(3+$page_url_length);

    // Access and permissions
    $edit_access = user_access("magellan advise edit log");

    // Determine the user name
    global $user;
    $advisor_id = $user->uid;

    $current_session = $_COOKIE[session_name()];

    // Get the images.
    $question_img_src = $base_url."/".daedalus_get_setting("question mark");

    $form = array();

    // Add the hidden help form. Paramaters are
    // (page url, show border, show break).
    $form = display_hidden_form($help_url, 1, 1);

    $session_id = db_result(db_query("SELECT id FROM {mag_advisor_session} WHERE advisor_id=%d", $advisor_id));
//echo "DB Session > ".$session_id."<br />";
//echo "Cur Session > ".$current_session."<br />";

    // If redirected from a session update and the
    $current_logid = db_result(db_query("SELECT id FROM {mag_session_log} WHERE advisor_id=%d AND session_id='%s'", $advisor_id, $current_session));
    if( $param[0] == 'update' && $current_logid == $param[1] ) {

        drupal_set_message(t('&nbsp Redirected from session log update REMOVE LATER......'));

        drupal_goto($base_url."/".$page_url);

    }

    if( $param[0] == 'delete' ) {

        // Display the title
        drupal_set_title(t("Delete Confimation <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));

        $message ="<br />
                   Are you sure you want to delete the current session log? This can not be undone.
                   <br /><br />
                   <ul>
                        <li>This will delete all references to the session notes.</li>
                        <li>This will delete all references to the sessions files.</li>
                        <li>This will end the current advising session.</li>
                   </ul>
                   <br />Are you sure you want to continue?";

        $form[] = array(
            '#type' => 'item',
            '#value' => t($message),
        );

        $form['delete-confirm'] = array(
            '#type' => 'submit',
            '#value' => t("I understand the consequences. Delete this session log forever."),
        );

        $form['delete-reverse'] = array(
            '#type' => 'submit',
            '#value' => t("On second thought, I better not. Take me back."),
        );

        // Submit hidden information.
        $form['session-id'] = array( '#type' => 'value', '#value' => $param[1], );
        $form['advisor-id'] = array( '#type' => 'value', '#value' => $param[2], );

    }

    elseif( $param[0] == 'delete_update' ) {

        // Display the title
        drupal_set_title(t("Delete Confimation " . $log_id . "<a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));

        $message ="<br />
                   Are you sure you want to delete session <strong><u>log ".$param[1]."</u></strong>? This can not be undone.
                   <br /><br />
                   <ul>
                        <li>This will delete all references to the session notes.</li>
                        <li>This will delete all references to the sessions files.</li>
                   </ul>
                   <br />Are you sure you want to continue?";

        $form[] = array(
            '#type' => 'item',
            '#value' => t($message),
        );

        $form['delete-update-confirm'] = array(
            '#type' => 'submit',
            '#value' => t("I understand the consequences. Delete this session log forever."),
        );

        $form['delete-reverse'] = array(
            '#type' => 'submit',
            '#value' => t("On second thought, I better not. Take me back."),
        );

        // Submit hidden information.
        $form['log-id'] = array( '#type' => 'value', '#value' => $param[1], );
        $form['advisor-id'] = array( '#type' => 'value', '#value' => $param[2], );

    }

    elseif( $param[0] == 'delete_file' ) {

        // Display the title
        drupal_set_title(t("Delete File Confimation <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));

        $message ="<br />
                   Are you sure you want to delete the <b>" . $param[3] . "</b> file? This can not be undone.
                   <br /><br />
                   <ul>
                        <li>This will delete all references to the session file.</li>
                        <li>Change this to JS to warn users that session changes will be discarded.</li>
                   </ul>
                   <br />Are you sure you want to continue?";

        $form[] = array(
            '#type' => 'item',
            '#value' => t($message),
        );

        $form['delete-file-confirm'] = array(
            '#type' => 'submit',
            '#value' => t("I understand the consequences. Delete this session file forever."),
        );

        $form['delete-file-reverse'] = array(
            '#type' => 'submit',
            '#value' => t("On second thought, I better not. Take me back."),
        );

        // Submit hidden information.
        $form['session-file'] = array( '#type' => 'value', '#value' => $param[1], );
        $form['log-id'] = array( '#type' => 'value', '#value' => $param[2], );
        $form['file-type'] = array( '#type' => 'value', '#value' => $param[3], );

    }

    // Download a file
    elseif( $param[0] == 'download_file' ) {

        $file_name = $param[1];
        $log_id = $param[2];

        // Check if the directory has already been created
        $download_directory = file_directory_path() . '/downloads';
        //$download_directory = absolute_path_to_drupal()."/graphviz/downloads"; // Tried writng to graphviz to no success...

        if( !file_exists($download_directory) ) {

            // Create a new directory for this sessions files.
            exec('mkdir ' . $download_directory);

            // Modify the permissions of this new directory.
            exec('chmod 775 ' . $download_directory);

        }

        $file_location = file_directory_path() . '/session'. $log_id . '/' . $file_name;
        $download_location = $download_directory . '/' . $file_name;

        // Copy the file to the download directory.
        // This directory will make it easy to collect
        // the garbage once the file is downloaded.
        exec('cp ' . $file_location . ' ' . $download_location);

        $file_contents = file_get_contents($download_location);

        if( $file_contents ){

            $decrypted_contents = aes_decrypt($file_contents);

            $decrypted_file = fopen($download_location, 'w');

            fwrite($decrypted_file, $decrypted_contents);

            fclose($decrypted_file);

        }

        // Link to the newly decrypted file.
        drupal_goto( file_create_url('downloads/' . $file_name) );

    }

    // View a session
    elseif( $param[0] == 'view' ) {

        $log_id = $param[1];

        // Display the title
        drupal_set_title(t("View Session Log <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));

        $session_info = db_fetch_array(db_query("SELECT id, advisor_id, student_id, session_time, meeting_type, session_note FROM {mag_session_log} WHERE id=%d", $log_id));

        $db_session_id = $session_info['id'];
        $advisor_id = $session_info['advisor_id'];
        $student_id = $session_info['student_id'];
        $session_time = $session_info['session_time'];
        $meeting_type = $session_info['meeting_type'];
        $session_note = $session_info['session_note'];


        if( $session_note ) {

            // Decrypt the session note.
            $session_note = aes_decrypt($session_note);

            // Trim Please
            $session_note = trim($session_note);

            // Remove any trailing new lines.
            if( substr($session_note, -6) == "<br />" ) {
                $session_note = substr_replace($session_note, '', -6);
            }

        }

        // Remove the seconds
        $time_display = substr_replace($session_time, '', strlen($session_time) - 3, strlen($session_time));

        $advisor_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $advisor_id));

        $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE id=%d", $student_id));

        $first_name   = $student['first_name'];
        $last_name    = $student['last_name'];

        $result = db_query("SELECT file_name, file_type FROM {mag_session_file} WHERE log_id=%d", $log_id);
        while( $row = db_fetch_array($result) ) {

            //$file_url = '<a href="' . file_create_url('session' . $log_id . '/' . $row['file_name']) . '" target="_blank" >' . $row['file_type'] . '</a>';
            $file_url = '<a href="' . $base_url . '/' . $page_url . '/download_file/' . $row['file_name'] . '/' . $log_id . '" target="_blank" >' . $row['file_type'] . '</a>';

            $upload_list .= '<li>' . $file_url . '</li>';

        }

        $form['session-log'] = array(
            '#type' => 'fieldset',
            '#title' => t('Academic Advising Session: ' . $log_id),
            '#collapsible' => FALSE,
            '#collapsed' => FALSE,
        );

        $form['session-log'][] = array(
            '#type' => 'checkbox',
            '#title' => t('In person session'),
            '#default_value' => $meeting_type,
            '#disabled' => TRUE,
            '#prefix' => '<blockquote>',
            '#suffix' => '</blockquote>',
        );

        $form['session-log'][] = array(
            '#type' => 'item',
            '#title' => t('Advisor'),
            '#value' => t($advisor_name),
            '#size' => 30,
            '#prefix' => '<blockquote><table><tr><td>',
            '#suffix' => '</td>',
        );

        $form['session-log'][] = array(
            '#type' => 'item',
            '#title' => t('Student'),
            '#value' => t($first_name." ".$last_name),
            '#size' => 30,
            '#prefix' => '<td>',
            '#suffix' => '</td>',
        );

        $form['session-log'][] = array(
            '#type' => 'item',
            '#title' => t('Session date and time'),
            '#value' => t($time_display),
            '#size' => 30,
            '#prefix' => '<td>',
            '#suffix' => '</td></tr></table></blockquote>',
        );

        $form['session-log'][] = array(
            '#type' => 'item',
            '#title' => t('Session Note'),
            '#value' => t($session_note),
            '#prefix' => '<blockquote>',
            '#suffix' => '</blockquote>',
        );

        if( $upload_list ) {

            $form['session-log'][] = array(
                '#type' => 'item',
                '#title' => t('Uploaded Files'),
                '#value' => t('<ol>'.$upload_list.'</ol>'),
                '#prefix' => '<br /><blockquote>',
                '#suffix' => '</blockquote>',
            );

        }else{

            $form['session-log'][] = array(
                '#type' => 'item',
                '#suffix' => '<br />',
            );

        }

        if( $session_id == $current_session ) {

            $form['session-log']['form-snapshot'] = array(
                '#type' => 'submit',
                '#value' => t('View form snapshot'),
                '#prefix' => '<br /><blockquote>',
            );

            $form['return-session'] = array(
                '#type' => 'submit',
                '#value' => t('Return To Current Advising Session'),
                '#suffix' => '</blockquote><br />',
            );

        }

        // Hidden session id
        $form['db-session-id'] = array( '#type' => 'value', '#value' => $db_session_id );

    }

    // Update a session
    elseif( $param[0] == 'update' ) {

        $log_id = $param[1];

        // Display the title
        drupal_set_title(t("Update Session Log <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));
        
        $session_info = db_fetch_array(db_query("SELECT id, advisor_id, student_id, session_time, meeting_type, session_note FROM {mag_session_log} WHERE id=%d", $log_id));

        $db_session_id = $session_info['id'];
        $advisor_id = $session_info['advisor_id'];
        $student_id = $session_info['student_id'];
        $session_time = $session_info['session_time'];
        $meeting_type = $session_info['meeting_type'];
        $session_note = $session_info['session_note'];


        if( $session_note ) {

            // Decrypt the session note.
            $session_note = aes_decrypt($session_note);

            // Trim Please
            $session_note = trim($session_note);

            // Remove any trailing new lines.
            if( substr($session_note, -6) == "<br />" ) {
                $session_note = substr_replace($session_note, '', -6);
            }

        }

        // Remove the seconds
        $time_display = substr_replace($session_time, '', strlen($session_time) - 3, strlen($session_time));

        $advisor_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $advisor_id));

        $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE id=%d", $student_id));

        $first_name   = $student['first_name'];
        $last_name    = $student['last_name'];

        $result = db_query("SELECT file_name, file_type FROM {mag_session_file} WHERE log_id=%d", $log_id);
        while( $row = db_fetch_array($result) ) {

            //$file_url = '<a href="' . file_create_url('session' . $log_id . '/' . $row['file_name']) . '" target="_blank" >' . $row['file_type'] . '</a>';
            $file_url = '<a href="' . $base_url . '/' . $page_url . '/download_file/' . $row['file_name'] . '/' . $log_id . '" target="_blank" >' . $row['file_type'] . '</a>';

            $delete_url = '<a href="' . $base_url . '/' . $page_url . '/delete_file/' . $row['file_name'] . '/' . $log_id . '/' . $row['file_type'] . '">delete</a>';

            $upload_list .= '<li>' . $file_url . ' - ' . $delete_url . '</li>';

        }

        // Remove the temporary downloads directory
        //exec('rm -r ' . file_directory_path() . "/downloads");

        $form['session-log'] = array(
            '#type' => 'fieldset',
            '#title' => t('Academic Advising Session: ' . $log_id),
            '#collapsible' => FALSE,
            '#collapsed' => FALSE,
        );

        $form['session-log']['session-type'] = array(
            '#type' => 'checkbox',
            '#title' => t('In person session'),
            '#default_value' => $meeting_type,
            '#prefix' => '<blockquote>',
            '#suffix' => '</blockquote>',
        );

        if( $edit_access ){

            $form['session-log']['session-advisor'] = array(
                '#type' => 'textfield',
                '#title' => t('Advisor'),
                '#default_value' => t($advisor_name),
                '#size' => 30,
                '#autocomplete_path' => 'autocomp/advisor',
                '#prefix' => '<blockquote><table><tr><td>',
                '#suffix' => '</td>',
            );

            $form['session-log']['session-student'] = array(
                '#type' => 'textfield',
                '#title' => t('Student'),
                '#default_value' => t($first_name." ".$last_name),
                '#size' => 30,
                '#autocomplete_path' => 'autocomp/student',
                '#prefix' => '<td>',
                '#suffix' => '</td>',
            );

            $form['session-log']['session-time-display'] = array(
                '#type' => 'textfield',
                '#title' => t('Session date and time'),
                '#default_value' => t($time_display),
                '#size' => 30,
                '#prefix' => '<td>',
                '#suffix' => '</td></tr></table></blockquote>',
            );

        }else{

            $form['session-log'][] = array(
                '#type' => 'item',
                '#title' => t('Advisor'),
                '#value' => t($advisor_name),
                '#size' => 30,
                '#prefix' => '<blockquote><table><tr><td>',
                '#suffix' => '</td>',
            );

            $form['session-log'][] = array(
                '#type' => 'item',
                '#title' => t('Student'),
                '#value' => t($first_name." ".$last_name),
                '#size' => 30,
                '#prefix' => '<td>',
                '#suffix' => '</td>',
            );

            $form['session-log'][] = array(
                '#type' => 'item',
                '#title' => t('Session date and time'),
                '#value' => t($time_display),
                '#size' => 30,
                '#prefix' => '<td>',
                '#suffix' => '</td></tr></table></blockquote>',
            );

        }

        $form['session-log']['session-note'] = array(
            '#type' => 'textarea',
            '#title' => t('Session Note'),
            '#default_value' => t($session_note),
            '#prefix' => '<blockquote>',
            '#suffix' => '</blockquote>',
        );

        // Create the upload dropdown, add a blockquote
        // indent this division with the Current Session.
        $form['session-log']['session-upload'] = array(
            '#type' => 'fieldset',
            '#title' => t('Upload Files'),
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
            '#prefix' => '<blockquote>',
        );

        if( $upload_list ) {

            $form['session-log']['session-upload']['file-list'] = array(
                '#type' => 'item',
                '#title' => t('Uploaded Files'),
                '#value' => t('<ol>'.$upload_list.'</ol>'),
                '#prefix' => '<blockquote>',
                '#suffix' => '</blockquote>',
            );

        }

        // Upload files. Make the hidden
        // textfields for input.
        for( $i=0; $i < 5; $i++ ) {

            if( $i == 0 ) {

                // Never hide the first value. The $hidden variables are for the
                // is_hidden attribute for the div surrounding the form item.
                $hidden_value = 'no';
                $uplo_title = 'Upload File';
                $type_title = 'File Type';

            }else {

                // for additional file upload fields assume hidden at first.
                $hidden_value = 'yes';
                $uplo_title = '';
                $type_title = '';

            }

            $form['session-log']['session-upload']["upload-file_$i"] = array(
                '#type' => 'file',
                '#title' => t($uplo_title),
                '#size' => 50,
                '#prefix' => "<div class='hidden-file-butt' is_hidden=" . $hidden_value. "><table style='display:inline;'><tr><td width=20px;>",
                '#suffix' => '</td>',
            );

            $form['session-log']['session-upload']["upload-type_$i"] = array(
                '#type' => 'select',
                '#title' => t($type_title),
                '#options' => array(
                        'Academic Waiver' => t('Academic Waiver'),
                        'Medical Notes' => t('Medical Notes'),
                        'Add-Drop Courses' => t('Add-Drop Courses'),
                        'Transcripts' => t('Transcripts'),
                        'Other' => t('Other'),
                ),
                '#prefix' => '<td>',
                '#suffix' => '</td></tr></table></div>',
            );

        }

        $form['session-log']['session-upload']['add-file'] = array(
            '#type' => 'item',
            '#value' => '<input type="button" class="add-file-butt" value="Add file">',
            '#prefix' => '<blockquote>',
            '#suffix' => '</blockquote></blockquote>',  // Close the blockquote for the 'session-upload' division
        );

        if( $session_id == $current_session ) {

            $form['session-log']['update-session'] = array(
                '#type' => 'submit',
                '#value' => t('Save session'),
                '#prefix' => '<br /><blockquote>',
            );

            $form['session-log']['delete-update-session'] = array(
                '#type' => 'submit',
                '#value' => t('Delete session'),
            );

            $form['session-log']['form-snapshot'] = array(
                '#type' => 'submit',
                '#value' => t('View form snapshot'),
            );

            $form['return-session'] = array(
                '#type' => 'submit',
                '#value' => t('Return To Current Advising Session'),
                '#suffix' => '</blockquote><br />',
            );

        }

        else {

            $form['session-log']['update-session'] = array(
                '#type' => 'submit',
                '#value' => t('Save session'),
                '#prefix' => '<br /><blockquote>',
            );

            $form['session-log']['delete-update-session'] = array(
                '#type' => 'submit',
                '#value' => t('Delete session'),
            );

        }

        // Pass the hidden session and advisor ids.
        $form['log-id'] = array( '#type' => 'value', '#value' => $log_id );
        $form['advisor-id'] = array( '#type' => 'value', '#value' => $advisor_id );
        $form['student-id'] = array( '#type' => 'value', '#value' => $student_id );
        $form['session-time'] = array( '#type' => 'value', '#value' => $session_time );
        $form['db-session-id'] = array( '#type' => 'value', '#value' => $db_session_id );

        $form['#attributes'] = array('enctype' => "multipart/form-data");

    }

    // If in session load the current session log.
    elseif( $session_id == $current_session ) {

        // Display the title
        drupal_set_title(t("Session Log <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));

        $advisor_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $advisor_id));

        $session_info = db_fetch_array(db_query("SELECT session_time, student_id, meeting_type, session_note FROM {mag_session_log} WHERE session_id='%s' AND advisor_id=%d", $session_id, $advisor_id));

        $student_id = $session_info['student_id'];
        $session_time = $session_info['session_time'];
        $meeting_type = $session_info['meeting_type'];
        $session_note = $session_info['session_note'];

        if( $session_note ) {

            // Decrypt the session note.
            $session_note = aes_decrypt($session_note);

            // Trim Please
            $session_note = trim($session_note);

            // Remove any trailing new lines.
            if( substr($session_note, -6) == "<br />" ) {
                $session_note = substr_replace($session_note, '', -6);
            }

        }

        // Remove the seconds
        $time_display = substr_replace($session_time, '', strlen($session_time) - 3, strlen($session_time));

        $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE id=%d", $student_id));

        $first_name   = $student['first_name'];
        $last_name    = $student['last_name'];

        $log_id = db_result(db_query("SELECT id FROM {mag_session_log} WHERE session_id='%s'", $session_id));

        $result = db_query("SELECT file_name, file_type FROM {mag_session_file} WHERE log_id=%d", $log_id);
        while( $row = db_fetch_array($result) ) {

            //$file_url = '<a href="' . file_create_url('session' . $log_id . '/' . $row['file_name']) . '" target="_blank" >' . $row['file_type'] . '</a>';
            $file_url = '<a href="' . $base_url . '/' . $page_url . '/download_file/' . $row['file_name'] . '/' . $log_id . '" target="_blank" >' . $row['file_type'] . '</a>';

            $delete_url = '<a href="' . $base_url . '/' . $page_url . '/delete_file/' . $row['file_name'] . '/' . $log_id . '/' . $row['file_type'] . '">delete</a>';

            $upload_list .= '<li>' . $file_url . ' - ' . $delete_url . '</li>';

        }

        $form['session-log'] = array(
            '#type' => 'fieldset',
            '#title' => t('Current Session'),
            '#collapsible' => FALSE,
            '#collapsed' => FALSE,
        );

        $form['session-log']['session-type'] = array(
            '#type' => 'checkbox',
            '#title' => t('In person session'),
            '#default_value' => $meeting_type,
            '#prefix' => '<blockquote>',
            '#suffix' => '</blockquote>',
        );

        if( $edit_access ) {

            $form['session-log']['session-advisor'] = array(
                '#type' => 'textfield',
                '#title' => t('Advisor'),
                '#default_value' => t($advisor_name),
                '#size' => 30,
                '#autocomplete_path' => 'autocomp/advisor',
                '#prefix' => '<blockquote><table><tr><td>',
                '#suffix' => '</td>',
            );

            $form['session-log']['session-student'] = array(
                '#type' => 'textfield',
                '#title' => t('Student'),
                '#default_value' => t($first_name." ".$last_name),
                '#size' => 30,
                '#autocomplete_path' => 'autocomp/student',
                '#prefix' => '<td>',
                '#suffix' => '</td>',
            );

            $form['session-log']['session-time-display'] = array(
                '#type' => 'textfield',
                '#title' => t('Session date and time'),
                '#default_value' => t($time_display),
                '#size' => 30,
                '#prefix' => '<td>',
                '#suffix' => '</td></tr></table></blockquote>',
            );

        }else{

            $form['session-log'][] = array(
                '#type' => 'item',
                '#title' => t('Advisor'),
                '#value' => t($advisor_name),
                '#size' => 30,
                '#prefix' => '<blockquote><table><tr><td>',
                '#suffix' => '</td>',
            );

            $form['session-log'][] = array(
                '#type' => 'item',
                '#title' => t('Student'),
                '#value' => t($first_name." ".$last_name),
                '#size' => 30,
                '#prefix' => '<td>',
                '#suffix' => '</td>',
            );

            $form['session-log'][] = array(
                '#type' => 'item',
                '#title' => t('Session date and time'),
                '#value' => t($time_display),
                '#size' => 30,
                '#prefix' => '<td>',
                '#suffix' => '</td></tr></table></blockquote>',
            );

        }

        $form['session-log']['session-note'] = array(
            '#type' => 'textarea',
            '#title' => t('Session Note'),
            '#default_value' => t($session_note),
            '#prefix' => '<blockquote>',
            '#suffix' => '</blockquote><br />',
        );

        // Create the upload dropdown, add a blockquote
        // indent this division with the Current Session.
        $form['session-log']['session-upload'] = array(
            '#type' => 'fieldset',
            '#title' => t('Upload Files'),
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
            '#prefix' => '<blockquote>',
        );

        if( $upload_list ) {

            $form['session-log']['session-upload']['file-list'] = array(
                '#type' => 'item',
                '#title' => t('Uploaded Files'),
                '#value' => t('<ol>'.$upload_list.'</ol>'),
                '#prefix' => '<blockquote>',
                '#suffix' => '</blockquote>',
            );

        }

        // Upload files. Make the hidden
        // textfields for input.
        for( $i=0; $i < 5; $i++ ) {

            if( $i == 0 ) {

                // Never hide the first value. The $hidden variables are for the
                // is_hidden attribute for the div surrounding the form item.
                $hidden_value = 'no';
                $uplo_title = 'Upload File';
                $type_title = 'File Type';

            }else {

                // for additional file upload fields assume hidden at first.
                $hidden_value = 'yes';
                $uplo_title = '';
                $type_title = '';

            }

            $form['session-log']['session-upload']["upload-file_$i"] = array(
                '#type' => 'file',
                '#title' => t($uplo_title),
                '#size' => 50,
                '#prefix' => "<div class='hidden-file-butt' is_hidden=" . $hidden_value. "><table style='display:inline;'><tr><td width=20px;>",
                '#suffix' => '</td>',
            );

            $form['session-log']['session-upload']["upload-type_$i"] = array(
                '#type' => 'select',
                '#title' => t($type_title),
                '#options' => array(
                        'Academic Waiver' => t('Academic Waiver'),
                        'Medical Notes' => t('Medical Notes'),
                        'Add-Drop Courses' => t('Add-Drop Courses'),
                        'Transcripts' => t('Transcripts'),
                        'Other' => t('Other'),
                ),
                '#prefix' => '<td>',
                '#suffix' => '</td></tr></table></div>',
            );

        }

        $form['session-log']['session-upload']['add-file'] = array(
            '#type' => 'item',
            '#value' => '<input type="button" class="add-file-butt" value="Add file">',
            '#prefix' => '<blockquote>',
            '#suffix' => '</blockquote></blockquote>',  // Close the blockquote for the 'session-upload' division
        );

        $form['session-log']['save-session'] = array(
            '#type' => 'submit',
            '#value' => t('Save session'),
            '#prefix' => '<br /><blockquote>',
        );

        $form['session-log']['delete-session'] = array(
            '#type' => 'submit',
            '#value' => t('Delete session'),
        );

        $form['session-log']['end-session'] = array(
            '#type' => 'submit',
            '#value' => t('End advising session'),
            '#suffix' => '</blockquote><br />',
        );

        // Pass the hidden session and advisor ids.
        $form['session-id'] = array( '#type' => 'value', '#value' => $session_id );
        $form['advisor-id'] = array( '#type' => 'value', '#value' => $advisor_id );
        $form['student-id'] = array( '#type' => 'value', '#value' => $student_id );
        $form['session-time'] = array( '#type' => 'value', '#value' => $session_time );

        $form['#attributes'] = array('enctype' => "multipart/form-data");

    }

    else {

        drupal_set_message(t('&nbsp Redirected from session log.'));

        drupal_goto($base_url . '/' . daedalus_get_setting("session history"));

    }

    return $form;

}

/**
 * Implementation of hook_validate().
 */
function daedalus_session_log_form_validate( $form, &$form_state ) {

    switch( $form_state['values']['op'] ) {

        case $form_state['values']['save-session']:
        case $form_state['values']['update-session']:

            // Access and permissions
            if( user_access("magellan advise edit log") ) {

                if( !$form_state['values']['session-advisor'] ) {
                    form_set_error('session-advisor', t('&nbsp; The session <b>Advisor</b> field is required.'));
                }

                if( !$form_state['values']['session-student'] ) {
                    form_set_error('session-student', t('&nbsp; The session <b>Student</b> field is required.'));
                }

                if( !$form_state['values']['session-time'] ) {
                    form_set_error('session-time', t('&nbsp; The <b>Session date and time</b> field is required.'));
                }

            }

            // Perform my own file validation because getting a validator
            // array to work properly is a total pain the my %*$&ing @$$!
            $valid_ext = daedalus_get_permissions();

            // Create the string of allowable file formats.
            for( $i = 0; $i < count($valid_ext); $i++ ) {

                if( $i == count($valid_ext) - 2 ) {
                    $ext_string .= "<b>" . $valid_ext[$i] . "</b> ";
                }

                elseif( $i == count($valid_ext) - 1 ) {
                    $ext_string .= "and <b>" . $valid_ext[$i] . "</b>";
                }

                else {
                    $ext_string .= "<b>" . $valid_ext[$i] . "</b>, ";
                }

            }

            for( $i=0; $i < 5; $i++ ) {

                $session_file = $_FILES['files']['name']["upload-file_$i"];

                if( $session_file ) {

                    $file_info = explode('.', $session_file);
                    $file_ext  = $file_info[1];

                    // Validate the file types
                    if( !in_array( $file_ext, $valid_ext ) ) {
                        form_set_error("upload-file_$i", t('&nbsp; Upload ' . $ext_string . ' file formats only for the session file.'));
                        form_set_error("upload-file_$i", t('&nbsp; ????'.$session_file.' '.$file_ext));
                    }

                    // Make sure the files do not contain any spaces.
                    $pos = strpos($session_file, ' ');

                    if( $pos !== FALSE ) {
                        form_set_error("upload-file_$i", t('&nbsp; The session file must not contain spaces.'));
                    }

                }

            }

            break;

    }
}

/**
 * Implementation of hook_submit().
 */
function daedalus_session_log_form_submit( $form, &$form_state ) {

    global $base_url;

    $page_url = daedalus_get_setting("session log");

    switch( $form_state['values']['op'] ) {

        case $form_state['values']['form-snapshot']:

            $db_session_id = $form_state['values']['db-session-id'];
            $session_time = $form_state['values']['session-time'];

            drupal_goto($base_url."/".daedalus_get_setting("program form")."/view/".$db_session_id."/".strtotime($session_time));

            break;

        case $form_state['values']['return-session']:

            drupal_set_message(t('Current session.'));

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['delete-reverse']:

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['delete-file-reverse']:

            $log_id = $form_state['values']['log-id'];

            drupal_goto($base_url."/".$page_url."/update/".$log_id);

            break;

         /////////////////////////////
        // DELETE SESSION INFORMATION
        case $form_state['values']['delete-session']:

            $session_id = $form_state['values']['session-id'];
            $advisor_id = $form_state['values']['advisor-id'];

            drupal_goto($base_url."/".$page_url."/delete/".$session_id."/".$advisor_id);

            break;

        case $form_state['values']['delete-confirm']:

            $session_id = $form_state['values']['session-id'];
            $advisor_id = $form_state['values']['advisor-id'];

            drupal_set_message(t('Session deleted and current session closed.'));

            db_query("DELETE FROM {mag_session_log} WHERE session_id='%s' AND advisor_id=%d", $session_id, $advisor_id);

            db_query("DELETE FROM {mag_advisor_session} WHERE id=%d", $session_id);

            // This generates a new session id.
            $_COOKIE[session_regenerate_id()];

            drupal_goto($base_url."/".daedalus_get_setting("advise student"));

            break;

        case $form_state['values']['delete-update-session']:

            $log_id = $form_state['values']['log-id'];
            $advisor_id = $form_state['values']['advisor-id'];

            drupal_goto($base_url."/".$page_url."/delete_update/".$log_id."/".$advisor_id);

            break;

        case $form_state['values']['delete-update-confirm']:

            $log_id = $form_state['values']['log-id'];
            $advisor_id = $form_state['values']['advisor-id'];

            drupal_set_message(t('&nbsp Session deleted.'));

            db_query("DELETE FROM {mag_session_log} WHERE id=%d AND advisor_id=%d", $log_id, $advisor_id);

            drupal_goto($base_url."/".daedalus_get_setting("session history"));

            break;

        case $form_state['values']['delete-file-confirm']:

            $log_id = $form_state['values']['log-id'];
            $session_file = $form_state['values']['session-file'];
            $file_type = $form_state['values']['file-type'];

            // Remove the file.
            exec('rm ' . file_directory_path() . '/session' . $log_id . '/' . $session_file);

            // Remove the file reference from the database.
            db_query("DELETE FROM {mag_session_file} WHERE log_id=%d and file_name='%s'", $log_id, $session_file);

            // If there are more files located in the database for the selected log
            if( db_result(db_query("SELECT COUNT(*) FROM {mag_session_file} WHERE log_id=%d", $log_id)) ) {

                drupal_set_message ( t('&nbsp; <b>' . $file_type . '</b> file deleted.') );

            }

            // If there are no more files
            // remove the file dirctory.
            else {

                // Remove the directory
                exec('rm -R ' . file_directory_path() . '/session' . $log_id);

                $file_check = 0;

                // Update the file status for the session.
                db_query("UPDATE {mag_session_log} SET file=%d WHERE id=%d", $file_check, $log_id);

                drupal_set_message ( t('&nbsp; <b>' . $file_type . '</b> file deleted including it\'s directory.') );

            }

            drupal_goto($base_url."/".$page_url."/update/".$log_id);

            break;

         ///////////////////////////
        // SAVE SESSION INFORMATION
        case $form_state['values']['save-session']:

            $session_type = $form_state['values']['session-type'];
            $session_note = $form_state['values']['session-note'];
            $session_advisor = $form_state['values']['session-advisor'];
            $session_student = $form_state['values']['session-student'];
            $session_time_display = $form_state['values']['session-time-display'];
            $session_time = $form_state['values']['session-time'];

            $session_id = $form_state['values']['session-id'];
            $advisor_id = $form_state['values']['advisor-id'];
            $student_id = $form_state['values']['student-id'];

            // Encrypt the session note and
            // indicate if there is a note.
            if( $session_note ) {

                // Trim Please
                $session_note = trim($session_note);

                // Remove any trailing new lines.
                if( substr($session_note, -6) == "<br />" ) {
                    $session_note = substr_replace($session_note, '', -6);
                }

                $session_note = aes_encrypt($session_note);
                $note_check = 1;

            }else {
                $note_check = 0;
            }

            // Indicate if there is a file saved for this session.
            if( db_result(db_query("SELECT COUNT(*) FROM {mag_session_file} WHERE log_id=%d", $log_id)) ){
                $file_check = 1;
            }else {
                $file_check = 0;
            }

            $log_id = db_result(db_query("SELECT id from {mag_session_log} WHERE session_id='%s' AND advisor_id=%d", $session_id, $advisor_id));

            // Update advisor id, student id and session time
            // only if the user has editing permissions.
            if( user_access("magellan advise edit log") ) {

                $update_advisor = $update_student = $update_time = FALSE;

                // Compare the original session advisor id to the possibly updated
                // id. If they are different the advisor has been updated.
                $session_advisor_id = db_result(db_query("SELECT uid FROM {users} WHERE name='%s'", $session_advisor));
                if( $advisor_id != $session_advisor_id ) {
                    $update_advisor = TRUE;
                }

                // If the student name has changed an update has occured.
                $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE id=%d", $student_id));

                $student_name   = $student['first_name'] . ' ' . $student['last_name'];

                if( $student_name != $session_student ){

                    $session_student_id = db_result(db_query("SELECT id FROM {mag_student_identification} WHERE user_name='%s'", $session_student));

                    if( $session_student_id ) {
                        $update_student = TRUE;
                    }

                }

                // Remove the seconds from the original session time and compare the value to the time
                // for the session log. If the two are different then the time has been updated.
                $session_time = substr_replace($session_time, '', strlen($session_time) - 3, strlen($session_time));
                if( $session_time != $session_time_display ) {

                    $update_time = TRUE;

                    // Add the seconds back
                    $session_time_display .= ":00";

                }

                // For degugging REMOVE LATER
                //drupal_set_message(t('&nbsp Advisor '.$advisor_id.'|'.$session_advisor_id. ' >> Student '.$student_id.'|'.$session_student_id.' >> Time '.$session_time.'|'.$session_time_display));

                // Perform the updates and indicate which log values have changed.
                if( $update_advisor && $update_student && $update_time ){

                    db_query("UPDATE {mag_session_log} SET advisor_id=%d, student_id=%d, session_time='%s', meeting_type=%d, session_note='%s'
                                                     WHERE session_id='%s' AND advisor_id=%d", $session_advisor_id, $session_student_id, $session_time_display, $session_type, $session_note, $session_id, $advisor_id);

                    drupal_set_message(t('Current session log updated including the advisor, student and session time.'));

                }

                elseif( $update_advisor && $update_student ){

                    db_query("UPDATE {mag_session_log} SET advisor_id=%d, student_id=%d, meeting_type=%d, session_note='%s'
                                                     WHERE session_id='%s' AND advisor_id=%d", $session_advisor_id, $session_student_id, $session_type, $session_note, $session_id, $advisor_id);

                    drupal_set_message(t('Current session log updated including the advisor and student ids.'));

                }

                elseif( $update_advisor && $update_time ){

                    db_query("UPDATE {mag_session_log} SET advisor_id=%d, session_time='%s', meeting_type=%d, session_note='%s'
                                                     WHERE session_id='%s' AND advisor_id=%d", $session_advisor_id, $session_time_display, $session_type, $session_note, $session_id, $advisor_id);

                    drupal_set_message(t('Current session log updated including the advisor and session time.'));

                }

                elseif( $update_student && $update_time ){

                    db_query("UPDATE {mag_session_log} SET student_id=%d, session_time='%s', meeting_type=%d, session_note='%s'
                                                     WHERE session_id='%s' AND advisor_id=%d", $session_student_id, $session_time_display, $session_type, $session_note, $session_id, $advisor_id);

                    drupal_set_message(t('Current session log updated including the student and session time.'));

                }

                elseif( $update_advisor ){

                    db_query("UPDATE {mag_session_log} SET advisor_id=%d, meeting_type=%d, session_note='%s'
                                                     WHERE session_id='%s' AND advisor_id=%d", $session_advisor_id, $session_type, $session_note, $session_id, $advisor_id);

                    drupal_set_message(t('Current session log updated including the advisor id.'));

                }

                elseif( $update_student ){

                    db_query("UPDATE {mag_session_log} SET student_id=%d, meeting_type=%d, session_note='%s'
                                                     WHERE session_id='%s' AND advisor_id=%d", $session_student_id, $session_type, $session_note, $session_id, $advisor_id);

                    drupal_set_message(t('Current session log updated including the student id.'));

                }

                elseif( $update_time ){

                    db_query("UPDATE {mag_session_log} SET session_time='%s', meeting_type=%d, session_note='%s'
                                                     WHERE session_id='%s' AND advisor_id=%d", $session_time_display, $session_type, $session_note, $session_id, $advisor_id);

                    drupal_set_message(t('Current session log updated including the session time.'));

                }

                // Regular Update
                else{

                    db_query("UPDATE {mag_session_log} SET meeting_type=%d, session_note='%s' WHERE session_id='%s' AND advisor_id=%d", $session_type, $session_note, $session_id, $advisor_id);

                    drupal_set_message(t('Current session log updated.'));

                }

            }

            else {

                db_query("UPDATE {mag_session_log} SET meeting_type=%d, session_note='%s' WHERE session_id='%s' AND advisor_id=%d", $session_type, $session_note, $session_id, $advisor_id);

                drupal_set_message(t('Current session log updated.'));

            }

              //////////////////////////////////////////////////////////////////
             // Now save any file attachments.
            ////////////////////////////////////
                                             //
            //////////////////////////////////
            // Perform my own file validation
            // because getting a validator
            // array to work properly is
            // a total pain in my @$$!

            $valid_ext = daedalus_get_permissions();

            // Iterate through each of
            // the file upload fields.
            for( $i=0; $i < 5; $i++ ) {

                $session_file = $_FILES['files']['name']["upload-file_$i"];
                $file_type = $form_state['values']["upload-type_$i"];

                $file_info = explode('.', $session_file);
                $file_ext  = $file_info[1];

                if( in_array( $file_ext, $valid_ext ) ) {

                    // Validate one more time just for fun.
                    $validators = array( 'file_validate_extensions' => array( $file_ext ) );

                    // Save the file to the file directory
                    $file = file_save_upload("upload-file_$i", $validators, file_directory_path());

                    if ( $file ) {

                        $file_check = 1;

                        // Check if the directory has already been created
                        $directory = file_directory_path() . '/session' . $log_id;

                        if( !file_exists($directory) ) {

                            // Create a new directory for this sessions files.
                            exec('mkdir ' . $directory);

                            // Modify the permissions of this new directory.
                            exec('chmod 775 ' . $directory);

                        }

                        // Move the file to the new directory.
                        exec('mv ' . file_directory_path() . '/' . $session_file . ' '. $directory . '/' . $session_file);

                        // Give full permissions to the file
                        exec('chmod 777 ' . $directory . '/' . $session_file);

                        // Get the file contents.
                        $file_contents = file_get_contents($directory . '/' . $session_file);

                        if( $file_contents ){

                            // Encrypt the file contents.
                            $encrypted_contents = aes_encrypt($file_contents);

                            // open the file that is being encrypted.
                            $encrypted_file = fopen($directory . '/' . $session_file, 'w');

                            // Write the encrypted contents into the file.
                            fwrite($encrypted_file, $encrypted_contents);

                            // Close the file.
                            fclose($encrypted_file);

                        }

                        // Make the file permanent
                        file_set_status ($file, FILE_STATUS_PERMANENT);

                        if( db_result(db_query("SELECT COUNT(*) FROM {mag_session_file} WHERE log_id=%d AND file_name='%s'", $log_id, $session_file)) ) {

                            $original_type = db_result(db_query("SELECT file_type FROM {mag_session_file} WHERE log_id=%d", $log_id));

                            // If file is being over written check to see
                            // if the file type is the same, if not update.
                            if( $original_type != $file_type ) {

                                drupal_set_message ( t('&nbsp; <b>' . $original_type . '</b> changed to <b>' . $file_type . '</b> and updated as <b>' . $session_file . '</b> in ' . $directory .'/<b>' . $session_file . '</b>') );

                                // Update the file_type for the uploaded duplicate file.
                                db_query("UPDATE {mag_session_file} SET file_type='%s' WHERE log_id=%d", $file_type, $log_id);

                            }else {

                                drupal_set_message ( t('&nbsp; <b>' . $file_type . '</b> file updated as <b>' . $session_file . '</b> in ' . $directory .'/<b>' . $session_file . '</b>') );

                            }


                        }else {

                            drupal_set_message ( t('&nbsp; <b>' . $file_type . '</b> file saved as <b>' . $session_file . '</b> in ' . $directory .'/<b>' . $session_file . '</b>') );

                            // Enter the filename into the database for this session.
                            db_query("INSERT INTO {mag_session_file} (log_id, file_name, file_type) VALUES (%d, '%s', '%s')", $log_id, $session_file, $file_type);

                        }

                    }

                }

            }

            // Update the note and file fields.
            db_query("UPDATE {mag_session_log} SET note=%d, file=%d WHERE id=%d AND advisor_id=%d", $note_check, $file_check, $log_id, $advisor_id);

            break;

         /////////////////////////////
        // UPDATE SESSION INFORMATION
        case $form_state['values']['update-session']:

            $session_type = $form_state['values']['session-type'];
            $session_note = $form_state['values']['session-note'];
            $session_advisor = $form_state['values']['session-advisor'];
            $session_student = $form_state['values']['session-student'];
            $session_time_display = $form_state['values']['session-time-display'];
            $session_time = $form_state['values']['session-time'];

            $advisor_id = $form_state['values']['advisor-id'];
            $student_id = $form_state['values']['student-id'];
            $log_id = $form_state['values']['log-id'];

            // Encrypt the session note and
            // indicate if there is a note.
            if( $session_note ) {

                // Trim Please
                $session_note = trim($session_note);

                // Remove any trailing new lines.
                if( substr($session_note, -6) == "<br />" ) {
                    $session_note = substr_replace($session_note, '', -6);
                }

                $session_note = aes_encrypt($session_note);
                $note_check = 1;

            }else {
                $note_check = 0;
            }

            // Indicate if there is a file saved for this session.
            if( db_result(db_query("SELECT COUNT(*) FROM {mag_session_file} WHERE log_id=%d", $log_id)) ){
                $file_check = 1;
            }else {
                $file_check = 0;
            }

            // Update advisor id, student id and session time
            // only if the user has editing permissions.
            if( user_access("magellan advise edit log") ) {

                $update_advisor = $update_student = $update_time = FALSE;

                // Compare the original session advisor id to the possibly updated
                // id. If they are different the advisor has been updated.
                $session_advisor_id = db_result(db_query("SELECT uid FROM {users} WHERE name='%s'", $session_advisor));
                if( $advisor_id != $session_advisor_id ) {
                    $update_advisor = TRUE;
                }

                // If the student name has changed an update has occured.
                $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE id=%d", $student_id));

                $student_name   = $student['first_name'] . ' ' . $student['last_name'];

                if( $student_name != $session_student ){

                    $session_student_id = db_result(db_query("SELECT id FROM {mag_student_identification} WHERE user_name='%s'", $session_student));

                    if( $session_student_id ) {
                        $update_student = TRUE;
                    }

                }

                // Remove the seconds from the original session time and compare the value to the time
                // for the session log. If the two are different then the time has been updated.
                $session_time = substr_replace($session_time, '', strlen($session_time) - 3, strlen($session_time));
                if( $session_time != $session_time_display ) {

                    $update_time = TRUE;

                    // Add the seconds back
                    $session_time_display .= ":00";
                }

// For degugging REMOVE LATER
//drupal_set_message(t('Advisor '.$advisor_id.'|'.$session_advisor_id. ' >> Student '.$student_id.'|'.$session_student_id.' >> Time '.$session_time.'|'.$session_time_display));

                // Perform the updates and indicate which log values have changed.
                if( $update_advisor && $update_student && $update_time ){

                    db_query("UPDATE {mag_session_log} SET advisor_id=%d, student_id=%d, session_time='%s', meeting_type=%d, session_note='%s'
                                                     WHERE id=%d AND advisor_id=%d", $session_advisor_id, $session_student_id, $session_time_display, $session_type, $session_note, $log_id, $advisor_id);

                    drupal_set_message(t('Session log ' . $log_id . ' updated including the advisor, student and session time.'));

                }

                elseif( $update_advisor && $update_student ){

                    db_query("UPDATE {mag_session_log} SET advisor_id=%d, student_id=%d, meeting_type=%d, session_note='%s'
                                                     WHERE id=%d AND advisor_id=%d", $session_advisor_id, $session_student_id, $session_type, $session_note, $log_id, $advisor_id);

                    drupal_set_message(t('Session log ' . $log_id . ' updated including the advisor and student ids.'));

                }

                elseif( $update_advisor && $update_time ){

                    db_query("UPDATE {mag_session_log} SET advisor_id=%d, session_time='%s', meeting_type=%d, session_note='%s'
                                                     WHERE id=%d AND advisor_id=%d", $session_advisor_id, $session_time_display, $session_type, $session_note, $log_id, $advisor_id);

                    drupal_set_message(t('Session log ' . $log_id . ' updated including the advisor and session time.'));

                }

                elseif( $update_student && $update_time ){

                    db_query("UPDATE {mag_session_log} SET student_id=%d, session_time='%s', meeting_type=%d, session_note='%s'
                                                     WHERE id=%d AND advisor_id=%d", $session_student_id, $session_time_display, $session_type, $session_note, $log_id, $advisor_id);

                    drupal_set_message(t('Session log ' . $log_id . ' updated including the student and session time.'));

                }

                elseif( $update_advisor ){

                    db_query("UPDATE {mag_session_log} SET advisor_id=%d, meeting_type=%d, session_note='%s'
                                                     WHERE id=%d AND advisor_id=%d", $session_advisor_id, $session_type, $session_note, $log_id, $advisor_id);

                    drupal_set_message(t('Session log ' . $log_id . ' updated including the advisor id.'));

                }

                elseif( $update_student ){

                    db_query("UPDATE {mag_session_log} SET student_id=%d, meeting_type=%d, session_note='%s'
                                                     WHERE id=%d AND advisor_id=%d", $session_student_id, $session_type, $session_note, $log_id, $advisor_id);

                    drupal_set_message(t('Session log ' . $log_id . ' updated including the student id.'));

                }

                elseif( $update_time ){

                    db_query("UPDATE {mag_session_log} SET session_time='%s', meeting_type=%d, session_note='%s'
                                                     WHERE id=%d AND advisor_id=%d", $session_time_display, $session_type, $session_note, $log_id, $advisor_id);

                    drupal_set_message(t('Session log ' . $log_id . ' updated including the session time.'));

                }

                // Regular update
                else{

                    db_query("UPDATE {mag_session_log} SET meeting_type=%d, session_note='%s' WHERE id=%d AND advisor_id=%d", $session_type, $session_note, $log_id, $advisor_id);

                    drupal_set_message(t('Session log ' . $log_id . ' updated.'));

                }

            }

            else {

                db_query("UPDATE {mag_session_log} SET meeting_type=%d, session_note='%s' WHERE id=%d AND advisor_id=%d", $session_type, $session_note, $log_id, $advisor_id);

                drupal_set_message(t('Current session log updated.'));
                //drupal_set_message(t('Current session log updated. type|'.$session_type.'|note|'.$session_note.'|sid|'.$session_id.'|aid|'.$advisor_id));

            }

              //////////////////////////////////////////////////////////////////
             // Now save any file attachments.
            ////////////////////////////////////
                                             //
            //////////////////////////////////
            // Perform my own file validation
            // because getting a validator
            // array to work properly is
            // a total pain in my @$$!

            $valid_ext = daedalus_get_permissions();

            // Iterate through each of
            // the file upload fields.
            for( $i=0; $i < 5; $i++ ) {

                $session_file = $_FILES['files']['name']["upload-file_$i"];
                $file_type = $form_state['values']["upload-type_$i"];

                $file_info = explode('.', $session_file);
                $file_ext  = $file_info[1];

                if( in_array( $file_ext, $valid_ext ) ) {

                    // Validate one more time just for fun.
                    $validators = array( 'file_validate_extensions' => array( $file_ext ) );

                    // Save the file to the file directory
                    $file = file_save_upload("upload-file_$i", $validators, file_directory_path());

                    if ( $file ) {

                        $file_check = 1;

                        // Check if the directory has already been created
                        $directory = file_directory_path() . '/session' . $log_id;

                        if( !file_exists($directory) ) {

                            // Create a new directory for this sessions files.
                            exec('mkdir ' . $directory);

                            // Modify the permissions of this new directory.
                            exec('chmod 775 ' . $directory);

                        }

                        // Move the file to the new directory.
                        exec('mv ' . file_directory_path() . '/' . $session_file . ' '. $directory . '/' . $session_file);

                        // Give full permissions to the file
                        exec('chmod 777 ' . $directory . '/' . $session_file);

                        // Get the file contents.
                        $file_contents = file_get_contents($directory . '/' . $session_file);

                        if( $file_contents ){

                            // Encrypt the file contents.
                            $encrypted_contents = aes_encrypt($file_contents);

                            // open the file that is being encrypted.
                            $encrypted_file = fopen($directory . '/' . $session_file, 'w');

                            // Write the encrypted contents into the file.
                            fwrite($encrypted_file, $encrypted_contents);

                            // Close the file.
                            fclose($encrypted_file);

                        }

                        // Make the file permanent NOT SURE IF THIS IS REQUIRED?
                        file_set_status ($file, FILE_STATUS_PERMANENT);

                        if( db_result(db_query("SELECT COUNT(*) FROM {mag_session_file} WHERE log_id=%d AND file_name='%s'", $log_id, $session_file)) ) {

                            $original_type = db_result(db_query("SELECT file_type FROM {mag_session_file} WHERE log_id=%d", $log_id));

                            // If file is being over written check to see
                            // if the file type is the same, if not update.
                            if( $original_type != $file_type ){

                                drupal_set_message ( t('&nbsp; <b>' . $original_type . '</b> changed to <b>' . $file_type . '</b> and updated as <b>' . $session_file . '</b> in ' . $directory .'/<b>' . $session_file . '</b>') );

                                // Update the file_type for the uploaded duplicate file.
                                db_query("UPDATE {mag_session_file} SET file_type='%s' WHERE log_id=%d", $file_type, $log_id);

                            }else{

                                drupal_set_message ( t('&nbsp; <b>' . $file_type . '</b> file updated as <b>' . $session_file . '</b> in ' . $directory .'/<b>' . $session_file . '</b>') );

                            }


                        }else{

                            drupal_set_message ( t('&nbsp; <b>' . $file_type . '</b> file saved as <b>' . $session_file . '</b> in ' . $directory .'/<b>' . $session_file . '</b>') );

                            // Enter the filename into the database for this session.
                            db_query("INSERT INTO {mag_session_file} (log_id, file_name, file_type) VALUES (%d, '%s', '%s')", $log_id, $session_file, $file_type);

                        }

                    }

                }

            }

            // Update the note and file fields.
            db_query("UPDATE {mag_session_log} SET note=%d, file=%d WHERE id=%d AND advisor_id=%d", $note_check, $file_check, $log_id, $advisor_id);

            break;

        case $form_state['values']['end-session']:

            $page_url = daedalus_get_setting("advise student");

            db_query("DELETE FROM {mag_advisor_session} WHERE id=%d", $form_state['values']['session-id']);

            drupal_set_message(t('&nbsp Current session closed.'));

            // This generates a new session id.
            $_COOKIE[session_regenerate_id()];

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['dae-help-submit']:

            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message("&nbsp; Help information saved.");

                drupal_goto($base_url."/".$page_url);

            }

            break;
    }
}


//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Program->Session_History Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_session_history_page() {
    return drupal_get_form("daedalus_session_history_form");
};

/**
* Menu Location: Daedalus -> Advise -> Session History
* URL Location:  daedalus/daedalus/advise/session_history
*
* Displays
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_session_history_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Get current page url.
    $page_url = $help_url = daedalus_get_setting("session history");

    // Get the session log url.
    $log_url = daedalus_get_setting("session log");

    // Determine the user name
    global $user;
    $advisor_id = $user->uid;

    $current_session = $_COOKIE[session_name()];

    // Store URL Parameters in $param array.
    $page_url_length = sizeof(explode('/',$page_url));

    $param = array();

    $param[0] = arg(0+$page_url_length);
    $param[1] = arg(1+$page_url_length);
    $param[2] = arg(2+$page_url_length);

    // Get the images.
    $question_img_src = $base_url."/".daedalus_get_setting("question mark");
    $update_button = "<img src='".$base_url."/".daedalus_get_setting("update button")."' alt='Update' style='vertical-align:middle;' />";
    $view_button = "<img src='".$base_url."/".daedalus_get_setting("view button")."' alt='View' style='vertical-align:middle;' />";

    // Access and permissions
    $edit_access = user_access("magellan advise edit log");

    // Display the title
    drupal_set_title(t("Session History <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>"));

    $form = array();

    // Add the hidden help form. Paramaters are
    // (page url, show border, show break).
    $form = display_hidden_form($help_url, 1, 1);

    $session_id = db_result(db_query("SELECT id FROM {mag_advisor_session} WHERE advisor_id=%d", $advisor_id));
//echo "DB Session > ".$session_id."<br />";
//echo "Cur Session > ".$current_session."<br />";

    // Apply css to change the table behaviour
    $css = '<style type="text/css">
                table.box{
                    border-style:solid;
                    border-width:3px;
                    border-color:#C0C0C0;
                }
            </style>';

    drupal_set_html_head($css);

    // Download a file
    if( $param[0] == 'download_file' ) {

        $file_name = $param[1];
        $log_id = $param[2];

        // Check if the directory has already been created
        $download_directory = file_directory_path() . '/downloads';

        if( !file_exists($download_directory) ) {

            // Create a new directory for this sessions files.
            exec('mkdir ' . $download_directory);

            // Modify the permissions of this new directory.
            exec('chmod 775 ' . $download_directory);

        }

        $file_location = file_directory_path() . '/session'. $log_id . '/' . $file_name;
        $download_location = $download_directory . '/' . $file_name;

        // Copy the file to the download directory.
        // This directory will make it easy to collect
        // the garbage once the file is downloaded.
        exec('cp ' . $file_location . ' ' . $download_location);

        $file_contents = file_get_contents($download_location);

        if( $file_contents ){

            $decrypted_contents = aes_decrypt($file_contents);

            $decrypted_file = fopen($download_location, 'w');

            fwrite($decrypted_file, $decrypted_contents);

            fclose($decrypted_file);

        }

        // Link to the newly decrypted file.
        drupal_goto( file_create_url('downloads/' . $file_name) );

    }
    
    // If the user is an advisor and the advisor has session logs saved
    // in the database. Get the logs and display the 50 most recent. An
    // option to view all session history is available. If a student id
    // or an advisor id selected then bypass this section to view......
    elseif( $edit_access && $param[0] != 'student' && $param[0] != 'advisor') {

        $form[] = array(
            '#type' => 'item',
            '#prefix' => '<table class="box"><tr bgcolor="#C0C0C0"><td colspan=6>',
            '#suffix' => '</td></tr>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Date'),
            '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Advisor'),
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Student'),
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Uploaded Files'),
            '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Session Notes'),
            '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
            '#suffix' => '</td></tr>',
        );

        $form[] = array(
            '#type' => 'item',
            '#value' => t('<tr bgcolor="#C0C0C0"><td colspan=6></td></tr>'),
        );

        if( $param[0] == 'all' ) {
            
            // No Limit
            $result = db_query("SELECT id, advisor_id, student_id, session_time, meeting_type, session_note FROM {mag_session_log} ORDER BY session_time DESC");
            
        }
        
        else {

            // Limited to the 50 most recent results.
            $result = db_query("SELECT id, advisor_id, student_id, session_time, meeting_type, session_note FROM {mag_session_log} ORDER BY session_time DESC LIMIT 0, 50");

        }

        $log = $advisor_ids = $student_ids = $advisor_names = $student_names = array();

        while( $row = db_fetch_array($result) ) {

            $log[$row['id']]['student_id'] = $row['student_id'];
            $log[$row['id']]['advisor_id'] = $row['advisor_id'];
            $log[$row['id']]['session_time'] = $row['session_time'];
            $log[$row['id']]['meeting_type'] = $row['meeting_type'];
            $log[$row['id']]['session_note'] = $row['session_note'];
            
            // Add unique advisor id's to an array associated to their username.
            if( !in_array( $row['advisor_id'], $advisor_ids) ){

                $advisor_ids[$row['advisor_id']] = $row['advisor_id'];
                $advisor_names[$row['advisor_id']] = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $row['advisor_id']));

            }

            // Add the advisor name to the array log.
            $log[$row['id']]['advisor_name'] = $advisor_names[$row['advisor_id']];

            // Add unique student id's to an array associated to their username.
            if( !in_array( $row['student_id'], $student_ids) ){

                $student_ids[$row['student_id']] = $row['student_id'];
                
                // Get the students name.
                $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE id=%d", $row['student_id']));

                $student_names[$row['student_id']] = $student['first_name'] . ' ' . $student['last_name'];

            }

            // Add the students name to the array log.
            $log[$row['id']]['student_name'] = $student_names[$row['student_id']];

        }

        foreach( $log as $session_id => $log_info ) {

            $student_id = $log_info['student_id'];
            $advisor_id = $log_info['advisor_id'];
            $student_name = $log_info['student_name'];
            $advisor_name = $log_info['advisor_name'];
            $session_time = $log_info['session_time'];
            $meeting_type = $log_info['meeting_type'];
            $session_note = $log_info['session_note'];

            if( $session_note ) {

                // Decrypt the session note.
                $session_note = aes_decrypt($session_note);

                // Trim please
                $session_note = trim($session_note);

                // Remove any trailing new lines.
                if( substr($session_note, -6) == "<br />" ) {
                    $session_note = substr_replace($session_note, '', -6);
                }

                // Truncate the note if it is
                // greater than 80 characters.
                if( strlen($session_note) > 80 ) {
                    $session_note = substr($session_note, 0, 80)."...";
                }

            }

            // Reformat the date from the session time.
            $new_time = strtotime($session_time);

            $session_date = date("M j Y", $new_time);

            $file_list = array();
            $session_files = '';
            $first_file = FALSE;

            $result = db_query("SELECT file_name, file_type FROM {mag_session_file} WHERE log_id=%d", $session_id);
            while( $row = db_fetch_array($result) ) {
                $file_list[$row['file_name']] = $row['file_type'];
            }

            if( $file_list ) {

                $file_count = count($file_list);

                $i = 1;

                foreach( $file_list as $filename => $filetype ) {

                    if( $file_count > 3 ) {

                        $title = 'File Name: ' . $filename . ' => File Type: ' . $filetype;

                        $filetype = $i++;

                    }else{
                        $title = 'File Name: ' . $filename;
                    }

                    // Link the file to it's directory location for easy downloading.
                    // The function file_create_url() is required for this to work.
                    if( !$first_file ) {

                        $session_files .= '<a href="' . $base_url . '/' . $page_url . '/download_file/' . $filename . '/' . $session_id . '" title="' . $title . '" target="_blank" >' . $filetype . '</a>';

                        $first_file = TRUE;

                    }else {
                        $session_files .= ', <a href="' . $base_url . '/' . $page_url . '/download_file/' . $filename . '/' . $session_id . '" title="' . $title . '" target="_blank" >' . $filetype . '</a>';
                    }

                }

            }

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_date),
                '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td>',
            );

            // Display the update button
            $form[] = array(
                '#type' => 'item',
                '#value' => t('<a href="' . $base_url . '/' . $log_url . '/update/' . $session_id . '">' . $update_button . '</a>'),
                '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                '#suffix' => '</td>',
            );

            $advisor_url = $base_url . '/' . $page_url . '/advisor/' . $advisor_id;

            $form[] = array(
                '#type' => 'item',
                '#value' => t('<a href="' . $advisor_url . '" title="View this student\'s entire academic advising session history.">' . $advisor_name . '</a>'),
                '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                '#suffix' => '</td>',
            );

            $student_url = $base_url . '/' . $page_url . '/student/' . $student_id;

            $form[] = array(
                '#type' => 'item',
                '#value' => t('<a href="' . $student_url . '" title="View this student\'s entire academic advising session history.">' . $student_name . '</a>'),
                '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_files),
                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_note),
                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td></tr>',
            );

        }

        $form[] = array(
            '#type' => 'item',
            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:0px 0px 4px 0px;" colspan=6></td></tr>'),
            '#suffix' => '</table>',
        );
        
        if( $param[0] != 'all' ) {

            $form['view-all'] = array(
                '#type' => 'submit',
                '#value' => t('View all history'),
            );

        }
        
        else {

            $form['advisor-history'] = array(
                '#type' => 'submit',
                '#value' => t('View less history'),
            );

        }

    }

    // View all sessions logged for the current student.
    elseif( $session_id == $current_session || $param[0] == 'student' ) {

        if( $param[0] == 'student' ){
            $student_id = $param[1];
        }else{
            $student_id = db_result(db_query("SELECT student_id FROM {mag_advisor_session} WHERE advisor_id=%d", $advisor_id));
        }

        // Get the students name.
        $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE id=%d", $student_id));

        $first_name   = $student['first_name'];
        $last_name    = $student['last_name'];

        $form[] = array(
            '#type' => 'item',
            '#prefix' => '<table class="box"><tr bgcolor="#C0C0C0"><td colspan=5>',
            '#suffix' => '</td></tr>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('<i>Student</i> ' . $first_name . ' ' . $last_name),
            '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;" colspan=5>',
            '#suffix' => '</td></tr>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Date'),
            '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Advisor'),
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Uploaded Files'),
            '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Session Notes'),
            '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
            //'#suffix' => '</td>',
            '#suffix' => '</td></tr>',
        );

        $form[] = array(
            '#type' => 'item',
            '#value' => t('<tr bgcolor="#C0C0C0"><td colspan=5></td></tr>'),
        );

        $advisor_ids = array();

        $result = db_query("SELECT id, advisor_id, session_time, meeting_type, session_note FROM {mag_session_log} WHERE student_id=%d ORDER BY session_time", $student_id);
        while( $row = db_fetch_array($result) ) {

            $session_info[$row['id']]['advisor_id'] = $row['advisor_id'];
            $session_info[$row['id']]['session_time'] = $row['session_time'];
            $session_info[$row['id']]['metting_type'] = $row['metting_type'];
            $session_info[$row['id']]['session_note'] = $row['session_note'];

            if( !in_array( $row['advisor_id'], $advisor_ids) ){

                $advisor_ids[$row['advisor_id']] = $row['advisor_id'];
                $advisor_names[$row['advisor_id']] = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $row['advisor_id']));

            }

            $session_info[$row['id']]['advisor_name'] = $advisor_names[$row['advisor_id']];

        }

        foreach( $session_info as $session_id => $session_data ) {

            $advisor_id = $session_data['advisor_id'];
            $advisor_name = $session_data['advisor_name'];
            $session_time = $session_data['session_time'];
            $metting_type = $session_data['meeting_type'];
            $session_note = $session_data['session_note'];


            if( $session_note ) {

                // Decrypt the session note.
                $session_note = aes_decrypt($session_note);

                // Trim Please
                $session_note = trim($session_note);

                // Remove any trailing new lines.
                if( substr($session_note, -6) == "<br />" ) {
                    $session_note = substr_replace($session_note, '', -6);
                }

                // Truncate the note if it is
                // greater than 80 characters.
                if( strlen($session_note) > 80 ) {
                    $session_note = substr($session_note, 0, 80)."...";
                }

            }

            // Reformat the date from the session time.
            $new_time = strtotime($session_time);

            $session_date = date("M j Y", $new_time);

            $file_list = array();
            $session_files = '';
            $first_file = FALSE;

            $result = db_query("SELECT file_name, file_type FROM {mag_session_file} WHERE log_id=%d", $session_id);
            while( $row = db_fetch_array($result) ) {
                $file_list[$row['file_name']] = $row['file_type'];
            }

            if( $file_list ) {

                $file_count = count($file_list);

                $i = 1;

                foreach( $file_list as $filename => $filetype ) {

                    if( $file_count > 3 ) {

                        $title = 'File Name: ' . $filename . ' => File Type: ' . $filetype;

                        $filetype = $i++;

                    }else{
                        $title = 'File Name: ' . $filename;
                    }

                    // Link the file to it's directory location for easy downloading.
                    // The function file_create_url() is required for this to work.
                    if( !$first_file ) {
                        
                        $session_files .= '<a href="' . $base_url . '/' . $page_url . '/download_file/' . $filename . '/' . $session_id . '" title="' . $title . '" target="_blank" >' . $filetype . '</a>';

                        $first_file = TRUE;

                    }else {
                        $session_files .= ', <a href="' . $base_url . '/' . $page_url . '/download_file/' . $filename . '/' . $session_id . '" title="' . $title . '" target="_blank" >' . $filetype . '</a>';
                    }

                }

            }

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_date),
                '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td>',
            );

            // If the user has editing access, either Magellan Admin or
            // Magellan Support Staff, the session logs are editable.
            if( $edit_access ) {

                $update_url = '<a href="' . $base_url . '/' . $log_url . '/update/' . $session_id . '">' . $update_button . '</a>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($update_url),
                    '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

            }

            // If not the logs are
            // not editable.
            else{

                $view_url = '<a href="' . $base_url . '/' . $log_url . '/view/' . $session_id . '">' . $view_button . '</a>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($view_url),
                    '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

            }

            $form[] = array(
                '#type' => 'item',
                '#value' => t($advisor_name),
                '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_files),
                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_note),
                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td></tr>',
            );

        }

        $form[] = array(
            '#type' => 'item',
            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:0px 0px 4px 0px;" colspan=5></td></tr>'),
            '#suffix' => '</table>',
        );

        if( $param[0] == 'student' ){

            if( $edit_access ) {

                $form['advisor-history'] = array(
                    '#type' => 'submit',
                    '#value' => t('Return'),
                );
                
            }
            
            else {

                $form['advisor-history'] = array(
                    '#type' => 'submit',
                    '#value' => t('View advisor history'),
                );

            }

        }

    }

    // View all sessions logged for the current student.
    elseif( $param[0] == 'advisor' ) {

        $advisor_id = $param[1];
        
        $advisor_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $advisor_id));

        $form[] = array(
            '#type' => 'item',
            '#prefix' => '<table class="box"><tr bgcolor="#C0C0C0"><td colspan=5>',
            '#suffix' => '</td></tr>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('<i>Advisor</i> ' . $advisor_name),
            '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;" colspan=5>',
            '#suffix' => '</td></tr>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Date'),
            '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Student'),
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Uploaded Files'),
            '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Session Notes'),
            '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
            //'#suffix' => '</td>',
            '#suffix' => '</td></tr>',
        );

        $form[] = array(
            '#type' => 'item',
            '#value' => t('<tr bgcolor="#C0C0C0"><td colspan=5></td></tr>'),
        );

        $student_ids = $student_names = array();

        $result = db_query("SELECT id, student_id, session_time, meeting_type, session_note FROM {mag_session_log} WHERE advisor_id=%d ORDER BY session_time", $advisor_id);
        while( $row = db_fetch_array($result) ) {

            $log[$row['id']]['student_id'] = $row['student_id'];
            $log[$row['id']]['session_time'] = $row['session_time'];
            $log[$row['id']]['metting_type'] = $row['metting_type'];
            $log[$row['id']]['session_note'] = $row['session_note'];

            // Add unique student id's to an array associated to their username.
            if( !in_array( $row['student_id'], $student_ids) ){

                $student_ids[$row['student_id']] = $row['student_id'];
                
                // Get the students name.
                $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE id=%d", $row['student_id']));

                $student_names[$row['student_id']] = $student['first_name'] . ' ' . $student['last_name'];

            }

            // Add the students name to the array log.
            $log[$row['id']]['student_name'] = $student_names[$row['student_id']];

        }

        foreach( $log as $session_id => $session_data ) {

            $student_name = $session_data['student_name'];
            $session_time = $session_data['session_time'];
            $metting_type = $session_data['meeting_type'];
            $session_note = $session_data['session_note'];


            if( $session_note ) {

                // Decrypt the session note.
                $session_note = aes_decrypt($session_note);

                // Trim Please
                $session_note = trim($session_note);

                // Remove any trailing new lines.
                if( substr($session_note, -6) == "<br />" ) {
                    $session_note = substr_replace($session_note, '', -6);
                }

                // Truncate the note if it is
                // greater than 80 characters.
                if( strlen($session_note) > 80 ) {
                    $session_note = substr($session_note, 0, 80)."...";
                }

            }

            // Reformat the date from the session time.
            $new_time = strtotime($session_time);

            $session_date = date("M j Y", $new_time);

            $file_list = array();
            $session_files = '';
            $first_file = FALSE;

            $result = db_query("SELECT file_name, file_type FROM {mag_session_file} WHERE log_id=%d", $session_id);
            while( $row = db_fetch_array($result) ) {
                $file_list[$row['file_name']] = $row['file_type'];
            }

            if( $file_list ) {

                $file_count = count($file_list);

                $i = 1;

                foreach( $file_list as $filename => $filetype ) {

                    if( $file_count > 3 ) {

                        $title = 'File Name: ' . $filename . ' => File Type: ' . $filetype;

                        $filetype = $i++;

                    }else{
                        $title = 'File Name: ' . $filename;
                    }

                    // Link the file to it's directory location for easy downloading.
                    // The function file_create_url() is required for this to work.
                    if( !$first_file ) {
                        
                        $session_files .= '<a href="' . $base_url . '/' . $page_url . '/download_file/' . $filename . '/' . $session_id . '" title="' . $title . '" target="_blank" >' . $filetype . '</a>';

                        $first_file = TRUE;

                    }else {
                        $session_files .= ', <a href="' . $base_url . '/' . $page_url . '/download_file/' . $filename . '/' . $session_id . '" title="' . $title . '" target="_blank" >' . $filetype . '</a>';
                    }

                }

            }

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_date),
                '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td>',
            );

            // If the user has editing access, either Magellan Admin or
            // Magellan Support Staff, the session logs are editable.
            if( $edit_access ) {

                $update_url = '<a href="' . $base_url . '/' . $log_url . '/update/' . $session_id . '">' . $update_button . '</a>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($update_url),
                    '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

            }

            // If not the logs are
            // not editable.
            else{

                $view_url = '<a href="' . $base_url . '/' . $log_url . '/view/' . $session_id . '">' . $view_button . '</a>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($view_url),
                    '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

            }

            $form[] = array(
                '#type' => 'item',
                '#value' => t($student_name),
                '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_files),
                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_note),
                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td></tr>',
            );

        }

        $form[] = array(
            '#type' => 'item',
            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:0px 0px 4px 0px;" colspan=5></td></tr>'),
            '#suffix' => '</table>',
        );

        if( $edit_access ) {

            $form['advisor-history'] = array(
                '#type' => 'submit',
                '#value' => t('Return'),
            );

        }

        else {

            $form['advisor-history'] = array(
                '#type' => 'submit',
                '#value' => t('View advisor history'),
            );

        }

    }

    // If the user is an advisor and the advisor has session logs saved
    // in the database. Get the logs and display the 50 most recent.
    elseif( db_result(db_query("SELECT COUNT(*) FROM {mag_session_log} WHERE advisor_id=%d", $advisor_id)) ) {

        $form[] = array(
            '#type' => 'item',
            '#prefix' => '<table class="box"><tr bgcolor="#C0C0C0"><td colspan=5>',
            '#suffix' => '</td></tr>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Date'),
            '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Student'),
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Uploaded Files'),
            '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Session Notes'),
            '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
            '#suffix' => '</td></tr>',
        );

        $form[] = array(
            '#type' => 'item',
            '#value' => t('<tr bgcolor="#C0C0C0"><td colspan=5></td></tr>'),
        );

        $log = $student_ids = $student_names = array();
        
        // Limited to the 50 most recent results.
        $result = db_query("SELECT id, student_id, session_time, meeting_type, session_note FROM {mag_session_log} WHERE advisor_id=%d ORDER BY session_time DESC LIMIT 0, 50", $advisor_id);
        while( $row = db_fetch_array($result) ) {

            $log[$row['id']]['student_id'] = $row['student_id'];
            $log[$row['id']]['session_time'] = $row['session_time'];
            $log[$row['id']]['meeting_type'] = $row['meeting_type'];
            $log[$row['id']]['session_note'] = $row['session_note'];
            
            // Add unique student id's to an array associated to their username.
            if( !in_array( $row['student_id'], $student_ids) ){

                $student_ids[$row['student_id']] = $row['student_id'];
                
                // Get the students name.
                $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE id=%d", $row['student_id']));

                $student_names[$row['student_id']] = $student['first_name'] . ' ' . $student['last_name'];

            }

            // Add the students name to the array log.
            $log[$row['id']]['student_name'] = $student_names[$row['student_id']];

        }

        foreach( $log as $session_id => $log_info ) {

            $student_id = $log_info['student_id'];
            $student_name = $log_info['student_name'];
            $session_time = $log_info['session_time'];
            $meeting_type = $log_info['meeting_type'];
            $session_note = $log_info['session_note'];

            if( $session_note ) {

                // Decrypt the session note.
                $session_note = aes_decrypt($session_note);

                // Trim please
                $session_note = trim($session_note);

                // Remove any trailing new lines.
                if( substr($session_note, -6) == "<br />" ) {
                    $session_note = substr_replace($session_note, '', -6);
                }

                // Truncate the note if it is
                // greater than 80 characters.
                if( strlen($session_note) > 80 ) {
                    $session_note = substr($session_note, 0, 80)."...";
                }

            }

            // Reformat the date from the session time.
            $new_time = strtotime($session_time);

            $session_date = date("M j Y", $new_time);

            $file_list = array();
            $session_files = '';
            $first_file = FALSE;

            $result = db_query("SELECT file_name, file_type FROM {mag_session_file} WHERE log_id=%d", $session_id);
            while( $row = db_fetch_array($result) ) {
                $file_list[$row['file_name']] = $row['file_type'];
            }

            if( $file_list ) {

                $file_count = count($file_list);

                $i = 1;

                foreach( $file_list as $filename => $filetype ) {

                    if( $file_count > 3 ) {

                        $title = 'File Name: ' . $filename . ' => File Type: ' . $filetype;

                        $filetype = $i++;

                    }else{
                        $title = 'File Name: ' . $filename;
                    }

                    // Link the file to it's directory location for easy downloading.
                    // The function file_create_url() is required for this to work.
                    if( !$first_file ) {

                        $session_files .= '<a href="' . $base_url . '/' . $page_url . '/download_file/' . $filename . '/' . $session_id . '" title="' . $title . '" target="_blank" >' . $filetype . '</a>';

                        $first_file = TRUE;

                    }else {
                        $session_files .= ', <a href="' . $base_url . '/' . $page_url . '/download_file/' . $filename . '/' . $session_id . '" title="' . $title . '" target="_blank" >' . $filetype . '</a>';
                    }

                }

            }

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_date),
                '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td>',
            );

            // If the user has editing access, either Magellan Admin or
            // Magellan Support Staff, the session logs are editable.
            if( $edit_access ) {

                $update_url = '<a href="' . $base_url . '/' . $log_url . '/update/' . $session_id . '">' . $update_button . '</a>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($update_url),
                    '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

            }

            // If not the logs are
            // not editable.
            else{

                $view_url = '<a href="' . $base_url . '/' . $log_url . '/view/' . $session_id . '">' . $view_button . '</a>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($view_url),
                    '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

            }

            $student_url = $base_url . '/' . $page_url . '/student/' . $student_id;

            $form[] = array(
                '#type' => 'item',
                '#value' => t('<a href="' . $student_url . '" title="View this student\'s entire academic advising session history.">' . $student_name . '</a>'),
                '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_files),
                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#value' => t($session_note),
                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td></tr>',
            );

        }

        $form[] = array(
            '#type' => 'item',
            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:0px 0px 4px 0px;" colspan=5></td></tr>'),
            '#suffix' => '</table>',
        );

    }

    else {
        drupal_set_message("&nbsp; The Advisor has no previous sessions.");
    }

    return $form;

}

/**
 * Implementation of hook_submit().
 */
function daedalus_session_history_form_submit( $form, &$form_state ) {

    global $base_url;

    $page_url = daedalus_get_setting("session history");
    $page_url_length = sizeof(explode("/",$page_url));

    $param[0] = arg(0+$page_url_length);

    switch($form_state['values']['op']) {

        case $form_state['values']['advisor-history']:

            drupal_goto($base_url.'/'.$page_url);

            break;
        
        case $form_state['values']['view-all']:

            drupal_goto($base_url.'/'.$page_url.'/all');

            break;

        case $form_state['values']['dae-help-submit']:

            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message("&nbsp; Help information saved.");

                if( $param[0] ) {
                    drupal_goto($base_url."/".$page_url."/".$param[0]);
                }else {
                    drupal_goto($base_url."/".$page_url);
                }
            }

            break;
    }
}



 // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// PRACTICING ENCRYPTION HERE FOR THE HECK OF IT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//$file = file_directory_path()."/test.txt";
//$encrypted_file = file_directory_path()."/test.gpg";
//
//echo "FILE LOCATION > " . $file . "<br />";
//
//$gpg = '/usr/bin/gpg';




// ENCRYPT
//exec('cat passphrase.txt | ' . $gpg . ' --passphrase-fd 0 -c < ' . $file . ' > ' . $encrypted_file);
// DECRYPT
//exec('cat passphrase.txt | ' . $gpg . ' --passphrase-fd 0 < ' . $encrypted_file . ' > ' . $file);

//$algorithms = mcrypt_list_algorithms();
//
//foreach ($algorithms as $cipher) {
//    echo "$cipher<br />\n";
//}




////////////////////////////////////////////////////////////////////////////////
//
//  ZEND FRAMEWORK
//
//  http://www.codediesel.com/php/encrypting-uploaded-files-in-php/
//
//
//$file = file_directory_path()."/test.txt";
//$encrypted_file = file_directory_path()."/test.enc.txt";
//
///* Load the Zend file encrypting filter */
//require_once('./Zend/Filter/File/Encrypt.php');
//
///*  Set various encryption options. */
//$options = array(
//    // Encryption type - Openssl or Mcrypt
//    'adapter' => 'mcrypt',
//    // Encryption algorithm
//    'algorithm' => 'rijndael-192',
//    // Encryption key
//    'key' => 'KFJGKDK$$##^FFS345678F54'
//);
//
///* Initialize the library and pass the options */
//$filter = new Zend_Filter_File_Encrypt($options);
//
///* Generate a random vector */
//$filter->setVector();
//
///* Set output filename, where the encrypted file will be stored. */
//$filter->setFilename($encrypted_file);
//
///* Now encrypt a file */
//$filter->filter($file);
//
///*
//    Save the vector in a DB or somewhere else,
//    we will need this during decryption
//*/
//$vector = $filter->getVector();
//
//echo "VECTOR >>> ".$vector;




////////////////////////////////////////////////////////////////////////////////
//
//  CRYPTASTIC
//
//  http://blog.ropardo.ro/2010/11/11/basic-file-encryption-with-mcrypt-using-cryptastic/
//
//
//$salt 		= 'some random string';
//$pass 		= 'password';
//$cryptastic = new cryptastic;
//$key  		= $cryptastic->pbkdf2($pass, $salt, 1000, 32) or
//				die("Failed to generate secret key.");
//$encryptPath = 'uploads/some_user_folder/';
///**
//* this is the real name of the file, stored for a nice download name
//* better than a md5 hash
//*/
//$resumeFilename = $filename_tmp = $_FILES['filename']['name'];
//$ext = explode(".", $filename_tmp);
//$name_rand = md5(uniqid(rand(), TRUE));
//$filename = $name_rand . "." . $ext[1]; // Create Random Resume Name
//
//if (move_uploaded_file($_FILES['filename']['tmp_name'], FILE_UPLOAD_DIR . $filename)) {
//
//	$msg 			= file_get_contents(FILE_UPLOAD_DIR.$filename);
//	$encrypted		= $cryptastic->encrypt($msg, $key) or
//					  die("Failed to complete encryption.");
//	$ext 			= substr($filename, 33, 3);
//	$fileName 		= md5($filename.time()) . '-'.$ext;
//	$encryptedFile 	= $encryptPath.$fileName;
//
//	$fHandle		= fopen($encryptedFile, 'w+');
//	fwrite($fHandle, $encrypted);
//	fclose($fHandle);
//	unlink(FILE_UPLOAD_DIR . $filename);
//}

    // TO REMOVE THE DOWNLOADS DIRECTORY
    //exec('rm -r ' . file_directory_path() . "/downloads");

    //exec('chmod 777 ' . file_directory_path() . "/downloads");

    //exec('/usr/bin/chown -R jimbob ' . file_directory_path() . "/downloads");

//    $download_directory = "/var/www/html/daedalus_dev/graphviz/downloads";
//
//    if( !file_exists($download_directory) ) {
//
//        // Create a new directory for this sessions files.
//        exec('mkdir ' . $download_directory);
//
//        // Modify the permissions of this new directory.
//        exec('chmod 775 ' . $download_directory);
//
//    }


// VARIOUS PATHS
//    echo absolute_path_to_drupal()."<br /><br />";
//    echo "BASE URL > ".$base_url."<br />";
//    echo "FILE DIR > ".file_directory_path()."<br />";
//    echo "DRUP PAT > ".drupal_get_path('module', 'daedalus');

////////////////////////////////////////////////////////////////////////////////
//
//
//    http://www.devx.com/webdev/Article/37821/1954
//
//
////////////////////////////////////////////////////////////////////////////////
////  THIS WORKS GREAT!!! BUT NOT FOR DOCX
////////////////////////////////////////////////////////////////////////////////
////$file = file_directory_path() . "/test.pdf";
////$file = file_directory_path() . "/MilkyWay.jpg";
//$file = file_directory_path() . "/test.docx";
//$file_contents = file_get_contents($file);
//
//echo "FILE LOCATION > " . $file . "<br />";
//
////echo $file_contents."<br />";
//
//if( $file_contents ){
//
//    //$encrypted_contents = aes_encrypt($file_contents);
//    $encrypted_contents = aes_decrypt($file_contents);
//
//    $encrypted_file = fopen($file,'w');
//
//    if( $encrypted_file ){
//        echo 'FILE WAS OPENED<br />';
//    }else{
//        echo 'FILE FAILED TO OPEN<br />';
//    }
//
//    $ok_encrypt = fwrite($encrypted_file, $encrypted_contents);
//
//    if($ok_encrypt){
//        echo 'The file '.$file.' has been encrypted/decrypted.';
//    }
//    else{
//        echo ("File encryption failed!");
//    }
//
//    fclose($encrypted_file);
//
//}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
//
// AN ATTEMPT TO ZIP THE DOCX THEN ENCRYPT.....
//
//$file = file_directory_path() . "/test.docx";
//$new_file = file_directory_path() . "/test.zip";
//$encrypted_file = file_directory_path()."/test.gpg";
//
//$gpg = '/usr/bin/gpg';
//
////exec('cp ' . $new_file . ' ' . $file);
//
////exec('mv ' . $file . ' ' . $new_file);
//
//// ENCRYPT
////exec('cat passphrase.txt | ' . $gpg . ' --passphrase-fd 0 -c < ' . $new_file . ' > ' . $encrypted_file);
//// DECRYPT
////exec('cat passphrase.txt | ' . $gpg . ' --passphrase-fd 0 < ' . $encrypted_file . ' > ' . $new_file);
//
////exec('mv ' . $new_file . ' ' . $file);
//
//
//exec('gunzip -S .ZIP ' . $new_file);


//
//$file_contents = file_get_contents($new_file);
//
////echo $file_contents."<br />";
//
//if($file_contents){
//
//    $encrypted_contents = aes_encrypt($file_contents);
//    //$encrypted_contents = aes_decrypt($file_contents);
//
//    $encrypted_file = fopen($new_file,'w');
//
//    if( $encrypted_file ){
//        echo 'FILE WAS OPENED<br />';
//    }else{
//        echo 'FILE FAILED TO OPEN<br />';
//    }
//
//    $ok_encrypt = fwrite($encrypted_file, $encrypted_contents);
//
//    if($ok_encrypt){
//        echo 'The file '.$new_file.' has been encrypted/decrypted.';
//    }
//    else{
//        echo ("File encryption failed!");
//    }
//
//    fclose($encrypted_file);
//
//}

//$file = file_directory_path() . "/test.txt";
//$file_contents = file_get_contents($file);
//
//echo "FILE LOCATION > " . $file . "<br />";
//
//echo $file_contents."<br />";
//
//if($file_contents){
//
//  $password = 'OctaviaAnghel';
//
//  //Calculates the md5 hash
//  $md5_data = md5($password);
//
//  //This function encrypts data
//  $crypt = crypt($password);
//
//  //Calculate the sha1 hash
//  $sha1 = sha1($password);
//
//  //$encrypted_file = @fopen($file,'w');
//  $encrypted_file = fopen($file,'w');
//
//  if( $encrypted_file ){
//      echo 'FILE WAS OPENED<br />';
//  }else{
//      echo 'FILE FAILED TO OPEN<br />';
//  }
//
//  //$ok_encrypt = @fwrite($encrypted_file, 'md5: '. $md5_data."\r\n".'crypt: '.$crypt."\r\n".'sha1: '.$sha1);
//  $ok_encrypt = fwrite($encrypted_file, 'md5: '. $md5_data."\r\n".'crypt: '.$crypt."\r\n".'sha1: '.$sha1);
//
//  if($ok_encrypt){
//     echo 'The encrypted code was succesfully created' . ' in encrypted_file.txt!!!' . ' ';
//  }
//  else{
//     echo ("The write of this file failed!");
//  }
//
//  @fclose($encrypted_file);
//
//}
