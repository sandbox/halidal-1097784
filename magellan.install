<?php

// $Id$

/**
 * @file magellan.install
 * Created by Dr. Blouin and Justin Joyce
 */

/**
 * Implementation of hook_schema().
 * 
 * - Tables prefixed with "mag" for easy managing.
 * 
 */
function magellan_schema() {
    
    $schema['mag_program_identification'] = array(
        'description' => 'Stores program identifications',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies program identifications',
            ),
            'program' => array(
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 50,
                'size' => 'normal',
                'description' => 'Stores the program name',
            ),
            'year' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 4,
                'size' => 'small',
                'description' => 'Stores the progrogram year',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_program_requirement'] = array(
        'description' => 'Stores program requirements',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies program requirements',
            ),
            'course_id' => array(
                'type' => 'int',
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores program requirement course ids',
            ),
            'description' => array(
                'type' => 'varchar',
                'length' => 150,
                'size' => 'normal',
                'description' => 'Stores program requirement descriptions',
            ),
            'code_filter' => array(
                'type' => 'varchar',
                'length' => 50,
                'size' => 'normal',
                'description' => 'Stores program requirement code filters',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_program_content'] = array(
        'description' => 'Stores ',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies ',
            ),
            'program_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores ',
            ),
            'requirement_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores ',
            ),
            'requirement_order' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 4,
                'size' => 'small',
                'description' => 'Stores ',
            ),
            'note' => array(
                'type' => 'varchar',
                'length' => 150,
                'size' => 'normal',
                'description' => 'Stores ',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_student_identification'] = array(
        'description' => 'Stores student identification',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies student identifications',
            ),
            'user_name' => array(
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 20,
                'size' => 'normal',
                'description' => 'Stores the student id user name',
            ),
            'first_name' => array(
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 40,
                'size' => 'normal',
                'description' => 'Stores the student first name',
            ),
            'last_name' => array(
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 40,
                'size' => 'normal',
                'description' => 'Stores the student last name',
            ),
            'current_selection' => array(
                'type' => 'int',
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the students current program selection',
            ),
            'official_selection' => array(
                'type' => 'int',
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the students official selection selected by an advisor',
            ),
            'goal_type' => array(
                'type' => 'varchar',
                'length' => 6,
                'size' => 'normal',
                'description' => 'Stores the students goal type',
            ),
            'goal_id' => array(
                'type' => 'int',
                'length' => 6,
                'size' => 'small',
                'description' => 'Stores the students goal id',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_advisor_program_selection'] = array(
        'description' => 'Stores advisor program selections',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies an advisor program selection',
            ),
            'selection_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the selection id',
            ),
            'advisor_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the advisor id',
            ),
            'student_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the student id',
            ),
            'program_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the program id',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_student_program_selection'] = array(
        'description' => 'Stores the students program selections',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies a student program selection',
            ),
            'student_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the student id',
            ),
            'program_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the program id',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_student_program_form'] = array(
        'description' => 'Stores student program forms',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies a student program form',
            ),
            'selection_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the forms selection id',
            ),
            'log_id' => array(
                'type' => 'int',
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the form elements log id',
            ),
            'log_time' => array(
                'type' => 'datetime',
                'size' => 'normal',
                'description' => 'Stores the form elements log time',
            ),
            'content_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the form elements {mag_program_content} content id',
            ),
            'requirement_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the form elements {mag_program_requirement} requirement id',
            ),
            'requirement_order' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 4,
                'size' => 'small',
                'description' => 'Stores the form elements requirement order',
            ),

            'requirement_met' => array(
                'type' => 'int',
                'not null' => TRUE,
                'default' => 0,
                'length' => 1,
                'size' => 'tiny',
                'description' => 'Stores the form elements requirement met',
            ),
            'mark' => array(
                'type' => 'varchar',
                'default' => 'N/A',
                'length' => 4,
                'size' => 'normal',
                'description' => 'Stores the form elements mark',
            ),
            'timing' => array(
                'type' => 'varchar',
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the form elements timing',
            ),
            'equivalent_course' => array(
                'type' => 'varchar',
                'length' => 20,
                'size' => 'normal',
                'description' => 'Stores the form elements equivalent course',
            ),
            'equivalent_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores the form elements equivalent id',
            ),
            'note' => array(
                'type' => 'varchar',
                'length' => 150,
                'size' => 'normal',
                'description' => 'Stores the form elements note',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_goal_label'] = array(
        'description' => 'Stores goal labels',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies a goal label',
            ),
            'goal_label' => array(
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 150,
                'size' => 'normal',
                'description' => 'Stores the textual label',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_goal_sets'] = array(
        'description' => 'Stores goal sets',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies a goal set',
            ),
            'label_id' => array(
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 150,
                'size' => 'normal',
                'description' => 'Stores the label id',
            ),
            'slo_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 5,
                'size' => 'small',
                'description' => 'Stores the slo id',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_goal_student_slo'] = array(
        'description' => 'Stores students slos by goal',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies a students goal slo',
            ),
            'student_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 5,
                'size' => 'small',
                'description' => 'Stores a student id',
            ),
            'slo_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 5,
                'size' => 'small',
                'description' => 'Stores a slo id',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_goal_student_course'] = array(
        'description' => 'Stores students courses by goal',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies a students goal course',
            ),
            'student_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 5,
                'size' => 'small',
                'description' => 'Stores a student id',
            ),
            'course_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 5,
                'size' => 'small',
                'description' => 'Stores a course id',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_advisor_session'] = array(
        'description' => 'Stores advisor sessions',
        'fields' => array(
            'id' => array(
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 26,
                'size' => 'normal',
                'description' => 'Identifies an advisor session with a drupal session id',
            ),
            'advisor_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 5,
                'size' => 'small',
                'description' => 'Stores a advisor id',
            ),
            'student_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 5,
                'size' => 'small',
                'description' => 'Stores a student id',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_session_log'] = array(
        'description' => 'Stores session logs',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies a session log',
            ),
            'advisor_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 5,
                'size' => 'small',
                'description' => 'Stores an advisor id',
            ),
            'student_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 5,
                'size' => 'small',
                'description' => 'Stores a student id',
            ),
            'session_id' => array(
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 26,
                'size' => 'normal',
                'description' => 'Stores a session id',
            ),
            'session_time' => array(
                'type' => 'datetime',
                'not null' => TRUE,
                'size' => 'normal',
                'description' => 'Stores a session time',
            ),
            'meeting_type' => array(
                'type' => 'int',
                'not null' => TRUE,
                'default' => 1,
                'length' => 1,
                'size' => 'tiny',
                'description' => 'Stores the meeting type',
            ),
            'note' => array(
                'type' => 'int',
                'not null' => TRUE,
                'default' => 0,
                'length' => 1,
                'size' => 'tiny',
                'description' => 'Stores a note',
            ),
            'file' => array(
                'type' => 'int',
                'not null' => TRUE,
                'default' => 0,
                'length' => 1,
                'size' => 'tiny',
                'description' => 'Stores a file checking boolean value',
            ),
            'entry' => array(
                'type' => 'int',
                'not null' => TRUE,
                'default' => 0,
                'length' => 1,
                'size' => 'tiny',
                'description' => 'Stores an entry checking boolean value',
            ),
            'session_note' => array(
                'type' => 'blob',
                'size' => 'normal',
                'description' => 'Stores a note checking boolean value',
            ),
        ),
        'primary key' => array('id'),
    );

    $schema['mag_session_file'] = array(
        'description' => 'Stores ',
        'fields' => array(
            'id' => array(
                'type' => 'serial',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Identifies ',
            ),
            'log_id' => array(
                'type' => 'int',
                'not null' => TRUE,
                'unsigned' => TRUE,
                'length' => 11,
                'size' => 'normal',
                'description' => 'Stores ',
            ),
            'file_name' => array(
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 50,
                'size' => 'normal',
                'description' => 'Stores ',
            ),
            'file_type' => array(
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 20,
                'size' => 'normal',
                'description' => 'Stores ',
            ),
        ),
        'primary key' => array('id'),
    );

    return $schema;

}

/**
 * Implementation of hook_install()
 *
 * - Installs the magellan database tables.
 * - Installs the magellan settings into {dae_settings}.
 * - Creates the magellan user types.
 * 
 */
function magellan_install() {

    drupal_install_schema('magellan');

      ////////////////////////////////////////            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     //////////////////////////////////////// URL SETTINGS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    ////////////////////////////////////////                \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    $id            = 1;
    $setting 	   = "my program";
    $description   = "The root url to view magellan program content.";
    $type	   = "url";
    $value	   = "daedalus/program";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 2;
    $setting 	   = "program form";
    $description   = "A child url to view magellan program content.";
    $type	   = "url";
    $value	   = "daedalus/program/form";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 3;
    $setting 	   = "program map";
    $description   = "A child url to view magellan program map content.";
    $type	   = "url";
    $value	   = "daedalus/program/map";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 4;
    $setting 	   = "program my strengths";
    $description   = "A child url to view magellan student learning outcome strenghts content.";
    $type	   = "url";
    $value	   = "daedalus/program/strengths";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 5;
    $setting 	   = "program nextstep";
    $description   = "A child url to view magellan nextstep content.";
    $type	   = "url";
    $value	   = "daedalus/program/nextstep";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 6;
    $setting 	   = "program goal";
    $description   = "A child url to view magellan goal content.";
    $type	   = "url";
    $value	   = "daedalus/program/goal";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 7;
    $setting 	   = "advise";
    $description   = "A child url to view magellan advising content.";
    $type	   = "url";
    $value	   = "daedalus/advise";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 8;
    $setting 	   = "advise student";
    $description   = "A child url to view magellan advising student content.";
    $type	   = "url";
    $value	   = "daedalus/advise/student";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 9;
    $setting 	   = "session log";
    $description   = "A child url to view magellan advising session log content.";
    $type	   = "url";
    $value	   = "daedalus/advise/session_log";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 10;
    $setting 	   = "session history";
    $description   = "A child url to view magellan advising session history content.";
    $type	   = "url";
    $value	   = "daedalus/advise/session_history";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 25;
    $setting 	   = "build programs";
    $description   = "A child url to build magellan program content.";
    $type	   = "url";
    $value	   = "daedalus/build/program";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 26;
    $setting 	   = "build goals";
    $description   = "A child url to build magellan goal label content.";
    $type	   = "url";
    $value	   = "daedalus/build/goal";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 33;
    $setting 	   = "manage programs";
    $description   = "A child url to manage magellan program content.";
    $type	   = "url";
    $value	   = "daedalus/manage/program";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);

    $id            = 34;
    $setting 	   = "manage goals";
    $description   = "A child url to manage magellan goal content.";
    $type	   = "url";
    $value	   = "daedalus/manage/goal";
    $DEFAULT_value = $value;
    db_query("INSERT INTO {dae_settings} (id,setting,description,type,value,DEFAULT_value)
                   VALUES (%d,'%s','%s','%s','%s','%s')", $id, $setting, $description, $type, $value, $DEFAULT_value);


      /////////////////////////////////////                 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     ///////////////////////////////////// CREATE USER TYPES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    /////////////////////////////////////                     \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    db_query("INSERT INTO {role} (name) VALUES ('Magellan Administrator'), ('Magellan Advisor'), ('Magellan Support Staff')");

}

/**
 * Implementation of hook_uninstall()
 *
 * - Unnstalls the magellan database tables.
 * - Removes the magellan user types.
 * 
 */
function magellan_uninstall() {

    drupal_uninstall_schema('magellan');

    // Prepare to delete the associated access arguments.
    $madm_rid = db_result(db_query("SELECT rid FROM {role} WHERE name='Magellan Administrator'"));
    $madv_rid = db_result(db_query("SELECT rid FROM {role} WHERE name='Magellan Advisor'"));
    $msup_rid = db_result(db_query("SELECT rid FROM {role} WHERE name='Magellan Support Staff'"));

    db_query("DELETE FROM {role} WHERE rid=%d OR rid=%d OR rid=%d", $madm_rid, $madv_rid, $msup_rid );

    db_query("DELETE FROM {permission} WHERE rid=%d OR rid=%d OR rid=%d", $madm_rid, $madv_rid, $msup_rid );

}