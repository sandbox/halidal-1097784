<?php

// $ID$

/**
 * @file magellan.menu.php
 * Created by Dr. Blouin and Justin Joyce
 */

// Implementation of hook_menu
function magellan_menu () {

	$weight = array();
        $weight['program'] = -20;
        $weight['advise']  = -10;
        $weight['browse']  = 1;
	$weight['analyse'] = 10;
        $weight['build']   = 20;
	$weight['manage']  = 30;

	// Menu items to all prepend with magellan.
	$items = array();
		
	// The magellan menu items will all be sub-menu's of the
        // already implemented daedalus menu and every other link
        // will fall under one of the four categories: browse,
        // analyse, build, manage.

        ///////////////////////////// PROGRAM /////////////////////////////

        $items[daedalus_get_setting('my program')] = array(
		'title' => t('My Program'),
		'page callback'	=> 'daedalus_program_page',
		'access arguments' => array('magellan program'),
		'type'	=> MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
		'weight' => $weight['program'],
	);

	$items[daedalus_get_setting('program form')] = array(
		'title' => t('Program Form'),
		'page callback'	=> 'daedalus_my_program_page',
		'access arguments' => array('magellan program forms'),
		'type'	=> MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
		'weight' => $weight['program']+1,
	);

	$items[daedalus_get_setting('program map')] = array(
		'title' => t('Program Map'),
		'page callback'	=> 'daedalus_program_map_page',
		'access arguments' => array('magellan program map'),
		'type'	=> MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
		'weight' => $weight['program']+2,
	);

        $items[daedalus_get_setting('program my strengths')] = array(
		'title' => t('My Strengths'),
		'page callback'	=> 'daedalus_program_strengths_page',
		'access arguments' => array('magellan program strengths'),
		'type'	=> MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
		'weight' => $weight['program']+3,
	);

        $items[daedalus_get_setting('program nextstep')] = array(
		'title' => t('Next Step'),
		'page callback'	=> 'daedalus_program_nextstep_page',
		'access arguments' => array('magellan program nextstep'),
		'type'	=> MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
		'weight' => $weight['program']+4,
	);

        $items[daedalus_get_setting('program goal')] = array(
		'title' => t('Goals'),
		'page callback'	=> 'daedalus_program_goal_page',
		'access arguments' => array('magellan program goal'),
		'type'	=> MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
		'weight' => $weight['program']+5,
	);


        ////////////////////////////// BUILD //////////////////////////////

	// Page for submitting a program
	$items[daedalus_get_setting('build programs')] = array(
		'title' => t('Programs'),
		'page callback'	=> 'daedalus_build_programs',
		'access arguments' => array('magellan build programs'),
		'type' => MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
                'weight' => $weight['build']+5,
	);

        // Page for submitting a goal
	$items[daedalus_get_setting('build goals')] = array(
		'title' => t('Goals'),
		'page callback'	=> 'daedalus_build_goals',
		'access arguments' => array('magellan build goals'),
		'type' => MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
                'weight' => $weight['build']+6,
	);

        ///////////////////////////// ADVISE /////////////////////////////

        $items[daedalus_get_setting('advise')] = array(
		'title' => t('Advise'),
		'page callback'	=> 'daedalus_advise_page',
		'access arguments' => array('magellan advise'),
		'type'	=> MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
		'weight' => $weight['advise'],
	);

	$items[daedalus_get_setting('advise student')] = array(
		'title' => t('Advise Student'),
		'page callback'	=> 'daedalus_advise_student_page',
		'access arguments' => array('magellan advise'),
		'type'	=> MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
		'weight' => $weight['advise']+1,
	);

        $items[daedalus_get_setting('session log')] = array(
		'title' => t('Session Log'),
		'page callback'	=> 'daedalus_session_log_page',
		'access arguments' => array('magellan advise'),
		'type'	=> MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
		'weight' => $weight['advise']+2,
	);

        $items[daedalus_get_setting('session history')] = array(
		'title' => t('Session History'),
		'page callback'	=> 'daedalus_session_history_page',
		'access arguments' => array('magellan advise'),
		'type'	=> MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
		'weight' => $weight['advise']+3,
	);

        ///////////////////////////// MANAGE /////////////////////////////

	// Page for managing a program
	$items[daedalus_get_setting('manage programs')] = array(
		'title' => t('Programs'),
		'page callback'	=> 'daedalus_manage_programs',
		'access arguments' => array('magellan manage programs'),
		'type' => MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
                'weight' => $weight['manage']+6,
	);

        // Page for managing a goal
	$items[daedalus_get_setting('manage goals')] = array(
		'title' => t('Goals'),
		'page callback'	=> 'daedalus_manage_goals',
		'access arguments' => array('magellan manage goals'),
		'type' => MENU_NORMAL_ITEM,
		'menu_name' => 'menu-daedalus',
                'weight' => $weight['manage']+7,
	);

        return $items;
}