<?php

// $ID$

/**
 * @file magellan.menu-build.php
 * Created by Dr. Blouin and Justin Joyce
 */

//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Build->Programs Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_build_programs() {
    return drupal_get_form("daedalus_build_programs_form");
};

/**
* Menu Location: Daedalus -> Build -> Programs
* URL Location:  daedalus/daedalus/build/programs
*
* Displays 
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_build_programs_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Get current page url.
    $page_url = $help_url = daedalus_get_setting("build programs");

    $page_url_length = sizeof(explode('/',$page_url));
    $page_url = $base_url."/".$page_url;

    // Store URL Parameters in $param array
    $param = array();
    $param[0] = arg(0+$page_url_length);    // Program created notification
    $param[1] = arg(1+$page_url_length);    // The program

    $question_img_src = $base_url."/".daedalus_get_setting("question mark");

    // The title is set with an 'a' tag with the class='show-help'. When the image is clicked the help section is shown.
    drupal_set_title("Build New Program <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>");

    $form = array();

    // Add the hidden help form. Paramaters are
    // (help url, show border, show break).
    $form = display_hidden_form($help_url, 1, 1);

    $form['program'] = array(
        '#type'  => 'textfield',
        '#title' => t("Program Name"),
    );

    $form['effective-year'] = array(
        '#type'  => 'textfield',
        '#title' => t("Effective Year"),
    );

    // Apply css to change the table behaviour
    $css = '<style type="text/css">
                table.box{
                    border-style:solid;
                    border-width:3px;
                    border-color:#C0C0C0;
                }
            </style>';

    drupal_set_html_head($css);

    $form[] = array(
        '#type' => 'item',
        '#title' => t('Description'),
        '#prefix' => '<table class="box"><tr class="odd"><td style="padding:0px 0px 0px 5px;">',
        '#suffix' => '</td>',
    );

    $form[] = array(
        '#type' => 'item',
        '#title' => t('Course'),
        '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
        '#suffix' => '</td>',
    );

    $form[] = array(
        '#type' => 'item',
        '#title' => t('Note'),
        '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
        '#suffix' => '</td>',
    );

    $form[] = array(
        '#type' => 'item',
        '#title' => t('Course Code Filter'),
        '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
        '#suffix' => '</td></tr>',
    );

    $textfields = daedalus_get_setting("program iteration");

    for( $i = 0; $i < $textfields; $i++ ) {

        // Close the table on the last
        // iteration of the program form.
        if( $i == ($textfields - 1) ) {
            $close_table = "</table>";
        }

        $form["description_$i"] = array(
            '#type' => 'textfield',
            '#size' => 40,
            '#autocomplete_path' => 'autocomp/description',
            '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form["course_$i"] = array (
            '#type' => 'textfield',
            '#size' => 35,
            '#autocomplete_path' => 'autocomp/course',
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form["note_$i"] = array (
            '#type' => 'textfield',
            '#size' => 30,
            '#prefix' => '<td style="padding:0px 5px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form["code-filter_$i"] = array (
            '#type' => 'textfield',
            '#size' => 20,
            '#autocomplete_path' => 'autocomp/multiplecodes',
            '#prefix' => '<td style="padding:0px 5px 0px 5px;">',
            '#suffix' => '</td></tr>'.$close_table,
        );

    }

    $form['amount-value'] = array(
        '#type' => 'select',
        '#options' => array(  1=>t('1'),
                              2=>t('2'),
                              3=>t('3'),
                              4=>t('4'),
                              5=>t('5'),
                              6=>t('6'),
                              7=>t('7'),
                              8=>t('8'),
                              9=>t('9'),
                             10=>t('10'),
                             11=>t('11'),
                             12=>t('12'),
                             13=>t('13'),
                             14=>t('14'),
                             15=>t('15'),
                             16=>t('16'),
                             17=>t('17'),
                             18=>t('18'),
                             19=>t('19'),
                             20=>t('20'),
                             21=>t('21'),
                             22=>t('22'),
                             23=>t('23'),
                             24=>t('24'),
                             25=>t('25'),
                             26=>t('26'),
                             27=>t('27'),
                             28=>t('28'),
                             29=>t('29'),
                             30=>t('30'),
                             31=>t('31'),
                             32=>t('32'),
                             33=>t('33'),
                             34=>t('34'),
                             35=>t('35'),
                             36=>t('36'),
                             37=>t('37'),
                             38=>t('38'),
                             39=>t('39'),
                             40=>t('40'),
                             41=>t('41'),
                             42=>t('42'),
                             43=>t('43'),
                             44=>t('44'),
                             45=>t('45'),
                             46=>t('46'),
                             47=>t('47'),
                             48=>t('48'),
                             49=>t('49'),
                             50=>t('50'),
                          ),
        '#default_value' => $textfields,
        '#prefix' => '<table><tr><th width=10px;>',
        '#suffix' => '</th>',
    );

    $form['submit-field-amount'] = array(
        '#type' => 'submit',
        '#value' => t('Change requirement field amount'),
        '#prefix' => '<th width=10px;>',
        '#suffix' => '</th>',
    );

    // Submission button.
    $form['submit-program'] = array(
        '#type'  => 'submit',
        '#value' => t('Create program'),
        '#prefix' => '<th>',
        '#suffix' => '</th></tr></table>',
    );

    // Submit the hiddent textfield amount
    $form['textfields'] = array('#type' => 'value', '#value' => $textfields);

    return $form;

} // function daedalus_browse_programs_form( $form )


/**
 * Implementation of hook_validate().
 */
function daedalus_build_programs_form_validate( $form, &$form_state ) {

    if( $form_state['values']['op'] == $form_state['values']['submit-program'] ) {

        $program = $form_state['values']['program'];
        $effect_year = $form_state['values']['effective-year'];
        $textfields = $form_state['values']['textfields'];

        if( !$form_state['values']['program'] ) {
            form_set_error('program', t('The field <b>Program Name</b> is required.'));
        }

        if( !$form_state['values']['effective-year'] ) {
            form_set_error('effective-year', t('The field <b>Effective Year</b> is required.'));
        }

        if( !is_numeric($form_state['values']['effective-year']) ) {
            form_set_error('effective-year', t('The field <b>Effective Year</b> must be an integer.'));
        }

        // Don't allow a duplicate program to be entered according to the program name and year.
        if( db_result(db_query("SELECT COUNT(*) FROM {mag_program_identification} WHERE program='%s' AND year=%d", $program, $effect_year)) ) {
            form_set_error('effective-year', t('The program <b>'.$program.'</b> has already been created for the year <b>'.$effect_year.'</b>.'));
        }

        $requirement = FALSE;

        for( $i = 0; $i < $textfields; $i++ ) {

            $description = $form_state['values']["description_$i"];
            $course      = $form_state['values']["course_$i"];

            if ( $description || $course ) {
                $requirement = TRUE;
                break;
            }
        }

        if( !$requirement ) {
            form_set_error('', t('You must enter at least one program requirement.'));
        }
    }
}

/**
 * Implementation of hook_submit().
 */
function daedalus_build_programs_form_submit( $form, &$form_state ) {

    global $base_url;

    switch($form_state['values']['op']) {

        case $form_state['values']['submit-field-amount']:

            $page_url = daedalus_get_setting("build programs");

            db_query("UPDATE {dae_settings} SET value=%d WHERE setting='program iteration'", $form_state['values']['amount-value'] );

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['submit-program']:

            $page_url = daedalus_get_setting("manage programs");

            $program = $form_state['values']['program'];
            $effective_year = $form_state['values']['effective-year'];
            $textfields = $form_state['values']['textfields'];

            // The array to compare the first character
            // of the description and add the number to
            // the description order value.
            $char_array = array("a" => 974, "b" => 975, "c" => 976, "d" => 977, "e" => 978, "f" => 979, 
                                "g" => 980, "h" => 981, "i" => 982, "j" => 983, "k" => 984, "l" => 985, 
                                "m" => 986, "n" => 987, "o" => 988, "p" => 989, "q" => 990, "r" => 991, 
                                "s" => 992, "t" => 993, "u" => 994, "v" => 995, "w" => 996, "x" => 997, 
                                "y" => 998, "z" => 999 );

            // Enter the program information into
            // the program_identification database.
            db_query("INSERT INTO {mag_program_identification} (program, year) VALUES ('%s', %d)", $program, $effective_year);

            // Select the newly created program's id.
            $program_id = db_result(db_query("SELECT id FROM {mag_program_identification} WHERE program='%s' AND year=%d", $program, $effective_year));

            // Iterate each field of the program.
            for( $i = 0; $i < $textfields; $i++ ) {

                $description = $form_state['values']["description_$i"];
                $note = $form_state['values']["note_$i"];
                $course = $form_state['values']["course_$i"];
                $course_id = get_course_id($course);
                $code_filter = $form_state['values']["code-filter_$i"];

                // Parsing the description or course and saving the courses in an incremental
                // 'order' field will facilitate the correct ordering of the courses in
                // the program by their level, either 1000, 2000, 3000 or 4000. If the description
                // does not contain a number and there is no course present, the description is
                // set to 9999 to indicate the "other requirements" section of the program.
                
                // Insert the program requirement and program
                // content if a course is entered.
                if( $course ) {

                    // Determine the course level.
                    $course_info = explode(" ", $course);
                    $course_order = $course_info[1];

                    // Select the course name from the database
                    // to display as the description.
                    $description = db_result(db_query("SELECT course_name FROM {dae_course} WHERE id=%d", $course_id));

                    if( !db_result(db_query("SELECT COUNT(*) FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter)) ) {

                        db_query("INSERT INTO {mag_program_requirement} (course_id, description, code_filter)
                                       VALUES (%d,'%s','%s')", $course_id, $description, $code_filter);

                        $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter));

                        db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                       VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $course_order, $note);

                    }

                    else {

                        $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter));

                        db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                       VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $course_order, $note);

                    }

                }

                // Insert the program requirement and program
                // content if there is no course entered.
                elseif( $description && !$course ) {

                    // Determine the course level from the description by matching
                    // the course number found in the description. If a number is 
                    // not found that matches 1000/2000/3000 or 4000 set the order
                    // value to 9999 to indicate an 'other' requirement.
                    if( preg_match('/(4)[0-9]{3}/', $description) ) {
                        
                        $description_order = 4;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    elseif( preg_match('/(3)[0-9]{3}/', $description) ) {
                        
                        $description_order = 3;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    elseif( preg_match('/(2)[0-9]{3}/', $description) ) {
                        
                        $description_order = 2;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    elseif( preg_match('/(1)[0-9]{3}/', $description) ) {
                        
                        $description_order = 1;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    else {
                        
                        $description_order = 9;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }

                    if( !db_result(db_query("SELECT COUNT(*) FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter)) ) {

                        db_query("INSERT INTO {mag_program_requirement} (course_id, description, code_filter)
                                       VALUES (%d,'%s','%s')", $course_id, $description, $code_filter);

                        $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter));

                        db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                       VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $description_order, $note);

                    }

                    else {

                        $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter));

                        db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                       VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $description_order, $note);

                    }

                }

            }

            drupal_set_message(t('The program <b>' . $program. '</b> has been created for the year <b>' . $effective_year . '</b>.'));

            drupal_goto($base_url."/".$page_url."/modify/".$program."/".$effective_year);

            break;

        case $form_state['values']['dae-help-submit']:

            $page_url = daedalus_get_setting("build programs");
            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message(t('Help information saved.'));

                drupal_goto($base_url."/".$page_url);
            }

            break;
    }
}


//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Build->Programs Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_build_goals() {
    return drupal_get_form("daedalus_build_goals_form");
};

/**
* Menu Location: Daedalus -> Build -> Goals
* URL Location:  daedalus/daedalus/build/goals
*
* Displays
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_build_goals_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Get current page url.
    $page_url = $help_url = daedalus_get_setting("build goals");

    $page_url_length = sizeof(explode('/',$page_url));
    $page_url = $base_url."/".$page_url;

    // Store URL Parameters in $param array
    $param = array();
    $param[0] = arg(0+$page_url_length);    // 
    $param[1] = arg(1+$page_url_length);    //
    $param[2] = arg(2+$page_url_length);    //
    $param[3] = arg(3+$page_url_length);    //

    $question_img_src = $base_url."/".daedalus_get_setting("question mark");

    // The title is set with an 'a' tag with the class='show-help'. When the image is clicked the help section is shown.
    drupal_set_title("Build New Goal <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>");

    $form = array();

    if( db_result(db_query("SELECT COUNT(*) FROM {dae_slo}")) ) {

        // Add the hidden help form. Paramaters are
        // (help url, show border, show break).
        $form = display_hidden_form($help_url, 1, 1);

        $form['goal-label'] = array(
            '#type'  => 'textfield',
            '#title' => t('Goal Label'),
            '#default_value' => t($goal_label),
        );

        $form['submit-goal'] = array(
            '#type' => 'submit',
            '#value' => t('Submit Goal'),
        );

    }else {
        // If there are no course codes, inform the user a course code must be created first.
        $url = $base_url."/".daedalus_get_setting("build learning outcomes");
        drupal_set_message(t('There are no student learning outcomes created. <a href="'.$url.'"><b>Create outcomes</b></a>'));
    }

    // Add check to make sure there are goals to manages

    return $form;
}


/**
 * Implementation of hook_validate().
 */
function daedalus_build_goals_form_validate( $form, &$form_state ) {

    switch($form_state['values']['op']) {

        case $form_state['values']['submit-goal']:

            $goal_label = $form_state['values']['goal-label'];

            if( !$goal_label ) {
                form_set_error('goal-label', t('You must enter a goal label.'));
            }

            if( db_result(db_query("SELECT COUNT(*) FROM {mag_goal_label} WHERE goal_label='%s'", $goal_label)) ) {
                form_set_error('goal-label', t('The goal label has already been created.'));
            }
        break;

    }
}


/**
 * Implementation of hook_submit().
 */
function daedalus_build_goals_form_submit( $form, &$form_state ) {

    global $base_url;

    switch($form_state['values']['op']) {

        case $form_state['values']['submit-goal']:

            $manage_url = daedalus_get_setting("manage goals");
            
            $goal_label = $form_state['values']['goal-label'];

            db_query("INSERT INTO {mag_goal_label} (goal_label) VALUES ('%s')", $goal_label);
            
            $label_id = db_result(db_query("SELECT id FROM {mag_goal_label} WHERE goal_label='%s'", $goal_label));

            // Return to the goal creation with appropriate URL parameters
            drupal_goto($base_url."/".$manage_url."/".$label_id);

        break;

        // Save the help information
        case $form_state['values']['dae-help-submit']:

            $page_url = daedalus_get_setting("build goals");

            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message(t('Help information saved.'));

                drupal_goto($base_url."/".$page_url);
            }
        break;

    }
}