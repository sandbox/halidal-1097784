<?php

// $ID$

/**
 * @file magellan.menu-manage.php
 * Created by Dr. Blouin and Justin Joyce
 */

//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Manage->Programs Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_manage_programs() {
    return drupal_get_form("daedalus_manage_programs_form");
};

/**
* Menu Location: Daedalus -> Manage -> Programs
* URL Location:  daedalus/daedalus/manage/programs
*
* Displays
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_manage_programs_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Get current page url.
    $page_url = $help_url = daedalus_get_setting("manage programs");

    $build_url = $base_url."/".daedalus_get_setting("build programs");

    $page_url_length = sizeof(explode('/',$page_url));
    $page_url = $base_url."/".$page_url;

    // Store URL Parameters in $param array
    $param = array();
    $param[0] = arg(0+$page_url_length);    //
    $param[1] = arg(1+$page_url_length);    //
    $param[2] = arg(2+$page_url_length);    //
    $param[3] = arg(3+$page_url_length);    // Additional Textfields Total

    $question_img_src = $base_url."/".daedalus_get_setting("question mark");

    // The title is set with an 'a' tag with the class='show-help'. When the image is clicked the help section is shown.
    drupal_set_title("Manage Programs <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>");

    $form = array();

    // Add the hidden help form. Paramaters are
    // (help url, show border, show break).
    $form = display_hidden_form($help_url, 1, 1);

     ///////////////////////////////////////////////////////////////////////////
    // Submit Program
    if( !$param[0] ) {

        // Have the user select their Program.
        $result = db_query("SELECT DISTINCT program FROM {mag_program_identification} ORDER BY program");

        while( $row = db_fetch_array($result) ) {
            $available_progams[$row['program']] = $row['program'];
            ++$flag;
        }

        if( $flag ) {

            $form['program'] = array(
                '#type'  => 'select',
                '#title' => t("Select a Program"),
                '#options' => $available_progams,
            );

            $form['submit-program'] = array(
                '#type' => 'submit',
                '#value' => t('Select program'),
            );

        }
        else {
            drupal_set_message("A program must first be created. Create one <a href='".$build_url."'>here</a>.");
        }

    }

     ///////////////////////////////////////////////////////////////////////////
    // Submit Program Year
    elseif( $param[0] == "submit" ) {

        // Have the user select their rogram year.
        $result = db_query("SELECT DISTINCT year FROM {mag_program_identification} WHERE program='%s' ORDER BY year DESC", $param[1]);

        while( $row = db_fetch_array($result) ) {
            $effective_years[$row['year']] = $row['year'];
        }

        $form['effective-year'] = array(
            '#type'  => 'select',
            '#title' => t("Select a program year"),
            '#options' => $effective_years,
        );

        $form['submit-year'] = array(
            '#type' => 'submit',
            '#value' => t('Select program year'),
        );

        // Submit hidden program value
        $form["program"] = array( '#type' => 'value', '#value' => $param[1] );

    }

     ///////////////////////////////////////////////////////////////////////////
    // Delete Program Information
    elseif( $param[0] == "delete" ) {
        
        drupal_set_title("Delete confirmation");

        $message ="<br />
                   Are you sure you want to delete the program <strong><u>".$param[1]." ".$param[2]."</u></strong>? This can not be undone.
                   <br /><br />
                   <ul>
                        <li>This will delete the program for the given year.</li>
                   </ul>
                   <br />Are you sure you want to continue?";

        $form[] = array(
            '#type' => 'item',
            '#value' => t($message),
        );

        $form['delete-forward'] = array(
            '#type' => 'submit',
            '#value' => t("I understand the consequences. Delete this program forever."),
        );

        $form['delete-reverse'] = array(
            '#type' => 'submit',
            '#value' => t("On second thought, I better not. Take me back."),
        );

        // Hidden values
        $form["program"] = array( '#type' => 'value', '#value' => $param[1] );
        $form['effective-year'] = array( '#type' => 'value', '#value' => $param[2] );

    }

     ///////////////////////////////////////////////////////////////////////////
    // Modify Program Information
    else {

        $program = $param[1];
        $year = $param[2];
        $additional_reqs = $param[3];

        if( $param[0] == "modify" ) {

            $form[] = array(
                '#type'  => 'textfield',
                '#default_value' => $param[1],
                '#disabled' => 'disabled',
                '#title' => t("Program Name"),
            );

            // Submit the hidden program info because the above
            // for is disabled and will not transfer the info.
            $form['program'] = array( '#type' => 'value', '#value' => $program );

            $form[] = array(
                '#type'  => 'textfield',
                '#default_value' => $param[2],
                '#disabled' => 'disabled',
                '#title' => t("Effective Year"),
            );

            // Same for the year.
            $form['effective-year'] = array( '#type' => 'value', '#value' => $year );

        }

        elseif( $param[0] == "duplicate" ) {

            // Here the info is passed.
            $form['program'] = array(
                '#type'  => 'textfield',
                '#default_value' => $program,
                '#title' => t("Program Name"),
            );

            $form['effective-year'] = array(
                '#type'  => 'textfield',
                '#default_value' => $year,
                '#title' => t("Effective Year"),
                '#description' => t("You must change the effective year value to duplicate the program.")
            );

        }

        // Apply css to change the table behaviour
        $css = '<style type="text/css">
                    table.box{
                        border-style:solid;
                        border-width:3px;
                        border-color:#C0C0C0;
                    }
                </style>';

        drupal_set_html_head($css);

        if( $param[0] == "modify" ) {

            // Submission button.
            $form['save-changes'] = array(
                '#type'  => 'submit',
                '#value' => t('Save program changes'),
            );

            $form['delete-program'] = array(
                '#type'  => 'submit',
                '#value' => t('Delete program'),
            );

            $form['delete-selected'] = array(
                '#type'  => 'submit',
                '#value' => t('Delete selected'),
            );

            $form['duplicate-program'] = array(
                '#type'  => 'submit',
                '#value' => t('Duplicate program'),
            );

            $form['cancel'] = array(
                '#type'  => 'submit',
                '#value' => t('Cancel'),
            );

            $form['submit-textfields'] = array(
                '#type' => 'submit',
                '#value' => t('Add requirement(s)'),
                '#weight' => 100,
                '#prefix' => '<table><tr style="border-top-style:solid; border-width:2px; border-color:#C0C0C0;"><td width="10px">',
                '#suffix' => '</td>',
            );

            $form['textfields-total'] = array(
                '#type' => 'select',
                '#options' => array(
                        '1' => t('1'),
                        '2' => t('2'),
                        '3' => t('3'),
                        '4' => t('4'),
                        '5' => t('5'),
                        '6' => t('6'),
                        '7' => t('7'),
                        '8' => t('8'),
                        '9' => t('9'),
                        '10' => t('10'),
                ),
                '#weight' => 100,
                '#default_value' => '1',
                '#prefix' => '<td align="left">',
                '#suffix' => '</td></tr></table>',
            );

            // Begin Table
            $form[] = array(
                '#type' => 'item',
                '#value' => t('<table class="box"><tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=5></td></tr>'),
            );

            $form[] = array(
                '#type' => 'item',
                '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 5px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#title' => t('Description'),
                '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                '#suffix' => '</td>',
            );
            
            // Additional checkboxes requires colspan modification
            $colspan = 5;

        }

        elseif( $param[0] == "duplicate" ) {

            // Submission button.
            $form['save-duplicate'] = array(
                '#type'  => 'submit',
                '#value' => t('Save duplicate program'),
            );

            $form['cancel-duplicate'] = array(
                '#type'  => 'submit',
                '#value' => t('Cancel'),
            );

            // Begin Table
            $form[] = array(
                '#type' => 'item',
                '#value' => t('<table class="box"><tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=4></td></tr>'),
            );

            $form[] = array(
                '#type' => 'item',
                '#title' => t('Description'),
                '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 5px;">',
                '#suffix' => '</td>',
            );

            $colspan = 4;

        }

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Course'),
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Note'),
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td>',
        );

        $form[] = array(
            '#type' => 'item',
            '#title' => t('Course Code Filter'),
            '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
            '#suffix' => '</td></tr>',
        );

        // Select the program id
        $program_id = db_result(db_query("SELECT id FROM {mag_program_identification} WHERE program='%s' AND year=%d", $program, $year));

        // Get each of the requiremnt id's for the selected program,
        // also determine the amount of textfields required to display
        // the selected program by counting the number of entries.
        $result = db_query("SELECT id, requirement_id, requirement_order, note FROM {mag_program_content} WHERE program_id=%d ORDER BY requirement_order", $program_id);
        while( $row = db_fetch_array($result) ){

            $content_array[$row['id']]['requirement_id'] = $row['requirement_id'];
            $content_array[$row['id']]['requirement_order'] = $row['requirement_order'];
            $content_array[$row['id']]['note'] = $row['note'];

        }

        // Get the amount of table rows to display.
        $textfields = count($content_array);

        foreach( $content_array as $cid => $content_info ) {

            $result = db_query("SELECT course_id, description, code_filter FROM {mag_program_requirement} WHERE id=%d", $content_info['requirement_id']);
            while( $row = db_fetch_array($result) ) {

                $content_array[$cid]['course_id'] = $row['course_id'];
                $content_array[$cid]['description'] = $row['description'];
                $content_array[$cid]['code_filter'] = $row['code_filter'];

                // Get the course
                $content_array[$cid]['course'] = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $row['course_id']));

            }

        }

        // If additional textfields requested add the additional empty
        // array elements to the array and increase the textfields.
        if( $additional_reqs ) {

            for( $i = 0; $i < $additional_reqs; $i++) {

                // Instead of a requiement array as
                // the key use the 'add_' with #.
                $content_array["add_$i"] = "";

            }

            $textfields = $textfields + $additional_reqs;

        }

        // Submit the hidden textfield amount and program id info.
        $form['textfields'] = array( '#type' => 'value', '#value' => $textfields );
        $form['program-id'] = array( '#type' => 'value', '#value' => $program_id );

        $i = 0;     // iteration to keep track of when to close the table.
        $level = 1; // iteration variable for the current level.

        foreach( $content_array as $cid => $content_info ) {

            $requirement_id = $content_info['requirement_id'];
            $course_id = $content_info['course_id'];
            $course = $content_info['course'];
            $req_order = $content_info['requirement_order'];
            $description = $content_info['description'];
            $note = $content_info['note'];
            $code_filter = $content_info['code_filter'];

            // Submit the hidden info.
            $form["course-id_$i"] = array( '#type' => 'value', '#value' => $course_id );
            $form["content-id_$i"] = array( '#type' => 'value', '#value' => $cid );
            $form["requirement-id_$i"] = array( '#type' => 'value', '#value' => $requirement_id );

            // Make sure that the course levels
            // do not jump up one level.
            $current_level = substr($req_order, 0, 1);
            if( $current_level > $level ) {
                $level = $current_level;
            }

            // By selelecting the course order and examining the first number,
            // the level of the course is determined. (1-4) The level is
            // incremented and the next level is not displayed until that number
            // is encountered.
            if( ($current_level == $level) && ($current_level < 9) ) {

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=' . $colspan . '><b><big>' . $level . '000 Level &nbsp;&nbsp;</big></b></td></tr>'),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                );

                $level++;

            }

            // Courses that do not have a level between 1000 and 4000 are set to
            // 9999. When an intial 9 is found the "Other" level is displayed.
            if( ($current_level == 9) && !$other_flag ) {

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=' . $colspan . '><b><big>Other Required Courses &nbsp;&nbsp;</big></b></td></tr>'),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                );

                $other_flag++;

            }

            // Additional textfields are given array values of "add_" with an
            // incremental suffix attached, beggining with 0.
            if( $cid == "add_0" && !$additional_flag ) {

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=' . $colspan . '><b><big>Additional Requirements &nbsp;&nbsp;</big></b></td></tr>'),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                );

                $additional_flag++;

            }

            if( $param[0] == "duplicate" ) {

                $form["description_$i"] = array(
                    '#type' => 'textfield',
                    '#size' => 60,
                    '#autocomplete_path' => 'autocomp/description',
                    '#default_value' => $description,
                    '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

                $form["course_$i"] = array (
                    '#type' => 'textfield',
                    '#size' => 15,
                    '#autocomplete_path' => 'autocomp/course',
                    '#default_value' => $course,
                    '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

                $form["note_$i"] = array (
                    '#type' => 'textfield',
                    '#size' => 35,
                    '#default_value' => $note,
                    '#prefix' => '<td style="padding:0px 5px 0px 5px;">',
                    '#suffix' => '</td>',
                );

                $form["code-filter_$i"] = array (
                    '#type' => 'textfield',
                    '#size' => 25,
                    '#default_value' => $code_filter,
                    '#autocomplete_path' => 'autocomp/multiplecodes',
                    '#prefix' => '<td style="padding:0px 5px 0px 5px;">',
                    '#suffix' => '</td></tr>',
                );

            }

            if( $param[0] == "modify" ) {

                if( !$additional_flag ) {

                    // An array of checkboxes to denote
                    // the removal of the program content.
                    $form["delete_$i"] = array(
                        '#type' => 'checkbox',
                        '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 5px;">',
                        '#suffix' => '</td>',
                    );

                }
                else {

                    // Display a space instead.
                    $form[] = array(
                        '#type' => 'item',
                        '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 5px;">',
                        '#suffix' => '</td>',
                    );

                }
 
                $form["description_$i"] = array(
                    '#type' => 'textfield',
                    '#size' => 60,
                    '#autocomplete_path' => 'autocomp/description',
                    '#default_value' => $description,
                    '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

                $form["course_$i"] = array (
                    '#type' => 'textfield',
                    '#size' => 15,
                    '#autocomplete_path' => 'autocomp/course',
                    '#default_value' => $course,
                    '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

                $form["note_$i"] = array (
                    '#type' => 'textfield',
                    '#size' => 35,
                    '#default_value' => $note,
                    '#prefix' => '<td style="padding:0px 5px 0px 5px;">',
                    '#suffix' => '</td>',
                );

                $form["code-filter_$i"] = array (
                    '#type' => 'textfield',
                    '#size' => 25,
                    '#default_value' => $code_filter,
                    '#autocomplete_path' => 'autocomp/multiplecodes',
                    '#prefix' => '<td style="padding:0px 5px 0px 5px;">',
                    '#suffix' => '</td></tr>',
                );

            }

            $i++;

        }

        // Close Table
        $form[] = array(
            '#type' => 'item',
            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr></table>'),
        );

    }

    return $form;

}


/**
 * Implementation of hook_validate().
 */
function daedalus_manage_programs_form_validate( $form, &$form_state ) {

    global $base_url;

    $page_url = $base_url."/".daedalus_get_setting("manage programs");

    switch($form_state['values']['op']) {

        case $form_state['values']['save-duplicate']:

            $program = $form_state['values']['program'];
            $effective_year = $form_state['values']['effective-year'];

            if( !$form_state['values']['program'] ) {
                form_set_error('program', t('The field <b>Program Name</b> is required.'));
            }

            if( !$form_state['values']['effective-year'] ) {
                form_set_error('effective-year', t('The field <b>Effective Year</b> is required.'));
            }

            if( !is_numeric($effective_year) ) {
                form_set_error('effective-year', t('The effective_year must be numeric.'));
            }

            if( strlen($effective_year) > 4 ) {
                form_set_error('effective-year', t('The effective_year must contain only four characters.'));
            }

            // Do not allow duplicate programs to be entered
            // according to the program name and year.
            if( db_result(db_query("SELECT COUNT(*) FROM {mag_program_identification} WHERE program='%s' AND year=%d", $program, $effective_year)) ) {
                form_set_error('effective-year', t('The program <b>'.$program.'</b> has already been created for the year <b>'.$effective_year.'</b>.'));
            }

            break;

        case $form_state['values']['save-changes']:

            if( !$form_state['values']['program'] ) {
                form_set_error('program', t('The field <b>Program Name</b> is required.'));
            }

            if( !$form_state['values']['effective-year'] ) {
                form_set_error('effective-year', t('The field <b>Effective Year</b> is required.'));
            }

            break;

    }
}


/**
 * Implementation of hook_submit().
 */
function daedalus_manage_programs_form_submit( $form, &$form_state ) {

    global $base_url;
    $page_url = daedalus_get_setting("manage programs");

    $program = $form_state['values']['program'];
    $effective_year = $form_state['values']['effective-year'];

    switch($form_state['values']['op']) {

        case $form_state['values']['submit-program']:
            drupal_goto($base_url."/".$page_url."/submit/".$program);
            break;

        case $form_state['values']['submit-year']:
            drupal_goto($base_url."/".$page_url."/modify/".$program."/".$effective_year);
            break;

        case $form_state['values']['duplicate-program']:
            drupal_goto($base_url."/".$page_url."/duplicate/".$program."/".$effective_year);
            break;

        case $form_state['values']['modify-program']:
            drupal_goto($base_url."/".$page_url."/modify/".$program."/".$effective_year);
            break;

        case $form_state['values']['submit-textfields']:
            drupal_goto($base_url."/".$page_url."/modify/".$program."/".$effective_year."/".$form_state['values']['textfields-total']);
            break;

        case $form_state['values']['delete-program']:
            drupal_goto($base_url."/".$page_url."/delete/".$program."/".$effective_year);
            break;

        case $form_state['values']['delete-reverse']:
            drupal_goto($base_url."/".$page_url."/modify/".$program."/".$effective_year);
            break;

        case $form_state['values']['delete-forward']:

            // Hidden program id value.
            $program_id = db_result(db_query("SELECT id FROM {mag_program_identification} WHERE program='%s' AND year=%d", $program, $effective_year));

            // Delete the program content.
            db_query("DELETE FROM {mag_program_content} WHERE program_id=%d", $program_id);

            // Delete the program
            db_query("DELETE FROM {mag_program_identification} WHERE id=%d", $program_id);

            drupal_set_message(t('The program <b>'.$program.'</b> for the year <b>'.$effective_year.'</b> has been deleted'));

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['cancel']:
            drupal_goto($base_url."/".$page_url);
            break;

        case $form_state['values']['cancel-duplicate']:
            drupal_goto($base_url."/".$page_url."/modify/".$program."/".$effective_year);
            break;

        case $form_state['values']['save-duplicate']:

            $program = $form_state['values']['program'];
            $effective_year = $form_state['values']['effective-year'];
            $textfields = $form_state['values']['textfields'];

            // The array to compare the first character
            // of the description and add the number to
            // the description order value.
            $char_array = array("a" => 974, "b" => 975, "c" => 976, "d" => 977, "e" => 978, "f" => 979, 
                                "g" => 980, "h" => 981, "i" => 982, "j" => 983, "k" => 984, "l" => 985, 
                                "m" => 986, "n" => 987, "o" => 988, "p" => 989, "q" => 990, "r" => 991, 
                                "s" => 992, "t" => 993, "u" => 994, "v" => 995, "w" => 996, "x" => 997, 
                                "y" => 998, "z" => 999 );

            // Enter the program information into
            // the program_identification database.
            db_query("INSERT INTO {mag_program_identification} (program, year) VALUES ('%s', %d)", $program, $effective_year);

            // Select the newly created program's id.
            $program_id = db_result(db_query("SELECT id FROM {mag_program_identification} WHERE program='%s' AND year=%d", $program, $effective_year));

            for( $i = 0; $i < $textfields; $i++ ) {

                $course = $form_state['values']["course_$i"];
                $course_id = $form_state['values']["course-id_$i"];
                $description = $form_state['values']["description_$i"];
                $note = $form_state['values']["note_$i"];
                $code_filter = $form_state['values']["code-filter_$i"];

                // Parsing the description and course and saving the courses in an incremental
                // course_order field will facilitate the correct ordering of the courses in
                // the program by their level, either 1000, 2000, 3000 or 4000. If the description
                // does not contain a number and there is no course present, the description is
                // set to 9999 to indicate the "other requirements" section of the program.

                // Insert the program requirement and program
                // content if a course is entered.
                if( $course ) {

                    // Determine the course level.
                    $course_info = explode(" ", $course);
                    $course_order = $course_info[1];

                    // Select the course name from the database to display as the
                    // description. Even if a new description is given for a course 
                    // disregard it and use the database entry for the course name.
                    $description = db_result(db_query("SELECT course_name FROM {dae_course} WHERE id=%d", $course_id));

                    if( !db_result(db_query("SELECT COUNT(*) FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter)) ) {

                        db_query("INSERT INTO {mag_program_requirement} (course_id, description, code_filter)
                                       VALUES (%d,'%s','%s')", $course_id, $description, $code_filter);

                        $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter));

                        db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                       VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $course_order, $note);

                    }

                    else {

                        // If the requirement has not changed no need
                        // to reselect the id from the database.
                        $requirement_id = $form_state['values']["requirement-id_$i"];

                        db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                       VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $course_order, $note);

                    }

                }

                // Insert the program requirement and program
                // content if there is no course entered.
                elseif( $description && !$course ) {

                    // Determine the course level from the description by matching
                    // the course number found in the description. If a number is 
                    // not found that matches 1000/2000/3000 or 4000 set the order
                    // value to 9999 to indicate an 'other' requirement.
                    if( preg_match('/(4)[0-9]{3}/', $description) ) {
                        
                        $description_order = 4;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    elseif( preg_match('/(3)[0-9]{3}/', $description) ) {
                        
                        $description_order = 3;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    elseif( preg_match('/(2)[0-9]{3}/', $description) ) {
                        
                        $description_order = 2;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    elseif( preg_match('/(1)[0-9]{3}/', $description) ) {
                        
                        $description_order = 1;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    else {
                        
                        $description_order = 9;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }

                    if( !db_result(db_query("SELECT COUNT(*) FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter)) ) {

                        db_query("INSERT INTO {mag_program_requirement} (course_id, description, code_filter)
                                       VALUES (%d,'%s','%s')", $course_id, $description, $code_filter);

                        $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter));

                        db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                       VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $description_order, $note);

                    }

                    else {

                        // If the requirement has not changed no need
                        // to reselect the id from the database.
                        $requirement_id = $form_state['values']["requirement-id_$i"];

                        db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                       VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $description_order, $note);

                    }

                }

            }

            drupal_set_message('Program has been duplicated.');

            drupal_goto($base_url."/".$page_url."/modify/".$program."/".$effective_year);

            break;

        case $form_state['values']['save-changes']:

            // Hidden textfield amount and program id.
            $textfields = $form_state['values']['textfields'];
            $program_id = $form_state['values']['program-id'];

            // The array to compare the first character
            // of the description and add the number to
            // the description order value.
            $char_array = array("a" => 974, "b" => 975, "c" => 976, "d" => 977, "e" => 978, "f" => 979, 
                                "g" => 980, "h" => 981, "i" => 982, "j" => 983, "k" => 984, "l" => 985, 
                                "m" => 986, "n" => 987, "o" => 988, "p" => 989, "q" => 990, "r" => 991, 
                                "s" => 992, "t" => 993, "u" => 994, "v" => 995, "w" => 996, "x" => 997, 
                                "y" => 998, "z" => 999 );

            // Determine the original amount of textfields required to display the
            // selected program. This will be compared against the hidden amount
            // to determine the location of any newly added lines to the program.
            $original_amount = db_result(db_query("SELECT COUNT(*) FROM {mag_program_content} WHERE program_id=%d", $program_id));

            // Update each line of the original program
            // excluding any of the additional requirements.
            for( $i = 0; $i < $original_amount; $i++ ) {

                $content_id = $form_state['values']["content-id_$i"];
                $course = $form_state['values']["course_$i"];
                $description = $form_state['values']["description_$i"];
                $note = $form_state['values']["note_$i"];
                $code_filter = $form_state['values']["code-filter_$i"];

                // Parsing the description and course and saving the courses in an incremental
                // course_order field will facilitate the correct ordering of the courses in
                // the program by their level, either 1000, 2000, 3000 or 4000. If the description
                // does not contain a number and there is no course present, the description is
                // set to 9999 to indicate the "other requirements" section of the program.
                //
                // Insert the program requirement and program
                // content if a course is entered.
                if( $course ) {

                    $course_id = get_course_id($course);

                    // Determine the course level.
                    $course_info = explode(" ", $course);
                    $course_order = $course_info[1];

                    // Select the course name from the database to display as the
                    // description. Even if a new description is given for a course 
                    // disregard it and use the database entry for the course name.
                    $description = db_result(db_query("SELECT course_name FROM {dae_course} WHERE id=%d", $course_id));

                    if( !db_result(db_query("SELECT COUNT(*) FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter)) ) {

                        db_query("INSERT INTO {mag_program_requirement} (course_id, description, code_filter)
                                       VALUES (%d,'%s','%s')", $course_id, $description, $code_filter);

                        $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter));

                        db_query("UPDATE {mag_program_content} SET requirement_id=%d, requirement_order=%d, note='%s' 
                                                             WHERE id=%d AND program_id=%d", $requirement_id, $course_order, $note, $content_id, $program_id);

                    }

                    else {

                        $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter));

                        db_query("UPDATE {mag_program_content} SET requirement_id=%d, requirement_order=%d, note='%s'
                                                             WHERE id=%d AND program_id=%d", $requirement_id, $course_order, $note, $content_id, $program_id);

                    }

                }

                // Insert the program requirement and program
                // content if there is no course entered.
                elseif( $description && !$course ) {

                    // Determine the course level from the description by matching
                    // the course number found in the description. If a number is 
                    // not found that matches 1000/2000/3000 or 4000 set the order
                    // value to 9999 to indicate an 'other' requirement.
                    if( preg_match('/(4)[0-9]{3}/', $description) ) {
                        
                        $description_order = 4;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    elseif( preg_match('/(3)[0-9]{3}/', $description) ) {
                        
                        $description_order = 3;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    elseif( preg_match('/(2)[0-9]{3}/', $description) ) {
                        
                        $description_order = 2;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    elseif( preg_match('/(1)[0-9]{3}/', $description) ) {
                        
                        $description_order = 1;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }
                    else {
                        
                        $description_order = 9;

                        $first_char = strtolower ( substr($description, 0, 1) );

                        $description_order .= $char_array[$first_char];

                    }

                    if( !db_result(db_query("SELECT COUNT(*) FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter)) ) {

                        db_query("INSERT INTO {mag_program_requirement} (description, code_filter)
                                       VALUES ('%s','%s')", $description, $code_filter);

                        $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter));

                        db_query("UPDATE {mag_program_content} SET requirement_id=%d, requirement_order=%d, note='%s'
                                                             WHERE id=%d AND program_id=%d", $requirement_id, $description_order, $note, $content_id, $program_id);

                    }

                    else {

                        $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter));

                        // The requirement has not changed
                        // only the content note has.
                        db_query("UPDATE {mag_program_content} SET requirement_id=%d, requirement_order=%d, note='%s'
                                                             WHERE id=%d AND program_id=%d", $requirement_id, $description_order, $note, $content_id, $program_id);

                    }

                }

            }

            // If the textfields are greater than the origianl program requirement
            // amount then additional requirements need to be inserted in the program.
            if( $textfields > $original_amount ) {

                for( $i = $original_amount; $i < $textfields; $i++ ) {

                    $course = $form_state['values']["course_$i"];
                    $description = $form_state['values']["description_$i"];
                    $note = $form_state['values']["note_$i"];
                    $code_filter = $form_state['values']["code-filter_$i"];

                    // Parsing the description and course and saving the courses in an incremental
                    // course_order field will facilitate the correct ordering of the courses in
                    // the program by their level, either 1000, 2000, 3000 or 4000. If the description
                    // does not contain a number and there is no course present, the description is
                    // set to 9999 to indicate the "other requirements" section of the program.
                    //
                    // Insert the program requirement and program
                    // content if a course is entered.
                    if( $course ) {

                        // If there is a course entered into the
                        // additional fields you must select the
                        // id because it's not included.
                        $course_id = get_course_id($course);

                        // Determine the course level.
                        $course_info = explode(" ", $course);
                        $course_order = $course_info[1];

                        // Select the course name from the database to display as the
                        // description. Even if a new description is given for a course 
                        // disregard it and use the database entry for the course name.
                        $description = db_result(db_query("SELECT course_name FROM {dae_course} WHERE id=%d", $course_id));

                        if( !db_result(db_query("SELECT COUNT(*) FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter)) ) {

                            db_query("INSERT INTO {mag_program_requirement} (course_id, description, code_filter)
                                           VALUES (%d,'%s','%s')", $course_id, $description, $code_filter);

                            $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter));

                            db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                           VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $course_order, $note);

                        }

                        else {

                            // New program requirements require the selection
                            // of the requirement id from the database.
                            $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE course_id=%d AND description='%s' AND code_filter='%s'", $course_id, $description, $code_filter));

                            db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                           VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $course_order, $note);

                        }

                    }

                    // Insert the program requirement and program
                    // content if there is no course entered.
                    elseif( $description && !$course ) {

                        // Determine the course level from the description by matching
                        // the course number found in the description. If a number is 
                        // not found that matches 1000/2000/3000 or 4000 set the order
                        // value to 9999 to indicate an 'other' requirement.
                        if( preg_match('/(4)[0-9]{3}/', $description) ) {

                            $description_order = 4;

                            $first_char = strtolower ( substr($description, 0, 1) );

                            $description_order .= $char_array[$first_char];

                        }
                        elseif( preg_match('/(3)[0-9]{3}/', $description) ) {

                            $description_order = 3;

                            $first_char = strtolower ( substr($description, 0, 1) );

                            $description_order .= $char_array[$first_char];

                        }
                        elseif( preg_match('/(2)[0-9]{3}/', $description) ) {

                            $description_order = 2;

                            $first_char = strtolower ( substr($description, 0, 1) );

                            $description_order .= $char_array[$first_char];

                        }
                        elseif( preg_match('/(1)[0-9]{3}/', $description) ) {

                            $description_order = 1;

                            $first_char = strtolower ( substr($description, 0, 1) );

                            $description_order .= $char_array[$first_char];

                        }
                        else {

                            $description_order = 9;

                            $first_char = strtolower ( substr($description, 0, 1) );

                            $description_order .= $char_array[$first_char];

                        }

                        if( !db_result(db_query("SELECT COUNT(*) FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter)) ) {

                            db_query("INSERT INTO {mag_program_requirement} (description, code_filter)
                                           VALUES ('%s','%s')", $description, $code_filter);

                            $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter));

                            db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                           VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $description_order, $note);

                        }

                        else {

                            // New program requirements require the selection
                            // of the requirement id from the database.
                            $requirement_id = db_result(db_query("SELECT id FROM {mag_program_requirement} WHERE description='%s' AND code_filter='%s'", $description, $code_filter));

                            db_query("INSERT INTO {mag_program_content} (program_id, requirement_id, requirement_order, note)
                                           VALUES (%d, %d, %d, '%s')", $program_id, $requirement_id, $description_order, $note);

                        }

                    }

                }

            }

            drupal_set_message('Program requirement changes have been saved.');

            drupal_goto($base_url."/".$page_url."/modify/".$program."/".$effective_year);

            break;

        case $form_state['values']['delete-selected']:

            // Hidden textfield amount.
            $textfields = $form_state['values']['textfields'];

            // If checked delete the requirement.
            for( $i = 0; $i < $textfields; $i++ ) {

                if( $form_state['values']["delete_$i"] ) {

                    db_query("DELETE FROM {mag_program_content} WHERE id=%d", $form_state['values']["content-id_$i"]);

                    $delete_flag++;

                }

            }

            if( $delete_flag ) {
                drupal_set_message('Program requirements have been deleted.');
            }else {
                drupal_set_message('No program requirements were selected for deletion.');
            }

            drupal_goto($base_url."/".$page_url."/modify/".$program."/".$effective_year);

            break;

        case $form_state['values']['dae-help-submit']:

            $page_url_length = sizeof(explode('/',$page_url));

            $param[0] = arg(0+$page_url_length);
            $param[1] = arg(1+$page_url_length);
            $param[2] = arg(2+$page_url_length);

            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message(t('Help information saved.'));

                if( $param[0] && $param[1] && $param[2] ) {
                    drupal_goto($base_url."/".$page_url."/".$param[0]."/".$param[1]."/".$param[2]);
                }

                elseif( $param[0] && $param[1] ) {
                    drupal_goto($base_url."/".$page_url."/".$param[0]."/".$param[1]);
                }

                elseif( $param[0]) {
                    drupal_goto($base_url."/".$page_url."/".$param[0]);
                }

                else {
                    drupal_goto($base_url."/".$page_url);
                }

            }

            break;

    }

}


//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Manage->Goals Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_manage_goals() {
    return drupal_get_form("daedalus_manage_goals_form");
};

/**
* Menu Location: Daedalus -> Manage -> Goals
* URL Location:  daedalus/daedalus/manage/goals
*
* Displays
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_manage_goals_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Get current page url.
    $page_url = $help_url = daedalus_get_setting("manage goals");

    $page_url_length = sizeof(explode('/',$page_url));
    $page_url = $base_url."/".$page_url;

    // Store URL Parameters in $param array
    $param = array();
    $param[0] = arg(0+$page_url_length);    //
    $param[1] = arg(1+$page_url_length);    //
    $param[2] = arg(2+$page_url_length);    //
    $param[3] = arg(3+$page_url_length);    // Additional Textfields Total

    $label_id  = $param[0];

    if( $param[1] == "alpha" ) {
        $sort_type = $param[1];
    }
    elseif( $param[3] == "alpha" ) {
        $sort_type = $param[3];
    }

    if( $param[1] == "selected" ) {
        $state = $param[1];
    }
    
    $tags = $param[2];

    $form = array();

    $question_img_src = $base_url."/".daedalus_get_setting("question mark");

    // The title is set with an 'a' tag with the class='show-help'. When the image is clicked the help section is shown.
    drupal_set_title("Manage Goal <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>");

    $form = array();

    // Add the hidden help form. Paramaters are
    // (help url, show border, show break).
    $form = display_hidden_form($help_url, 1, 1);

    // The JavaScript directs the browser here
    // when a labeled SLO is selected for deletion.
    if( $param[1] == "delete_slo" ) {
        db_query("DELETE FROM {mag_goal_sets} WHERE label_id=%d AND slo_id=%d", $label_id, $param[2]);
    }

    // The JavaScript directs the browser here
    // when a label is selected for deletion.
    elseif( $param[0] == "delete_label") {

        // Delete the goal label but leave the corresponding goal set intact.
        // This is due to the selection of the set when a user that has selected
        // the deleted goal label views their goal. The label id will still be active.
        db_query("DELETE FROM {mag_goal_label} WHERE id=%d", $param[1]);
    }

    // Display the Tag Cloud to add enable the adding of
    // SLOs to the selected goal label by its id number.
    elseif( is_numeric($label_id) ) {

        $goal_label = db_result(db_query("SELECT goal_label FROM {mag_goal_label} WHERE id=%d", $label_id));

        $question_img_src = $base_url."/".daedalus_get_setting("question mark");

        // The title is set with an 'a' tag with the class='show-help'. When the image is clicked the help section is shown.
        drupal_set_title("Manage Goal Label \"".$goal_label."\" <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>");

        // Create the list of SLO's added to the selected label.
        $result = db_query("SELECT slo_id FROM {mag_goal_sets} WHERE label_id=%d", $label_id);
        while( $row = db_fetch_array($result) ) {
            $goal_slos[] = $row['slo_id'];

            $goal_text = db_result(db_query("SELECT slo_text FROM {dae_slo} WHERE id=%d ORDER BY slo_text", $row['slo_id']));

            $message = "Are you sure you want to remove the student learning outcome \"".$goal_text."\"?";
            $del_url = $page_url."/".$label_id."/delete_slo/".$row['slo_id'];
            $delete  = "<a href='".$del_url."' class=removable message='".$message."'><strong>delete</strong></a>";

            $goal_list .= "<li>".$goal_text." ".$delete."</li>";
        }

        if( $goal_slos ) {

            // Create the list of placeholders to accommodate a variable number of arguments.
            $placeholders = implode(' AND ', array_fill(0, count($goal_slos), "slo_id != %d"));

            // Add the tag identification...
            $placeholders = $placeholders . " AND tag_id=%d";
        }

        // Maximum font size from dbase settings, appends extra <big> tags.
        $max_font_size = daedalus_get_setting("tag cloud max font size");

        // Apply the css to change the line height of the tag cloud
        $css = '<style type="text/css">
                    p.cloud{
                        line-height: '.daedalus_get_setting("tag cloud height percent").'%;
                        border-style:solid;
                        border-width:3px;
                        border-color:#C0C0C0;
                        padding-top:20px;
                        padding-bottom:15px;
                        padding-left:5px;
                        padding-right:5px;
                    }
                </style>';

        drupal_set_html_head($css);

        // If a tag is not selected display the entire tag cloud
        if( $state != "selected" && $state != "delete" ) {

            $i = 0;
            $min = $max = -1;
            $result = db_query("SELECT * FROM {dae_tag} ORDER BY tag_label");

            // Create the tag array
            while( $row = db_fetch_array($result) ) {

                // Do not count tags that have
                // been added to the goal set.
                if( $goal_slos && $placeholders ) {

                    $query_values = $goal_slos;
                    array_push($query_values, $row['id']);

                    $tag_count = db_result(db_query("SELECT COUNT(*) FROM {dae_slo_tag} WHERE $placeholders", $query_values));

                }else {
                    $tag_count = db_result(db_query("SELECT COUNT(*) FROM {dae_slo_tag} WHERE tag_id=%d", $row['id']));
                }

                $tag_array[$i]["tag_id"] = $row['id'];
                $tag_array[$i]["label"]  = $row['tag_label'];
                $tag_array[$i]["count"]  = $tag_count;
                $i++;

                // Store the min and max tag count to
                // help calculate the tag cloud text size.
                if( $min == -1 ) {
                    $min = $max = $tag_count;
                }elseif( $tag_count < $min && $tag_count != 0 ) {
                    $min = $tag_count;
                }elseif( $tag_count > $max ) {
                    $max = $tag_count;
                }
            }

            if( $tag_array ) {
                foreach($tag_array as $value ) {

                    // Make sure we don't divide by 0
                    if( $max == $min && $max > 1 ) {
                        $min = $max-1;
                    }elseif($max == $min && $max == 1 ) {
                        $max++;
                    }

                    // Log scale formula found online
                    $weight = (log($value["count"])-log($min))/(log($max)-log($min));

                    switch( $weight ) {
                        case ($weight == 0):
                            $size = 1;
                            break;
                        case ($weight > 0 && $weight <= 0.4):
                            $size = 2;
                            break;
                        case ($weight > 0.4 && $weight <= 0.6):
                            $size = 3;
                            break;
                        case ($weight > 0.6 && $weight <= 0.8):
                            $size = 4;
                            break;
                        case ($weight > 0.8 && $weight <= 1.0):
                            $size = 5;
                            break;
                    }

                    $size += $max_font_size;

                    // Make the font bigger for the right number of times
                    $big_open = $big_end = "";
                    for( $i = 0; $i < $size; $i++ ) {
                            $big_open .= "<big>";
                            $big_end .= "</big>";
                    }

                    // Show the number of times the SLO has been tagged.
                    $par_count = "<small><small>(".$value['count'].")</small></small>";

                    if( $value['count'] > 0 ) {

                        if( $slo_id == "alpha" ) {
                            $cloud_string .= $big_open."<a href='".$page_url."/".$label_id."/selected/".$value['tag_id']."/".$slo_id."' ><b>#".$value["label"]."</b></a>".$big_end."".$par_count." &nbsp;";
                        }else {
                            $cloud_string .= $big_open."<a href='".$page_url."/".$label_id."/selected/".$value['tag_id']."' ><b>#".$value["label"]."</b></a>".$big_end."".$par_count." &nbsp;";
                        }
                    }
                }
            }
        }else {       // Else a tag has been selected so narrow the tags in the
                     // cloud tag accoring to the select tag. tag tag tag

            if( !strrpos($tags,"_") ) {      // If the tags are not delimited by an underscore
                                            // select the results with only the singel tag.

                // Create the selected tag string, selecting this tag
                // will return to the full list of learning outcomes.
                $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags));

                $selected_tags = "<a href=\"".$page_url."/".$label_id."/".$sort_type."\">#".$tag_label."</a>";

                // Get all the slo id's.
                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d ORDER BY slo_id", $tags);
                while( $row = db_fetch_array($result) ) {
                    $slo_array[] = $row['slo_id'];
                }

                // Create the list of placeholders to accommodate a variable number of arguments.
                $slo_placeholders = implode(' OR ', array_fill(0, count($slo_array), "slo_id=%d"));

                $slo_query = "SELECT DISTINCT tag_id FROM {dae_slo_tag} WHERE ".$slo_placeholders." ORDER BY tag_id";

                $result = db_query($slo_query, $slo_array);

                while( $row = db_fetch_array($result) ) {
                    if($row['tag_id'] != $tags ) {
                        $result_tag[] = $row['tag_id'];
                    }
                }

                if( $result_tag ) {

                    foreach($result_tag as $current_tag) {

                        $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $current_tag));

                        $slo_array1 = $slo_array2 = array();

                        // The next group of loops calculate the total number of SLO's associated
                        // with the the current tag being computed for the tag cloud.
                        //
                        // Get the slo ids using the URL's tag, removing any goal slos present
                        if( $goal_slos && $placeholders ) {

                            $query_values = $goal_slos;
                            array_push($query_values, $tags);

                            $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                        }else {
                            $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags);
                        }

                        while( $row = db_fetch_array($result) ) {
                            $slo_array1[] = $row['slo_id'];
                        }

                        // Get the slo ids using the current tag
                        if( $goal_slos && $placeholders ) {

                            $query_values = $goal_slos;
                            array_push($query_values, $current_tag);

                            $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                        }else {
                            $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $current_tag);
                        }

                        while( $row = db_fetch_array($result) ) {
                            $slo_array2[] = $row['slo_id'];
                        }

                        // Combine each lists make sure there are no duplicate
                        // entries and the number of matching items is the count.
                        $temp_slos = array_intersect($slo_array1, $slo_array2);
                        $temp_slos = array_unique($temp_slos);
                        $tag_count = count($temp_slos);

                        $tag_array[$current_tag]["tag_id"] = $current_tag;
                        $tag_array[$current_tag]["count"]  = $tag_count;
                        $tag_array[$current_tag]["label"]  = $tag_label;

                        // Store the min and max tag count to
                        // help calculate the tag cloud text size.
                        if( !$min ) {
                            $min = $max = $tag_count;
                        }elseif( $tag_count < $min ) {
                            $min = $tag_count;
                        }else if( $tag_count > $max ) {
                            $max = $tag_count;
                        }
                    }

                    // Create the tag cloud
                    if( $tag_array ) {

                        foreach($tag_array as $value ) {

                            // Make sure we don't divide by 0
                            if( $max == $min && $max > 1 ) {
                                $min = $max-1;
                            }elseif($max == $min && $max == 1 ) {
                                $max++;
                            }

                            $weight = (log($value["count"])-log($min))/(log($max)-log($min));

                            switch( $weight ) {
                                case ($weight == 0):
                                    $size = 1;
                                    break;
                                case ($weight > 0 && $weight <= 0.4):
                                    $size = 2;
                                    break;
                                case ($weight > 0.4 && $weight <= 0.6):
                                    $size = 3;
                                    break;
                                case ($weight > 0.6 && $weight <= 0.8):
                                    $size = 4;
                                    break;
                                case ($weight > 0.8 && $weight <= 1.0):
                                    $size = 5;
                                    break;
                            }

                            $size += $max_font_size;

                            // Increment the tag size by
                            // adding multiple "BIG" tags
                            $big_open = $big_end = "";
                            for($i = 0; $i < $size; $i++) {
                                    $big_open .= "<big>";
                                    $big_end .= "</big>";
                            }

                            // Show the number of times the SLO has been tagged.
                            $par_count = "<small><small>(".$value['count'].")</small></small>";

                            if( $value['count'] > 0 ) {
                                $cloud_string .= $big_open."<a href=\"".$page_url."/".$label_id."/selected/".$tags."_".$value['tag_id']."/".$sort_type."\" ><b>#".$value["label"]."</b></a>".$big_end."".$par_count." &nbsp;";
                            }
                        }
                    }
                }
            } // if( !strrpos($tags,"_") )
            else {
                // Create the selected tag string with a twist. When two tags have been selected
                // the tag cloud is not required. When creating the selected tags string instead
                // of deselecting the tag deselect only the link that is clicked.
                $tags = explode("_", $tags);

                if( count($tags) == 2 ) {

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[0]));
                    $selected_tags = "<a href=\"".$page_url."/selected/".$tags[1]."/".$sort_type."\">#".$tag_label."</a>";

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[1]));
                    $selected_tags .= "&nbsp;&nbsp;<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."/".$sort_type."\">#".$tag_label."</a>";

                    $selected_tags .= "<br /><b><a href=\"".$page_url."/".$label_id."/".$sort_type."\">deselect all</a></b>";

                    $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d OR tag_id=%d ORDER BY slo_id", $tags[0], $tags[1]);

                    while( $row = db_fetch_array($result) ) {
                        $slo_array[] = $row['slo_id'];
                    }

                    // Create the list of placeholders to accommodate a variable number of arguments.
                    $slo_placeholders = implode(' OR ', array_fill(0, count($slo_array), "slo_id=%d"));

                    $slo_query = "SELECT DISTINCT tag_id FROM {dae_slo_tag} WHERE ".$slo_placeholders." ORDER BY tag_id";

                    $result = db_query($slo_query, $slo_array);

                    while( $row = db_fetch_array($result) ) {
                        if( $row['tag_id'] != $tags[0] && $row['tag_id'] != $tags[1] ) {
                            $result_tag[] = $row['tag_id'];
                        }
                    }

                    if($result_tag) {

                        foreach($result_tag as $current_tag) {

                            $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $current_tag));

                            $slo_array1 = $slo_array2 = $slo_array3 = array();

                            // The next group of loops calculate the total number of SLO's associated
                            // with the the current tag being computed for the tag cloud.
                            //
                            // Get the slo ids using the URL's tag, removing any goal slos present
                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $tags[0]);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[0]);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array1[] = $row['slo_id'];
                            }

                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $tags[1]);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[1]);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array2[] = $row['slo_id'];
                            }

                            // Get the slo ids using the current tag, removing any goal slos present
                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $current_tag);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $current_tag);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array3[] = $row['slo_id'];
                            }

                            // Combine each lists make sure there are no duplicate
                            // entries and the number of matching items is the count.
                            $temp_slos = array_intersect($slo_array1, $slo_array2);
                            $temp_slos = array_intersect($slo_array3, $temp_slos);
                            $temp_slos = array_unique($temp_slos);
                            $tag_count = count($temp_slos);

                            if( $tag_count > 0 ) {
                                $tag_array[$current_tag]["tag_id"] = $current_tag;
                                $tag_array[$current_tag]["count"]  = $tag_count;
                                $tag_array[$current_tag]["label"]  = $tag_label;

                                // Store the min and max tag count to
                                // help calculate the tag cloud text size.
                                if( !$min ) {
                                    $min = $max = $tag_count;
                                }elseif( $tag_count < $min ) {
                                    $min = $tag_count;
                                }else if( $tag_count > $max ) {
                                    $max = $tag_count;
                                }
                            }
                        }

                        // Create the tag cloud
                        if( $tag_array ) {

                            foreach($tag_array as $value ) {

                                // Make sure we don't divide by 0
                                if( $max == $min && $max > 1 ) {
                                    $min = $max-1;
                                }elseif($max == $min && $max == 1 ) {
                                    $max++;
                                }

                                $weight = (log($value["count"])-log($min))/(log($max)-log($min));

                                switch( $weight ) {
                                    case ($weight == 0):
                                        $size = 1;
                                        break;
                                    case ($weight > 0 && $weight <= 0.4):
                                        $size = 2;
                                        break;
                                    case ($weight > 0.4 && $weight <= 0.6):
                                        $size = 3;
                                        break;
                                    case ($weight > 0.6 && $weight <= 0.8):
                                        $size = 4;
                                        break;
                                    case ($weight > 0.8 && $weight <= 1.0):
                                        $size = 5;
                                        break;
                                }

                                $size += $max_font_size;

                                // Increment the tag size by
                                // adding multiple "BIG" tags
                                $big_open = $big_end = "";
                                for($i = 0; $i < $size; $i++) {
                                        $big_open .= "<big>";
                                        $big_end .= "</big>";
                                }

                                // Show the number of times the SLO has been tagged.
                                $par_count = "<small><small>(".$value['count'].")</small></small>";

                                if( $value['count'] > 0 ) {
                                    $cloud_string .= $big_open."<a href='".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[1]."_".$value['tag_id']."/".$sort_type."' ><b>#".$value["label"]."</b></a>".$big_end."".$par_count." &nbsp;";
                                }
                            }
                        }
                    }
                }elseif( count($tags) == 3 ) {

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[0]));
                    $selected_tags = "<a href=\"".$page_url."/".$label_id."/selected/".$tags[1]."_".$tags[2]."/".$sort_type."\">#".$tag_label."</a>";

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[1]));
                    $selected_tags .= "&nbsp;&nbsp;<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[2]."/".$sort_type."\">#".$tag_label."</a>";

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[2]));
                    $selected_tags .= "&nbsp;&nbsp;<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[1]."/".$sort_type."\">#".$tag_label."</a>";

                    $selected_tags .= "<br /><b><a href=\"".$page_url."/".$label_id."/".$sort_type."\">deselect all</a></b>";

                    $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d OR tag_id=%d OR tag_id=%d ORDER BY slo_id", $tags[0], $tags[1], $tags[2]);
                    while( $row = db_fetch_array($result) ) {
                        $slo_array[] = $row['slo_id'];
                    }

                    // Create the list of placeholders to accommodate a variable number of arguments.
                    $slo_placeholders = implode(' OR ', array_fill(0, count($slo_array), "slo_id=%d"));

                    $slo_query = "SELECT DISTINCT tag_id FROM {dae_slo_tag} WHERE ".$slo_placeholders." ORDER BY tag_id";

                    $result = db_query($slo_query, $slo_array);

                    while( $row = db_fetch_array($result) ) {
                        if( $row['tag_id'] != $tags[0] && $row['tag_id'] != $tags[1] && $row['tag_id'] != $tags[2] ) {
                            $result_tag[] = $row['tag_id'];
                        }
                    }

                    if( $result_tag ) {

                        foreach($result_tag as $current_tag) {

                            $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $current_tag));

                            $slo_array1 = $slo_array2 = $slo_array3 = $slo_array4 = array();

                            // The next group of loops calculate the total number of SLO's associated
                            // with the the current tag being computed for the tag cloud.
                            //
                            // Get the slo ids using the URL's tag, removing any goal slos present
                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $tags[0]);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[0]);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array1[] = $row['slo_id'];
                            }

                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $tags[1]);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[1]);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array2[] = $row['slo_id'];
                            }

                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $tags[2]);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[2]);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array3[] = $row['slo_id'];
                            }

                            // Get the slo ids using the current tag
                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $current_tag);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $current_tag);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array4[] = $row['slo_id'];
                            }

                            // Combine each lists make sure there are no duplicate
                            // entries and the number of matching items is the count.
                            $temp_slos = array_intersect($slo_array1, $slo_array2);
                            $temp_slos = array_intersect($slo_array3, $temp_slos);
                            $temp_slos = array_intersect($slo_array4, $temp_slos);
                            $temp_slos = array_unique($temp_slos);
                            $tag_count = count($temp_slos);

                            if( $tag_count > 0 ) {

                                $tag_array[$current_tag]["tag_id"] = $current_tag;
                                $tag_array[$current_tag]["count"]  = $tag_count;
                                $tag_array[$current_tag]["label"]  = $tag_label;

                                // Store the min and max tag count to
                                // help calculate the tag cloud text size.
                                if( !$min ) {
                                    $min = $max = $tag_count;
                                }elseif( $tag_count < $min ) {
                                    $min = $tag_count;
                                }else if( $tag_count > $max ) {
                                    $max = $tag_count;
                                }
                            }
                        }

                        // Create the tag cloud
                        if( $tag_array ) {

                            foreach($tag_array as $value ) {

                                // Make sure we don't divide by 0
                                if( $max == $min && $max > 1 ) {
                                    $min = $max-1;
                                }elseif($max == $min && $max == 1 ) {
                                    $max++;
                                }

                                $weight = (log($value["count"])-log($min))/(log($max)-log($min));

                                switch( $weight ) {
                                    case ($weight == 0):
                                        $size = 1;
                                        break;
                                    case ($weight > 0 && $weight <= 0.4):
                                        $size = 2;
                                        break;
                                    case ($weight > 0.4 && $weight <= 0.6):
                                        $size = 3;
                                        break;
                                    case ($weight > 0.6 && $weight <= 0.8):
                                        $size = 4;
                                        break;
                                    case ($weight > 0.8 && $weight <= 1.0):
                                        $size = 5;
                                        break;
                                }

                                $size += $max_font_size;

                                // Increment the tag size by
                                // adding multiple "BIG" tags
                                $big_open = $big_end = "";
                                for($i = 0; $i < $size; $i++) {
                                        $big_open .= "<big>";
                                        $big_end .= "</big>";
                                }

                                // Show the number of times the SLO has been tagged.
                                $par_count = "<small><small>(".$value['count'].")</small></small>";

                                if( $value['count'] > 0 ) {
                                    $cloud_string .= $big_open."<a href='".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[1]."_".$tags[2]."_".$value['tag_id']."/".$sort_type."' ><b>#".$value["label"]."</b></a>".$big_end."".$par_count." &nbsp;";
                                }
                            }
                        }
                    }

                }elseif( count($tags) == 4 ) {

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[0]));
                    $selected_tags = "<a href=\"".$page_url."/".$label_id."/selected/".$tags[1]."_".$tags[2]."_".$tags[3]."/".$sort_type."\">#".$tag_label."</a>";

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[1]));
                    $selected_tags .= "&nbsp;&nbsp;<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[2]."_".$tags[3]."/".$sort_type."\">#".$tag_label."</a>";

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[2]));
                    $selected_tags .= "&nbsp;&nbsp;<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[1]."_".$tags[3]."/".$sort_type."\">#".$tag_label."</a>";

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[3]));
                    $selected_tags .= "&nbsp;&nbsp;<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[1]."_".$tags[2]."/".$sort_type."\">#".$tag_label."</a>";

                    $selected_tags .= "<br /><b><a href=\"".$page_url."/".$label_id."/".$sort_type."\">deselect all</a></b>";

                    $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d OR tag_id=%d OR tag_id=%d OR tag_id=%d ORDER BY slo_id", $tags[0], $tags[1], $tags[2], $tags[3]);
                    while( $row = db_fetch_array($result) ) {
                        $slo_array[] = $row['slo_id'];
                    }

                    // Create the list of placeholders to accommodate a variable number of arguments.
                    $slo_placeholders = implode(' OR ', array_fill(0, count($slo_array), "slo_id=%d"));

                    $slo_query = "SELECT DISTINCT tag_id FROM {dae_slo_tag} WHERE ".$slo_placeholders." ORDER BY tag_id";

                    $result = db_query($slo_query, $slo_array);

                    while( $row = db_fetch_array($result) ) {

                        if( $row['tag_id'] != $tags[0] && $row['tag_id'] != $tags[1] && $row['tag_id'] != $tags[2] && $row['tag_id'] != $tags[3] ) {
                            $result_tag[] = $row['tag_id'];
                        }

                    }

                    if( $result_tag ) {

                        foreach($result_tag as $current_tag) {

                            $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $current_tag));

                            $slo_array1 = $slo_array2 = $slo_array3 = $slo_array4 = $slo_array5 = array();

                            // The next group of loops calculate the total number of SLO's associated
                            // with the the current tag being computed for the tag cloud.
                            //
                            // Get the slo ids using the URL's tag, removing any goal slos present
                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $tags[0]);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[0]);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array1[] = $row['slo_id'];
                            }

                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $tags[1]);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[1]);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array2[] = $row['slo_id'];
                            }

                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $tags[2]);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[2]);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array3[] = $row['slo_id'];
                            }

                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $tags[3]);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[3]);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array4[] = $row['slo_id'];
                            }

                            // Get the slo ids using the current tag
                            if( $goal_slos && $placeholders ) {

                                $query_values = $goal_slos;
                                array_push($query_values, $current_tag);

                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE $placeholders", $query_values);

                            }else {
                                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $current_tag);
                            }

                            while( $row = db_fetch_array($result) ) {
                                $slo_array5[] = $row['slo_id'];
                            }

                            // Combine each lists make sure there are no duplicate
                            // entries and the number of matching items is the count.
                            $temp_slos = array_intersect($slo_array1, $slo_array2);
                            $temp_slos = array_intersect($slo_array3, $temp_slos);
                            $temp_slos = array_intersect($slo_array4, $temp_slos);
                            $temp_slos = array_intersect($slo_array5, $temp_slos);
                            $temp_slos = array_unique($temp_slos);
                            $tag_count = count($temp_slos);

                            if( $tag_count > 0 ) {
                                $tag_array[$current_tag]["tag_id"] = $current_tag;
                                $tag_array[$current_tag]["count"]  = $tag_count;
                                $tag_array[$current_tag]["label"]  = $tag_label;

                                // Store the min and max tag count to
                                // help calculate the tag cloud text size.
                                if( !$min ) {
                                    $min = $max = $tag_count;
                                }elseif( $tag_count < $min ) {
                                    $min = $tag_count;
                                }else if( $tag_count > $max ) {
                                    $max = $tag_count;
                                }
                            }
                        }

                        // Create the tag cloud
                        if( $tag_array ) {

                            foreach($tag_array as $value ) {

                                // Make sure we don't divide by 0
                                if( $max == $min && $max > 1 ) {
                                    $min = $max-1;
                                }elseif($max == $min && $max == 1 ) {
                                    $max++;
                                }

                                $weight = (log($value["count"])-log($min))/(log($max)-log($min));
                                if( $value["count"] != 0 ) {
                                    $size = round( ($max_font_size * $weight), 1 );      // round to one decimal
                                }else {
                                    $size = -1;
                                }

                                // Increment the tag size by
                                // adding multiple "BIG" tags
                                $big_open = $big_end = "";
                                for($i = 0; $i < $size; $i++) {
                                        $big_open .= "<big>";
                                        $big_end .= "</big>";
                                }

                                if( $value['count'] > 0 ) {
                                    $cloud_string .= "&nbsp;".$big_open."<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[1]."_".$tags[2]."_".$tags[3]."_".$value['tag_id']."/".$sort_type."\" ><b>#".$value["label"]."</></a>".$big_end." <small>(".$value['count'].")</small>&nbsp;";
                                }
                            }
                        }
                    }

                }elseif( count($tags) == 5 ) {

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[0]));
                    $selected_tags = "<a href=\"".$page_url."/".$label_id."/selected/".$tags[1]."_".$tags[2]."_".$tags[3]."_".$tags[4]."/".$sort_type."\">#".$tag_label."</a>";

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[1]));
                    $selected_tags .= "&nbsp;&nbsp;<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[2]."_".$tags[3]."_".$tags[4]."/".$sort_type."\">#".$tag_label."</a>";

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[2]));
                    $selected_tags .= "&nbsp;&nbsp;<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[1]."_".$tags[3]."_".$tags[4]."/".$sort_type."\">#".$tag_label."</a>";

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[3]));
                    $selected_tags .= "&nbsp;&nbsp;<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[1]."_".$tags[2]."_".$tags[4]."/".$sort_type."\">#".$tag_label."</a>";

                    $tag_label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tags[4]));
                    $selected_tags .= "&nbsp;&nbsp;<a href=\"".$page_url."/".$label_id."/selected/".$tags[0]."_".$tags[1]."_".$tags[2]."_".$tags[3]."/".$sort_type."\">#".$tag_label."</a>";

                    $selected_tags .= "<br /><b><a href=\"".$page_url."/".$label_id."/".$sort_type."\">deselect all</a></b>";

                }
            }
        } // end else

        if( $cloud_string ) {
            $cloud_string = "<p class='cloud'>".$cloud_string."</p>";
        }else {
            $cloud_string = "<ul><li><i>No matches found</i></li></ul>";
        }

        $form['goal-label'] = array(
            '#type'  => 'textfield',
            '#title' => t('Goal Label'),
            '#default_value' => t($goal_label),
        );

        if( $goal_list ) {

            $form[] = array(
                '#type' => 'item',
                '#title' => 'Selected Learning Outcomes',
                '#value' => "<ol>".$goal_list."</ol>",
            );

        }else {

            $form[] = array(
                '#type' => 'item',
                '#title' => 'Selected Learning Outcomes',
                '#value' => "<ul><li>No matches found</li></ul>",
            );

        }

        // Some debugging information for adjusting the appearance of
        // the tag cloud form of buttons. No setting, just change it
        // to true when you want to change the database values based
        // on visual cues instead of guessing the numbers.
        if( daedalus_get_setting("show tag cloud settings") ) {

            $form['increase-percent'] = array(
                '#type' => 'submit',
                '#value' => "Increase spacing",
            );
            $form['decrease-percent'] = array(
                '#type' => 'submit',
                '#value' => "Decrease spacing",

            );
            $form['increase-max'] = array(
                '#type' => 'submit',
                '#value' => "Increase font",
            );
            $form['decrease-max'] = array(
                '#type' => 'submit',
                '#value' => "Decrease font",
            );

        }

        $form['tags'] = array(
            '#title' => t('Tags to narrow search <small>(<i>add multiple student learning outcomes below</i>)</small>'),
            '#value' => t($cloud_string),
            '#type'  => 'item',
        );

        if( $state == "selected" ) {

            $form['selected'] = array(
                '#title' => t('Selected tags'),
                '#value' => t($selected_tags),
                '#type'  => 'item',
            );

        }

        if( $tags ) {

            if( is_array($tags) ) {

                // Get the slo ids using the 1st tag
                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[0]);
                while( $row = db_fetch_array($result) ) {
                    $slo_array1[] = $row['slo_id'];
                }

                // Get the slo ids using the 2nd tag
                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[1]);
                while( $row = db_fetch_array($result) ) {
                    $slo_array2[] = $row['slo_id'];
                }

                // Get the slo ids using the 3nd tag
                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[2]);
                while( $row = db_fetch_array($result) ) {
                    $slo_array3[] = $row['slo_id'];
                }

                // Get the slo ids using the 4nd tag
                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[3]);
                while( $row = db_fetch_array($result) ) {
                    $slo_array4[] = $row['slo_id'];
                }

                // Get the slo ids using the 4nd tag
                $result = db_query("SELECT slo_id FROM {dae_slo_tag} WHERE tag_id=%d", $tags[4]);
                while( $row = db_fetch_array($result) ) {
                    $slo_array5[] = $row['slo_id'];
                }

                // Combine the arrays according to the amount of tags selected
                if( count($tags) == 2 ) {
                    $slo_array = array_intersect($slo_array1, $slo_array2);
                }elseif( count($tags) == 3 ) {
                    $slo_array = array_intersect($slo_array1, $slo_array2);
                    $slo_array = array_intersect($slo_array3, $slo_array);
                }elseif( count($tags) == 4 ) {
                    $slo_array = array_intersect($slo_array1, $slo_array2);
                    $slo_array = array_intersect($slo_array3, $slo_array);
                    $slo_array = array_intersect($slo_array4, $slo_array);
                }elseif( count($tags) == 5) {
                    $slo_array = array_intersect($slo_array1, $slo_array2);
                    $slo_array = array_intersect($slo_array3, $slo_array);
                    $slo_array = array_intersect($slo_array4, $slo_array);
                    $slo_array = array_intersect($slo_array5, $slo_array);
                }

                // Get the SLO's from the selected array
                if( $sort_type == "alpha" ) {

                    foreach( $slo_array as $sid ) {

                        $slo_text = db_result(db_query("SELECT slo_text FROM {dae_slo} WHERE id=%d", $sid));

                        if( $goal_slos ) {

                            if( !in_array($sid, $goal_slos) ) {
                                $selected_slos[$sid] = $slo_text;
                            }

                        }else {
                            $selected_slos[$sid] = $slo_text;
                        }

                    }
                    asort($selected_slos);

                }else {

                    $query_string = "SELECT * FROM {dae_slo} WHERE ";

                    for( $i = 0; $i < count($slo_array); $i++ ) {

                        if( $i < (count($slo_array)-1) ) {
                            $query_string .= "id=%d OR ";
                        }else {
                            $query_string .= "id=%d";
                        }

                    }

                    $query_string .= " ORDER BY slo_rank ASC, slo_text ASC";

                    $result = db_query($query_string, $slo_array);

                    while( $row = db_fetch_array($result) ) {

                        if( $goal_slos ) {

                            if( !in_array($row['id'], $goal_slos) ) {
                                $selected_slos[$row['id']] = $row['slo_text']." <small>(".$row['slo_rank'].")</small>";
                            }

                        }else {
                            $selected_slos[$row['id']] = $row['slo_text']." <small>(".$row['slo_rank'].")</small>";
                        }
                    }
                }

            }else {

                // Use the $slo array created earlier
                if( $sort_type == "alpha" ) {

                    foreach( $slo_array as $sid ) {

                        $slo_text = db_result(db_query("SELECT slo_text FROM {dae_slo} WHERE id=%d", $sid));

                        if( $goal_slos ) {

                            if( !in_array($sid, $goal_slos) ) {
                                $selected_slos[$sid] = $slo_text;
                            }

                        }else {
                            $selected_slos[$sid] = $slo_text;
                        }
                    }
                    asort($selected_slos);

                }else {

                    $query_string = "SELECT * FROM {dae_slo} WHERE ";

                    for( $i = 0; $i < count($slo_array); $i++ ) {

                        if( $i < (count($slo_array)-1) ) {
                            $query_string .= "id=%d OR ";
                        }else {
                            $query_string .= "id=%d";
                        }

                    }

                    $query_string .= " ORDER BY slo_rank ASC, slo_text ASC";

                    $result = db_query($query_string, $slo_array);

                    while( $row = db_fetch_array($result) ) {

                        if( $goal_slos ) {

                            if( !in_array($row['id'], $goal_slos) ) {
                                $selected_slos[$row['id']] = $row['slo_text']." <small>(".$row['slo_rank'].")</small>";
                            }
                        
                        }else {
                            $selected_slos[$row['id']] = $row['slo_text']." <small>(".$row['slo_rank'].")</small>";
                        }

                    }
                }
            }
        }else {

            // Make a list of every slo as a checkbox
            if( $sort_type == "alpha" ) {

                $result = db_query("SELECT * FROM {dae_slo} ORDER BY slo_text");
                while( $row = db_fetch_array($result) ) {

                    if( $goal_slos ) {

                        if( !in_array($row['id'], $goal_slos) ) {
                            $selected_slos[$row['id']] = $row['slo_text'];
                        }

                    }else {
                        $selected_slos[$row['id']] = $row['slo_text'];
                    }
                }
            }else {

                $result = db_query("SELECT * FROM {dae_slo} ORDER BY slo_rank ASC, slo_text ASC");
                while( $row = db_fetch_array($result) ) {

                    if( $goal_slos ) {

                        if( !in_array($row['id'], $goal_slos) ) {
                            $selected_slos[$row['id']] = $row['slo_text']." <small>(".$row['slo_rank'].")</small>";
                        }

                    }else {
                        $selected_slos[$row['id']] = $row['slo_text']." <small>(".$row['slo_rank'].")</small>";
                    }
                }
            }
        }

        // When all the SLOs are selected it may be a long list. Here we can
        // display the function buttons at the top as well as the bottom.
        // Only show the top select buttons if there are no tags selected or
        // there are more than 10 learning outcomes displayed.
        if( !$tags || (count($selected_slos) >= 14) ) {

            $form['submit-top'] = array(
                '#type' => 'submit',
                '#value' => t('Submit Learning Outcomes'),
            );

            // Display buttons according to the sort order
            if( $sort_type == "alpha" ) {

                $form['order-rank-top'] = array(
                    '#type' => 'submit',
                    '#value' => t('Order by rank')
                );

            }else {

                $form['order-alpha-top'] = array(
                    '#type' => 'submit',
                    '#value' => t('Order alphabetically')
                );

            }
        }

        // Display the SLO's with checkboxes
        if( $selected_slos ) {

            $form['checkboxes'] = array(
                '#type' => 'checkboxes',
                '#options' => $selected_slos,
            );

            $form['submit-bot'] = array(
                '#type' => 'submit',
                '#value' => t('Submit Learning Outcomes'),
            );

        }else {

            $form[] = array(
                '#type' => 'item',
                '#value' => '<ul><li>No student learning outcomes returned.</li></ul>'
            );

        }

        // Display buttons according to the sort order
        if( $sort_type == "alpha" ) {

            $form['order-rank-bot'] = array(
                '#type' => 'submit',
                '#value' => t('Order by rank')
            );

        }else {

            $form['order-alpha-bot'] = array(
                '#type' => 'submit',
                '#value' => t('Order alphabetically')
            );

        }

        // Submit hidden goal label.
        $form['goal-label'] = array( '#type' => 'value', '#value' => $goal_label, );

    }

    // Select a goal to manage
    else {

        $question_img_src = $base_url."/".daedalus_get_setting("question mark");

        // The title is set with an 'a' tag with the class='show-help'. When the image is clicked the help section is shown.
        drupal_set_title("Select a Goal Label <a class='show-help'><img src='".$question_img_src."' align='right' alt='?' /></a>");

        $result = db_query("SELECT * FROM {mag_goal_label} ORDER BY goal_label");
        while( $row = db_fetch_array($result) ) {

            $label_url = $page_url."/".$row['id'];

            $message = "Are you sure you want to delete the course exclusion \"".$row['goal_label']."\"";
            $delete_url = $page_url."/delete_label/".$row['id'];
            $delete_label = " - <a href='".$delete_url."' class=removable message='".$message."'><strong>delete</strong></a>";

            $label_output .= "<li><a href='".$label_url."'>".$row['goal_label']."</a>".$delete_label."</li>";

        }

        // If labels are selected,
        // display the list.
        if( $label_output ) {

            $form['selected-label'] = array (
                '#type' => 'item',
                '#value' => t('<ul>'.$label_output.'</ul>'),
            );

        }

        // Else direct the user
        // to create a label.
        else {

            $url = $base_url."/".daedalus_get_setting("build goals");
            drupal_set_message(t('There are no labels created. <a href="'.$url.'"><b>Create label</b></a>'));

        }

    }

    return $form;
}


/**
 * Implementation of hook_validate().
 */
function daedalus_manage_goals_form_validate( $form, &$form_state ) {

    switch($form_state['values']['op']) {

        case $form_state['values']['submit-top']:
        case $form_state['values']['submit-bot']:

            $selected_slos = FALSE;

            // Get the list of checked learning outcomes and set a
            // flag if one of the checkboxes have been selected.
            foreach( $form_state['values']['checkboxes'] as $key => $value ) {

                if( $value != "0" ) {
                    $selected_slos = TRUE;

                    break;
                }

            }

            if( !$selected_slos ) {
                form_set_error('', t('At least one learning outcome must be selected.'));
            }

        break;

    }
}


/**
 * Implementation of hook_submit().
 */
function daedalus_manage_goals_form_submit( $form, &$form_state ) {

    global $base_url;

    $page_url = daedalus_get_setting("manage goals");

    $page_url_length = sizeof(explode('/',$page_url));

    // Store URL Parameters in $param array
    $param = array();
    $param[0] = arg(0+$page_url_length);
    $param[1] = arg(1+$page_url_length);
    $param[2] = arg(2+$page_url_length);
    $param[3] = arg(3+$page_url_length);

    $label_id  = $param[0];
    $state     = $param[1];
    $tags      = $param[2];
    $sort_type = $param[3];

    // Get the list of checked learning outcomes.
    if( $form_state['values']['checkboxes'] ) {
        foreach( $form_state['values']['checkboxes'] as $key => $value ) {
            if( $value != "0" ) {
                // Store slos in an array
                $slo_array[] = $key;
                // and a list delimited with underscores
                if( $slo_list != "" ) {
                    $slo_list .= "_";
                }
                $slo_list .= $key;
            }
        }
    }

    switch($form_state['values']['op']) {

        case $form_state['values']['order-alpha-top']:
        case $form_state['values']['order-alpha-bot']:

            if( $param[0] && $param[1] && $param[2] ) {
                drupal_goto($base_url."/".$page_url."/".$label_id."/".$state."/".$tags."/alpha");
            }else {
                drupal_goto($base_url."/".$page_url."/".$label_id."/alpha");
            }

        break;

        case $form_state['values']['order-rank-top']:
        case $form_state['values']['order-rank-bot']:

            if( $param[0] && $param[1] && $param[2] ) {
                drupal_goto($base_url."/".$page_url."/".$label_id."/".$state."/".$tags);
            }else {
                drupal_goto($base_url."/".$page_url."/".$label_id);
            }

        break;

        // Buttons for changing the settings. They just change some db values.
        // "increase-percent","decrease-percent","increase-max","decrease-max"
        case $form_state['values']['increase-percent']:

            $tag_height = daedalus_get_setting("tag cloud height percent");

            $value = $tag_height + 20;

            db_query("UPDATE {dae_settings} SET value=%d WHERE setting='tag cloud height percent'", $value);

        break;

        case $form_state['values']['decrease-percent']:

            $tag_height = daedalus_get_setting("tag cloud height percent");


            if( $tag_height > 40  ) {
                    $value = $tag_height - 20;
            }else {
                    $value = $tag_height - 10;
            }

            db_query("UPDATE {dae_settings} SET value=%d WHERE setting='tag cloud height percent'", $value);

        break;

        case $form_state['values']['increase-max']:

            $max_font_size = daedalus_get_setting("tag cloud max font size");

            $value = $max_font_size + 1;

            db_query("UPDATE {dae_settings} SET value=%d WHERE setting='tag cloud max font size'", $value);

        break;

        case $form_state['values']['decrease-max']:

            $max_font_size = daedalus_get_setting("tag cloud max font size");

            if( $max_font_size >= 1 ) {
                $value = $max_font_size - 1;
                db_query("UPDATE {dae_settings} SET value=%d WHERE setting='tag cloud max font size'", $value);
            }else {
                drupal_set_message(t('Font size is currently set to its smallest value.'));
            }

        break;

        case $form_state['values']['submit-top']:
        case $form_state['values']['submit-bot']:

            $goal_label = $form_state['values']['goal-label'];

            if( $slo_array ) {

                foreach( $slo_array as $sid ) {
                    db_query("INSERT INTO {mag_goal_sets} (label_id, slo_id) VALUES (%d, %d)", $label_id, $sid);
                }

                drupal_set_message(t('Learning outcomes have been added to <b>'.$goal_label.'</b>'));
            }

            // Return to the goal creation with appropriate URL parameters
            if( $param[0] && $param[1] && $param[2] && $param[3] ) {
                drupal_goto($base_url."/".$page_url."/".$label_id."/".$state."/".$tags."/".$sort_type);
            }elseif( $param[0] && $param[1] && $param[2] ) {
                drupal_goto($base_url."/".$page_url."/".$label_id."/".$state."/".$tags);
            }else {
                drupal_goto($base_url."/".$page_url."/".$label_id);
            }

        break;

        // Save the help information
        case $form_state['values']['dae-help-submit']:

            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message('Help information saved.');

                if( $param[0] && $param[1] && $param[2] ) {
                    drupal_goto($base_url."/".$page_url."/".$param[0]."/".$param[1]."/".$param[2]);
                }elseif( $param[0] && $param[1] ) {
                    drupal_goto($base_url."/".$page_url."/".$param[0]."/".$param[1]);
                }if( $param[0] ) {
                    drupal_goto($base_url."/".$page_url."/".$param[0]);
                }else {
                    drupal_goto($base_url."/".$page_url);
                }
            }
        break;

    }
}
