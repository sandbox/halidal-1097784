<?php

// $ID$

/**
 * @file magellan.menu-program.php
 * Created by Dr. Blouin and Justin Joyce
 */

//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Program Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_program_page() {
	return drupal_get_form('daedalus_program_form');
}

/**
* Menu Location: Daedalus -> Program
* URL Location:  daedalus/daedalus/program
*
* Displays the Program menu.
* Links to:
* 	* Program Form
*	* Skill Transcript
*	* Next Step
*
* @global <type> $base_url
* @param  <type> $form
* @return string
*/
function daedalus_program_form( $form ) {

    // This gives the base site url.
    // This is also a variable that may be
    // configured in the settings for drupal.
    global $base_url;

    // Reference for the pages image.
    $css = '<!-- Image Source: Circuit Board PCB <http://circuitboardpcb.net/circuit-board-drawing-2/> -->';
    drupal_set_html_head($css);

    // Set current page url.
    $page_url = daedalus_get_setting("my program");

    $program_img_src = $base_url."/".daedalus_get_setting("daedalus program image");

    // Set a blank title
    drupal_set_title("");

    $form = array();

    // Displays the program menu items. The menu item URLs
    // are stored in the correct order in the database.
    $result = db_query("SELECT * FROM {dae_settings} WHERE type = 'url' ORDER BY id");

    while($row = db_fetch_array($result)) {

        $setting = $row['value'];

        $title = ucwords($row['setting']);

        $pos = strpos($setting,$page_url);

        if( $pos !== FALSE && $setting != $page_url) {

            if( stripos($setting,"form") ) {

                $description = "Create your undergraduate program.";
                
                $program_list .= "<p><font size='2'><a href='".$base_url."/".$setting."'>".$title."</a> - ".$description."</font></p>";

            }

            elseif( stripos($setting,"map") ) {

                $description = "View your undergraduate program visually as a course map.";

                $program_list .= "<p><font size='2'><a href='".$base_url."/".$setting."'>".$title."</a> - ".$description."</font></p>";

            }

            elseif( stripos($setting,"strengths") ) {

                $description = "View learning strengths visually through tag association.";

                $program_list .= "<p><font size='2'><a href='".$base_url."/".$setting."'>".$title."</a> - ".$description."</font></p>";

            }

            elseif( stripos($setting,"nextstep") ) {

                $description = "View which courses become available while completing your program.";

                $program_list .= "<p><font size='2'><a href='".$base_url."/".$setting."'>".$title."</a> - ".$description."</font></p>";

            }
            
            elseif( stripos($setting,"goal") ) {

                $description = "Create a program or career goal to achieve.";

                $program_list .= "<p><font size='2'><a href='".$base_url."/".$setting."'>".$title."</a> - ".$description."</font></p>";

            }

        }

    }

    $program_output = '<font size="10"><b>My Program</b></font>';
    $program_output.= '<br /><br /><br />';
    $program_output.= '<div><img src="'.$program_img_src.'" /></div>';
    $program_output.= '<br />'.$program_list;

    $form[] = array(
        '#type'  => 'item',
        '#value' => t($program_output),
    );

    return $form;

} // function daedalus_program_form( $form )


//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Program->Form Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_my_program_page() {
    return drupal_get_form("daedalus_my_program_form");
};

/**
* Menu Location: Daedalus -> Browse -> Programs
* URL Location:  daedalus/daedalus/program/form
*
* Displays 
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_my_program_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Determine the user name
    global $user;
    $user_id = $user->uid;

    // If the current user is a Magellan Advisor determine if there is a current advising session open.
    if( db_result(db_query("SELECT COUNT(*) FROM {role, users_roles} WHERE role.name='Magellan Advisor' AND users_roles.uid=%d AND users_roles.rid = role.rid", $user_id)) ) {

        $result = db_query("SELECT id, student_id FROM {mag_advisor_session} WHERE advisor_id=%d", $user_id);
        while( $row = db_fetch_array($result) ) {
            $session_id  = $row['id'];
            $session_sid = $row['student_id'];
        }

        // Get the session name.
        $current_session = $_COOKIE[session_name()];
//echo "DB Session > ".$session_id."<br />";
//echo "Cr Session > ".$current_session."<br />";

        // Current session, ask the user if they would like to end the session.
        if( $session_id == $current_session ) {
            $in_session = TRUE;
            $advising_text = "Advising:";
            $user_name = db_result(db_query("SELECT user_name FROM {mag_student_identification} WHERE id=%d", $session_sid));
        }

        // Not in session, set flag to redirect the
        // advisor back to select a student to advise.
        else {
            $no_session = TRUE;
//echo "ADVISOR NOT IN SESSION<br />";
            // TODO: REMOVE, ONLY HERE WHILE ACCESS GIVEN TO "daedalus manage"
            $user_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $user_id));
        }

    }

    else {
        // Determine the user name
        $user_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $user_id));
    }
//echo "USER NAME > ".$user_name."<br />";

    // If the user is an advisor, the advisor must be currently be in
    // a session to view the program form information of a student. If 
    // a student is viewing their program information they will never be
    // redirected because they do not have advisor privilidges, the flag
    // will never be set to true for a student.
    if( !$no_session || user_access("daedalus manage") ) {      // **** user_access("daedalus manage) TEMPORARY ONLY TO ALLOW FOR EASIER DEVELOPMENT ****** REMOVE LATER *****

        // Get current page url.
        $page_url = $help_url = daedalus_get_setting("program form");

        // Get page urls.
        $course_url = $base_url."/".daedalus_get_setting("manage courses");
        $build_url = $base_url."/".daedalus_get_setting("build programs");
        $log_url = $base_url."/".daedalus_get_setting("session log");

        // Store URL Parameters in $param array.
        $page_url_length = sizeof(explode('/',$page_url));
        $page_url = $base_url."/".$page_url;

        $param = array();

        $param[0] = arg(0+$page_url_length);    // "view/create", determines if the program has been selected or is being created.
        $param[1] = arg(1+$page_url_length);    // $program, the selected program.
        $param[2] = arg(2+$page_url_length);    // $year, the selected program year.
        $param[3] = arg(3+$page_url_length);    // Email message

        // Get the images.
        $computer_science_logo = $base_url."/".daedalus_get_setting("computer science logo");
        $small_computer_science_logo = $base_url."/".daedalus_get_setting("computer science logo small");

        // Access and permissions
        $edit_access = user_access("magellan advise edit log");

        $form = array();

        // Add the hidden help form. Paramaters are
        // (help url, show border, show break).
        $form = display_hidden_form($help_url, 1, 1);


            ////////////////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////////////////
          ///            E       O
         ///       I   S   S   I   N
        ///          N       S
        if( !$param[0] && $in_session ) {

            // If an advisor is in session, determine if the advisor
            // has created a program form for the current student.
            if( db_result(db_query("SELECT official_selection FROM {mag_student_identification} WHERE user_name='%s'", $user_name)) ) {

                // Direct the user to their form.
                if( db_result(db_query("SELECT COUNT(*) FROM {mag_student_program_form} WHERE selection_id=(SELECT official_selection FROM {mag_student_identification} WHERE user_name='%s')", $user_name)) ) {
                    drupal_goto($page_url."/view/");
                }

                // If the user is in the database but has not
                // created their form direct them to do so now.
                elseif( db_result(db_query("SELECT COUNT(*) FROM {mag_student_identification} WHERE user_name='%s'", $user_name)) ) {
                    drupal_goto($page_url."/create/");
                }

            }

            // If the user has not selected 
            // a program, direct them to.
            else {
//echo "IN SESSION CREATE PROGRAM <br />";
//echo "USER NAME > ".$user_name."<br />";
                // Have the user select their Program for the first time.
                $result = db_query("SELECT DISTINCT program FROM {mag_program_identification} ORDER BY program");
                while( $row = db_fetch_array($result) ) {
                    $available_progams[$row['program']] = $row['program'];
                    ++$flag;
                }

                // If there are programs
                // available to select.
                if( $flag ) {

                    $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name));

                    $first_name = $student['first_name'];
                    $last_name = $student['last_name'];

                    // If the first and last names are not present
                    // enter this information for the first time.
                    if( !$first_name && !$last_name ) {

                        drupal_set_title(t("Submit Student Information <a class='show-help'><img src='".$small_computer_science_logo."' align='right' alt='?' /></a>"));

                        $form['first-name'] = array(
                            '#type'  => 'textfield',
                            '#title' => t('Enter First Name'),
                        );

                        $form['last-name'] = array(
                            '#type'  => 'textfield',
                            '#title' => t('Enter Last Name'),
                            '#suffix' => '<br />',
                        );

                        $form['program'] = array(
                            '#type'  => 'select',
                            '#title' => t('Select an official program'),
                            '#options' => $available_progams,
                        );

                        // submit-program is an insert query to
                        // enter the first, last and user names.
                        $form['submit-program'] = array(
                            '#type' => 'submit',
                            '#value' => t('Submit'),
                        );
//echo "USER NAME ???? ".$user_name."<br />";
                        $form['user-name'] = array( '#type' => 'value', '#value' => $user_name );

                    }

                    // Else, the user has entered information before,
                    // Do not allow the user to modify their username.
                    else {

                        drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$small_computer_science_logo."' align='right' alt='?' /></a>"));

                        $form['first-name'] = array(
                            '#type'  => 'textfield',
                            '#title' => t('Enter First Name'),
                            '#default_value' => $first_name,
                            '#disabled' => TRUE,
                        );

                        $form['last-name'] = array(
                            '#type'  => 'textfield',
                            '#title' => t('Enter Last Name'),
                            '#default_value' => $last_name,
                            '#disabled' => TRUE,
                        );

                        $form['first-name'] = array( '#type' => 'value', '#value' => $first_name );
                        $form['last-name'] = array( '#type' => 'value', '#value' => $last_name );

                        $form['program'] = array(
                            '#type'  => 'select',
                            '#title' => t('Select an official program'),
                            '#options' => $available_progams,
                        );
                        
                        // submit-program2 redirects
                        // to select a program year.
                        $form['submit-program2'] = array(
                            '#type' => 'submit',
                            '#value' => t('Submit'),
                        );

                    }

                }

                else {

                    // Display link to build a program
                    // if the user has build access.
                    if( user_access('daedalus build') ) {
                        drupal_set_message(t("A program must first be created. Create one <a href='".$build_url."'>here</a>."));
                    }
                    else {
                        drupal_set_message(t('A program must first be created. Please contact a system administrator.') , 'warning');
                    }

                }

            }

        }
        
            ////////////////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////////////////
          ///      T       E       O
         ///     O   I   S   S   I   N
        ///    N       N       S       !
        elseif( !$param[0] && !$in_session ) {

            // Determine if the student user has
            // a currently selected program form.
            if( db_result(db_query("SELECT current_selection FROM {mag_student_identification} WHERE user_name='%s'", $user_name)) ) {

                // Direct the user to their form if it exists.
                if( db_result(db_query("SELECT COUNT(*) FROM {mag_student_program_form} WHERE selection_id=(SELECT current_selection FROM {mag_student_identification} WHERE user_name='%s')", $user_name)) ) {
                    drupal_goto($page_url."/view/");
                }

                // If the user is in the database but has not
                // created their form direct them to do so now.
                elseif( db_result(db_query("SELECT COUNT(*) FROM {mag_student_identification} WHERE user_name='%s'", $user_name)) ) {
                    drupal_goto($page_url."/create/");
                }

            }

            // A student deletes a program, they are directed to switch
            // to another if available or create a new program.
            elseif( db_result(db_query("SELECT COUNT(*) FROM {mag_student_program_selection} WHERE student_id=(SELECT id FROM {mag_student_identification} WHERE user_name='%s')", $user_name )) ) {

                // Initially there will never be a param[1]. When the user selects a previously created program or creates a new one, the process will  ??????????????? WTF
                if( !$param[1] ) {

                    $student_info = db_fetch_array(db_query("SELECT id, official_selection FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

                    $student_id = $student_info['id'];
                    $official_selection = $student_info['official_selection'];

                    $result = db_query("SELECT DISTINCT id, program_id FROM {mag_student_program_selection} WHERE student_id=%d", $student_id );
                    while( $row = db_fetch_array($result) ) {

                        $selected_info = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $row['program_id']));

                        if( $row['id'] == $official_selection ) {
                            $selected_programs[$row['id']] = 'Official Program';
                        }
                        
                        else {
                            $selected_programs[$row['id']] = $selected_info['program'] . ' / ' . $selected_info['year'];
                        }

                    }

                    if( $selected_programs ) {

                        $form['selected-program'] = array(
                            '#type'  => 'select',
                            '#title' => t('Select previously created program'),
                            '#options' => $selected_programs,
                        );

                        $form['submit-selected-program'] = array(
                            '#type' => 'submit',
                            '#value' => t('Select program'),
                            '#suffix' => '<br /><br /><br />',
                        );

                    }

                    // Have the user select their Program for the first time.
                    $result = db_query("SELECT DISTINCT program FROM {mag_program_identification} ORDER BY program");
                    while( $row = db_fetch_array($result) ) {
                        $available_progams[$row['program']] = $row['program'];
                        ++$flag;
                    }

                    // If there are programs
                    // available to select.
                    if( $flag ) {

                        $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

                        $first_name   = $student['first_name'];
                        $last_name    = $student['last_name'];

                        drupal_set_title(t("<b>".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$small_computer_science_logo."' align='right' alt='?' /></a>"));

                        $form['program'] = array(
                            '#type'  => 'select',
                            '#title' => t('Select new program'),
                            '#options' => $available_progams,
                        );

                        $form['submit-new-program'] = array(
                            '#type' => 'submit',
                            '#value' => t('Add new program'),
                        );

                        $form['user-name'] = array( '#type' => 'value', '#value' => $user_name );

                    }

                }

                // Select the additional programs year.
                else {

                    $student = db_fetch_array(db_query("SELECT id, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name));

                    $student_id = $student['id'];
                    $first_name = $student['first_name'];
                    $last_name = $student['last_name'];

                    drupal_set_title(t("<b>".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$small_computer_science_logo."' align='right' alt='?' /></a></i>"));

                    $program = $param[1];

                    $current_year = FALSE;

                    // Have the user select their rogram year.
                    $result = db_query("SELECT DISTINCT year FROM {mag_program_identification} WHERE program='%s' ORDER BY year DESC", $program);
                    while( $row = db_fetch_array($result) ) {

                        if( !$current_year ) {
                            $current_year = $row['year'];
                            $program_years[$row['year']] = "Current Year";
                        }else {
                            $program_years[$row['year']] = $row['year'];
                        }
                    }

                    $form['effective-year'] = array(
                        '#title' => t("Select program year"),
                        '#type'  => 'select',
                        '#options' => $program_years,
                        '#default_value' => $current_year,
                    );

                    $form['submit']['submit-new-year'] = array(
                        '#type' => 'submit',
                        '#value' => t('Submit program year'),
                    );

                    $form['submit']['cancel'] = array(
                        '#type' => 'submit',
                        '#value' => t('Cancel'),
                    );

                    // Submit hidden program value
                    $form['student-id'] = array( '#type' => 'value', '#value' => $student_id );
                    $form['program'] = array( '#type' => 'value', '#value' => $program );
                    $form['user-name'] = array( '#type' => 'value', '#value' => $user_name );

                }

            }

            // If a student user has not entered their name 
            // into the system and has not selected a program.
            else {

                // Have the user select their Program for the first time.
                $result = db_query("SELECT DISTINCT program FROM {mag_program_identification} ORDER BY program");
                while( $row = db_fetch_array($result) ) {
                    $available_progams[$row['program']] = $row['program'];
                    ++$flag;
                }

                // If there are programs
                // available to select.
                if( $flag ) {

                    $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name));

                    $first_name   = $student['first_name'];
                    $last_name    = $student['last_name'];

                    // If the first and last names are not present
                    // enter this information for the first time.
                    if( !$first_name && !$last_name ) {

                        drupal_set_title(t("Submit Student Information <a class='show-help'><img src='".$small_computer_science_logo."' align='right' alt='?' /></a>"));

                        $form['first-name'] = array(
                            '#type'  => 'textfield',
                            '#title' => t('Enter First Name'),
                        );

                        $form['last-name'] = array(
                            '#type'  => 'textfield',
                            '#title' => t('Enter Last Name'),
                            '#suffix' => '<br />',
                        );

                        $form['program'] = array(
                            '#type'  => 'select',
                            '#title' => t('Select a program'),
                            '#options' => $available_progams,
                        );

                        // submit-program is an insert query to
                        // enter the first, last and user names.
                        $form['submit-program'] = array(
                            '#type' => 'submit',
                            '#value' => t('Submit'),
                        );

                        $form['user-name'] = array( '#type' => 'value', '#value' => $user_name );

                    }

                    // Else, the user has entered information before,
                    // Do not allow the user to modify their username.
                    else {

                        drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$small_computer_science_logo."' align='right' alt='?' /></a>"));

                        $form['first-name'] = array(
                            '#type'  => 'textfield',
                            '#title' => t('Enter First Name'),
                            '#default_value' => $first_name,
                            '#disabled' => TRUE,
                        );

                        $form['last-name'] = array(
                            '#type'  => 'textfield',
                            '#title' => t('Enter Last Name'),
                            '#default_value' => $last_name,
                            '#disabled' => TRUE,
                            '#suffix' => '<br />',
                        );

                        $form['first-name'] = array( '#type' => 'value', '#value' => $first_name );
                        $form['last-name'] = array( '#type' => 'value', '#value' => $last_name );

                        $form['program'] = array(
                            '#type'  => 'select',
                            '#title' => t('Select a program'),
                            '#options' => $available_progams,
                        );

                        // submit-program2 redirects
                        // to select a program year.
                        $form['submit-program2'] = array(
                            '#type' => 'submit',
                            '#value' => t('Submit'),
                        );

                    }

                }

                else {

                    // Display link to build a program
                    // if the user has build access.
                    if( user_access('daedalus build') ) {
                        drupal_set_message(t("A program must first be created. Create one <a href='".$build_url."'>here</a>."));
                    }
                    else {
                        drupal_set_message(t('A program must first be created. Please contact a system administrator.') , 'warning');
                    }

                }

            }

        }

            ////////////////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////////////////
          ///         M           R    M
         ///    S   B   I   P   O   A
        ///       U       T   R       
        elseif( $param[0] == 'submit' ) {

            // The second stage of the submit process
            // where the program year is selected.

            $student = db_fetch_array(db_query("SELECT id, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name));

            $student_id = $student['id'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];

            drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$small_computer_science_logo."' align='right' alt='?' /></a></i>"));

            $program = $param[1];

            $current_year = FALSE;

            // Have the user select their rogram year.
            $result = db_query("SELECT DISTINCT year FROM {mag_program_identification} WHERE program='%s' ORDER BY year DESC", $program);
            while( $row = db_fetch_array($result) ) {

                if( !$current_year ) {
                    $current_year = $row['year'];
                    $program_years[$row['year']] = "Current Year";
                }else {
                    $program_years[$row['year']] = $row['year'];
                }
            }

            if( $in_session ) {

                $form['effective-year'] = array(
                    '#title' => t("Select official program year"),
                    '#type'  => 'select',
                    '#options' => $program_years,
                    '#default_value' => $current_year,
                );

                $form['submit']['advisor-submit-year'] = array(
                    '#type' => 'submit',
                    '#value' => t('Submit'),
                );

            }

            else {

                $form['effective-year'] = array(
                    '#title' => t("Select program year"),
                    '#type'  => 'select',
                    '#options' => $program_years,
                    '#default_value' => $current_year,
                );

                $form['submit']['student-submit-year'] = array(
                    '#type' => 'submit',
                    '#value' => t('Submit'),
                );

            }

            $form['submit']['cancel'] = array(
                '#type' => 'submit',
                '#value' => t('Cancel'),
            );

            // Submit hidden program value
            $form['student-id'] = array( '#type' => 'value', '#value' => $student_id );
            $form['program'] = array( '#type' => 'value', '#value' => $program );
            $form['user-name'] = array( '#type' => 'value', '#value' => $user_name );

        }
        
            ////////////////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////////////////
          ///    A         R       A
         ///       D    P    O   R   M
        ///          D         G
        elseif( $param[0] == 'add_program' ) {

            if( !$param[1] ) {

                // Have the user select their Program for the first time.
                $result = db_query("SELECT DISTINCT program FROM {mag_program_identification} ORDER BY program");

                while( $row = db_fetch_array($result) ) {
                    $available_progams[$row['program']] = $row['program'];
                    ++$flag;
                }

                // If there are programs
                // available to select.
                if( $flag ) {

                    $student = db_fetch_array(db_query("SELECT first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name));

                    $first_name   = $student['first_name'];
                    $last_name    = $student['last_name'];

                    drupal_set_title(t("<b>".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$small_computer_science_logo."' align='right' alt='?' /></a>"));

                    $form['program'] = array(
                        '#type'  => 'select',
                        '#title' => t('Select a new program'),
                        '#options' => $available_progams,
                    );

                    $form['submit-new-program'] = array(
                        '#type' => 'submit',
                        '#value' => t('Submit'),
                    );

                    $form['user-name'] = array( '#type' => 'value', '#value' => $user_name );

                }

            }

            // Select the additional programs year.
            else {

                $student = db_fetch_array(db_query("SELECT id, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name));

                $student_id = $student['id'];
                $first_name = $student['first_name'];
                $last_name = $student['last_name'];

                drupal_set_title(t("<b>".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$small_computer_science_logo."' align='right' alt='?' /></a></i>"));

                $program = $param[1];

                $current_year = FALSE;

                // Have the user select their rogram year.
                $result = db_query("SELECT DISTINCT year FROM {mag_program_identification} WHERE program='%s' ORDER BY year DESC", $program);
                while( $row = db_fetch_array($result) ) {

                    if( !$current_year ) {
                        $current_year = $row['year'];
                        $program_years[$row['year']] = "Current Year";
                    }else {
                        $program_years[$row['year']] = $row['year'];
                    }
                }

                $form['effective-year'] = array(
                    '#title' => t("Select new program year"),
                    '#type'  => 'select',
                    '#options' => $program_years,
                    '#default_value' => $current_year,
                );

                $form['submit']['submit-new-year'] = array(
                    '#type' => 'submit',
                    '#value' => t('Submit'),
                );

                $form['submit']['cancel'] = array(
                    '#type' => 'submit',
                    '#value' => t('Cancel'),
                );

                // Submit hidden program value
                $form['student-id'] = array( '#type' => 'value', '#value' => $student_id );
                $form['program'] = array( '#type' => 'value', '#value' => $program );
                $form['user-name'] = array( '#type' => 'value', '#value' => $user_name );

            }

        }

            ////////////////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////////////////
          ///     E   E       R   G   A
         ///    D   L   T   P   O   R   M
        ///               E
        elseif( $param[0] == 'delete' ) {

            if( $in_session ) {

                if( is_numeric($param[1]) ) {

                    $official_selection = $param[1];

                    $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $official_selection));

                    $form['advisor-selection'] = array( '#type' => 'value', '#value' => $official_selection );

                    $official = ' student ';

                }
                
                else {

                    $official_selection = db_result(db_query("SELECT official_selection FROM {mag_student_identification} WHERE user_name='%s'", $user_name));

                    $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $official_selection));

                    $form['official-selection'] = array( '#type' => 'value', '#value' => $official_selection );

                    $official = ' official ';

                }

            }
            
            else {

                $current_selection = db_result(db_query("SELECT current_selection FROM {mag_student_identification} WHERE user_name='%s'", $user_name));

                $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $current_selection));

                $form['current-selection'] = array( '#type' => 'value', '#value' => $current_selection );
                
                $official = ' ';

            }

            $program_info = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $program_id));

            $program = $program_info['program'];
            $year = $program_info['year'];

            drupal_set_title("Delete confirmation");

            $message ="<br />
                       Are you sure you want to delete the" . $official . "program <strong>" . $program . "</strong> effective <strong>" . $year . "</strong>? This can not be undone.
                       <br /><br />
                       This will delete the:
                       <br />
                       <ul>
                            <li>course mark information.</li>
                            <li>course requirement completed information.</li>
                            <li>course timing information.</li>
                            <li>equivalent course information.</li>
                            <li>requirement note information.</li>
                       </ul>
                       <br />Are you sure you want to continue?";

            $form[] = array(
                '#type' => 'item',
                '#value' => t($message),
            );

            $form['delete-forward'] = array(
                '#type' => 'submit',
                '#value' => t("I understand the consequences. Delete this program forever."),
            );

            $form['delete-reverse'] = array(
                '#type' => 'submit',
                '#value' => t("On second thought, I better not. Take me back."),
            );

            // Hidden values
            $form["program"] = array( '#type' => 'value', '#value' => $program );
            $form["effective-year"] = array( '#type' => 'value', '#value' => $year );
            $form["user-name"] = array( '#type' => 'value', '#value' => $user_name );

        }

            ////////////////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////////////////
          ///     W           R   G
         ///    S   I   C   P   O   R   M
        ///           T   H           A
        elseif( $param[0] == 'switch' ) {

            $new_selection = $param[1];
            $student_id = $param[2];

            db_query("UPDATE {mag_student_identification} SET current_selection=%d WHERE id=%d", $new_selection, $student_id );

            drupal_set_message(t('The program has been switched.'));

            drupal_goto($page_url."/view");

        }

            ////////////////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////////////////
          ///       A       D       O
         ///      M   I   A   V   S   R
        ///     E       L       I
        elseif( $param[0] == 'email_advisor' ) {

            $advisor_list = $student = $program_info = array();

            $student_info = db_fetch_array(db_query("SELECT id, current_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $selection_id = $student_info['current_selection'];
            $first_name = $student_info['first_name'];
            $last_name = $student_info['last_name'];
            $student_id = $student_info['id'];

            // Get the program id
            $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $selection_id ));

            // Get the program information
            $program = db_result(db_query("SELECT program FROM {mag_program_identification} WHERE id=%d", $program_id ));

            // Get the students program information
            $program_info = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $program_id ));

            $program = $program_info['program'];
            $program_year = $program_info['year'];

            drupal_set_title(t("<b>".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program."<br />Email Advisor</small></i>"));

            $selected_advisor = $param[1];
            $selected_message = $param[2];

            $role_id = db_result(db_query("SELECT rid FROM {role} WHERE name='Magellan Advisor'"));

            $result = db_query("SELECT uid FROM {users_roles} WHERE rid=%d", $role_id);
            while( $row = db_fetch_array($result) ) {
                $advisor_list[$row['uid']] = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $row['uid'] ));
            }

            $form['selected-advisor'] = array(
                '#title' => t("Select advisor"),
                '#type'  => 'select',
                '#options' => $advisor_list,
            );

            $form['advisor-message'] = array(
                '#type' => 'textarea',
                '#title' => t('Message'),
            );

            $form['submit-email'] = array(
                '#type' => 'submit',
                '#value' => t('Email'),
            );

            $form['first-name'] = array( '#type' => 'value', '#value' => $first_name, );
            $form['last-name'] = array( '#type' => 'value', '#value' => $last_name, );

        }

            //////////////////////////////////////////////////////////////////
           //////////////////////////////////////////////////////////////////
          ///       R               R       A
         ///      T   A   S   E   P   O   R   M
        ///             N   F   R       G
        elseif( $param[0] == 'transfer' ) {

            $student = db_fetch_array(db_query("SELECT id, official_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $selection_id = $student['official_selection'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];
            $student_id = $student['id'];

            // Get the program id
            $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $selection_id ));

            // Get the program information
            $program = db_result(db_query("SELECT program FROM {mag_program_identification} WHERE id=%d", $program_id ));

            // Get the students program information
            $program_info = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $program_id ));

            $program = $program_info['program'];
            $program_year = $program_info['year'];

            if( $param[1] == 'confirm' ) {

                $transfer_name = $param[2];
                $transfer_year = $param[3];
                
                $original_program = $transfer_program = array();

                drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program." / ".$program_year."<br />Transfer Program: ".$transfer_name." / ".$transfer_year."</small></i>"));

                $transfer_id = db_result(db_query("SELECT id FROM {mag_program_identification} WHERE program='%s' AND year=%d", $transfer_name, $transfer_year ));

                $result = db_query("SELECT content_id, requirement_id, requirement_met, requirement_order, equivalent_course, equivalent_id, timing, mark, note
                                      FROM {mag_student_program_form} WHERE selection_id=%d AND mark != 'N/A' ORDER BY log_time", $selection_id);
                while( $row = db_fetch_array($result) ) {

                    // This is a complete listing of the requirements in the program.
                    $original_program[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $original_program[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $original_program[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $original_program[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $original_program[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $original_program[$row['content_id']]['timing'] = $row['timing'];
                    $original_program[$row['content_id']]['mark'] = $row['mark'];
                    $original_program[$row['content_id']]['note'] = $row['note'];

                }

//foreach( $original_program as $cid => $content ) {
//    echo $cid . " > ".$content['requirement_id']."|".$content['mark']."<br />";
//}
//echo "Program ID ".$program_id."<br />Transfer Program ID ".$transfer_id."<br /><br />";

                $result = db_query("SELECT id, requirement_id, requirement_order, note FROM {mag_program_content} WHERE program_id=%d ORDER BY requirement_order", $transfer_id);
                while( $row = db_fetch_array($result) ) {

                    $transfer_program[$row['id']]['requirement_id'] = $row['requirement_id'];
                    $transfer_program[$row['id']]['requirement_order'] = $row['requirement_order'];
                    $transfer_program[$row['id']]['note'] = $row['note'];

                }

                $completed_transfer = array();

                foreach( $original_program as $cid => $req ) {
//echo $req['requirement_id']." > ".$req['mark']."<br />";

                    foreach( $transfer_program as $tid => $content ) {
//echo "&nbsp;&nbsp; ".$tid."|".$content['requirement_id'].", ";

                        if( $req['requirement_id'] == $content['requirement_id'] && !in_array($tid, $completed_transfer) ) {

                            $transfer_program[$tid]['requirement_met'] = $req['requirement_met'];
                            $transfer_program[$tid]['requirement_order'] = $req['requirement_order'];
                            $transfer_program[$tid]['equivalent_course'] = $req['equivalent_course'];
                            $transfer_program[$tid]['equivalent_id'] = $req['equivalent_id'];
                            $transfer_program[$tid]['timing'] = $req['timing'];
                            $transfer_program[$tid]['mark'] = $req['mark'];
                            $transfer_program[$tid]['note'] = $req['note'];

                            $completed_transfer[$tid] = $tid;

                            unset($original_program[$cid]);

                            break;

                        }

                    }
//echo "<br />";
                }
//echo "<br /><br />";
//foreach( $original_program as $cid => $content ) {
//    echo $cid . " > ".$content['requirement_id']."|".$content['mark']."<br />";
//}

//foreach( $transfer_program as $cid => $content ) {
//    echo $cid . " > ".$content['requirement_id']."|".$content['mark']."|".$content['timing']." > ".$content['set']."<br />";
//}

                // 8*********8*********8*********8*********8*********8*********8*********8*********8*********8*********8*********8
                // If there is any remaining program content we have to add them to 'other section' of the transfer_program array

                // Get the amount of table rows to display.
                $textfields = count($transfer_program);

                // Get additional information for the content array.
                foreach( $transfer_program as $tid => $content ) {

                    $result = db_query("SELECT course_id, description, code_filter FROM {mag_program_requirement} WHERE id=%d", $content['requirement_id']);
                    while( $row = db_fetch_array($result) ) {

                        $transfer_program[$tid]['course_id'] = $row['course_id'];
                        $transfer_program[$tid]['description'] = $row['description'];
                        $transfer_program[$tid]['code_filter'] = $row['code_filter'];

                        // Get the course
                        $transfer_program[$tid]['course'] = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $row['course_id']));

                    }

                }

//foreach( $transfer_program as $tid => $content ) {
//    echo $tid . " > ".$content['requirement_id']." - ".$content['mark']." - ".$content['timing']." > ".$content['course']." - ".$content['course_id']." - ".$content['equivalent_course']." >> ".$content['requirement_order']." << <br />";
//}

//foreach( $original_program as $tid => $content ) {
//    echo $tid . " > ".$content['requirement_id']." - ".$content['mark']." - ".$content['timing']." > ".$content['course']." - ".$content['course_id']." - ".$content['equivalent_course']." - ".$content['equivalent_id']." >> ".$content['requirement_order']." << <br />";
//}
//echo "<br /><br />";
                if( $original_program ) {

                    $textfields = count($original_program);

                    foreach( $original_program as $tid => $content ) {

                        $result = db_query("SELECT course_id, description, code_filter FROM {mag_program_requirement} WHERE id=%d", $content['requirement_id']);
                        while( $row = db_fetch_array($result) ) {

                            $original_program[$tid]['course_id'] = $row['course_id'];
                            $original_program[$tid]['description'] = $row['description'];
                            $original_program[$tid]['code_filter'] = $row['code_filter'];

                            // Get the course
                            $original_program[$tid]['course'] = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $row['course_id']));

                        }

                    }

                }
//foreach( $original_program as $tid => $content ) {
//    echo $tid . " > ".$content['requirement_id']." - ".$content['mark']." - ".$content['timing']." > ".$content['course']." - ".$content['course_id']." - ".$content['equivalent_course']." - ".$content['equivalent_id']." >> ".$content['requirement_order']." << <br />";
//}
                // Apply css to change the table behaviour
                $css = '<style type="text/css">
                            table.box{
                                border-style:solid;
                                border-width:3px;
                                border-color:#C0C0C0;
                            }
                        </style>';

                drupal_set_html_head($css);

                if( $transfer_program ) {

                    // Begin Table
                    $form[] = array(
                        '#type' => 'item',
                        '#value' => t('<table class="box"><tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('X'),
                        '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 5px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Mark'),
                        '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Term/Year'),
                        '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Course'),
                        '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Description'),
                        '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Equivalent'),
                        '#prefix' => '<td style="padding:0px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Note'),
                        '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                        '#suffix' => '</td></tr>',
                    );

                    // Set up the date values for the
                    // display in the timing dropdown.
                    $current_year = date('Y');

                    $minu_fiv = $current_year - 5;
                    $minu_fou = $current_year - 4;
                    $minu_thr = $current_year - 3;
                    $minu_two = $current_year - 2;
                    $minu_one = $current_year - 1;
                    $plus_one = $current_year + 1;
                    $plus_two = $current_year + 2;
                    $plus_thr = $current_year + 3;
                    $plus_fou = $current_year + 4;
                    $plus_fiv = $current_year + 5;

                    $i = 0;     // iteration to keep track of when to close the table.
                    $level = 1; // iteration variable for the current level.

                      //////////////////////////////////////////////////////////////
                     //////////////////////////////////////////////////////////////
                    // Iterate each program requirement
                    foreach( $transfer_program as $content_id => $program_info ) {

                        $requirement_id = $program_info['requirement_id'];
                        $requirement_order = $program_info['requirement_order'];
                        $requirement_met = $program_info['requirement_met'];
                        $mark = $program_info['mark'];
                        $timing = $program_info['timing'];
                        $equivalent = $program_info['equivalent_course'];
                        $equivalent_id = $program_info['equivalent_id'];
                        $note = $program_info['note'];

                        $description = $program_info['description'];
                        $course_id = $program_info['course_id'];

                        // Submit hidden student program form info
                        $form["content-id_$i"] = array( '#type' => 'value', '#value' => $content_id );
                        $form["requirement-id_$i"] = array( '#type' => 'value', '#value' => $requirement_id );
                        $form["requirement-order_$i"] = array( '#type' => 'value', '#value' => $requirement_order );

                        if( !$equivalent_id ) {

                            // If no equivalent id, get the course info from the requirement course id.
                            //$course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $course_id));
                            $course = $program_info['course'];

                            // Submit hidden course information only if there is no equivalent id. This
                            // will prevent a course_$i being passed that is not a requirement. If it
                            // were to be passed, the updating a the soft requirements would not work.
                            $form["course_$i"]    = array( '#type' => 'value', '#value' => $course );
                            $form["course-id_$i"] = array( '#type' => 'value', '#value' => $course_id );

                        }

                        else{

                            $course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $equivalent_id));
                            //$course = $program_info['course']; ?????

                        }

                        // Make sure that the course levels
                        // do not jump up one level.
                        $current_level = substr($requirement_order, 0, 1);
//echo "|".$requirement_order."|<br />";
                        if( $current_level > $level ) {
                            $level = $current_level;
                        }

                        if( $current_level == $level ) {

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                            );

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=7><b><big>' . $level . '000 Level &nbsp;&nbsp;</big></b></td></tr>'),
                            );

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                            );

                            $level++;

                        }

                        if( ($current_level == 9) && !$other_flag ) {

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                            );

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=7><b><big>Other Required Courses &nbsp;&nbsp;</big></b></td></tr>'),
                            );

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                            );

                            $other_flag++;

                        }


                        $form["requirement-met_$i"] = array(
                            '#type' => 'checkbox',
                            '#default_value' => $requirement_met,
                            '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 2px;">',
                            '#suffix' => '</td>',
                        );

                        // If the requirements below are met,
                        // the fields are no longer editable.
                        if( $requirement_met ) {

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t($mark),
                                '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                '#suffix' => '</td>',
                            );

                            $form["mark_$i"] = array( '#type' => 'value', '#value' => $mark );

                            $form[] = array(
                                '#type' => 'item',
                                '#size' => 12,
                                '#value' => t($timing),
                                '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                '#suffix' => '</td>',
                            );

                            $form["timing_$i"] = array( '#type' => 'value', '#value' => $timing );


                        }

                        else {

                            $form["mark_$i"] = array(
                                '#type' => 'select',
                                '#options' => array(
                                                 'N/A' => t(''),
                                                  'A+' => t('A+'),
                                                  'A'  => t('A'),
                                                  'A-' => t('A-'),
                                                  'B+' => t('B+'),
                                                  'B'  => t('B'),
                                                  'B-' => t('B-'),
                                                  'C+' => t('C+'),
                                                  'C'  => t('C'),
                                                  'C-' => t('C-'),
                                                  'D'  => t('D'),
                                                  'F'  => t('F'),
                                                 'INC' => t('INC'),
                                                'PASS' => t('PASS'),
                                                   'W' => t('W'),
                                              ),
                                '#default_value' => $mark,
                                '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                '#suffix' => '</td>',
                            );

                            // TODO: MAKE THE THE DEFAULT VALUE APPEAR CLOSER TO THE CURRENT SEMESTER WHEN THE TIMING VALUE IS NOT PRESENT!
                            $form["timing_$i"] = array(
                                '#type' => 'select',
                                '#options' => array(          '' => t(''),
                                                 'W' . $minu_fiv => t('W' . $minu_fiv),
                                                 'S' . $minu_fiv => t('S' . $minu_fiv),
                                                 'F' . $minu_fiv => t('F' . $minu_fiv),
                                                 'W' . $minu_fou => t('W' . $minu_fou),
                                                 'S' . $minu_fou => t('S' . $minu_fou),
                                                 'F' . $minu_fou => t('F' . $minu_fou),
                                                 'W' . $minu_thr => t('W' . $minu_thr),
                                                 'S' . $minu_thr => t('S' . $minu_thr),
                                                 'F' . $minu_thr => t('F' . $minu_thr),
                                                 'W' . $minu_two => t('W' . $minu_two),
                                                 'S' . $minu_two => t('S' . $minu_two),
                                                 'F' . $minu_two => t('F' . $minu_two),
                                                 'W' . $minu_one => t('W' . $minu_one),
                                                 'S' . $minu_one => t('S' . $minu_one),
                                                 'F' . $minu_one => t('F' . $minu_one),
                                                 'W' . $current_year => t('W' . $current_year),
                                                 'S' . $current_year => t('S' . $current_year),
                                                 'F' . $current_year => t('F' . $current_year),
                                                 'W' . $plus_one => t('W' . $plus_one),
                                                 'S' . $plus_one => t('S' . $plus_one),
                                                 'F' . $plus_one => t('F' . $plus_one),
                                                 'W' . $plus_two => t('W' . $plus_two),
                                                 'S' . $plus_two => t('S' . $plus_two),
                                                 'F' . $plus_two => t('F' . $plus_two),
                                                 'W' . $plus_thr => t('W' . $plus_thr),
                                                 'S' . $plus_thr => t('S' . $plus_thr),
                                                 'F' . $plus_thr => t('F' . $plus_thr),
                                                 'W' . $plus_fou => t('W' . $plus_fou),
                                                 'S' . $plus_fou => t('S' . $plus_fou),
                                                 'F' . $plus_fou => t('F' . $plus_fou),
                                                 'W' . $plus_fiv => t('W' . $plus_fiv),
                                                 'S' . $plus_fiv => t('S' . $plus_fiv),
                                                 'F' . $plus_fiv => t('F' . $plus_fiv),
                                              ),
                                '#default_value' => $timing,
                                '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                '#suffix' => '</td>',
                            );

                        }
                        
                        // Course is not editable
                        $course_info = explode(" ", $course);
                        $dept = $course_info[0];
                        $numb = $course_info[1];

                        $form[] = array(
                            '#type' => 'item',
                            '#size' => 25,
                            '#value' => t("<a href='".$course_url."/".$dept."/".$numb."' target='_blank'><b>".$course."</a></b>"),
                            '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                            '#suffix' => '</td>',
                        );

                        // Description is not editable
                        $form[] = array(
                            '#type' => 'item',
                            '#size' => 40,
                            '#value' => t($description),
                            '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                            '#suffix' => '</td>',
                        );

                        // Pass hidden description information to the submit function.
                        $form["description_$i"] = array( '#type' => 'value', '#value' => $description );

                        if( $requirement_met ) {

                            $form[] = array(
                                '#type' => 'item',
                                '#size' => 20,
                                '#value' => $equivalent,
                                '#prefix' => '<td style="padding:0px;">',
                                '#suffix' => '</td>',
                            );

                            $form["equivalent-course_$i"] = array( '#type' => 'value', '#value' => $equivalent );

                            $form[] = array(
                                '#type' => 'item',
                                '#size' => 27,
                                '#value' => t($note),
                                '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                                '#suffix' => '</td></tr>',
                            );

                            $form["note_$i"] = array( '#type' => 'value', '#value' => $note );

                        }
                        else {

                            // If the current requirement is a required core
                            // course, disable the equivalent field.
                            if( $course_id ) {

                                $form["equivalent-course_$i"] = array(
                                    '#type' => 'textfield',
                                    '#size' => 20,
                                    '#disabled' => TRUE,
                                    '#autocomplete_path' => 'autocomp/course',
                                    '#prefix' => '<td style="padding:0px;">',
                                    '#suffix' => '</td>',
                                );

                            }

                            else {

                                $form["equivalent-course_$i"] = array(
                                    '#type' => 'textfield',
                                    '#size' => 20,
                                    '#default_value' => $equivalent,
                                    '#autocomplete_path' => 'autocomp/course',
                                    '#prefix' => '<td style="padding:0px;">',
                                    '#suffix' => '</td>',
                                );

                            }

                            $form["note_$i"] = array(
                                '#type' => 'textfield',
                                '#size' => 27,
                                '#default_value' => t($note),
                                '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                                '#suffix' => '</td></tr>',
                            );

                        }

                        $i++;

                    } // END WHILE

                }

                // Close Table
                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr></table>'),
                );

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                if( $original_program ) {

                    // Begin Table
                    $form[] = array(
                        '#type' => 'item',
                        '#value' => t('<table class="box"><tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('X'),
                        '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 5px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Mark'),
                        '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Term/Year'),
                        '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Course'),
                        '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Description'),
                        '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Equivalent'),
                        '#prefix' => '<td style="padding:0px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Note'),
                        '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                        '#suffix' => '</td></tr>',
                    );

                    $level = 1; // iteration variable for the current level.

                      //////////////////////////////////////////////////////////////
                     //////////////////////////////////////////////////////////////
                    // Iterate each program requirement
                    foreach( $original_program as $content_id => $program_info ) {

                        $requirement_id = $program_info['requirement_id'];
                        $requirement_order = $program_info['requirement_order'];
                        $requirement_met = $program_info['requirement_met'];
                        $mark = $program_info['mark'];
                        $timing = $program_info['timing'];
                        $equivalent = $program_info['equivalent_course'];
                        $equivalent_id = $program_info['equivalent_id'];
                        $note = $program_info['note'];

                        $description = $program_info['description'];
                        $course_id = $program_info['course_id'];

                        // Submit hidden student program form info
                        $form["content-id_$i"] = array( '#type' => 'value', '#value' => $content_id );
                        $form["requirement-id_$i"] = array( '#type' => 'value', '#value' => $requirement_id );
                        $form["requirement-order_$i"] = array( '#type' => 'value', '#value' => $requirement_order );

                        if( !$equivalent_id ) {

                            // If no equivalent id, get the course info from the requirement course id.
                            //$course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $course_id));
                            $course = $program_info['course'];

                            // Submit hidden course information only if there is no equivalent id. This
                            // will prevent a course_$i being passed that is not a requirement. If it
                            // were to be passed, the updating a the soft requirements would not work.
                            $form["course_$i"]    = array( '#type' => 'value', '#value' => $course );
                            $form["course-id_$i"] = array( '#type' => 'value', '#value' => $course_id );

                        }

                        else{

                            $course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $equivalent_id));
                            //$course = $program_info['course']; ?????

                        }

                        // Make sure that the course levels
                        // do not jump up one level.
                        $current_level = substr($requirement_order, 0, 1);
//echo "|".$requirement_order."|<br />";
                        if( $current_level > $level ) {
                            $level = $current_level;
                        }

                        if( $current_level == $level ) {

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                            );

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=7><b><big>' . $level . '000 Level Non Transferable Courses &nbsp;&nbsp;</big></b></td></tr>'),
                            );

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                            );

                            $level++;

                        }

                        if( ($current_level == 9) && !$other_flag ) {

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                            );

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=7><b><big>Other Non Transferable Courses &nbsp;&nbsp;</big></b></td></tr>'),
                            );

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                            );

                            $other_flag++;

                        }


                        $form["requirement-met_$i"] = array(
                            '#type' => 'checkbox',
                            '#default_value' => $requirement_met,
                            '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 2px;">',
                            '#suffix' => '</td>',
                        );

                        // If the requirements below are met,
                        // the fields are no longer editable.
                        if( $requirement_met ) {

                            $form[] = array(
                                '#type' => 'item',
                                '#value' => t($mark),
                                '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                '#suffix' => '</td>',
                            );

                            $form["mark_$i"] = array( '#type' => 'value', '#value' => $mark );

                            $form[] = array(
                                '#type' => 'item',
                                '#size' => 12,
                                '#value' => t($timing),
                                '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                '#suffix' => '</td>',
                            );

                            $form["timing_$i"] = array( '#type' => 'value', '#value' => $timing );


                        }

                        else {

                            $form["mark_$i"] = array(
                                '#type' => 'select',
                                '#options' => array(
                                                 'N/A' => t(''),
                                                  'A+' => t('A+'),
                                                  'A'  => t('A'),
                                                  'A-' => t('A-'),
                                                  'B+' => t('B+'),
                                                  'B'  => t('B'),
                                                  'B-' => t('B-'),
                                                  'C+' => t('C+'),
                                                  'C'  => t('C'),
                                                  'C-' => t('C-'),
                                                  'D'  => t('D'),
                                                  'F'  => t('F'),
                                                 'INC' => t('INC'),
                                                'PASS' => t('PASS'),
                                                   'W' => t('W'),
                                              ),
                                '#default_value' => $mark,
                                '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                '#suffix' => '</td>',
                            );

                            // TODO: MAKE THE THE DEFAULT VALUE APPEAR CLOSER TO THE CURRENT SEMESTER WHEN THE TIMING VALUE IS NOT PRESENT!
                            $form["timing_$i"] = array(
                                '#type' => 'select',
                                '#options' => array(          '' => t(''),
                                                 'W' . $minu_fiv => t('W' . $minu_fiv),
                                                 'S' . $minu_fiv => t('S' . $minu_fiv),
                                                 'F' . $minu_fiv => t('F' . $minu_fiv),
                                                 'W' . $minu_fou => t('W' . $minu_fou),
                                                 'S' . $minu_fou => t('S' . $minu_fou),
                                                 'F' . $minu_fou => t('F' . $minu_fou),
                                                 'W' . $minu_thr => t('W' . $minu_thr),
                                                 'S' . $minu_thr => t('S' . $minu_thr),
                                                 'F' . $minu_thr => t('F' . $minu_thr),
                                                 'W' . $minu_two => t('W' . $minu_two),
                                                 'S' . $minu_two => t('S' . $minu_two),
                                                 'F' . $minu_two => t('F' . $minu_two),
                                                 'W' . $minu_one => t('W' . $minu_one),
                                                 'S' . $minu_one => t('S' . $minu_one),
                                                 'F' . $minu_one => t('F' . $minu_one),
                                                 'W' . $current_year => t('W' . $current_year),
                                                 'S' . $current_year => t('S' . $current_year),
                                                 'F' . $current_year => t('F' . $current_year),
                                                 'W' . $plus_one => t('W' . $plus_one),
                                                 'S' . $plus_one => t('S' . $plus_one),
                                                 'F' . $plus_one => t('F' . $plus_one),
                                                 'W' . $plus_two => t('W' . $plus_two),
                                                 'S' . $plus_two => t('S' . $plus_two),
                                                 'F' . $plus_two => t('F' . $plus_two),
                                                 'W' . $plus_thr => t('W' . $plus_thr),
                                                 'S' . $plus_thr => t('S' . $plus_thr),
                                                 'F' . $plus_thr => t('F' . $plus_thr),
                                                 'W' . $plus_fou => t('W' . $plus_fou),
                                                 'S' . $plus_fou => t('S' . $plus_fou),
                                                 'F' . $plus_fou => t('F' . $plus_fou),
                                                 'W' . $plus_fiv => t('W' . $plus_fiv),
                                                 'S' . $plus_fiv => t('S' . $plus_fiv),
                                                 'F' . $plus_fiv => t('F' . $plus_fiv),
                                              ),
                                '#default_value' => $timing,
                                '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                '#suffix' => '</td>',
                            );

                        }
                        
                        // Course is not editable
                        $course_info = explode(" ", $course);
                        $dept = $course_info[0];
                        $numb = $course_info[1];

                        $form[] = array(
                            '#type' => 'item',
                            '#size' => 25,
                            '#value' => t("<a href='".$course_url."/".$dept."/".$numb."' target='_blank'><b>".$course."</a></b>"),
                            '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                            '#suffix' => '</td>',
                        );

                        // Description is not editable
                        $form[] = array(
                            '#type' => 'item',
                            '#size' => 40,
                            '#value' => t($description),
                            '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                            '#suffix' => '</td>',
                        );

                        // Pass hidden description information to the submit function.
                        $form["description_$i"] = array( '#type' => 'value', '#value' => $description );

                        if( $requirement_met ) {

                            $form[] = array(
                                '#type' => 'item',
                                '#size' => 20,
                                '#value' => $equivalent,
                                '#prefix' => '<td style="padding:0px;">',
                                '#suffix' => '</td>',
                            );

                            $form["equivalent-course_$i"] = array( '#type' => 'value', '#value' => $equivalent );

                            $form[] = array(
                                '#type' => 'item',
                                '#size' => 27,
                                '#value' => t($note),
                                '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                                '#suffix' => '</td></tr>',
                            );

                            $form["note_$i"] = array( '#type' => 'value', '#value' => $note );

                        }
                        else {

                            // If the current requirement is a required core
                            // course, disable the equivalent field.
                            if( $course_id ) {

                                $form["equivalent-course_$i"] = array(
                                    '#type' => 'textfield',
                                    '#size' => 20,
                                    '#disabled' => TRUE,
                                    '#autocomplete_path' => 'autocomp/course',
                                    '#prefix' => '<td style="padding:0px;">',
                                    '#suffix' => '</td>',
                                );

                            }

                            else {

                                $form["equivalent-course_$i"] = array(
                                    '#type' => 'textfield',
                                    '#size' => 20,
                                    '#default_value' => $equivalent,
                                    '#autocomplete_path' => 'autocomp/course',
                                    '#prefix' => '<td style="padding:0px;">',
                                    '#suffix' => '</td>',
                                );

                            }

                            $form["note_$i"] = array(
                                '#type' => 'textfield',
                                '#size' => 27,
                                '#default_value' => t($note),
                                '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                                '#suffix' => '</td></tr>',
                            );

                        }

                        $i++;

                    } // END WHILE

                }

                // Close Table
                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr></table>'),
                );

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                $form['complete-transfer'] = array(
                    '#type' => 'submit',
                    '#value' => t('Complete Transfer'),
                );

                $form['cancel'] = array(
                    '#type' => 'submit',
                    '#value' => t('Cancel'),
                );

                $log_info = db_fetch_array(db_query("SELECT id, session_time, advisor_id FROM {mag_session_log} WHERE session_id='%s'", $session_id));

                $log_id = $log_info['id'];
                $log_time = $log_info['session_time'];

                $advisor_id = $log_info['advisor_id'];
//echo "TEXTFIELDS > ".$textfields."<br />";
//echo "SESSION ID > ".$session_id."<br />";
//echo "SELECTION ID > ".$selection_id."<br />";
//echo "ADVISOR ID > ".$advisor_id."<br />";
//echo "STUDENT ID > ".$student_id."<br />";
//echo "LOG ID > ".$log_id."<br />";
//echo "LOG TIME > ".$log_time;

                $form['orig-program-name'] = array( '#type' => 'value', '#value' => $program );
                $form['orig-program-year'] = array( '#type' => 'value', '#value' => $program_year );
                $form['orig-program-id'] = array( '#type' => 'value', '#value' => $program_id );

                $form['trans-program-name'] = array( '#type' => 'value', '#value' => $transfer_name );
                $form['trans-program-year'] = array( '#type' => 'value', '#value' => $transfer_year );
                $form['trans-program-id'] = array( '#type' => 'value', '#value' => $transfer_id );

                $form['textfields'] = array( '#type' => 'value', '#value' => $textfields );

                $form['log-id'] = array( '#type' => 'value', '#value' => $log_id );
                $form['log-time'] = array( '#type' => 'value', '#value' => $log_time );

                $form['advisor-id'] = array( '#type' => 'value', '#value' => $advisor_id );
                $form['student-id'] = array( '#type' => 'value', '#value' => $student_id );

                $form['selection-id'] = array( '#type' => 'value', '#value' => $selection_id );

            }

            elseif( $param[1] == 'year' ) {

                drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program." / ".$program_year."<br />Transfer Program: ".$param[2]."</small></i>"));

                // Have the user select their rogram year.
                $result = db_query("SELECT DISTINCT year FROM {mag_program_identification} WHERE program='%s' ORDER BY year DESC", $param[2]);
                while( $row = db_fetch_array($result) ) {
                    $program_years[$row['year']] = $row['year'];
                }

                $form['transfer-year'] = array(
                    '#title' => t("Select transfer year"),
                    '#type'  => 'select',
                    '#options' => $program_years,
                );

                $form['transfer-confirm'] = array(
                    '#type' => 'submit',
                    '#value' => t('Submit'),
                );

                $form['cancel'] = array(
                    '#type' => 'submit',
                    '#value' => t('Cancel'),
                );

                $form['transfer-program'] = array( '#type' => 'value', '#value' => $param[2] );

            }

            else {

                drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program." / ".$program_year."<br />Transfer Program Information</small></i>"));

                // Have the user select their Program for the first time.
                $result = db_query("SELECT DISTINCT program FROM {mag_program_identification} ORDER BY program");
                while( $row = db_fetch_array($result) ) {
                    $available_progams[$row['program']] = $row['program'];
                }

                $form['transfer-program'] = array(
                    '#title' => t("Select transfer program"),
                    '#type'  => 'select',
                    '#options' => $available_progams,
                );

                $form['transfer-submit'] = array(
                    '#type' => 'submit',
                    '#value' => t('Submit'),
                );

                $form['cancel'] = array(
                    '#type' => 'submit',
                    '#value' => t('Cancel'),
                );

            }

        }

            //////////////////////////////////////////////////////////////////
           //////////////////////////////////////////////////////////////////
          ///     T   D           O
         ///    S   U   E   T   R   G   A
        ///               N   P       R   M
        elseif( $param[0] == 'student_program' ) {

            $student = db_fetch_array(db_query("SELECT id, official_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $official_selection = $student['official_selection'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];
            $student_id = $student['id'];

            $selection_id = $param[1];

            // Get the program id
            $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $selection_id ));

            // Get the program information
            $program = db_result(db_query("SELECT program FROM {mag_program_identification} WHERE id=%d", $program_id ));

            // Get the students program information
            $program_info = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $program_id ));

            $program = $program_info['program'];
            $program_year = $program_info['year'];

            drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program."<br />View student program started: ".$program_year."</small></i>"));

//echo "PROGRAM ID > ".$program_id."<br />";
//echo "SELECTION ID > ".$selection_id."<br />";
//echo "OFFICIAL SELECTION > ".$official_selection."<br />";
            
            // Apply css to change the table behaviour
            $css = '<style type="text/css">
                        table.box{
                            border-style:solid;
                            border-width:3px;
                            border-color:#C0C0C0;
                        }
                    </style>';

            drupal_set_html_head($css);

            // Begin Table
            $form[] = array(
                '#type' => 'item',
                '#value' => t('<table class="box"><tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=6></td></tr>'),
            );

            $form[] = array(
                '#type' => 'item',
                '#title' => t('Mark'),
                '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#title' => t('Term/Year'),
                '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#title' => t('Course'),
                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#title' => t('Description'),
                '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#title' => t('Equivalent'),
                '#prefix' => '<td style="padding:0px;">',
                '#suffix' => '</td>',
            );

            $form[] = array(
                '#type' => 'item',
                '#title' => t('Note'),
                '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                '#suffix' => '</td></tr>',
            );

            // Using the students current selection id, create the list of selected 
            // programs for the student including (if not already selected) a link 
            // to swith to the official program.
            $curr_selection_id = $selection_id;

            $student_selections = array();

            // Selection already selected...
            $result = db_query("SELECT id, program_id FROM {mag_student_program_selection} WHERE student_id=%d AND id != %d AND id != %d ORDER BY id", $student_id, $curr_selection_id, $official_selection );
            while( $row = db_fetch_array($result) ) {

                $student_selections[$row['id']]['program_id'] = $row['program_id'];

                $current_program = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $row['program_id'] ));

                $student_selections[$row['id']]['program'] = $current_program['program'];
                $student_selections[$row['id']]['year'] = $current_program['year'];

            }

            // Display the link to switch back to the official program.
            $student_programs .= '[<a href="' . $page_url . '">Official Program</a>] ';

            if( $student_selections ) {

                $total_programs = count($student_selections);

                foreach( $student_selections as $selid => $program_array ) {

                    $student_programs .= '[<a href="' . $page_url . '/student_program/' . $selid . '">' . $program_array['program'] . '/' . $program_array['year'] . '</a>] ';

                }

            }

            if( $student_programs ) {

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Student programs'),
                    '#value' => t($student_programs),
                );

            }

            // Get the textfield amount from the selection id.
            $textfields = db_result(db_query("SELECT COUNT(*) FROM {mag_student_program_form} WHERE selection_id=%d", $selection_id ));

            // Get all the students program requirements and order them by the requirement_order field.
            // To order the form by the requirement order and also where the requirement is met. requires
            // selecting each level respectively and putting the levels together in order.
            //      ie: WHERE course_order >= 0 AND coursce_order < 2000 etc...

             ///////////////////////////////////////////////////////////////////
            // Level 1000
            $result1 = db_query("SELECT id, content_id, log_time, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, equivalent_id, note
                                   FROM {mag_student_program_form}
                                  WHERE selection_id=%d AND requirement_order < 2000
                                  ORDER BY mark ASC, requirement_order ASC", $selection_id);

            while( $row = db_fetch_array($result1) ) {

                // If the content_id already exists in the program requirement
                // array, determine the session time for each.
                if( $program_requirement[$row['content_id']] ) {

                    $previous_time = strtotime($program_requirement[$row['content_id']]['log_time']);
                    $new_time = strtotime($row['log_time']);

                    // If the duplicate or new session time is greater
                    // than the previous session time for the current
                    // content_id, overwrite the information because
                    // this is the most current information for the
                    // selected content_id.
                    if( $new_time > $previous_time ) {

                        $program_requirement[$row['content_id']]['id'] = $row['id'];
                        $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                        $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                        $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                        $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                        $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                        $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                        $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                        $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                        $program_requirement[$row['content_id']]['note'] = $row['note'];

                    }

                }

                // If the content_id is not already in the
                // program requirement array, add it then!
                else {

                    $program_requirement[$row['content_id']]['id'] = $row['id'];
                    $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                    $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                    $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                    $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $program_requirement[$row['content_id']]['note'] = $row['note'];

                }

            }

             ///////////////////////////////////////////////////////////////////
            // Level 2000
            $result2 = db_query("SELECT id, content_id, log_time, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, equivalent_id, note
                                   FROM {mag_student_program_form}
                                  WHERE selection_id=%d AND requirement_order >= 2000 AND requirement_order < 3000
                                  ORDER BY mark ASC, requirement_order ASC", $selection_id);

            while( $row = db_fetch_array($result2) ) {

                if( $program_requirement[$row['content_id']] ) {

                    $previous_time = strtotime($program_requirement[$row['content_id']]['log_time']);
                    $new_time = strtotime($row['log_time']);

                    if( $new_time > $previous_time ) {

                        $program_requirement[$row['content_id']]['id'] = $row['id'];
                        $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                        $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                        $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                        $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                        $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                        $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                        $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                        $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                        $program_requirement[$row['content_id']]['note'] = $row['note'];

                    }

                }

                else {

                    $program_requirement[$row['content_id']]['id'] = $row['id'];
                    $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                    $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                    $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                    $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $program_requirement[$row['content_id']]['note'] = $row['note'];

                }

            }

             ///////////////////////////////////////////////////////////////////
            // Level 3000
            $result3 = db_query("SELECT id, content_id, log_time, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, equivalent_id, note
                                   FROM {mag_student_program_form}
                                  WHERE selection_id=%d AND requirement_order >= 3000 AND requirement_order < 4000
                                  ORDER BY mark ASC, requirement_order ASC", $selection_id);

            while( $row = db_fetch_array($result3) ) {

                if( $program_requirement[$row['content_id']] ) {

                    $previous_time = strtotime($program_requirement[$row['content_id']]['log_time']);
                    $new_time = strtotime($row['log_time']);

                    if( $new_time > $previous_time ) {

                        $program_requirement[$row['content_id']]['id'] = $row['id'];
                        $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                        $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                        $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                        $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                        $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                        $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                        $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                        $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                        $program_requirement[$row['content_id']]['note'] = $row['note'];

                    }

                }

                else {

                    $program_requirement[$row['content_id']]['id'] = $row['id'];
                    $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                    $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                    $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                    $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $program_requirement[$row['content_id']]['note'] = $row['note'];

                }

            }

             ///////////////////////////////////////////////////////////////////
            // Level 4000
            $result4 = db_query("SELECT id, content_id, log_time, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, equivalent_id, note
                                   FROM {mag_student_program_form}
                                  WHERE selection_id=%d AND requirement_order >= 4000 AND requirement_order < 5000
                                  ORDER BY mark ASC, requirement_order ASC", $selection_id);

            while( $row = db_fetch_array($result4) ) {

                if( $program_requirement[$row['content_id']] ) {

                    $previous_time = strtotime($program_requirement[$row['content_id']]['log_time']);
                    $new_time = strtotime($row['log_time']);

                    if( $new_time > $previous_time ) {

                        $program_requirement[$row['content_id']]['id'] = $row['id'];
                        $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                        $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                        $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                        $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                        $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                        $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                        $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                        $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                        $program_requirement[$row['content_id']]['note'] = $row['note'];

                    }

                }

                else {

                    $program_requirement[$row['content_id']]['id'] = $row['id'];
                    $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                    $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                    $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                    $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $program_requirement[$row['content_id']]['note'] = $row['note'];

                }

            }

             ///////////////////////////////////////////////////////////////////
            // Other Requirements
            $result5 = db_query("SELECT id, content_id, log_time, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, equivalent_id, note
                                   FROM {mag_student_program_form}
                                  WHERE selection_id=%d AND requirement_order >= 5000
                                  ORDER BY mark ASC, requirement_order ASC", $selection_id);

            while( $row = db_fetch_array($result5) ) {

                if( $program_requirement[$row['content_id']] ) {

                    $previous_time = strtotime($program_requirement[$row['content_id']]['log_time']);
                    $new_time = strtotime($row['log_time']);

                    if( $new_time > $previous_time ) {

                        $program_requirement[$row['content_id']]['id'] = $row['id'];
                        $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                        $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                        $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                        $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                        $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                        $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                        $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                        $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                        $program_requirement[$row['content_id']]['note'] = $row['note'];

                    }

                }

                else {

                    $program_requirement[$row['content_id']]['id'] = $row['id'];
                    $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                    $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                    $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                    $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $program_requirement[$row['content_id']]['note'] = $row['note'];

                }

            }

            // Set up the date values for the
            // display in the timing dropdown.
            $current_year = date('Y');

            $minu_fiv = $current_year - 5;
            $minu_fou = $current_year - 4;
            $minu_thr = $current_year - 3;
            $minu_two = $current_year - 2;
            $minu_one = $current_year - 1;
            $plus_one = $current_year + 1;
            $plus_two = $current_year + 2;
            $plus_thr = $current_year + 3;
            $plus_fou = $current_year + 4;
            $plus_fiv = $current_year + 5;

            $i = 0;     // iteration to keep track of when to close the table.
            $level = 1; // iteration variable for the current level.

            if( $program_requirement ) {

                  //////////////////////////////////////////////////////////////
                 //////////////////////////////////////////////////////////////
                // Iterate each program requirement
                foreach( $program_requirement as $content_id => $program_info ) {

                    //$student_program_id = $program_info['id'];
                    $requirement_id = $program_info['requirement_id'];
                    $requirement_order = $program_info['requirement_order'];
                    $requirement_met = $program_info['requirement_met'];
                    $mark = $program_info['mark'];
                    $timing = $program_info['timing'];
                    $equivalent = $program_info['equivalent_course'];
                    $equivalent_id = $program_info['equivalent_id'];
                    $note = $program_info['note'];

                    // Submit hidden student program form info
                    $form["student-program-id_$i"] = array( '#type' => 'value', '#value' => $program_info['id'] );
                    $form["requirement-id_$i"] = array( '#type' => 'value', '#value' => $requirement_id );
                    $form["requirement-order_$i"] = array( '#type' => 'value', '#value' => $requirement_order );

                    // Get the requirement info from each requirement
                    $requirement_info = db_fetch_array(db_query("SELECT course_id, description FROM {mag_program_requirement} WHERE id=%d", $requirement_id));

                    $description = $requirement_info['description'];
                    $course_id = $requirement_info['course_id'];

                    if( !$equivalent_id ) {

                        // If no equivalent id, get the course info from the requirement course id.
                        $course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $course_id));

                        // Submit hidden course information only if there is no equivalent id. This
                        // will prevent a course_$i being passed that is not a requirement. If it
                        // were to be passed, the updating a the soft requirements would not work.
                        $form["course_$i"]    = array( '#type' => 'value', '#value' => $course );
                        $form["course-id_$i"] = array( '#type' => 'value', '#value' => $course_id );

                    }

                    else{

                        $course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $equivalent_id));

                    }

                    // Make sure that the course levels
                    // do not jump up one level.
                    $current_level = substr($requirement_order, 0, 1);
                    if( $current_level > $level ) {
                        $level = $current_level;
                    }

                    if( $current_level == $level ) {

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=6></td></tr>'),
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=6><b><big>' . $level . '000 Level &nbsp;&nbsp;</big></b></td></tr>'),
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=6></td></tr>'),
                        );

                        $level++;

                    }

                    if( ($current_level == 9) && !$other_flag ) {

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=6></td></tr>'),
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=6><b><big>Other Required Courses &nbsp;&nbsp;</big></b></td></tr>'),
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=6></td></tr>'),
                        );

                        $other_flag++;

                    }

                    $form["mark_$i"] = array(
                        '#type' => 'select',
                        '#options' => array(
                                         'N/A' => t(''),
                                          'A+' => t('A+'),
                                          'A'  => t('A'),
                                          'A-' => t('A-'),
                                          'B+' => t('B+'),
                                          'B'  => t('B'),
                                          'B-' => t('B-'),
                                          'C+' => t('C+'),
                                          'C'  => t('C'),
                                          'C-' => t('C-'),
                                          'D'  => t('D'),
                                          'F'  => t('F'),
                                         'INC' => t('INC'),
                                        'PASS' => t('PASS'),
                                           'W' => t('W'),
                                      ),
                        '#default_value' => $mark,
                        '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
                        '#suffix' => '</td>',
                    );

                    // TODO: MAKE THE THE DEFAULT VALUE APPEAR CLOSER TO THE CURRENT SEMESTER WHEN THE TIMING VALUE IS NOT PRESENT!
                    $form["timing_$i"] = array(
                        '#type' => 'select',
                        '#options' => array(
                                         '' => t(''),
                                         'W' . $minu_fiv => t('W' . $minu_fiv),
                                         'S' . $minu_fiv => t('S' . $minu_fiv),
                                         'F' . $minu_fiv => t('F' . $minu_fiv),
                                         'W' . $minu_fou => t('W' . $minu_fou),
                                         'S' . $minu_fou => t('S' . $minu_fou),
                                         'F' . $minu_fou => t('F' . $minu_fou),
                                         'W' . $minu_thr => t('W' . $minu_thr),
                                         'S' . $minu_thr => t('S' . $minu_thr),
                                         'F' . $minu_thr => t('F' . $minu_thr),
                                         'W' . $minu_two => t('W' . $minu_two),
                                         'S' . $minu_two => t('S' . $minu_two),
                                         'F' . $minu_two => t('F' . $minu_two),
                                         'W' . $minu_one => t('W' . $minu_one),
                                         'S' . $minu_one => t('S' . $minu_one),
                                         'F' . $minu_one => t('F' . $minu_one),
                                         'W' . $current_year => t('W' . $current_year),
                                         'S' . $current_year => t('S' . $current_year),
                                         'F' . $current_year => t('F' . $current_year),
                                         'W' . $plus_one => t('W' . $plus_one),
                                         'S' . $plus_one => t('S' . $plus_one),
                                         'F' . $plus_one => t('F' . $plus_one),
                                         'W' . $plus_two => t('W' . $plus_two),
                                         'S' . $plus_two => t('S' . $plus_two),
                                         'F' . $plus_two => t('F' . $plus_two),
                                         'W' . $plus_thr => t('W' . $plus_thr),
                                         'S' . $plus_thr => t('S' . $plus_thr),
                                         'F' . $plus_thr => t('F' . $plus_thr),
                                         'W' . $plus_fou => t('W' . $plus_fou),
                                         'S' . $plus_fou => t('S' . $plus_fou),
                                         'F' . $plus_fou => t('F' . $plus_fou),
                                         'W' . $plus_fiv => t('W' . $plus_fiv),
                                         'S' . $plus_fiv => t('S' . $plus_fiv),
                                         'F' . $plus_fiv => t('F' . $plus_fiv),
                                      ),
                        '#default_value' => $timing,
                        '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                        '#suffix' => '</td>',
                    );

                    // Course is not editable
                    $course_info = explode(" ", $course);
                    $dept = $course_info[0];
                    $numb = $course_info[1];

                    $form[] = array(
                        '#type' => 'item',
                        '#size' => 25,
                        '#value' => t("<a href='".$course_url."/".$dept."/".$numb."' target='_blank'><b>".$course."</a></b>"),
                        '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                        '#suffix' => '</td>',
                    );

                    // Description is not editable
                    $form[] = array(
                        '#type' => 'item',
                        '#size' => 40,
                        '#value' => t($description),
                        '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                        '#suffix' => '</td>',
                    );

                    $form["description_$i"] = array( '#type' => 'value', '#value' => $description, );

                    // If the current requirement is a required core
                    // course, disable the equivalent field.
                    if( $course_id ) {

                        $form["equivalent-course_$i"] = array(
                            '#type' => 'textfield',
                            '#size' => 20,
                            '#disabled' => TRUE,
                            '#autocomplete_path' => 'autocomp/course',
                            '#prefix' => '<td style="padding:0px;">',
                            '#suffix' => '</td>',
                        );

                    }

                    else {

                        $form["equivalent-course_$i"] = array(
                            '#type' => 'textfield',
                            '#size' => 20,
                            '#default_value' => $equivalent,
                            '#autocomplete_path' => 'autocomp/course',
                            '#prefix' => '<td style="padding:0px;">',
                            '#suffix' => '</td>',
                        );

                    }

                    $form["note_$i"] = array(
                        '#type' => 'textfield',
                        '#size' => 27,
                        '#default_value' => t($note),
                        '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                        '#suffix' => '</td></tr>',
                    );

                    $i++;

                } // foreach( $program_requirement as $content_id => $program_info )

            } // if( $program_requirement )

            // Close Table
            $form[] = array(
                '#type' => 'item',
                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=6></td></tr></table>'),
            );
            
            $form['update-program'] = array(
                '#type' => 'submit',
                '#value' => t('Update program'),
            );

            $form['delete-program'] = array(
                '#type' => 'submit',
                '#value' => t('Delete program'),
            );

            $form['end-session'] = array(
                '#type' => 'submit',
                '#value' => t('End advising session'),
            );

//echo "TEXTFIELDS >> ".$textfields."<br />";
//echo "SELECTION ID >> ".$selection_id."<br />";
            // Submit hidden values for either view or create
            $form['textfields'] = array( '#type' => 'value', '#value' => $textfields );
            $form['user-name'] = array( '#type' => 'value', '#value' => $user_name );
            $form['student-id'] = array( '#type' => 'value', '#value' => $student_id );
            
            $form['selection-id'] = array( '#type' => 'value', '#value' => $selection_id );
            $form['advisor-update'] = array( '#type' => 'value', '#value' => 1 );

        }


            //////////////////////////////////////////////////////////////////
           //////////////////////////////////////////////////////////////////
          ///     C   E           O
         ///        R   A   E   R   G   A
        ///               T   P       R   M
        elseif( $param[0] == 'create' ) {

            // Get the user's student information.
            if( $in_session ) {

                $student = db_fetch_array(db_query("SELECT id, official_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

                $selection_id = $student['official_selection'];
                $first_name = $student['first_name'];
                $last_name = $student['last_name'];
                $student_id = $student['id'];
                
                $program_type = 'Create official program starting:';

            }

            else {

                $student = db_fetch_array(db_query("SELECT id, official_selection, current_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

                // If there is an advisor selection this variable will
                // indicate that this program should be displayed.
                $official_selection = $student['official_selection'];

                $selection_id = $student['current_selection'];
                $first_name = $student['first_name'];
                $last_name = $student['last_name'];
                $student_id = $student['id'];
                
                $program_type = 'Create tentative program starting:';

            }

            // Get the program id
            $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $selection_id ));

            // Get the program information
            $program = db_result(db_query("SELECT program FROM {mag_program_identification} WHERE id=%d", $program_id ));

            // Get the students program information
            $program_info = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $program_id ));

            $program = $program_info['program'];
            $program_year = $program_info['year'];

            drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program."<br />".$program_type." ".$program_year."</small></i>"));

            // Apply css to change the table behaviour
            $css = '<style type="text/css">
                        table.box{
                            border-style:solid;
                            border-width:3px;
                            border-color:#C0C0C0;
                        }
                    </style>';

            drupal_set_html_head($css);

            if( $in_session ) {

                $colspan = 7;

                // Begin Table
                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<table class="box"><tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('X'),
                    '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 5px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Mark'),
                    '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Term/Year'),
                    '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Course'),
                    '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Description'),
                    '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Equivalent'),
                    '#prefix' => '<td style="padding:0px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Note'),
                    '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                    '#suffix' => '</td></tr>',
                );

            }
            
            else {

                $colspan = 6;

                // Begin Table
                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<table class="box"><tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=6></td></tr>'),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Mark'),
                    '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 8px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Term/Year'),
                    '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Course'),
                    '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Description'),
                    '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Equivalent'),
                    '#prefix' => '<td style="padding:0px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Note'),
                    '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                    '#suffix' => '</td></tr>',
                );

            }

            $i = 0;     // iteration to keep track of when to close the table.
            $level = 1; // iteration variable for the current level.

            $current_year = date('Y');

            $minu_fiv = $current_year - 5;
            $minu_fou = $current_year - 4;
            $minu_thr = $current_year - 3;
            $minu_two = $current_year - 2;
            $minu_one = $current_year - 1;
            $plus_one = $current_year + 1;
            $plus_two = $current_year + 2;
            $plus_thr = $current_year + 3;
            $plus_fou = $current_year + 4;
            $plus_fiv = $current_year + 5;

            // Get each of the requiremnt id's for the selected program,
            // also determine the amount of textfields required to display
            // the selected program by counting the number of entries.
            $result = db_query("SELECT id, requirement_id, requirement_order, note FROM {mag_program_content} WHERE program_id=%d ORDER BY requirement_order", $program_id);
            while( $row = db_fetch_array($result) ) {

                $content_array[$row['id']]['requirement_id'] = $row['requirement_id'];
                $content_array[$row['id']]['requirement_order'] = $row['requirement_order'];
                $content_array[$row['id']]['note'] = $row['note'];

            }

            // Get the amount of table rows to display.
            $textfields = count($content_array);

            // Get additional information for the content array.
            foreach( $content_array as $cid => $content_info ) {

                $result = db_query("SELECT course_id, description, code_filter FROM {mag_program_requirement} WHERE id=%d", $content_info['requirement_id']);
                while( $row = db_fetch_array($result) ) {

                    $content_array[$cid]['course_id'] = $row['course_id'];
                    $content_array[$cid]['description'] = $row['description'];
                    $content_array[$cid]['code_filter'] = $row['code_filter'];

                    // Get the course
                    $content_array[$cid]['course'] = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $row['course_id']));

                }

            }

            $i = 0;     // iteration to keep track of when to close the table.
            $level = 1; // iteration variable for the current level.

            foreach( $content_array as $cid => $content_info ) {

                $requirement_id = $content_info['requirement_id'];
                $course_id = $content_info['course_id'];
                $course = $content_info['course'];
                $requirement_order = $content_info['requirement_order'];
                $description = $content_info['description'];
                $note = $content_info['note'];
                $code_filter = $content_info['code_filter'];

                // Make sure that the course levels
                // do not jump up one level.
                $current_level = substr($requirement_order, 0, 1);
                if( $current_level > $level ) {
                    $level = $current_level;
                }

                if( $current_level == $level ) {

                    $form[] = array(
                        '#type' => 'item',
                        '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=' . $colspan . '><b><big>' . $level . '000 Level &nbsp;&nbsp;</big></b></td></tr>'),
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                    );

                    $level++;

                }

                if( ($current_level == 9) && !$other_flag ) {

                    $form[] = array(
                        '#type' => 'item',
                        '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=' . $colspan . '><b><big>Other Required Courses &nbsp;&nbsp;</big></b></td></tr>'),
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                    );

                    $other_flag++;

                }

                if( $in_session ) {

                    $form["requirement-met_$i"] = array(
                        '#type' => 'checkbox',
                        '#default_value' => 0,
                        '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 2px;">',
                        '#suffix' => '</td>',
                    );

                    $form["mark_$i"] = array(
                        '#type' => 'select',
                        '#options' => array(
                                         'N/A' => t(''),
                                          'A+' => t('A+'),
                                          'A'  => t('A'),
                                          'A-' => t('A-'),
                                          'B+' => t('B+'),
                                          'B'  => t('B'),
                                          'B-' => t('B-'),
                                          'C+' => t('C+'),
                                          'C'  => t('C'),
                                          'C-' => t('C-'),
                                          'D'  => t('D'),
                                          'F'  => t('F'),
                                         'INC' => t('INC'),
                                        'PASS' => t('PASS'),
                                           'W' => t('W'),
                                      ),
                        '#default_value' => 'N/A',
                        '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                        '#suffix' => '</td>',
                    );

                    $form["timing_$i"] = array(
                        '#type' => 'select',
                        '#options' => array(
                                         '' => t(''),
                                         'W' . $minu_fiv => t('W' . $minu_fiv),
                                         'S' . $minu_fiv => t('S' . $minu_fiv),
                                         'F' . $minu_fiv => t('F' . $minu_fiv),
                                         'W' . $minu_fou => t('W' . $minu_fou),
                                         'S' . $minu_fou => t('S' . $minu_fou),
                                         'F' . $minu_fou => t('F' . $minu_fou),
                                         'W' . $minu_thr => t('W' . $minu_thr),
                                         'S' . $minu_thr => t('S' . $minu_thr),
                                         'F' . $minu_thr => t('F' . $minu_thr),
                                         'W' . $minu_two => t('W' . $minu_two),
                                         'S' . $minu_two => t('S' . $minu_two),
                                         'F' . $minu_two => t('F' . $minu_two),
                                         'W' . $minu_one => t('W' . $minu_one),
                                         'S' . $minu_one => t('S' . $minu_one),
                                         'F' . $minu_one => t('F' . $minu_one),
                                         'W' . $current_year => t('W' . $current_year),
                                         'S' . $current_year => t('S' . $current_year),
                                         'F' . $current_year => t('F' . $current_year),
                                         'W' . $plus_one => t('W' . $plus_one),
                                         'S' . $plus_one => t('S' . $plus_one),
                                         'F' . $plus_one => t('F' . $plus_one),
                                         'W' . $plus_two => t('W' . $plus_two),
                                         'S' . $plus_two => t('S' . $plus_two),
                                         'F' . $plus_two => t('F' . $plus_two),
                                         'W' . $plus_thr => t('W' . $plus_thr),
                                         'S' . $plus_thr => t('S' . $plus_thr),
                                         'F' . $plus_thr => t('F' . $plus_thr),
                                         'W' . $plus_fou => t('W' . $plus_fou),
                                         'S' . $plus_fou => t('S' . $plus_fou),
                                         'F' . $plus_fou => t('F' . $plus_fou),
                                         'W' . $plus_fiv => t('W' . $plus_fiv),
                                         'S' . $plus_fiv => t('S' . $plus_fiv),
                                         'F' . $plus_fiv => t('F' . $plus_fiv),
                                      ),
                        '#default_value' => $timing,
                        '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                        '#suffix' => '</td>',
                    );

                    // Course is not editable
                    $course_info = explode(" ", $course);
                    $dept = $course_info[0];
                    $numb = $course_info[1];

                    $form[] = array(
                        '#type' => 'item',
                        '#size' => 25,
                        '#value' => t("<a href='".$course_url."/".$dept."/".$numb."' target='_blank'><b>".$course."</a></b>"), // Open course in new window.
                        '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                        '#suffix' => '</td>',
                    );

                    // Pass hidden course information
                    $form["course_$i"]    = array( '#type' => 'value', '#value' => $course );
                    $form["course-id_$i"] = array( '#type' => 'value', '#value' => $course_id );

                    // Description is not editable
                    $form[] = array(
                        '#type' => 'item',
                        '#size' => 40,
                        '#value' => t($description),
                        '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                        '#suffix' => '</td>',
                    );

                    // Pass hidden description information to the submit function.
                    $form["description_$i"] = array( '#type' => 'value', '#value' => $description );

                    // If the current requirement is a required core
                    // course, disable the equivalent field.
                    if( $course_id ) {

                        $form["equivalent-course_$i"] = array(
                            '#type' => 'textfield',
                            '#size' => 20,
                            '#disabled' => TRUE,
                            '#autocomplete_path' => 'autocomp/course',
                            '#prefix' => '<td style="padding:0px;">',
                            '#suffix' => '</td>',
                        );

                    }

                    else {

                        $form["equivalent-course_$i"] = array(
                            '#type' => 'textfield',
                            '#size' => 20,
                            '#default_value' => $equivalent,
                            '#autocomplete_path' => 'autocomp/course',
                            '#prefix' => '<td style="padding:0px;">',
                            '#suffix' => '</td>',
                        );

                    }

                    $form["note_$i"] = array(
                        '#type' => 'textfield',
                        '#size' => 27,
                        '#default_value' => t($note),
                        '#prefix' => '<td style="padding:0px 4px 0px 4px">',
                        '#suffix' => '</td></tr>',
                    );

                }

                else {

                    $form["mark_$i"] = array(
                        '#type' => 'select',
                        '#options' => array(
                                         'N/A' => t(''),
                                          'A+' => t('A+'),
                                          'A'  => t('A'),
                                          'A-' => t('A-'),
                                          'B+' => t('B+'),
                                          'B'  => t('B'),
                                          'B-' => t('B-'),
                                          'C+' => t('C+'),
                                          'C'  => t('C'),
                                          'C-' => t('C-'),
                                          'D'  => t('D'),
                                          'F'  => t('F'),
                                         'INC' => t('INC'),
                                        'PASS' => t('PASS'),
                                           'W' => t('W'),
                                      ),
                        '#default_value' => 'N/A',
                        '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 8px;">',
                        '#suffix' => '</td>',
                    );

                    $form["timing_$i"] = array(
                        '#type' => 'select',
                        '#options' => array(
                                         '' => t(''),
                                         'W' . $minu_fiv => t('W' . $minu_fiv),
                                         'S' . $minu_fiv => t('S' . $minu_fiv),
                                         'F' . $minu_fiv => t('F' . $minu_fiv),
                                         'W' . $minu_fou => t('W' . $minu_fou),
                                         'S' . $minu_fou => t('S' . $minu_fou),
                                         'F' . $minu_fou => t('F' . $minu_fou),
                                         'W' . $minu_thr => t('W' . $minu_thr),
                                         'S' . $minu_thr => t('S' . $minu_thr),
                                         'F' . $minu_thr => t('F' . $minu_thr),
                                         'W' . $minu_two => t('W' . $minu_two),
                                         'S' . $minu_two => t('S' . $minu_two),
                                         'F' . $minu_two => t('F' . $minu_two),
                                         'W' . $minu_one => t('W' . $minu_one),
                                         'S' . $minu_one => t('S' . $minu_one),
                                         'F' . $minu_one => t('F' . $minu_one),
                                         'W' . $current_year => t('W' . $current_year),
                                         'S' . $current_year => t('S' . $current_year),
                                         'F' . $current_year => t('F' . $current_year),
                                         'W' . $plus_one => t('W' . $plus_one),
                                         'S' . $plus_one => t('S' . $plus_one),
                                         'F' . $plus_one => t('F' . $plus_one),
                                         'W' . $plus_two => t('W' . $plus_two),
                                         'S' . $plus_two => t('S' . $plus_two),
                                         'F' . $plus_two => t('F' . $plus_two),
                                         'W' . $plus_thr => t('W' . $plus_thr),
                                         'S' . $plus_thr => t('S' . $plus_thr),
                                         'F' . $plus_thr => t('F' . $plus_thr),
                                         'W' . $plus_fou => t('W' . $plus_fou),
                                         'S' . $plus_fou => t('S' . $plus_fou),
                                         'F' . $plus_fou => t('F' . $plus_fou),
                                         'W' . $plus_fiv => t('W' . $plus_fiv),
                                         'S' . $plus_fiv => t('S' . $plus_fiv),
                                         'F' . $plus_fiv => t('F' . $plus_fiv),
                                      ),
                        '#default_value' => $timing,
                        '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                        '#suffix' => '</td>',
                    );

                    // Course is not editable
                    $course_info = explode(" ", $course);
                    $dept = $course_info[0];
                    $numb = $course_info[1];

                    $form[] = array(
                        '#type' => 'item',
                        '#size' => 25,
                        '#value' => t("<a href='".$course_url."/".$dept."/".$numb."' target='_blank'><b>".$course."</a></b>"), // Open course in new window.
                        '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                        '#suffix' => '</td>',
                    );

                    // Pass hidden course information
                    $form["course_$i"]    = array( '#type' => 'value', '#value' => $course );
                    $form["course-id_$i"] = array( '#type' => 'value', '#value' => $course_id );

                    // Description is not editable
                    $form[] = array(
                        '#type' => 'item',
                        '#size' => 40,
                        '#value' => t($description),
                        '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                        '#suffix' => '</td>',
                    );

                    // Pass hidden description information to the submit function.
                    $form["description_$i"] = array( '#type' => 'value', '#value' => $description );

                    // If the current requirement is a required core
                    // course, disable the equivalent field.
                    if( $course_id ) {

                        $form["equivalent-course_$i"] = array(
                            '#type' => 'textfield',
                            '#size' => 20,
                            '#disabled' => TRUE,
                            '#autocomplete_path' => 'autocomp/course',
                            '#prefix' => '<td style="padding:0px;">',
                            '#suffix' => '</td>',
                        );

                    }

                    else {

                        $form["equivalent-course_$i"] = array(
                            '#type' => 'textfield',
                            '#size' => 20,
                            '#default_value' => $equivalent,
                            '#autocomplete_path' => 'autocomp/course',
                            '#prefix' => '<td style="padding:0px;">',
                            '#suffix' => '</td>',
                        );

                    }

                    $form["note_$i"] = array(
                        '#type' => 'textfield',
                        '#size' => 27,
                        '#default_value' => t($note),
                        '#prefix' => '<td style="padding:0px 4px 0px 4px">',
                        '#suffix' => '</td></tr>',
                    );

                }

                // Submit hidden table info.
                $form["content-id_$i"] = array( '#type' => 'value', '#value' => $cid );
                $form["requirement-id_$i"] = array( '#type' => 'value', '#value' => $requirement_id );
                $form["requirement-order_$i"] = array( '#type' => 'value', '#value' => $requirement_order );

                $i++;

            } // END WHILE

            // Close Table
            $form[] = array(
                '#type' => 'item',
                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr></table>'),
            );

            if( $in_session ) {

                $form['submit']['create-program'] = array(
                    '#type' => 'submit',
                    '#value' => t('Create official program'),
                );

                $form['submit']['advisor-cancel'] = array(
                    '#type' => 'submit',
                    '#value' => t('Cancel'),
                );
                
                // If in session and the session id is
                // present pass the hidden log info.
                if( $session_id ) {

                    $log_info = db_fetch_array(db_query("SELECT id, session_time FROM {mag_session_log} WHERE session_id='%s'", $session_id));

                    $log_id = $log_info['id'];
                    $log_time = $log_info['session_time'];
//echo "<br />";
//echo "?? LOG ID > ".$log_id."<br />";
//echo "?? LOG TIME > ".$log_time;

                    $form['log-id'] = array( '#type' => 'value', '#value' => $log_id );
                    $form['log-time'] = array( '#type' => 'value', '#value' => $log_time );
                    $form['session-id'] = array( '#type' => 'value', '#value' => $session_id );

                }

            }

            else {

                $form['submit']['create-program'] = array(
                    '#type' => 'submit',
                    '#value' => t('Create my program'),
                );

                $form['submit']['student-cancel'] = array(
                    '#type' => 'submit',
                    '#value' => t('Cancel'),
                );

            }

//echo "<br />";
//echo "TEXTFIELDS >> ".$textfields."<br />";
//echo "SELECTION ID >> ".$selection_id."<br />";
            // Submit hidden values
            $form['textfields'] = array( '#type' => 'value', '#value' => $textfields );
            $form['user-name'] = array( '#type' => 'value', '#value' => $user_name );
            $form['student-id'] = array( '#type' => 'value', '#value' => $student_id );
            $form['selection-id'] = array( '#type' => 'value', '#value' => $selection_id );

        } // End Create
        
        
            ////////////////////////////////////////////////////////////////////
           ////////////////////////////////////////////////////////////////////
          ///         W       G
         ///    V   E   P   O   R   M
        ///       I       R       A
        elseif( $param[0] == 'view' ) {

            // Get the user's student information.
            if( $in_session ) {

                $student = db_fetch_array(db_query("SELECT id, official_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

                $selection_id = $student['official_selection'];
                $first_name = $student['first_name'];
                $last_name = $student['last_name'];
                $student_id = $student['id'];

                $program_type = 'Official program started:';
                
            }

            else {

                $student = db_fetch_array(db_query("SELECT id, official_selection, current_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

                // If there is an advisor selection this variable will
                // indicate that this program should be displayed.
                $official_selection = $student['official_selection'];
                
                $selection_id = $student['current_selection'];
                $first_name = $student['first_name'];
                $last_name = $student['last_name'];
                $student_id = $student['id'];

                if( $official_selection == $selection_id ) {

                    $program_type = 'Official program started: ';

                    // The official program has been selected by the user,
                    // set a flag to prevent the user from editing the form.
                    $viewing_official = TRUE;

                }
                
                else {
                    $program_type = 'Tentative program started: ';
                }

            }

            // Get the program id
            $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $selection_id ));

            // Get the program information
            $program = db_result(db_query("SELECT program FROM {mag_program_identification} WHERE id=%d", $program_id ));

            // Get the students program information
            $program_info = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $program_id ));

            $program = $program_info['program'];
            $program_year = $program_info['year'];

            drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program."<br />".$program_type." ".$program_year."</small></i>"));

            // Apply css to change the table behaviour
            $css = '<style type="text/css">
                        table.box{
                            border-style:solid;
                            border-width:3px;
                            border-color:#C0C0C0;
                        }
                    </style>';

            drupal_set_html_head($css);

            // If in session and param[1] is set, an advisor is viewing the 
            // snapshot of a previous session. Remove the form controls for
            // this and increase the spacing between form elements.
            if( ($in_session && $param[1]) || $viewing_official ) {

                $colspan = 7;

                // Begin Table
                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<table class="box"><tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('X'),
                    '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Mark'),
                    '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Term/Year'),
                    '#prefix' => '<td style="padding:0px 0px 0px 25px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Course'),
                    '#prefix' => '<td style="padding:0px 0px 0px 25px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Description'),
                    '#prefix' => '<td style="padding:0px 0px 0px 25px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Equivalent'),
                    '#prefix' => '<td style="padding:0px 0px 0px 25px;">',
                    '#suffix' => '</td>',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#title' => t('Note'),
                    '#prefix' => '<td style="padding:0px 25px 0px 25px;">',
                    '#suffix' => '</td></tr>',
                );
                
            }

            else {

                if( $in_session ) {

                    $colspan = 7;

                    // Begin Table
                    $form[] = array(
                        '#type' => 'item',
                        '#value' => t('<table class="box"><tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=7></td></tr>'),
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('X'),
                        '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 5px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Mark'),
                        '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Term/Year'),
                        '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Course'),
                        '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Description'),
                        '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Equivalent'),
                        '#prefix' => '<td style="padding:0px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Note'),
                        '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                        '#suffix' => '</td></tr>',
                    );

                }

                else {

                    $colspan = 6;

                    // Begin Table
                    $form[] = array(
                        '#type' => 'item',
                        '#value' => t('<table class="box"><tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=6></td></tr>'),
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Mark'),
                        '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Term/Year'),
                        '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Course'),
                        '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Description'),
                        '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Equivalent'),
                        '#prefix' => '<td style="padding:0px;">',
                        '#suffix' => '</td>',
                    );

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Note'),
                        '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                        '#suffix' => '</td></tr>',
                    );

                }

            }

//// Determine if the current browse
//// is Microsoft's Internet Explorer.
//if( preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT']) ) {
//    $browser = "MSIE";
//}else {
//    $browser = "OK";
//}

             ///////////////////////////////////////////////////////////////////
            // Display In Session Program Information
            //
            // Selecting the official selection id, create the list of all
            // previous sessions to iterate by the date. Display results at
            // the top of the form. Also select and display any program forms
            // that the student may have created.
            if( $in_session ) {

                $curr_selection_id = $selection_id;

                // A previous log has been selected.
                if( $param[1] ) {

                    // Determine the session id for the previous log.
                    $url_selection_id = db_result(db_query("SELECT DISTINCT selection_id FROM {mag_student_program_form} WHERE log_id=%d", $param[1] ));

                    // If the previous log is from a transferred course
                    // change the selection id to retrieve the correct
                    // information to display.
                    if( $selection_id != $url_selection_id ) {

                        $selection_id = $url_selection_id;

                        // Get the program id for the previous program to display in a new title.
                        $prev_program_id = db_result(db_query("SELECT program_id FROM {mag_advisor_program_selection} WHERE selection_id=%d", $url_selection_id ));

                        $prev_program_info = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $prev_program_id ));
                        $prev_program = $prev_program_info['program'];
                        $prev_program_year = $prev_program_info['year'];

                        drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$prev_program."<br />Transfer program started: ".$prev_program_year."</small></i>"));

                    }

                }

                // Currently in session, this is the log id for the current session.
                $log_info = db_fetch_array(db_query("SELECT id, session_time FROM {mag_session_log} WHERE session_id='%s'", $session_id ));

                $log_id = $log_info['id'];
                $log_time = $log_info['session_time'];
//echo "URL LOG ID ".$param[1]."<br />";
//echo "LOG ID ".$log_id."<br />";
//echo "CURRENT SELECTION ID ".$curr_selection_id."<br />";
//echo "SELECTION ID ".$selection_id."<br />";
//echo "URL SELECTION ID ".$url_selection_id."<br />";

                $transfer_selection = $trans_sessions = array();

                   /////////////////////////////////////////////////////////////
                  // TRANSFER PROGRAM INFORMATION
                 //
                // Get the selection id's from any program that a student has transfered from.
                $result = db_query("SELECT selection_id, program_id FROM {mag_advisor_program_selection} WHERE student_id=%d", $student_id );
                while( $row = db_fetch_array($result) ) {
                    $transfer_selection[$row['selection_id']] = $row['program_id'];
                }

                if( $transfer_selection ) {

                    foreach( $transfer_selection as $sid => $pid ) {

                        // Get each session associated with the students program
                        // form. Order the results in order by their session time.
                        $result = db_query("SELECT DISTINCT log_id, log_time FROM {mag_student_program_form} WHERE selection_id=%d ORDER BY log_time", $sid );
                        while( $row = db_fetch_array($result) ) {

                            // Make sure there is a session id.
                            if( $row['log_id'] ) {

                                $trans_sessions[$row['log_id']] = $row['log_time'];

                            }

                        }

                    }

                    if( $trans_sessions ) {

                        $i = 1;
                        $trans_count = count($trans_sessions);

                        // Change the log viewing type
                        // according to the user access.
                        if( $edit_access ) {
                            $view_type = 'update';
                        }else{
                            $view_type = 'view';
                        }

                        if( $trans_count > 10 ) {

                            foreach( $trans_sessions as $db_session_id => $db_session_time ) {

                                if( $db_session_id == $log_id ) {

                                    if( !$param[1] ) {
                                        $previous_sessions .= '[<a href="' . $log_url . '">view current note</a>] ';
                                    }

                                    elseif( $param[1] != $db_session_id ) {
                                        $previous_sessions .= '[<a href="' . $page_url . '/view">current session</a>] ';
                                    }

                                }

                                else {

                                    if( $param[1] != $db_session_id ) {
                                        $previous_sessions .= '[<a href="' . $page_url . '/view/' . $db_session_id . '/' . strtotime($db_session_time) . '">'.$i.'</a>] ';
                                    }

                                    else {
                                        $previous_sessions .= '[<a href="' . $log_url . '/' . $view_type . '/' . $db_session_id . '">view note</a>] ';
                                    }

                                }

                                $i++;

                            }

                        }

                        // If the total sessions is less than 10
                        // display more information with the date
                        else {

                            foreach( $trans_sessions as $db_session_id => $db_session_time ) {

                                $time_link = strtotime($db_session_time);

                                $time_display =  date("M j Y", $time_link);

                                if( $db_session_id == $log_id ) {

                                    if( !$param[1] ) {
                                        $previous_sessions .= '[<a href="' . $log_url . '">view current note</a>] ';
                                    }

                                    elseif( $param[1] != $db_session_id ) {
                                        $previous_sessions .= '[<a href="' . $page_url . '/view">current session</a>] ';
                                    }

                                }

                                else {

                                    if( $param[1] != $db_session_id ) {
                                        $previous_sessions .= '[<a href="' . $page_url . '/view/' . $db_session_id . '/' . $time_link . '">'.$time_display.'</a>] ';
                                    }

                                    else {
                                        $previous_sessions .= '[<a href="' . $log_url . '/' . $view_type . '/' . $db_session_id . '">view note</a>] ';
                                    }

                                }

                            }

                        }

                    }

                }

                $prog_sessions = array();
                $add_session = TRUE;

                   /////////////////////////////////////////////////////////////
                  // CURRENT PROGRAM INFOMATION
                 //
                // Get each session associated with the students current program
                // form. Order the results in order by their session time.
                $result = db_query("SELECT DISTINCT log_id, log_time FROM {mag_student_program_form} WHERE selection_id=%d ORDER BY log_time", $curr_selection_id );
                while( $row = db_fetch_array($result) ) {

                    // Make sure there is a session id.
                    if( $row['log_id'] ) {

                        $prog_sessions[$row['log_id']] = $row['log_time'];

                        // If the selected id is found set
                        // a flag to avoid adding this to
                        // the output array.
                        if( $row['log_id'] == $log_id ) {
                            $add_session = FALSE;
                        }

                    }

                }

                if( $add_session ) {
                    $prog_sessions[$log_id] = $log_time;
                }

                if( $prog_sessions ) {

                    $i = 1;
                    $total_sessions = count($prog_sessions) + $trans_count;

                    // Change the log viewing type
                    // according to the user access.
                    if( $edit_access ) {
                        $view_type = 'update';
                    }else{
                        $view_type = 'view';
                    }

                    if( $total_sessions > 10 ) {

                        foreach( $prog_sessions as $db_session_id => $db_session_time ) {

                            if( $db_session_id == $log_id ) {

                                if( !$param[1] ) {
                                    $previous_sessions .= '[<a href="' . $log_url . '">view current note</a>] ';
                                }

                                elseif( $param[1] != $db_session_id ) {
                                    $previous_sessions .= '[<a href="' . $page_url . '/view">current session</a>] ';
                                }

                            }

                            else {

                                if( $param[1] != $db_session_id ) {
                                    $previous_sessions .= '[<a href="' . $page_url . '/view/' . $db_session_id . '/' . strtotime($db_session_time) . '">'.$i.'</a>] ';
                                }

                                else {
                                    $previous_sessions .= '[<a href="' . $log_url . '/' . $view_type . '/' . $db_session_id . '">view note</a>] ';
                                }

                            }

                            $i++;

                        }

                    }

                    // If the total sessions is less than 10
                    // display more information with the date
                    else {

                        foreach( $prog_sessions as $db_session_id => $db_session_time ) {

                            $time_link = strtotime($db_session_time);

                            $time_display =  date("M j Y", $time_link);

                            if( $db_session_id == $log_id ) {

                                if( !$param[1] ) {
                                    $previous_sessions .= '[<a href="' . $log_url . '">view current note</a>] ';
                                }

                                elseif( $param[1] != $db_session_id ) {
                                    $previous_sessions .= '[<a href="' . $page_url . '/view">current session</a>] ';
                                }

                            }

                            else {

                                if( $param[1] != $db_session_id ) {
//// Every other browser handles the button style wrapped link no problem. Go figure?
//$form['session-link'][] = array(
//    '#type' => 'item',
//    '#value' => t('<input type="button" value="' . $time_display . '" style="width:1in" />'),
//    '#prefix' => '<a href="' . $page_url . '/view/' . $db_session_id . '/' . $time_link . '">',
//    '#suffix' => '</a>',
//);

                                    $previous_sessions .= '[<a href="' . $page_url . '/view/' . $db_session_id . '/' . $time_link . '">'.$time_display.'</a>] ';
                                }

                                else {
                                    $previous_sessions .= '[<a href="' . $log_url . '/' . $view_type . '/' . $db_session_id . '">view note</a>] ';
                                }

                            }

                        }

                    }

                    // Display the previous session
                    // history information here.
                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Session History'),
                        '#value' => t($previous_sessions),
                    );

                    // Create the database query cutoff date
                    // depending on the selected log id.
                    if( is_numeric($param[1]) ) {

                        $cutoff_date = date("Y-m-d H:i:s", $param[2]);

                        $log_cutoff = 'AND log_time <= "' . $cutoff_date . '" ';

                    }
                    
                     ///////////////////////////////////////////////////////////
                    // Create the list of student program forms.
                    $student_selections = array();

                    // Selection already selected...
                    $result = db_query("SELECT id, program_id FROM {mag_student_program_selection} WHERE student_id=%d AND id != %d AND id != %d ORDER BY id", $student_id, $curr_selection_id, $official_selection );
                    while( $row = db_fetch_array($result) ) {

                        $student_selections[$row['id']]['program_id'] = $row['program_id'];

                        $current_program = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $row['program_id'] ));

                        $student_selections[$row['id']]['program'] = $current_program['program'];
                        $student_selections[$row['id']]['year'] = $current_program['year'];

                    }

                    if( $student_selections ) {

                        foreach( $student_selections as $selid => $program_array ) {

                            $student_programs .= '[<a href="' . $page_url . '/student_program/' . $selid . '">' . $program_array['program'] . '/' . $program_array['year'] . '</a>] ';

                        }

                    }

                    // Display the students programs.
                    if( $student_programs ) {

                        $form[] = array(
                            '#type' => 'item',
                            '#title' => t('Student programs'),
                            '#value' => t($student_programs),
                        );

                    }

                }

            }

            // Using the students current selection id, create the
            // list of selected programs for the student including
            // (if not already selected) a link to swith to the
            // official program.
            else {

                $curr_selection_id = $selection_id;

                $student_programs = array();

                // Selection already selected...
                $result = db_query("SELECT id, program_id FROM {mag_student_program_selection} WHERE student_id=%d AND id != %d AND id != %d ORDER BY id", $student_id, $curr_selection_id, $official_selection );
                while( $row = db_fetch_array($result) ) {

                    $student_programs[$row['id']]['program_id'] = $row['program_id'];

                    $current_program = db_fetch_array(db_query("SELECT program, year FROM {mag_program_identification} WHERE id=%d", $row['program_id'] ));

                    $student_programs[$row['id']]['program'] = $current_program['program'];
                    $student_programs[$row['id']]['year'] = $current_program['year'];

                }

                // Display the link to switch to the official program, only
                // if the official program form is not the current selection.
                if( $official_selection && ($official_selection != $curr_selection_id) ) {
                    $alternate_programs .= '[<a href="' . $page_url . '/switch/' . $official_selection . '/' . $student_id . '">Official Program</a>] ';
                }

                if( $student_programs ) {

                    foreach( $student_programs as $selid => $program_array ) {

                        $alternate_programs .= '[<a href="' . $page_url . '/switch/' . $selid . '/' . $student_id . '">' . $program_array['program'] . '/' . $program_array['year'] . '</a>] ';

                    }

                }

                if( $alternate_programs ) {

                    $form[] = array(
                        '#type' => 'item',
                        '#title' => t('Switch programs'),
                        '#value' => t($alternate_programs),
                    );

                }

            }

            // Get the textfield amount from the selection id.
            $textfields = db_result(db_query("SELECT COUNT(*) FROM {mag_student_program_form} WHERE selection_id=%d", $selection_id ));

            // Get all the students program requirements and order them by the requirement_order field.
            // To order the form by the requirement order and also where the requirement is met. requires
            // selecting each level respectively and putting the levels together in order.
            //      ie: WHERE course_order >= 0 AND coursce_order < 2000 etc...

             ///////////////////////////////////////////////////////////////////
            // Level 1000
            $result1 = db_query("SELECT id, content_id, log_time, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, equivalent_id, note
                                   FROM {mag_student_program_form}
                                  WHERE selection_id=%d AND requirement_order < 2000 $log_cutoff
                                  ORDER BY mark ASC, requirement_order ASC", $selection_id);

            while( $row = db_fetch_array($result1) ) {

                // If the content_id already exists in the program requirement
                // array, determine the session time for each.
                if( $program_requirement[$row['content_id']] ) {

                    $previous_time = strtotime($program_requirement[$row['content_id']]['log_time']);
                    $new_time = strtotime($row['log_time']);

                    // If the duplicate or new session time is greater
                    // than the previous session time for the current
                    // content_id, overwrite the information because
                    // this is the most current information for the
                    // selected content_id.
                    if( $new_time > $previous_time ) {

                        $program_requirement[$row['content_id']]['id'] = $row['id'];
                        $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                        $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                        $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                        $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                        $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                        $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                        $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                        $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                        $program_requirement[$row['content_id']]['note'] = $row['note'];

                    }

                }

                // If the content_id is not already in the
                // program requirement array, add it then!
                else {

                    $program_requirement[$row['content_id']]['id'] = $row['id'];
                    $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                    $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                    $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                    $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $program_requirement[$row['content_id']]['note'] = $row['note'];

                }

            }

             ///////////////////////////////////////////////////////////////////
            // Level 2000
            $result2 = db_query("SELECT id, content_id, log_time, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, equivalent_id, note
                                   FROM {mag_student_program_form}
                                  WHERE selection_id=%d AND requirement_order >= 2000 AND requirement_order < 3000 $log_cutoff
                                  ORDER BY mark ASC, requirement_order ASC", $selection_id);

            while( $row = db_fetch_array($result2) ) {

                if( $program_requirement[$row['content_id']] ) {

                    $previous_time = strtotime($program_requirement[$row['content_id']]['log_time']);
                    $new_time = strtotime($row['log_time']);

                    if( $new_time > $previous_time ) {

                        $program_requirement[$row['content_id']]['id'] = $row['id'];
                        $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                        $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                        $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                        $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                        $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                        $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                        $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                        $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                        $program_requirement[$row['content_id']]['note'] = $row['note'];

                    }

                }

                else {

                    $program_requirement[$row['content_id']]['id'] = $row['id'];
                    $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                    $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                    $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                    $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $program_requirement[$row['content_id']]['note'] = $row['note'];

                }

            }

             ///////////////////////////////////////////////////////////////////
            // Level 3000
            $result3 = db_query("SELECT id, content_id, log_time, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, equivalent_id, note
                                   FROM {mag_student_program_form}
                                  WHERE selection_id=%d AND requirement_order >= 3000 AND requirement_order < 4000 $log_cutoff
                                  ORDER BY mark ASC, requirement_order ASC", $selection_id);

            while( $row = db_fetch_array($result3) ) {

                if( $program_requirement[$row['content_id']] ) {

                    $previous_time = strtotime($program_requirement[$row['content_id']]['log_time']);
                    $new_time = strtotime($row['log_time']);

                    if( $new_time > $previous_time ) {

                        $program_requirement[$row['content_id']]['id'] = $row['id'];
                        $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                        $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                        $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                        $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                        $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                        $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                        $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                        $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                        $program_requirement[$row['content_id']]['note'] = $row['note'];

                    }

                }

                else {

                    $program_requirement[$row['content_id']]['id'] = $row['id'];
                    $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                    $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                    $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                    $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $program_requirement[$row['content_id']]['note'] = $row['note'];

                }

            }

             ///////////////////////////////////////////////////////////////////
            // Level 4000
            $result4 = db_query("SELECT id, content_id, log_time, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, equivalent_id, note
                                   FROM {mag_student_program_form}
                                  WHERE selection_id=%d AND requirement_order >= 4000 AND requirement_order < 5000 $log_cutoff
                                  ORDER BY mark ASC, requirement_order ASC", $selection_id);

            while( $row = db_fetch_array($result4) ) {

                if( $program_requirement[$row['content_id']] ) {

                    $previous_time = strtotime($program_requirement[$row['content_id']]['log_time']);
                    $new_time = strtotime($row['log_time']);

                    if( $new_time > $previous_time ) {

                        $program_requirement[$row['content_id']]['id'] = $row['id'];
                        $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                        $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                        $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                        $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                        $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                        $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                        $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                        $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                        $program_requirement[$row['content_id']]['note'] = $row['note'];

                    }

                }

                else {

                    $program_requirement[$row['content_id']]['id'] = $row['id'];
                    $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                    $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                    $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                    $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $program_requirement[$row['content_id']]['note'] = $row['note'];

                }

            }

             ///////////////////////////////////////////////////////////////////
            // Other Requirements
            $result5 = db_query("SELECT id, content_id, log_time, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, equivalent_id, note
                                   FROM {mag_student_program_form}
                                  WHERE selection_id=%d AND requirement_order >= 5000 $log_cutoff
                                  ORDER BY mark ASC, requirement_order ASC", $selection_id);

            while( $row = db_fetch_array($result5) ) {

                if( $program_requirement[$row['content_id']] ) {

                    $previous_time = strtotime($program_requirement[$row['content_id']]['log_time']);
                    $new_time = strtotime($row['log_time']);

                    if( $new_time > $previous_time ) {

                        $program_requirement[$row['content_id']]['id'] = $row['id'];
                        $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                        $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                        $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                        $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                        $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                        $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                        $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                        $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                        $program_requirement[$row['content_id']]['note'] = $row['note'];

                    }

                }

                else {

                    $program_requirement[$row['content_id']]['id'] = $row['id'];
                    $program_requirement[$row['content_id']]['log_time'] = $row['log_time'];
                    $program_requirement[$row['content_id']]['requirement_id'] = $row['requirement_id'];
                    $program_requirement[$row['content_id']]['requirement_order'] = $row['requirement_order'];
                    $program_requirement[$row['content_id']]['requirement_met'] = $row['requirement_met'];
                    $program_requirement[$row['content_id']]['mark'] = $row['mark'];
                    $program_requirement[$row['content_id']]['timing'] = $row['timing'];
                    $program_requirement[$row['content_id']]['equivalent_course'] = $row['equivalent_course'];
                    $program_requirement[$row['content_id']]['equivalent_id'] = $row['equivalent_id'];
                    $program_requirement[$row['content_id']]['note'] = $row['note'];

                }

            }

            // Set up the date values for the
            // display in the timing dropdown.
            $current_year = date('Y');

            $minu_fiv = $current_year - 5;
            $minu_fou = $current_year - 4;
            $minu_thr = $current_year - 3;
            $minu_two = $current_year - 2;
            $minu_one = $current_year - 1;
            $plus_one = $current_year + 1;
            $plus_two = $current_year + 2;
            $plus_thr = $current_year + 3;
            $plus_fou = $current_year + 4;
            $plus_fiv = $current_year + 5;

            $i = 0;     // iteration to keep track of when to close the table.
            $level = 1; // iteration variable for the current level.

            if( $program_requirement ) {

                  //////////////////////////////////////////////////////////////
                 //////////////////////////////////////////////////////////////
                // Iterate each program requirement
                foreach( $program_requirement as $content_id => $program_info ) {

                    //$student_program_id = $program_info['id'];
                    $requirement_id = $program_info['requirement_id'];
                    $requirement_order = $program_info['requirement_order'];
                    $requirement_met = $program_info['requirement_met'];
                    $mark = $program_info['mark'];
                    $timing = $program_info['timing'];
                    $equivalent = $program_info['equivalent_course'];
                    $equivalent_id = $program_info['equivalent_id'];
                    $note = $program_info['note'];

                    // Submit hidden student program form info
                    $form["student-program-id_$i"] = array( '#type' => 'value', '#value' => $program_info['id'] );
                    $form["requirement-id_$i"] = array( '#type' => 'value', '#value' => $requirement_id );
                    $form["requirement-order_$i"] = array( '#type' => 'value', '#value' => $requirement_order );

                    // Get the requirement info from each requirement
                    $requirement_info = db_fetch_array(db_query("SELECT course_id, description FROM {mag_program_requirement} WHERE id=%d", $requirement_id));

                    $description = $requirement_info['description'];
                    $course_id = $requirement_info['course_id'];

                    if( !$equivalent_id ) {

                        // If no equivalent id, get the course info from the requirement course id.
                        $course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $course_id));

                        // Submit hidden course information only if there is no equivalent id. This
                        // will prevent a course_$i being passed that is not a requirement. If it
                        // were to be passed, the updating a the soft requirements would not work.
                        $form["course_$i"]    = array( '#type' => 'value', '#value' => $course );
                        $form["course-id_$i"] = array( '#type' => 'value', '#value' => $course_id );

                    }

                    else{

                        $course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $equivalent_id));

                    }

                    // Make sure that the course levels
                    // do not jump up one level.
                    $current_level = substr($requirement_order, 0, 1);
                    if( $current_level > $level ) {
                        $level = $current_level;
                    }

                    if( $current_level == $level ) {

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=' . $colspan . '><b><big>' . $level . '000 Level &nbsp;&nbsp;</big></b></td></tr>'),
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                        );

                        $level++;

                    }

                    if( ($current_level == 9) && !$other_flag ) {

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr class="odd"><td style="padding:0px 4px 0px 4px;" align="right" colspan=' . $colspan . '><b><big>Other Required Courses &nbsp;&nbsp;</big></b></td></tr>'),
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr>'),
                        );

                        $other_flag++;

                    }

                    // Disable all form controls if an advisor is viewing a 
                    // previous form or a student is viewing the official form.
                    if( ($in_session && $param[1]) || $viewing_official ) {

                        $form[] = array(
                            '#type' => 'checkbox',
                            '#default_value' => $requirement_met,
                            '#disabled' => TRUE,
                            '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
                            '#suffix' => '</td>',
                        );

                        if( $mark == 'N/A' ) {
                            $mark = '';
                        }

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t($mark),
                            '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                            '#suffix' => '</td>',
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t($timing),
                            '#prefix' => '<td style="padding:0px 0px 0px 25px;">',
                            '#suffix' => '</td>',
                        );

                        $course_info = explode(" ", $course);
                        $dept = $course_info[0];
                        $numb = $course_info[1];

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t("<a href='".$course_url."/".$dept."/".$numb."' target='_blank'><b>".$course."</a></b>"),
                            '#prefix' => '<td style="padding:0px 0px 0px 25px;">',
                            '#suffix' => '</td>',
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t($description),
                            '#prefix' => '<td style="padding:0px 0px 0px 25px;">',
                            '#suffix' => '</td>',
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => $equivalent,
                            '#prefix' => '<td style="padding:0px 0px 0px 25px;">',
                            '#suffix' => '</td>',
                        );

                        $form[] = array(
                            '#type' => 'item',
                            '#value' => t($note),
                            '#prefix' => '<td style="padding:0px 25px 0px 25px;">',
                            '#suffix' => '</td></tr>',
                        );

                    }

                    // Display form with form controls.
                    else {

                        if( $in_session ) {

                            $form["requirement-met_$i"] = array(
                                '#type' => 'checkbox',
                                '#default_value' => $requirement_met,
                                '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 2px;">',
                                '#suffix' => '</td>',
                            );

                            // If the requirements below are met,
                            // the fields are no longer editable.
                            if( $requirement_met ) {

                                $form[] = array(
                                    '#type' => 'item',
                                    '#value' => t($mark),
                                    '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                    '#suffix' => '</td>',
                                );

                                $form["mark_$i"] = array( '#type' => 'value', '#value' => $mark );

                            }

                            else {

                                $form["mark_$i"] = array(
                                    '#type' => 'select',
                                    '#options' => array(
                                                     'N/A' => t(''),
                                                      'A+' => t('A+'),
                                                      'A'  => t('A'),
                                                      'A-' => t('A-'),
                                                      'B+' => t('B+'),
                                                      'B'  => t('B'),
                                                      'B-' => t('B-'),
                                                      'C+' => t('C+'),
                                                      'C'  => t('C'),
                                                      'C-' => t('C-'),
                                                      'D'  => t('D'),
                                                      'F'  => t('F'),
                                                     'INC' => t('INC'),
                                                    'PASS' => t('PASS'),
                                                       'W' => t('W'),
                                                  ),
                                    '#default_value' => $mark,
                                    '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                    '#suffix' => '</td>',
                                );

                            }

                            if( $requirement_met ) {

                                $form[] = array(
                                    '#type' => 'item',
                                    '#size' => 12,
                                    '#value' => t($timing),
                                    '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                    '#suffix' => '</td>',
                                );

                                $form["timing_$i"] = array( '#type' => 'value', '#value' => $timing );

                            }

                            else {

                                // TODO: MAKE THE THE DEFAULT VALUE APPEAR CLOSER TO THE CURRENT SEMESTER WHEN THE TIMING VALUE IS NOT PRESENT!
                                $form["timing_$i"] = array(
                                    '#type' => 'select',
                                    '#options' => array(
                                                     '' => t(''),
                                                     'W' . $minu_fiv => t('W' . $minu_fiv),
                                                     'S' . $minu_fiv => t('S' . $minu_fiv),
                                                     'F' . $minu_fiv => t('F' . $minu_fiv),
                                                     'W' . $minu_fou => t('W' . $minu_fou),
                                                     'S' . $minu_fou => t('S' . $minu_fou),
                                                     'F' . $minu_fou => t('F' . $minu_fou),
                                                     'W' . $minu_thr => t('W' . $minu_thr),
                                                     'S' . $minu_thr => t('S' . $minu_thr),
                                                     'F' . $minu_thr => t('F' . $minu_thr),
                                                     'W' . $minu_two => t('W' . $minu_two),
                                                     'S' . $minu_two => t('S' . $minu_two),
                                                     'F' . $minu_two => t('F' . $minu_two),
                                                     'W' . $minu_one => t('W' . $minu_one),
                                                     'S' . $minu_one => t('S' . $minu_one),
                                                     'F' . $minu_one => t('F' . $minu_one),
                                                     'W' . $current_year => t('W' . $current_year),
                                                     'S' . $current_year => t('S' . $current_year),
                                                     'F' . $current_year => t('F' . $current_year),
                                                     'W' . $plus_one => t('W' . $plus_one),
                                                     'S' . $plus_one => t('S' . $plus_one),
                                                     'F' . $plus_one => t('F' . $plus_one),
                                                     'W' . $plus_two => t('W' . $plus_two),
                                                     'S' . $plus_two => t('S' . $plus_two),
                                                     'F' . $plus_two => t('F' . $plus_two),
                                                     'W' . $plus_thr => t('W' . $plus_thr),
                                                     'S' . $plus_thr => t('S' . $plus_thr),
                                                     'F' . $plus_thr => t('F' . $plus_thr),
                                                     'W' . $plus_fou => t('W' . $plus_fou),
                                                     'S' . $plus_fou => t('S' . $plus_fou),
                                                     'F' . $plus_fou => t('F' . $plus_fou),
                                                     'W' . $plus_fiv => t('W' . $plus_fiv),
                                                     'S' . $plus_fiv => t('S' . $plus_fiv),
                                                     'F' . $plus_fiv => t('F' . $plus_fiv),
                                                  ),
                                    '#default_value' => $timing,
                                    '#prefix' => '<td style="padding:0px 0px 0px 4px;">',
                                    '#suffix' => '</td>',
                                );

                            }

                            // Course is not editable
                            $course_info = explode(" ", $course);
                            $dept = $course_info[0];
                            $numb = $course_info[1];

                            $form[] = array(
                                '#type' => 'item',
                                '#size' => 25,
                                '#value' => t("<a href='".$course_url."/".$dept."/".$numb."' target='_blank'><b>".$course."</a></b>"),
                                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                                '#suffix' => '</td>',
                            );

                            // Description is not editable
                            $form[] = array(
                                '#type' => 'item',
                                '#size' => 40,
                                '#value' => t($description),
                                '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                                '#suffix' => '</td>',
                            );

                            // Pass hidden description information to the submit function.
                            $form["description_$i"] = array( '#type' => 'value', '#value' => $description );

                            if( $requirement_met ) {

                                $form[] = array(
                                    '#type' => 'item',
                                    '#size' => 20,
                                    '#value' => $equivalent,
                                    '#prefix' => '<td style="padding:0px;">',
                                    '#suffix' => '</td>',
                                );

                                $form["equivalent-course_$i"] = array( '#type' => 'value', '#value' => $equivalent );

                            }
                            else {

                                // If the current requirement is a required core
                                // course, disable the equivalent field.
                                if( $course_id ) {

                                    $form["equivalent-course_$i"] = array(
                                        '#type' => 'textfield',
                                        '#size' => 20,
                                        '#disabled' => TRUE,
                                        '#autocomplete_path' => 'autocomp/course',
                                        '#prefix' => '<td style="padding:0px;">',
                                        '#suffix' => '</td>',
                                    );

                                }

                                else {

                                    $form["equivalent-course_$i"] = array(
                                        '#type' => 'textfield',
                                        '#size' => 20,
                                        '#default_value' => $equivalent,
                                        '#autocomplete_path' => 'autocomp/course',
                                        '#prefix' => '<td style="padding:0px;">',
                                        '#suffix' => '</td>',
                                    );

                                }

                            }

                            if( $requirement_met ) {

                                $form[] = array(
                                    '#type' => 'item',
                                    '#size' => 27,
                                    '#value' => t($note),
                                    '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                                    '#suffix' => '</td></tr>',
                                );

                                $form["note_$i"] = array( '#type' => 'value', '#value' => $note );

                            }
                            else {

                                $form["note_$i"] = array(
                                    '#type' => 'textfield',
                                    '#size' => 27,
                                    '#default_value' => t($note),
                                    '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                                    '#suffix' => '</td></tr>',
                                );

                            }

                            $i++;

                        }
                    
                        else {

                            $form["mark_$i"] = array(
                                '#type' => 'select',
                                '#options' => array(
                                                 'N/A' => t(''),
                                                  'A+' => t('A+'),
                                                  'A'  => t('A'),
                                                  'A-' => t('A-'),
                                                  'B+' => t('B+'),
                                                  'B'  => t('B'),
                                                  'B-' => t('B-'),
                                                  'C+' => t('C+'),
                                                  'C'  => t('C'),
                                                  'C-' => t('C-'),
                                                  'D'  => t('D'),
                                                  'F'  => t('F'),
                                                 'INC' => t('INC'),
                                                'PASS' => t('PASS'),
                                                   'W' => t('W'),
                                              ),
                                '#default_value' => $mark,
                                '#prefix' => '<tr class="odd"><td style="padding:0px 0px 0px 10px;">',
                                '#suffix' => '</td>',
                            );

                            // TODO: MAKE THE THE DEFAULT VALUE APPEAR CLOSER TO THE CURRENT SEMESTER WHEN THE TIMING VALUE IS NOT PRESENT!
                            $form["timing_$i"] = array(
                                '#type' => 'select',
                                '#options' => array(
                                                 '' => t(''),
                                                 'W' . $minu_fiv => t('W' . $minu_fiv),
                                                 'S' . $minu_fiv => t('S' . $minu_fiv),
                                                 'F' . $minu_fiv => t('F' . $minu_fiv),
                                                 'W' . $minu_fou => t('W' . $minu_fou),
                                                 'S' . $minu_fou => t('S' . $minu_fou),
                                                 'F' . $minu_fou => t('F' . $minu_fou),
                                                 'W' . $minu_thr => t('W' . $minu_thr),
                                                 'S' . $minu_thr => t('S' . $minu_thr),
                                                 'F' . $minu_thr => t('F' . $minu_thr),
                                                 'W' . $minu_two => t('W' . $minu_two),
                                                 'S' . $minu_two => t('S' . $minu_two),
                                                 'F' . $minu_two => t('F' . $minu_two),
                                                 'W' . $minu_one => t('W' . $minu_one),
                                                 'S' . $minu_one => t('S' . $minu_one),
                                                 'F' . $minu_one => t('F' . $minu_one),
                                                 'W' . $current_year => t('W' . $current_year),
                                                 'S' . $current_year => t('S' . $current_year),
                                                 'F' . $current_year => t('F' . $current_year),
                                                 'W' . $plus_one => t('W' . $plus_one),
                                                 'S' . $plus_one => t('S' . $plus_one),
                                                 'F' . $plus_one => t('F' . $plus_one),
                                                 'W' . $plus_two => t('W' . $plus_two),
                                                 'S' . $plus_two => t('S' . $plus_two),
                                                 'F' . $plus_two => t('F' . $plus_two),
                                                 'W' . $plus_thr => t('W' . $plus_thr),
                                                 'S' . $plus_thr => t('S' . $plus_thr),
                                                 'F' . $plus_thr => t('F' . $plus_thr),
                                                 'W' . $plus_fou => t('W' . $plus_fou),
                                                 'S' . $plus_fou => t('S' . $plus_fou),
                                                 'F' . $plus_fou => t('F' . $plus_fou),
                                                 'W' . $plus_fiv => t('W' . $plus_fiv),
                                                 'S' . $plus_fiv => t('S' . $plus_fiv),
                                                 'F' . $plus_fiv => t('F' . $plus_fiv),
                                              ),
                                '#default_value' => $timing,
                                '#prefix' => '<td style="padding:0px 0px 0px 5px;">',
                                '#suffix' => '</td>',
                            );

                            // Course is not editable
                            $course_info = explode(" ", $course);
                            $dept = $course_info[0];
                            $numb = $course_info[1];

                            $form[] = array(
                                '#type' => 'item',
                                '#size' => 25,
                                '#value' => t("<a href='".$course_url."/".$dept."/".$numb."' target='_blank'><b>".$course."</a></b>"),
                                '#prefix' => '<td style="padding:0px 0px 0px 10px;">',
                                '#suffix' => '</td>',
                            );

                            // Description is not editable
                            $form[] = array(
                                '#type' => 'item',
                                '#size' => 40,
                                '#value' => t($description),
                                '#prefix' => '<td style="padding:0px 0px 0px 15px;">',
                                '#suffix' => '</td>',
                            );
                            
                            $form["description_$i"] = array( '#type' => 'value', '#value' => $description, );

                            // If the current requirement is a required core
                            // course, disable the equivalent field.
                            if( $course_id ) {

                                $form["equivalent-course_$i"] = array(
                                    '#type' => 'textfield',
                                    '#size' => 20,
                                    '#disabled' => TRUE,
                                    '#autocomplete_path' => 'autocomp/course',
                                    '#prefix' => '<td style="padding:0px;">',
                                    '#suffix' => '</td>',
                                );

                            }

                            else {

                                $form["equivalent-course_$i"] = array(
                                    '#type' => 'textfield',
                                    '#size' => 20,
                                    '#default_value' => $equivalent,
                                    '#autocomplete_path' => 'autocomp/course',
                                    '#prefix' => '<td style="padding:0px;">',
                                    '#suffix' => '</td>',
                                );

                            }

                            $form["note_$i"] = array(
                                '#type' => 'textfield',
                                '#size' => 27,
                                '#default_value' => t($note),
                                '#prefix' => '<td style="padding:0px 4px 0px 4px;">',
                                '#suffix' => '</td></tr>',
                            );

                            $i++;

                        }
                        
                    }

                } // foreach( $program_requirement as $content_id => $program_info )

            } // if( $program_requirement )

            // Close Table
            $form[] = array(
                '#type' => 'item',
                '#value' => t('<tr bgcolor="#C0C0C0"><td style="padding:1.5px 0px 1.5px 0px;" colspan=' . $colspan . '></td></tr></table>'),
            );

            if( !$in_session ) {

                if( !$viewing_official ) {
                    
                    $form['update-program'] = array(
                        '#type' => 'submit',
                        '#value' => t('Update my program'),
                    );

                    $form['delete-program'] = array(
                        '#type' => 'submit',
                        '#value' => t('Delete my program'),
                    );

                }

                $program_max = daedalus_get_setting("student program total");

//echo "TOTAL PROGRAMS > ".$total_programs."<br />";
//echo "PROGRAM MAX > ".$program_max."<br />";

                // If the user has reached the allowable
                // amount of programs disable the ability
                // to add another program.
                if( $total_programs < $program_max ) {

                    $form['add-program'] = array(
                        '#type' => 'submit',
                        '#value' => t('Add new program'),
                    );

                }

                else {

                    $form['add-program'] = array(
                        '#type' => 'submit',
                        '#value' => t('Add new program'),
                        '#disabled' => TRUE,
                    );

                }

                $form['email-advisor'] = array(
                    '#type' => 'submit',
                    '#value' => t('Email an advisor'),
                );

            }

            else {

                // If another session is being viewed then
                // send a flag to the validate form not to
                // make any updates to the form. This can
                // only be done while in the current session.
                if( !$param[1] ) {

                    $form['update-program'] = array(
                        '#type' => 'submit',
                        '#value' => t('Update program'),
                    );

                }

                $form['delete-program'] = array(
                    '#type' => 'submit',
                    '#value' => t('Delete program'),
                );

                $form['transfer-program'] = array(
                    '#type' => 'submit',
                    '#value' => t('Transfer program'),
                );

                $form['end-session'] = array(
                    '#type' => 'submit',
                    '#value' => t('End advising session'),
                );
                
                if( $session_id ) {
//echo "<br />";
//echo "?? LOG ID > ".$log_id."<br />";
//echo "?? LOG TIME > ".$log_time."<br />";
                    $form['log-id'] = array( '#type' => 'value', '#value' => $log_id );
                    $form['log-time'] = array( '#type' => 'value', '#value' => $log_time );
                    $form['session-id'] = array( '#type' => 'value', '#value' => $session_id );
                    $form['selection-id'] = array( '#type' => 'value', '#value' => $selection_id );

                }

            }

//echo "TEXTFIELDS >> ".$textfields."<br />";
//echo "SELECTION ID >> ".$selection_id."<br />";
            // Submit hidden values for either view or create
            $form['textfields'] = array( '#type' => 'value', '#value' => $textfields );
            $form['user-name'] = array( '#type' => 'value', '#value' => $user_name );
            $form['student-id'] = array( '#type' => 'value', '#value' => $student_id );

        }

        else {
            echo "<br />WTF DO I DO NOW?<br />";
        }

    } // END if( !$no_session )

    else {

        drupal_set_message(t('You must be in session to view a students program form.'));

        drupal_goto($base_url."/".daedalus_get_setting("advise student"));

    }

    return $form;

} // function daedalus_browse_programs_form( $form )


/**
 * Implementation of hook_validate().
 */
function daedalus_my_program_form_validate( $form, &$form_state ) {

//    if( $form_state['values']['op'] == $form_state['values']['submit-program'] || $form_state['values']['op'] == $form_state['values']['submit-program2'] ) {
//
//        if( !$form_state['values']['first-name'] ) {
//            form_set_error('first-name', t('The field <b>First Name</b> is required.'));
//        }
//
//        if( !$form_state['values']['last-name'] ) {
//            form_set_error('last-name', t('The field <b>Last Name</b> is required.'));
//        }
//
//    }
    
    switch($form_state['values']['op']) {

        case $form_state['values']['submit-program']:
        case $form_state['values']['submit-program2']:
            
            if( !$form_state['values']['first-name'] ) {
                form_set_error('first-name', t('The field <b>First Name</b> is required.'));
            }

            if( !$form_state['values']['last-name'] ) {
                form_set_error('last-name', t('The field <b>Last Name</b> is required.'));
            }

            break;
        
        case $form_state['values']['update-program']:

            // Make sure the equivalent courses are in the 
            // proper format of department CSCI number 1100.
            $textfields = $form_state['values']['textfields'];

            for( $i = 0; $i < $textfields; $i++ ) {

                $equivalent_course = $form_state['values']["equivalent-course_$i"];
       
                if( $equivalent_course ) {

                    $equivalent_info = explode(" ", $equivalent_course);
                    $dept = $equivalent_info[0];
                    $numb = $equivalent_info[1];

                    if( strlen($equivalent_course) != 9 ) {
                        form_set_error("equivalent-course_$i", t('The equivalent course is invalid.'));
                    }

                    elseif( preg_match("/[0-9]/", $dept) ) {
                        form_set_error("equivalent-course_$i", t('The equivalent course department is invalid.'));
                    }
                    
                    elseif( strlen($numb) != 4 || !is_numeric($numb) ) {
                        form_set_error("equivalent-course_$i", t('The equivalent course number is invalid.'));
                    }

                }

            }
            
            break;

    }

}


/**
 * Implementation of hook_submit().
 */
function daedalus_my_program_form_submit( $form, &$form_state ) {

    global $base_url;
    $page_url = daedalus_get_setting("program form");

    $program = $form_state['values']['program'];
    $program_year = $form_state['values']['effective-year'];

    $user_name  = $form_state['values']['user-name'];
    $first_name = $form_state['values']['first-name'];
    $last_name  = $form_state['values']['last-name'];

    switch($form_state['values']['op']) {

        case $form_state['values']['add-program']:

            drupal_goto($base_url."/".$page_url."/add_program");

            break;

        case $form_state['values']['end-session']:

            db_query("DELETE FROM {mag_advisor_session} WHERE id=%d", $form_state['values']['session-id'] );

            drupal_set_message(t('Current session closed.'));

            // This generates a new session id.
            $_COOKIE[session_regenerate_id()];

            drupal_goto($base_url."/".daedalus_get_setting("advise student"));

            break;

        case $form_state['values']['submit-program']:

            db_query("INSERT INTO {mag_student_identification} (user_name, first_name, last_name)
                               VALUES ('%s','%s','%s')", $user_name, $first_name, $last_name );

            drupal_goto($base_url."/".$page_url."/submit/".$program);

            break;

        case $form_state['values']['submit-program2']:

            drupal_goto($base_url."/".$page_url."/submit/".$program);

            break;

        case $form_state['values']['submit-new-program']:

            drupal_goto($base_url."/".$page_url."/add_program/".$program);

            break;

        case $form_state['values']['email-advisor']:

            drupal_goto($base_url."/".$page_url."/email_advisor");

            break;

        case $form_state['values']['submit-email']:

            // Get the advisors email address
            $to = db_result(db_query("SELECT mail FROM {users} WHERE uid=%d", $form_state['values']['selected-advisor'] ));

            $subject = "Academic advising has been requested by ".$form_state['values']['first-name']." ".$form_state['values']['last-name'];

            $message = $form_state['values']['advisor-message'];

            if( mail($to, $subject, $message) ) {
                drupal_set_message($form_state['values']['first-name']." ".$form_state['values']['last-name'].", your request for academic advising has been sent.");
            } else {
                drupal_set_message($form_state['values']['first-name']." ".$form_state['values']['last-name'].", your request for academic advising has <b>failed</b> to be sent.");
            }

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['transfer-program']:

            drupal_goto($base_url."/".$page_url."/transfer/");

            break;

        case $form_state['values']['transfer-submit']:

            drupal_goto($base_url."/".$page_url."/transfer/year/".$form_state['values']['transfer-program']);

            break;

        case $form_state['values']['transfer-confirm']:

            drupal_goto($base_url."/".$page_url."/transfer/confirm/".$form_state['values']['transfer-program']."/".$form_state['values']['transfer-year']);

            break;

        case $form_state['values']['advisor-submit-year']:

            $student_id = $form_state['values']['student-id'];

            $program_id = db_result(db_query("SELECT id FROM {mag_program_identification} WHERE program='%s' AND year=%d", $program, $program_year ));

            db_query("INSERT INTO {mag_student_program_selection} (student_id, program_id) VALUES (%d,%d)", $student_id, $program_id );

            $official_selection = db_result(db_query("SELECT id FROM {mag_student_program_selection} WHERE student_id=%d AND program_id=%d", $student_id, $program_id ));

            db_query("UPDATE {mag_student_identification} SET official_selection=%d WHERE id=%d", $official_selection, $student_id );

            drupal_goto($base_url."/".$page_url."/create/");

            break;

        case $form_state['values']['student-submit-year']:
            
            $student_id = $form_state['values']['student-id'];

            $program_id = db_result(db_query("SELECT id FROM {mag_program_identification} WHERE program='%s' AND year=%d", $program, $program_year ));

            db_query("INSERT INTO {mag_student_program_selection} (student_id, program_id) VALUES (%d,%d)", $student_id, $program_id );

            $current_selection = db_result(db_query("SELECT id FROM {mag_student_program_selection} WHERE student_id=%d AND program_id=%d", $student_id, $program_id ));

            db_query("UPDATE {mag_student_identification} SET current_selection=%d WHERE id=%d", $current_selection, $student_id );

            drupal_goto($base_url."/".$page_url."/create/".$current_selection);

            break;

        case $form_state['values']['submit-new-year']:
//
// FIND A WAY TO ALLOW TO ADD THE SAME PROGRAM AS THE OFFICIAL SELECTION
//
            $student_id = $form_state['values']['student-id'];

            $program_id = db_result(db_query("SELECT id FROM {mag_program_identification} WHERE program='%s' AND year=%d", $program, $program_year ));

            if( db_result(db_query("SELECT COUNT(*) FROM {mag_student_program_selection} WHERE student_id=%d AND program_id=%d", $student_id, $program_id )) ) {

                drupal_set_message(t('Program creation failed, you can not add duplicate programs.'), 'warning');

                drupal_goto($base_url."/".$page_url."/view");

            }

            else {

                db_query("INSERT INTO {mag_student_program_selection} (student_id, program_id) VALUES (%d,%d)", $student_id, $program_id );

                $current_selection = db_result(db_query("SELECT id FROM {mag_student_program_selection} WHERE student_id=%d AND program_id=%d", $student_id, $program_id ));

                db_query("UPDATE {mag_student_identification} SET current_selection=%d WHERE id=%d", $current_selection, $student_id );

                drupal_goto($base_url."/".$page_url."/create/".$current_selection);

            }

            break;

        case $form_state['values']['cancel']:

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['advisor-cancel']:

            // Update the student identification to remove the program and the effective year value.
            db_query("UPDATE {mag_student_identification} SET official_selection='' WHERE user_name='%s'", $user_name);

            db_query("DELETE FROM {mag_student_program_selection} WHERE id=%d", $form_state['values']['selection-id']);

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['student-cancel']:

            // Update the student identification to remove the program and the effective year value.
            db_query("UPDATE {mag_student_identification} SET current_selection='' WHERE user_name='%s'", $user_name);

            db_query("DELETE FROM {mag_student_program_selection} WHERE id=%d", $form_state['values']['selection-id']);

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['submit-selected-program']:

            db_query("UPDATE {mag_student_identification} SET current_selection=%d WHERE user_name='%s'", $form_state['values']['selected-program'], $user_name );

            drupal_goto($base_url."/".$page_url."/view");

            break;

        case $form_state['values']['complete-transfer']:

            $textfields = $form_state['values']['textfields'];

            $log_time = $form_state['values']['log-time'];
            $log_id = $form_state['values']['log-id'];

            $program_name = $form_state['values']['orig-program-name'];
            $program_year = $form_state['values']['orig-program-year'];
            $program_id = $form_state['values']['orig-program-id'];

            $transfer_name = $form_state['values']['trans-program-name'];
            $transfer_year = $form_state['values']['trans-program-year'];
            $transfer_id = $form_state['values']['trans-program-id'];    // the program id

            $advisor_id = $form_state['values']['advisor-id'];
            $student_id = $form_state['values']['student-id'];

            $selection_id = $form_state['values']['selection-id'];

            // Update the session log, set the boolean 'entry' field to one which will
            // inform that this session has changes logged and should not be deleted.
            db_query("UPDATE {mag_session_log} SET entry='1' WHERE id=%d", $log_id);

            db_query("INSERT INTO {mag_advisor_program_selection} (selection_id, advisor_id, student_id, program_id) VALUES (%d,%d,%d,%d)", $selection_id, $advisor_id, $student_id, $program_id );

            db_query("DELETE FROM {mag_student_program_selection} WHERE id=%d", $selection_id);

            db_query("INSERT INTO {mag_student_program_selection} (student_id, program_id) VALUES (%d,%d)", $student_id, $transfer_id );

            $transfer_selection = db_result(db_query("SELECT id FROM {mag_student_program_selection} WHERE student_id=%d AND program_id=%d", $student_id, $transfer_id ));

            db_query("UPDATE {mag_student_identification} SET official_selection=%d WHERE id=%d", $transfer_selection, $student_id );

            for( $i = 0; $i < $textfields; $i++ ) {

                $content_id = $form_state['values']["content-id_$i"];
                $requirement_id = $form_state['values']["requirement-id_$i"];
                $requirement_order = $form_state['values']["requirement-order_$i"];
                $requirement_met = $form_state['values']["requirement-met_$i"];
                $mark = $form_state['values']["mark_$i"];
                $timing = $form_state['values']["timing_$i"];
                $course = $form_state['values']["course_$i"];
                $course_id = $form_state['values']["course-id_$i"];
                $description = $form_state['values']["description_$i"];
                $equivalent_course = $form_state['values']["equivalent-course_$i"];
                $note = $form_state['values']["note_$i"];

                // Insert the program core course requirement info indicated
                // by the $course element. This value will mean there is no
                // unique equivalent course information to be added.
                if( $course ) {

                    db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, note)
                                   VALUES (%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s','%s')", $transfer_selection, $log_id, $log_time, $content_id, $requirement_id, $requirement_order, $requirement_met, $mark, $timing, $equivalent_course, $note);

                }

                // If the equivalent course is located in daedalus and is mapped,
                // get the course id and enter the value as the equivalent id.
                elseif( db_result(db_query("SELECT COUNT(*) FROM {dae_course} WHERE course='%s' AND mapped=1", $equivalent_course)) ) {

                    $equivalent_id = db_result(db_query("SELECT id FROM {dae_course} WHERE course='%s'", $equivalent_course));

                    // Determine the course level.
                    $equivalent_info = explode(" ", $equivalent_course);
                    $equivalent_order = $equivalent_info[1];

                    db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_id, note)
                                   VALUES (%d,%d,'%s',%d,%d,%d,'%s','%s',%d,'%s')", $transfer_selection, $log_id, $log_time, $content_id, $requirement_id, $equivalent_order, $requirement_met, $mark, $timing, $equivalent_id, $note);

                }

                // Insert any other requirement.
                elseif( $description && !$course ) {

                    // If the equivalent course is present, parse the
                    // value and enter the equivalent order value.
                    if( $equivalent_course ) {

                        // Determine the course level.
                        $equivalent_info = explode(" ", $equivalent_course);
                        $equivalent_order = $equivalent_info[1];

                        db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, note)
                                       VALUES (%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s','%s')", $transfer_selection, $log_id, $log_time, $content_id, $requirement_id, $equivalent_order, $requirement_met, $mark, $timing, $equivalent_course, $note);

                    }

                    // Finally just add the
                    // requirement values.
                    else {

                        db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, note)
                                       VALUES (%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s','%s')", $transfer_selection, $log_id, $log_time, $content_id, $requirement_id, $requirement_order, $requirement_met, $mark, $timing, $equivalent_course, $note);

                    }

                }

            }

            drupal_set_message(t("Program transfer from ".$program_name." ".$program_year." to ".$transfer_name." ".$transfer_year." completed."));

            drupal_goto($base_url."/".$page_url."/view/");

            break;

        case $form_state['values']['create-program']:

            $textfields = $form_state['values']['textfields'];
            $log_time = $form_state['values']['log-time'];
            $log_id = $form_state['values']['log-id'];

            // If the log id and time are present an
            // advisor has created an official program.
            if( $log_id && $log_time ) {

                $official_selection = $form_state['values']['selection-id'];

                // Update the session log, set the boolean 'entry' field to one which will
                // inform that this session has changes logged and should not be deleted.
                db_query("UPDATE {mag_session_log} SET entry='1' WHERE id=%d", $log_id);

                for( $i = 0; $i < $textfields; $i++ ) {

                    $content_id = $form_state['values']["content-id_$i"];
                    $requirement_id = $form_state['values']["requirement-id_$i"];
                    $requirement_order = $form_state['values']["requirement-order_$i"];
                    $requirement_met = $form_state['values']["requirement-met_$i"];
                    $mark = $form_state['values']["mark_$i"];
                    $timing = $form_state['values']["timing_$i"];
                    $course = $form_state['values']["course_$i"];
                    $course_id = $form_state['values']["course-id_$i"];
                    $description = $form_state['values']["description_$i"];
                    $equivalent_course = $form_state['values']["equivalent-course_$i"];
                    $note = $form_state['values']["note_$i"];

                    // Insert the program core course requirement info indicated 
                    // by the $course element. This value will mean there is no 
                    // unique equivalent course information to be added.
                    if( $course ) {

                        db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, note)
                                       VALUES (%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s','%s')", $official_selection, $log_id, $log_time, $content_id, $requirement_id, $requirement_order, $requirement_met, $mark, $timing, $equivalent_course, $note);

                    }

                    // If the equivalent course is located in daedalus and is mapped,
                    // get the course id and enter the value as the equivalent id.
                    elseif( db_result(db_query("SELECT COUNT(*) FROM {dae_course} WHERE course='%s' AND mapped=1", $equivalent_course)) ) {

                        $equivalent_id = db_result(db_query("SELECT id FROM {dae_course} WHERE course='%s'", $equivalent_course));

                        // Determine the course level.
                        $equivalent_info = explode(" ", $equivalent_course);
                        $equivalent_order = $equivalent_info[1];

                        db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_id, note)
                                       VALUES (%d,%d,'%s',%d,%d,%d,'%s','%s',%d,'%s')", $official_selection, $log_id, $log_time, $content_id, $requirement_id, $equivalent_order, $requirement_met, $mark, $timing, $equivalent_id, $note);

                    }

                    // Insert any other requirement.
                    elseif( $description && !$course ) {

                        // If the equivalent course is present, parse the
                        // value and enter the equivalent order value.
                        if( $equivalent_course ) {

                            // Determine the course level.
                            $equivalent_info = explode(" ", $equivalent_course);
                            $equivalent_order = $equivalent_info[1];

                            db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, note)
                                           VALUES (%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s','%s')", $official_selection, $log_id, $log_time, $content_id, $requirement_id, $equivalent_order, $requirement_met, $mark, $timing, $equivalent_course, $note);

                        }

                        // Finally just add the 
                        // requirement values.
                        else {

                            db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, note)
                                           VALUES (%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s','%s')", $official_selection, $log_id, $log_time, $content_id, $requirement_id, $requirement_order, $requirement_met, $mark, $timing, $equivalent_course, $note);

                        }

                    }

                }

            }

            else {

                $current_selection = $form_state['values']['selection-id'];

                for( $i = 0; $i < $textfields; $i++ ) {

                    $content_id = $form_state['values']["content-id_$i"];
                    $requirement_id = $form_state['values']["requirement-id_$i"];
                    $requirement_order = $form_state['values']["requirement-order_$i"];
                    $mark = $form_state['values']["mark_$i"];
                    $timing = $form_state['values']["timing_$i"];
                    $course = $form_state['values']["course_$i"];
                    $course_id = $form_state['values']["course-id_$i"];
                    $description = $form_state['values']["description_$i"];
                    $equivalent_course = $form_state['values']["equivalent-course_$i"];
                    $note = $form_state['values']["note_$i"];

                    // Insert the program core course requirement info indicated 
                    // by the $course element. This value will mean there is no 
                    // unique equivalent course information to be added.
                    if( $course ) {

                        db_query("INSERT INTO {mag_student_program_form} (selection_id, content_id, requirement_id, requirement_order, mark, timing, equivalent_course, note)
                                       VALUES (%d,%d,%d,%d,'%s','%s','%s','%s')", $current_selection, $content_id, $requirement_id, $requirement_order, $mark, $timing, $equivalent_course, $note);

                    }

                    // If the equivalent course is located in daedalus and is mapped,
                    // get the course id and enter the value as the equivalent id.
                    elseif( db_result(db_query("SELECT COUNT(*) FROM {dae_course} WHERE course='%s' AND mapped=1", $equivalent_course)) ) {

                        $equivalent_id = db_result(db_query("SELECT id FROM {dae_course} WHERE course='%s'", $equivalent_course));

                        // Determine the course level.
                        $equivalent_info = explode(" ", $equivalent_course);
                        $equivalent_order = $equivalent_info[1];

                        db_query("INSERT INTO {mag_student_program_form} (selection_id, content_id, requirement_id, requirement_order, mark, timing, equivalent_id, note)
                                       VALUES (%d,%d,%d,%d,'%s','%s',%d,'%s')", $current_selection, $content_id, $requirement_id, $equivalent_order, $mark, $timing, $equivalent_id, $note);

                    }

                    // Insert any other requirement.
                    elseif( $description && !$course ) {

                        // If the equivalent course is present, parse the
                        // value and enter the equivalent order value.
                        if( $equivalent_course ) {

                            // Determine the course level.
                            $equivalent_info = explode(" ", $equivalent_course);
                            $equivalent_order = $equivalent_info[1];

                            db_query("INSERT INTO {mag_student_program_form} (selection_id, content_id, requirement_id, requirement_order, mark, timing, equivalent_course, note)
                                           VALUES (%d,%d,%d,%d,'%s','%s','%s','%s')", $current_selection, $content_id, $requirement_id, $equivalent_order, $mark, $timing, $equivalent_course, $note);

                        }
                        else {

                            db_query("INSERT INTO {mag_student_program_form} (selection_id, content_id, requirement_id, requirement_order, mark, timing, equivalent_course, note)
                                           VALUES (%d,%d,%d,%d,'%s','%s','%s','%s')", $current_selection, $content_id, $requirement_id, $requirement_order, $mark, $timing, $equivalent_course, $note);

                        }

                    }

                }

            }

            drupal_set_message("The program has been created.");

            drupal_goto($base_url."/".$page_url."/view/");

            break;

        case $form_state['values']['update-program']:

            $textfields = $form_state['values']['textfields'];
            $log_time = $form_state['values']['log-time'];
            $log_id = $form_state['values']['log-id'];

            // Logid and time, advisor is modifying
            // a students official program form.
            if( $log_id && $log_time ) {

                $official_selection = $form_state['values']['selection-id'];

                for( $i = 0; $i < $textfields; $i++ ) {

                    // Get the information from the program form.
                    $student_program_id  = $form_state['values']["student-program-id_$i"];
                    $requirement_id = $form_state['values']["requirement-id_$i"];
                    $requirement_met = $form_state['values']["requirement-met_$i"];
                    $requirement_order = $form_state['values']["requirement-order_$i"];
                    $mark = $form_state['values']["mark_$i"];
                    $timing = $form_state['values']["timing_$i"];
                    $course = $form_state['values']["course_$i"];
                    $course_id = $form_state['values']["course-id_$i"];
                    $description = $form_state['values']["description_$i"];
                    $equivalent_course = $form_state['values']["equivalent-course_$i"];
                    $equivalent_id = db_result(db_query("SELECT id FROM {dae_course} WHERE course='%s'", $equivalent_course));
                    $note = $form_state['values']["note_$i"];

                    // Get the database values for the current program requirement.
                    $row = db_fetch_array(db_query("SELECT * FROM {mag_student_program_form} WHERE id=%d", $student_program_id));

                    $db_content_id = $row['content_id'];
                    $db_log_id = $row['log_id'];
                    $db_requirement_id = $row['requirement_id'];
                    $db_requirement_met = $row['requirement_met'];
                    $db_requirement_order = $row['requirement_order'];
                    $db_mark = $row['mark'];
                    $db_timing = $row['timing'];
                    $db_equivalent_course = $row['equivalent_course'];
                    $db_equivalent_id = db_result(db_query("SELECT id FROM {dae_course} WHERE course='%s'", $db_equivalent_course));
                    $db_note = $row['note'];

                    // Only apply changes if the database values
                    // are different from the program form.
                    if( $db_requirement_id != $requirement_id ||
                        $db_requirement_met != $requirement_met ||
                        $db_requirement_order != $requirement_order ||
                        $db_mark != $mark ||
                        $db_timing != $timing ||
                        $db_equivalent_course != $equivalent_course ||
                        $db_equivalent_id != $equivalent_id ||
                        $db_note != $note ) {

//drupal_goto($base_url."/".$page_url."/view/id_".$student_program_id."_".$row['id']."/rid_".$requirement_id."_".$row['requirement_id']."/rmet_".$requirement_met."_".$row['requirement_met']."/reqord_".$requirement_order."_".$row['requirement_order']."/mark_".$mark."_".$row['mark']."/timing_".$timing."_".$row['timing']."/eqcour_".$equivalent_course."_".$row['equivalent_course']."/eqid_".$equivalent_id)."_".$row['equivalent_id'];

                        // Update the session log, set the boolean 'entry' field to one which will
                        // inform that this session has changes logged and should not be deleted.
                        db_query("UPDATE {mag_session_log} SET entry='1' WHERE id=%d", $log_id);

                        // Insert the program requirement and program
                        // content if a course is entered.
                        if( $course ) {

                            // If changes are made in the current session
                            // update the values, if it is not the current
                            // session insert the values into the database.
                            if( $db_log_id != $log_id ) {

                                db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, note)
                                               VALUES (%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s')", $official_selection, $log_id, $log_time, $db_content_id, $requirement_id, $requirement_order, $requirement_met, $mark, $timing, $note);

                            }

                            else {

                                db_query("UPDATE {mag_student_program_form} SET requirement_id=%d, requirement_order=%d, requirement_met=%d, mark='%s', timing='%s', note='%s'
                                                                          WHERE id=%d AND content_id=%d", $requirement_id, $requirement_order, $requirement_met, $mark, $timing, $note, $student_program_id, $db_content_id);

                            }

                        }

                        // If the equivalent course is located in daedalus and is mapped,
                        // get the course id and enter the value as the equivalent id.
                        elseif( db_result(db_query("SELECT COUNT(*) FROM {dae_course} WHERE course='%s' AND mapped=1", $equivalent_course)) ) {

                            $equivalent_id = db_result(db_query("SELECT id FROM {dae_course} WHERE course='%s'", $equivalent_course));

                            // Determine the course level.
                            $equivalent_info = explode(" ", $equivalent_course);
                            $equivalent_order = $equivalent_info[1];

                            // Enter a blank equivalent course.
                            $equivalent_course = '';

                            if( $db_log_id != $log_id ) {

                                db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_id, note)
                                          VALUES (%d,%d,'%s',%d,%d,%d,%d,'%s','%s',%d,'%s')", $official_selection, $log_id, $log_time, $db_content_id, $requirement_id, $equivalent_order, $requirement_met, $mark, $timing, $equivalent_id, $note);

                            }

                            else {

                                db_query("UPDATE {mag_student_program_form} SET requirement_id=%d, requirement_order=%d, requirement_met=%d, mark='%s', timing='%s', equivalent_course='%s', equivalent_id=%d, note='%s'
                                           WHERE id=%d AND content_id=%d", $requirement_id, $equivalent_order, $requirement_met, $mark, $timing, $equivalent_course, $equivalent_id, $note, $student_program_id, $db_content_id);

                            }

                        }

                        // Insert the program requirement and program
                        // content if there is no course entered.
                        elseif( $description && !$course ) {

                            if( db_result(db_query("SELECT equivalent_id FROM {mag_student_program_form} WHERE id=%d", $student_program_id)) && $equivalent_course ) {

                                // Determine the course level.
                                $equivalent_info = explode(" ", $equivalent_course);
                                $equivalent_order = $equivalent_info[1];

                                $equivalent_id = '';

                                if( $db_log_id != $log_id ) {

                                    db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, note)
                                              VALUES (%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s','%s')", $official_selection, $log_id, $log_time, $db_content_id, $requirement_id, $equivalent_order, $requirement_met, $mark, $timing, $equivalent_course, $note);

                                }

                                else {

                                    db_query("UPDATE {mag_student_program_form} SET requirement_id=%d, requirement_order=%d, requirement_met=%d, mark='%s', timing='%s', equivalent_course='%s', equivalent_id=%d, note='%s'
                                                WHERE id=%d AND content_id=%d", $requirement_id, $equivalent_order, $requirement_met, $mark, $timing, $equivalent_course, $equivalent_id, $note, $student_program_id, $db_content_id);

                                }

                            }

                            else {

                                if( $db_log_id != $log_id ) {

                                    db_query("INSERT INTO {mag_student_program_form} (selection_id, log_id, log_time, content_id, requirement_id, requirement_order, requirement_met, mark, timing, equivalent_course, note)
                                              VALUES (%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s','%s')", $official_selection, $log_id, $log_time, $db_content_id, $requirement_id, $requirement_order, $requirement_met, $mark, $timing, $equivalent_course, $note);

                                }

                                else {

                                    db_query("UPDATE {mag_student_program_form} SET requirement_id=%d, requirement_order=%d, requirement_met=%d, mark='%s', timing='%s', equivalent_course='%s', note='%s'
                                               WHERE id=%d AND content_id=%d", $requirement_id, $requirement_order, $requirement_met, $mark, $timing, $equivalent_course, $note, $student_program_id, $db_content_id);

                                }

                            }

                        }

                    }

                }

            }

            // If no log information simply update
            // a students selected program from.
            else {

                $advisor_update = $form_state['values']['advisor-update'];
                $selection_id = $form_state['values']['selection-id'];

                for( $i = 0; $i < $textfields; $i++ ) {

                    // Get the information from the program form.
                    $student_program_id  = $form_state['values']["student-program-id_$i"];
                    $requirement_id = $form_state['values']["requirement-id_$i"];
                    $requirement_order = $form_state['values']["requirement-order_$i"];
                    $mark = $form_state['values']["mark_$i"];
                    $timing = $form_state['values']["timing_$i"];
                    $course = $form_state['values']["course_$i"];
                    $course_id = $form_state['values']["course-id_$i"];
                    $description = $form_state['values']["description_$i"];
                    $equivalent_course = $form_state['values']["equivalent-course_$i"];
                    $equivalent_id = db_result(db_query("SELECT id FROM {dae_course} WHERE course='%s'", $equivalent_course));
                    $note = $form_state['values']["note_$i"];

//if( $i==2 ) {
//    drupal_goto($base_url."/".$page_url."/".$course."/".$equivalent_course."/".$description);
//}
                    // Insert the program requirement and program
                    // content if a course is entered.
                    if( $course ) {

                        db_query("UPDATE {mag_student_program_form} SET requirement_id=%d, requirement_order=%d, mark='%s', timing='%s', note='%s'
                                                                  WHERE id=%d", $requirement_id, $requirement_order, $mark, $timing, $note, $student_program_id);

                    }

                    // If the equivalent course is located in daedalus and is mapped,
                    // get the course id and enter the value as the equivalent id.
                    elseif( db_result(db_query("SELECT COUNT(*) FROM {dae_course} WHERE course='%s' AND mapped=1", $equivalent_course)) ) {

                        $equivalent_id = db_result(db_query("SELECT id FROM {dae_course} WHERE course='%s'", $equivalent_course));

                        // Determine the course level.
                        $equivalent_info = explode(" ", $equivalent_course);
                        $equivalent_order = $equivalent_info[1];

                        // Enter a blank equivalent course.
                        $equivalent_course = '';

                        db_query("UPDATE {mag_student_program_form} SET requirement_id=%d, requirement_order=%d, mark='%s', timing='%s', equivalent_course='%s', equivalent_id=%d, note='%s'
                                   WHERE id=%d", $requirement_id, $equivalent_order, $mark, $timing, $equivalent_course, $equivalent_id, $note, $student_program_id);

                    }

                    // Insert the program requirement and program
                    // content if there is no course entered.
                    elseif( $description && !$course ) {

                        if( db_result(db_query("SELECT equivalent_id FROM {mag_student_program_form} WHERE id=%d", $student_program_id)) && $equivalent_course ) {

                            // Determine the course level.
                            $equivalent_info = explode(" ", $equivalent_course);
                            $equivalent_order = $equivalent_info[1];

                            $equivalent_id = '';

                            db_query("UPDATE {mag_student_program_form} SET requirement_id=%d, requirement_order=%d, mark='%s', timing='%s', equivalent_course='%s', equivalent_id=%d, note='%s'
                                        WHERE id=%d", $requirement_id, $equivalent_order, $mark, $timing, $equivalent_course, $equivalent_id, $note, $student_program_id);

                        }

                        else {

                            db_query("UPDATE {mag_student_program_form} SET requirement_id=%d, requirement_order=%d, mark='%s', timing='%s', equivalent_course='%s', note='%s'
                                       WHERE id=%d", $requirement_id, $requirement_order, $mark, $timing, $equivalent_course, $note, $student_program_id);

                        }

                    }

                }

            }

            drupal_set_message("Program has been updated.");

            if( $advisor_update ) {
                drupal_goto($base_url."/".$page_url."/student_program/".$selection_id);
            }
            
            else {
                drupal_goto($base_url."/".$page_url."/view/");
            }

            break;

        case $form_state['values']['delete-program']:
            
            $advisor_update = $form_state['values']['advisor-update'];
            $selection_id = $form_state['values']['selection-id'];

            if( $advisor_update ) {
                drupal_goto($base_url."/".$page_url."/delete/".$selection_id);
            }
            
            else {
                drupal_goto($base_url."/".$page_url."/delete");
            }            

            break;

        case $form_state['values']['delete-reverse']:

            $advisor_selection = $form_state['values']['advisor-selection'];

            if( $advisor_selection ) {
                drupal_goto($base_url."/".$page_url."/student_program/".$advisor_selection);
            }
            
            else {
                drupal_goto($base_url."/".$page_url."/view");
            }

            break;

        case $form_state['values']['delete-forward']:

            // One or the other will be available.
            $current_selection = $form_state['values']['current-selection'];
            
            $official_selection = $form_state['values']['official-selection'];

            if( $current_selection ) {

                db_query("DELETE FROM {mag_student_program_form} WHERE selection_id=%d", $current_selection );

                db_query("DELETE FROM {mag_student_program_selection} WHERE id=%d", $current_selection );

                db_query("UPDATE {mag_student_identification} SET current_selection='' WHERE user_name='%s'", $user_name );

                drupal_set_message(t('The program <b>'.$program.'</b> started in <b>'.$program_year.'</b> has been deleted. <br/><i>Please select a new program.</i>'));

            }

            if( $advisor_selection ) {

                db_query("DELETE FROM {mag_student_program_form} WHERE selection_id=%d", $advisor_selection );

                db_query("DELETE FROM {mag_student_program_selection} WHERE id=%d", $advisor_selection );
                
                if( db_result(db_query("SELECT COUNT(*) FROM {mag_student_identification} WHERE official_selection=%d AND user_name='%s'", $advisor_selection, $user_name )) ) {
                    db_query("UPDATE {mag_student_identification} SET official_selection='' WHERE user_name='%s'", $user_name );
                }
                
                if( db_result(db_query("SELECT COUNT(*) FROM {mag_student_identification} WHERE current_selection=%d AND user_name='%s'", $advisor_selection, $user_name )) ) {
                    db_query("UPDATE {mag_student_identification} SET current_selection='' WHERE user_name='%s'", $user_name );
                }

                drupal_set_message(t('The program <b>'.$program.'</b> started in <b>'.$program_year.'</b> has been deleted.'));

            }

            elseif( $official_selection ) {

                db_query("DELETE FROM {mag_student_program_form} WHERE selection_id=%d", $official_selection );

                db_query("DELETE FROM {mag_student_program_selection} WHERE id=%d", $official_selection );

                db_query("UPDATE {mag_student_identification} SET official_selection='' WHERE user_name='%s'", $user_name );

                drupal_set_message(t('The program <b>'.$program.'</b> started in <b>'.$program_year.'</b> has been deleted. <br/><i>Please select a new program.</i>'));

            }

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['dae-help-submit']:

            $page_url_length = sizeof(explode('/',$page_url));

            $param[0] = arg(0+$page_url_length);
            $param[1] = arg(1+$page_url_length);
            $param[2] = arg(2+$page_url_length);

            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message(t('Help information saved.'));

                    if( $param[0] && $param[1] && $param[2] ) {
                        drupal_goto($base_url."/".$page_url."/".$param[0]."/".$param[1]."/".$param[2]);
                    }elseif( $param[0] && $param[1] ) {
                        drupal_goto($base_url."/".$page_url."/".$param[0]."/".$param[1]);
                    }elseif( $param[0]) {
                        drupal_goto($base_url."/".$page_url."/".$param[0]);
                    }else {
                        drupal_goto($base_url."/".$page_url);
                    }
            }

            break;

    }

}


//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Program->Form Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_program_map_page() {
    return drupal_get_form("daedalus_program_map_form");
};

/**
* Menu Location: Daedalus -> Browse -> Map
* URL Location:  daedalus/daedalus/program/map
*
* Displays
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_program_map_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Determine the user name
    global $user;
    $user_id = $user->uid;

    // If the current user is a Magellan Advisor determine if there is a current advising session open.
    if( db_result(db_query("SELECT COUNT(*) FROM {role, users_roles} WHERE role.name='Magellan Advisor' AND users_roles.uid=%d AND users_roles.rid = role.rid", $user_id)) ) {

        $result = db_query("SELECT id, student_id FROM {mag_advisor_session} WHERE advisor_id=%d", $user_id);
        while( $row = db_fetch_array($result) ) {
            $session_id  = $row['id'];
            $session_sid = $row['student_id'];
        }

        // Get the session name.
        $current_session = $_COOKIE[session_name()];

        // If in session, set the user name as
        // to the selected students username.
        if( $session_id == $current_session ) {

            $in_session = TRUE;
            $user_name = db_result(db_query("SELECT user_name FROM {mag_student_identification} WHERE id=%d", $session_sid));

        }

        // Not in session, set flag to redirect the
        // advisor back to select a student to advise.
        else {
            $no_session = TRUE;
            // TODO: REMOVE, ONLY HERE WHILE ACCESS GIVEN TO "daedalus manage"
            $user_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $user_id));
        }

    }

    // User is not an advisor, select
    // the current users username.
    else {
        // Determine the user name
        $user_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $user_id));
    }

    // If the user is an advisor, the advisor must be currently be in
    // a session to view the program form information of a student. If
    // a student is viewing their program information they will never be
    // redirected because they do not have advisor privilidges, the flag
    // will never be set to true for a student.
    if( !$no_session || user_access("daedalus manage") ) {      // **** user_access("daedalus manage) TEMPORARY ONLY TO ALLOW FOR EASIER DEVELOPMENT ****** REMOVE LATER *****

        // Get current page url.
        $page_url = $help_url = daedalus_get_setting("program form");

        // Get page urls.
        $course_url = $base_url."/".daedalus_get_setting("manage courses");

        // Get access information
        $build_access = user_access("daedalus build");

        // Store URL Parameters in $param array.
        $page_url_length = sizeof(explode('/',$page_url));
        $page_url = $base_url."/".$page_url;

        $param = array();

        $param[0] = arg(0+$page_url_length);    // "view/create", determines if the program has been selected or is being created.
        $param[1] = arg(1+$page_url_length);    // $program, the selected program.
        $param[2] = arg(2+$page_url_length);    // $year, the selected program year.

        // Get the images.
        $computer_science_logo       = $base_url."/".daedalus_get_setting("computer science logo");
        $small_computer_science_logo = $base_url."/".daedalus_get_setting("computer science logo small");

        // Get the user's student information.
        if( $in_session ) {

            $student = db_fetch_array(db_query("SELECT id, official_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $selection_id = $student['official_selection'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];
            $student_id = $student['id'];

            $advising_text = 'Advising:';

            $official_text = 'Official Program Map:';

        }

        else {

            $student = db_fetch_array(db_query("SELECT id, official_selection, current_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $official_selection = $student['official_selection'];
            $selection_id = $student['current_selection'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];
            $student_id = $student['id'];
            
            if( $selection_id == $official_selection ) {
                $official_text = 'Official Program Map:';
            }
            
            else {
                $official_text = 'Program Map:';
            }

        }

        // Get the program id
        $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $selection_id ));

        // Get the program information
        $program = db_result(db_query("SELECT program FROM {mag_program_identification} WHERE id=%d", $program_id ));

        // If the program is not selected, redirect to the program form
        // page and set a warning to inform the user to select a program.
        if( !$program_id ) {

            drupal_set_message(t('You must first select a program before you can view your program map.'), 'warning');

            drupal_goto($base_url."/".daedalus_get_setting("program form"));

        }else {

            // Determine if the graphviz directory is properly installed
            // and get the working directory and path for the dot program.
            $installed = graphviz_installed($pwd, $graphviz_path);

            if( $installed ) {

                drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program."<br />".$official_text."</small></i>"));

                $form = array();

                // Add the hidden help form. Paramaters are
                // (help url, show border, show break).
                $form = display_hidden_form($help_url, 1, 1);

                  //////////////////////////////////////////////////////////////////////////
                 //////////////////////////////////////////////////////////////////////////
                // CREATE THE MAP

                $map_output  = "digraph Constellation {\n";
                $map_output .= "size=\"10,13\"\n";

                $core_courses = $completed_courses = array();

                // Determine the core courses for the selected users program.
                $result = db_query("SELECT requirement_id FROM {mag_program_content} WHERE program_id=%d", $program_id);
                while( $row = db_fetch_array($result) ) {
                    $requirement_ids[] = $row['requirement_id'];
                }

                if( $requirement_ids ) {

                    // Create the list of placeholders to accommodate a variable number of arguments.
                    $placeholders = implode(' OR ', array_fill(0, count($requirement_ids), "id=%d"));

                    $result = db_query("SELECT course_id FROM {mag_program_requirement} WHERE course_id!=0 AND ($placeholders) ORDER BY course_id", $requirement_ids);
                    while( $row = db_fetch_array($result) ) {
                        $core_courses[$row['course_id']] = $row['course_id'];
                    }

                    // Determine which courses have been completed by the user
                    $result = db_query("SELECT mark, requirement_id, equivalent_id FROM {mag_student_program_form} WHERE mark!='N/A' AND selection_id=%d", $selection_id);
                    while( $row = db_fetch_array($result) ) {

                        // If an equivalent course is valid it has now
                        // become apart of the core required courses.
                        if( $row['equivalent_id'] ) {
                            $core_courses[$row['equivalent_id']] = $row['equivalent_id'];
                        }

                        switch( $row['mark'] ) {
                            case "A+":
                            case "A":
                            case "A-":
                            case "B+":
                            case "B":
                            case "B-":
                            case "C+":
                            case "C":
                            case "C-":

                                // If an equivalent course is completed 
                                // add it to the completed course list.
                                if( $row['equivalent_id'] ) {
                                    $completed_courses[$row['equivalent_id']] = $row['equivalent_id'];
                                }

                                else {

                                    // Select the course id from the requirement.
                                    $completed_id = db_result(db_query("SELECT course_id FROM {mag_program_requirement} WHERE id=%d", $row['requirement_id']));

                                    $completed_courses[$completed_id] = $completed_id;

                                }

                            break;

                        }

                    }

                    // Create the list of placeholders to accommodate a variable number of arguments.
                    $placeholders = implode(' OR ', array_fill(0, count($core_courses), "id=%d"));

                    $result = db_query("SELECT * FROM {dae_course} WHERE $placeholders", $core_courses);
                    while( $row = db_fetch_array($result) ) {

                        if( $row['mapped'] || $build_access ) {

                            $current_url = $course_url."/".$row['course_code']."/".$row['course_number'];

                            if( in_array($row['id'], $completed_courses) ) {
                                $map_output .= " course".$row['id']." [shape=box, style=\"rounded,filled\", label=\"".$row['course']."\", URL=\"".$current_url."\", target=\"_blank\", fontsize=9];\n";
                            }
                            else {
                                $map_output .= " course".$row['id']." [shape=box, style=\"rounded\", label=\"".$row['course']."\", URL=\"".$current_url."\", target=\"_blank\", fontsize=9];\n";
                            }

                        }else {

                            $current_url = "http://www.registrar.dal.ca/calendar/class.php?subj=".$row['course_code']."&num=".$row['course_number'];

                            if( in_array($row['id'], $completed_courses) ) {
                                $map_output .= " course".$row['id']." [shape=box, style=\"rounded,filled\", label=\"".$row['course']."\", URL=\"".$current_url."\", target=\"_blank\", fontsize=9];\n";
                            }
                            else {
                                $map_output .= " course".$row['id']." [shape=box, style=\"rounded\", label=\"".$row['course']."\", URL=\"".$current_url."\", target=\"_blank\", fontsize=9];\n";
                            }
                        }
                    }

                    $completed_courses = array_diff($completed_courses, $core_courses);

                } // if( $requirement_ids )

                if( $completed_courses ) {

                    // Create the list of placeholders to accommodate a variable number of arguments.
                    $placeholders = implode(' OR ', array_fill(0, count($completed_courses), "id=%d"));

                    $result = db_query("SELECT * FROM {dae_course} WHERE $placeholders", $completed_courses);
                    while( $row = db_fetch_array($result) ) {

                        if( $row['mapped'] || $build_access ) {

                            $current_url = $course_url."/".$row['course_code']."/".$row['course_number'];

                            if( in_array($row['id'], $completed_courses) ) {
                                $map_output .= " course".$row['id']." [shape=box, style=\"rounded,filled\", label=\"".$row['course']."\", URL=\"".$current_url."\", target=\"_blank\", fontsize=9];\n";
                            }

                            else {
                                $map_output .= " course".$row['id']." [shape=box, style=\"rounded\", label=\"".$row['course']."\", URL=\"".$current_url."\", target=\"_blank\", fontsize=9];\n";
                            }

                        }

                        else {

                            $current_url = "http://www.registrar.dal.ca/calendar/class.php?subj=".$row['course_code']."&num=".$row['course_number'];

                            if( in_array($row['id'], $completed_courses) ) {
                                $map_output .= " course".$row['id']." [shape=box, style=\"rounded,filled\", label=\"".$row['course']."\", URL=\"".$current_url."\", target=\"_blank\", fontsize=9];\n";
                            }

                            else {
                                $map_output .= " course".$row['id']." [shape=box, style=\"rounded\", label=\"".$row['course']."\", URL=\"".$current_url."\", target=\"_blank\", fontsize=9];\n";
                            }
                        }
                    }
                }

                // Show the prerequiste links for each core course.
                foreach( $core_courses as $cid ) {

                    $result = db_query("SELECT prereq_id, set_id FROM {dae_prereq_course} WHERE course_id=%d", $cid);
                    while( $row = db_fetch_array($result) ) {

                        // Add dashed lines to indicate a set.
                        if( $row['set_id'] > 1 ) {

                            if( in_array($row['prereq_id'], $core_courses) ) {
                                $map_output .= " course".$row['prereq_id']." -> course".$cid." [style=\"setlinewidth(2), dashed\"];\n";
                            }

                            elseif( in_array($row['prereq_id'], $completed_courses) ) {
                                $map_output .= " course".$row['prereq_id']." -> course".$cid." [style=\"setlinewidth(2), dashed\"];\n";
                            }

                        }

                        else {

                            if( in_array($row['prereq_id'], $core_courses) ) {
                                $map_output .= " course".$row['prereq_id']." -> course".$cid." [style=\"setlinewidth(2)\"];\n";
                            }

                            elseif( in_array($row['prereq_id'], $completed_courses) ) {
                                $map_output .= " course".$row['prereq_id']." -> course".$cid." [style=\"setlinewidth(2)\"];\n";
                            }

                        }
//                        if( in_array($row['prereq_id'], $core_courses) ) {
//                            $map_output .= " course".$row['prereq_id']." -> course".$cid." [style=\"setlinewidth(2)\"];\n";
//                        }
//
//                        elseif( in_array($row['prereq_id'], $completed_courses) ) {
//                            $map_output .= " course".$row['prereq_id']." -> course".$cid." [style=\"setlinewidth(2)\"];\n";
//                        }
                    }
                }

                $map_output .= "}";

                  //////////////////////////////////////////////////////////////////////////
                 //////////////////////////////////////////////////////////////////////////
                // SAVE THE MAP OUTPUT AND CREATE THE GRAPH

                // Remove old user maps
                system('rm '.$pwd.'/graphviz/program'.$student_id.'*');

                // Get the iteration to append to the file.
                // This prevents the browser from displaying
                // the cached file because the location changes.
                $iteration = daedalus_get_setting("graphviz iteration");

                // Update the iteration value
                db_query("UPDATE {dae_settings} SET VALUE=%d WHERE setting='%s'", $iteration+1, "graphviz iteration");

                // The file name
                $program_map = "program".$student_id."_map".$iteration;

                // Write the .png output to file
                $myFile = $pwd."/graphviz/".$program_map.".dot";
                $fileHandle = fopen($myFile, 'w+') or die("can't open file");
                fwrite($fileHandle, $map_output);
                fclose($fileHandle);

                // Execute dot to create the map output.
                system($graphviz_path.'dot '.$pwd.'/graphviz/'.$program_map.'.dot -Tcmapx -o '.$pwd.'/graphviz/'.$program_map.'.map');

                // Execute dot to create the gif output.
                system($graphviz_path.'dot '.$pwd.'/graphviz/'.$program_map.'.dot -Tgif -o '.$pwd.'/graphviz/'.$program_map.'.gif');

                daedalus_parse_imagemap( $pwd.'/graphviz/'.$program_map.'.map' );

                // Store the newly creatd map into a variable
                // This is appended to the HTML to make the
                // gif image file linkable.
                $myFile = $pwd."/graphviz/".$program_map.".map";
                $file = fopen($myFile, "rb") or exit("Unable to open file!");

                while(!feof($file)) {
                    $map_file .= fgets($file);
                }

                fclose($file);

                  //////////////////////////////////////////////////////////////////////////
                 //////////////////////////////////////////////////////////////////////////
                // DISPLAY THE INFORMATION FOR THE USER

                $program_map_img = "<img id='Constellation' src='".$base_url."/graphviz/".$program_map.".gif' usemap='#Constellation' />";

                // The goal map legend image url.
                $goal_legend = "<img src='".$base_url."/".daedalus_get_setting("goal legend")."' align='top' />";

                // Display the legend with the user map. Add the map file to
                // the prefix. This is required to make the image map functional.
                $form[] = array(
                    '#type' => 'item',
                    '#prefix' => t($map_file."<br />".$program_map_img),
                );

            }

            else {
                drupal_set_message(t('There is a problem with the graphviz installation. Please contact an administrator.'), 'warning');
            }

        }

        // If in a session display session logout button.
        if( $in_session ) {

            $form['end-session'] = array(
                '#type' => 'submit',
                '#value' => t('End advising session'),
                '#prefix' => '<br /><br />'
            );

            // Submit the hidden session id.
            $form['$session_id'] = array( '#type' => 'value', '#value' => $session_id, );

        }

        return $form;

    } // END if( !$no_session )

    else {

        drupal_set_message(t('You must be in session to view a students program map.'));

        drupal_goto($base_url."/".daedalus_get_setting("advise student"));

    }
}


/**
 * Implementation of hook_submit().
 */
function daedalus_program_map_form_submit( $form, &$form_state ) {

    switch($form_state['values']['op']) {

        case $form_state['values']['end-session']:

            db_query("DELETE FROM {mag_advisor_session} WHERE id=%d", $form_state['values']['session-id']);

            drupal_set_message(t('Current session closed.'));

            // This generates a new session id.
            $_COOKIE[session_regenerate_id()];

            drupal_goto($base_url."/".daedalus_get_setting("advise student"));

            break;

    }
}


//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Program->Strengths Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_program_strengths_page() {
    return drupal_get_form("daedalus_program_strengths_form");
};

/**
* Menu Location: Daedalus -> Program -> Strengths
* URL Location:  daedalus/daedalus/program/strengths
*
* Displays 
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_program_strengths_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Determine the user name
    global $user;
    $user_id = $user->uid;

    // If the current user is a Magellan Advisor determine if there is a current advising session open.
    if( db_result(db_query("SELECT COUNT(*) FROM {role, users_roles} WHERE role.name='Magellan Advisor' AND users_roles.uid=%d AND users_roles.rid = role.rid", $user_id)) ) {

        $result = db_query("SELECT id, student_id FROM {mag_advisor_session} WHERE advisor_id=%d", $user_id);
        while( $row = db_fetch_array($result) ) {
            $session_id  = $row['id'];
            $session_sid = $row['student_id'];
        }

        // Get the session name.
        $current_session = $_COOKIE[session_name()];

        // If in session, set the user name as
        // to the selected students username.
        if( $session_id == $current_session ) {

            $in_session = TRUE;
            $user_name = db_result(db_query("SELECT user_name FROM {mag_student_identification} WHERE id=%d", $session_sid));

        }

        // Not in session, set flag to redirect the
        // advisor back to select a student to advise.
        else {
            $no_session = TRUE;
            // TODO: REMOVE, ONLY HERE WHILE ACCESS GIVEN TO "daedalus manage"
            $user_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $user_id));
        }

    }

    // User is not an advisor, select
    // the current users username.
    else {
        // Determine the user name
        $user_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $user_id));
    }

    // If the user is an advisor, the advisor must be currently be in
    // a session to view the program form information of a student. If
    // a student is viewing their program information they will never be
    // redirected because they do not have advisor privilidges, the flag
    // will never be set to true for a student.
    if( !$no_session || user_access("daedalus manage") ) {      // **** user_access("daedalus manage) TEMPORARY ONLY TO ALLOW FOR EASIER DEVELOPMENT ****** REMOVE LATER *****

        // Get current page url.
        $page_url = $help_url = daedalus_get_setting("program my strengths");

        // Get page urls
        $slos_url    = $base_url."/".daedalus_get_setting("manage learning outcomes");
        $browse_url  = $base_url."/".daedalus_get_setting("browse learning outcomes");

        $page_url_length = sizeof(explode('/',$page_url));
        $page_url = $base_url."/".$page_url;

        // Store URL Parameters in $param array
        $param = array();
        $param[0] = arg(0+$page_url_length);    // Detemine to view tag cloud or the slo list

        // Get the user's student information.
        if( $in_session ) {

            $student = db_fetch_array(db_query("SELECT id, official_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $selection_id = $student['official_selection'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];
            $student_id = $student['id'];

            $advising_text = 'Advising:';

            $official_text = 'My Official Program Strengths:';

        }

        else {

            $student = db_fetch_array(db_query("SELECT id, official_selection, current_selection, first_name, last_name FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $official_selection= $student['official_selection'];
            $selection_id = $student['current_selection'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];
            $student_id = $student['id'];
            
            if( $selection_id == $official_selection ) {
                $official_text = 'My Official Program Strengths:';
            }
            
            else {
                $official_text = 'My Program Strengths:';
            }

        }

        // Get the program id
        $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $selection_id ));

        // Get the program information
        $program = db_result(db_query("SELECT program FROM {mag_program_identification} WHERE id=%d", $program_id ));

        // Display the title, if the user has selected their program display the username and program information.
        // If the user has not selected their program prompt them to do so with a link the the program form page.
        if( $program_id && $first_name && $last_name ) {

            if( $in_session ) {
                
            }

            $computer_science_logo = $base_url."/".daedalus_get_setting("computer science logo");

            drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program."<br />".$official_text."</small></i>"));

        }

        else {

            drupal_set_message(t('You must first select a program before you can view your program strengths.'), 'warning');

            drupal_goto($base_url."/".daedalus_get_setting("program form"));

        }

        $form = array();

        // Add the hidden help form. Paramaters are
        // (help url, show border, show break).
        $form = display_hidden_form($help_url, 1, 1);

        // Apply css to change the table behaviour
        $css = '<style type="text/css">
                    table.box{
                        border-style:solid;
                        border-width:3px;
                        border-color:#C0C0C0;
                    }
                    p.cloud{
                            line-height: '. daedalus_get_setting("tag cloud height percent") .'%;
                            padding-top:10px;
                            padding-bottom:0px;
                            padding-left:5px;
                            padding-right:5px;
                        }
                </style>';

        drupal_set_html_head($css);

        // Scale the <big> tags up by the max font value
        $max_font_size = daedalus_get_setting("tag cloud max font size");

        // Open the Table
        $form[] = array(
            '#type' => 'item',
            '#value' => t('<table class="box">'),
        );

          //////////////////////////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
        // Excellent Level values are selected by the grades A+, A, A-
        $course_info = $slo_info = $tag_info = array();
        $cloud_string = "";

        $result = db_query("SELECT requirement_id, equivalent_id FROM {mag_student_program_form} WHERE selection_id=%d AND mark LIKE 'A%'", $selection_id);
        while( $row = db_fetch_array($result) ) {

            $Arequirement_ids[$row['requirement_id']] = $row['requirement_id'];

            // Add equivalent courses
            if( $row['equivalent_id'] ) {
                $course_info[] = $row['equivalent_id'];
            }

        }

        if( $Arequirement_ids ) {

            // Create the list of placeholders to accommodate a variable number of arguments.
            $Aplaceholders = implode(' OR ', array_fill(0, count($Arequirement_ids), "id=%d"));

            $result = db_query("SELECT course_id FROM {mag_program_requirement} WHERE $Aplaceholders", $Arequirement_ids);
            while( $row = db_fetch_array($result) ) {
                $course_info[] = $row['course_id'];
            }

        }

        if( $course_info ) {

            foreach( $course_info as $course_id ) {

                $result = db_query("SELECT DISTINCT slo_id FROM {dae_course_slo} WHERE course_id=%d", $course_id);

                while( $row = db_fetch_array($result) ) {
                    $slo_info[$row['slo_id']] = $row['slo_id'];
                }

            }

            if( !$param[0] && $slo_info ) {

                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">Excellent Level (A+ to A-)</font></b></td>
                            <td align="right"><a href="'.$page_url.'/excellent">View excellent outcomes</a></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                );

                // Enters each new tag id into an array. If the tag id
                // already exists the array value is incremented creating
                // a count for each id for its appearances.
                foreach( $slo_info as $slo_id ) {

                    $result = db_query("SELECT DISTINCT tag_id FROM {dae_slo_tag} WHERE slo_id=%d", $slo_id);

                    while( $row = db_fetch_array($result) ) {
                        if($tag_info[$row['tag_id']]) {
                            $tag_info[$row['tag_id']] = $tag_info[$row['tag_id']] + 1;
                        }
                        else {
                            $tag_info[$row['tag_id']] = 1;
                        }

                    }

                }

                // Determin the maximum and minimum tag counts
                foreach( $tag_info as $tag_id => $count ) {

                    if( !$min && !$max ) {
                        $min = $max = $count;
                    }
                    
                    else {

                        if( $count > $max ) {
                            $max = $count;
                        }elseif( $count < $min ) {
                            $min = $count;
                        }

                    }

                }

                // If max and min are equal the weight
                // value will be divided by zero.
                if( $min == $max ) {
                    $max++;
                }

                // Create the cloud string with each tag given
                // <big> tags to increase the size of the text.
                foreach( $tag_info as $tag_id => $count ) {

                    $label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tag_id));

                    // Log scale formula found online
                    $weight = (log($count)-log($min))/(log($max)-log($min));

                    switch( $weight ) {
                        case ($weight == 0):
                            $size = 1;
                            break;
                        case ($weight > 0 && $weight <= 0.4):
                            $size = 2;
                            break;
                        case ($weight > 0.4 && $weight <= 0.6):
                            $size = 3;
                            break;
                        case ($weight > 0.6 && $weight <= 0.8):
                            $size = 4;
                            break;
                        case ($weight > 0.8 && $weight <= 1.0):
                            $size = 5;
                            break;
                    }

                    $size += $max_font_size;

                    // Make the font bigger according to the size value.
                    $big_open = $big_end = "";
                    for( $i = 0; $i < $size; $i++ ) {
                            $big_open .= "<big>";
                            $big_end .= "</big>";
                    }

                    // Show the number of times the SLO has been tagged in
                    // the students completed courses.
                    $par_count="<small><small>(".$count.")</small></small>";

                    $cloud_string .= $big_open."<a href='".$browse_url."/".$tag_id."' target='_blank' ><b>#".$label."</b></a>".$big_end."".$par_count." &nbsp;";

                }

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr><td colspan=2><p class="cloud">'.$cloud_string.'</p></td></tr>'),
                );

            }

            elseif( $param[0] == "excellent" && $slo_info ) {

                foreach( $slo_info as $slo_id ) {

                    $slo_text = db_result(db_query("SELECT slo_text FROM {dae_slo} WHERE id=%d", $slo_id));

                    $slo_string .= "<li><a href='".$slos_url."/".$slo_id."'>".$slo_text."</a></li>";

                }

                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">Excellent Level (A+ to A-)</font></b></td>
                            <td align="right"><a href="'.$page_url.'">Return</a></td>
                          </tr>';

                $table_open = '';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr><td colspan=2><ul>'.$slo_string.'</ul></td></tr>'),
                );

            }

            // If there are courses but they do not have 
            // any SLO info, indicate there is no info.
            elseif( !$param[0] ) {
 
                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">Excellent Level (A+ to A-)</font></b></td>
                            <td align="right"></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                    '#suffix' => t('<tr><td colspan=2><ul><li>#No information available</li></ul></td></tr>')
                );

            }

        } // if( $course_info )

        else {
            $excellent_flag = TRUE;
        }

          //////////////////////////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
        // Very Good Level values are selected by the grades B+, B, B-
        $course_info = $slo_info = $tag_info = array();
        $cloud_string = "";

        $result = db_query("SELECT requirement_id, equivalent_id FROM {mag_student_program_form} WHERE selection_id=%d AND mark LIKE 'B%'", $selection_id);
        while( $row = db_fetch_array($result) ) {

            $Brequirement_ids[$row['requirement_id']] = $row['requirement_id'];

            // Add equivalent courses
            if( $row['equivalent_id'] ) {
                $course_info[] = $row['equivalent_id'];
            }

        }


        if( $Brequirement_ids ) {

            // Create the list of placeholders to accommodate a variable number of arguments.
            $Bplaceholders = implode(' OR ', array_fill(0, count($Brequirement_ids), "id=%d"));

            $result = db_query("SELECT course_id FROM {mag_program_requirement} WHERE $Bplaceholders", $Brequirement_ids);
            while( $row = db_fetch_array($result) ) {
                $course_info[] = $row['course_id'];
            }

        }

        if( $course_info ) {

            foreach( $course_info as $form_id => $course_id ) {

                $result = db_query("SELECT DISTINCT slo_id FROM {dae_course_slo} WHERE course_id=%d", $course_id);

                while( $row = db_fetch_array($result) ) {
                    $slo_info[$row['slo_id']] = $row['slo_id'];
                }

            }

            if( !$param[0] && $slo_info ) {

                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">Very Good Level (B+ to B-)</font></b></td>
                            <td align="right"><a href="'.$page_url.'/verygood">View very good outcomes</a></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                );

                foreach( $slo_info as $slo_id ) {

                    $result = db_query("SELECT DISTINCT tag_id FROM {dae_slo_tag} WHERE slo_id=%d", $slo_id);

                    while( $row = db_fetch_array($result) ) {
                        if($tag_info[$row['tag_id']]) {
                            $tag_info[$row['tag_id']] = $tag_info[$row['tag_id']] + 1;
                        }
                        else {
                            $tag_info[$row['tag_id']] = 1;
                        }
                    }
                }

                foreach( $tag_info as $tag_id => $count ) {

                    if( !$min && !$max ) {
                        $min = $max = $count;
                    }else {
                        if( $count > $max ) {
                            $max = $count;
                        }elseif( $count < $min ) {
                            $min = $count;
                        }
                    }
                }

                // If max and min are equal the weight
                // value will be divided by zero.
                if( $min == $max ) {
                    $max++;
                }

                foreach( $tag_info as $tag_id => $count ) {

                    $label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tag_id));

                    // Log scale formula found online
                    $weight = (log($count)-log($min))/(log($max)-log($min));


                    switch( $weight ) {
                        case ($weight == 0):
                            $size = 1;
                            break;
                        case ($weight > 0 && $weight <= 0.4):
                            $size = 2;
                            break;
                        case ($weight > 0.4 && $weight <= 0.6):
                            $size = 3;
                            break;
                        case ($weight > 0.6 && $weight <= 0.8):
                            $size = 4;
                            break;
                        case ($weight > 0.8 && $weight <= 1.0):
                            $size = 5;
                            break;
                    }

                    // Make the font bigger for the right number of times
                    $big_open = $big_end = "";
                    for( $i = 0; $i < $size; $i++ ) {
                            $big_open .= "<big>";
                            $big_end .= "</big>";
                    }

                    // Show the number of times the SLO has been tagged.
                    $par_count="<small><small>(".$count.")</small></small>";

                    $cloud_string .= $big_open."<a href='".$browse_url."/".$tag_id."' target='_blank' ><b>#".$label."</b></a>".$big_end."".$par_count." &nbsp;";
                }

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr><td colspan=2><p class="cloud">'.$cloud_string.'</p></td></tr>'),
                );
            }

            elseif( $param[0] == "verygood" && $slo_info ) {

                foreach( $slo_info as $slo_id ) {
                    $slo_text = db_result(db_query("SELECT slo_text FROM {dae_slo} WHERE id=%d", $slo_id));
                    $slo_string .= "<li><a href='".$slos_url."/".$slo_id."'>".$slo_text."</a></li>";
                }

                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">Very Good Level (B+ to B-)</font></b></td>
                            <td align="right"><a href="'.$page_url.'">Return</a></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr><td colspan=2><ul>'.$slo_string.'</ul></td></tr>'),
                );
            }
            
            // If there are courses but they do not have 
            // any SLO info, indicate there is no info.
            elseif( !$param[0] ) {
 
                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">Very Good Level (B+ to B-)</font></b></td>
                            <td align="right"></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                    '#suffix' => t('<tr><td colspan=2><ul><li>#No information available</li></ul></td></tr>')
                );

            }

        } // if( $course_info )

        else {
            $very_flag = TRUE;
        }

          //////////////////////////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
        // Acceptable Level values are selected by the grades C+, C, C-
        $course_info = $slo_info = $tag_info = array();
        $cloud_string = "";

        $result = db_query("SELECT requirement_id, equivalent_id FROM {mag_student_program_form} WHERE selection_id=%d AND mark LIKE 'C%'", $selection_id);
        while( $row = db_fetch_array($result) ) {

            $Crequirement_ids[$row['requirement_id']] = $row['requirement_id'];

            // Add equivalent courses
            if( $row['equivalent_id'] ) {
                $course_info[] = $row['equivalent_id'];
            }

        }

        if( $Crequirement_ids ) {

            // Create the list of placeholders to accommodate a variable number of arguments.
            $Cplaceholders = implode(' OR ', array_fill(0, count($Crequirement_ids), "id=%d"));

            $result = db_query("SELECT course_id FROM {mag_program_requirement} WHERE $Cplaceholders", $Crequirement_ids);
            while( $row = db_fetch_array($result) ) {
                $course_info[] = $row['course_id'];
            }

        }

        if( $course_info ) {

            foreach( $course_info as $form_id => $course_id ) {

                $result = db_query("SELECT DISTINCT slo_id FROM {dae_course_slo} WHERE course_id=%d", $course_id);

                while( $row = db_fetch_array($result) ) {
                    $slo_info[$row['slo_id']] = $row['slo_id'];
                }
            }

            if( !$param[0] && $slo_info ) {
                
                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">Acceptable Level (C+ to C-)</font></b></td>
                            <td align="right"><a href="'.$page_url.'/acceptable">View acceptable outcomes</a></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                );

                foreach( $slo_info as $slo_id ) {

                    $result = db_query("SELECT DISTINCT tag_id FROM {dae_slo_tag} WHERE slo_id=%d", $slo_id);

                    while( $row = db_fetch_array($result) ) {
                        if($tag_info[$row['tag_id']]) {
                            $tag_info[$row['tag_id']] = $tag_info[$row['tag_id']] + 1;
                        }
                        else {
                            $tag_info[$row['tag_id']] = 1;
                        }
                    }
                }

                foreach( $tag_info as $tag_id => $count ) {

                    if( !$min && !$max ) {
                        $min = $max = $count;
                    }else
                        if( $count > $max ) {
                            $max = $count;
                        }elseif( $count < $min ) {
                            $min = $count;
                        }
                }

                // If max and min are equal the weight
                // value will be divided by zero.
                if( $min == $max ) {
                    $max++;
                }

                foreach( $tag_info as $tag_id => $count ) {

                    $label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tag_id));

                    // Log scale formula found online
                    $weight = (log($count)-log($min))/(log($max)-log($min));

                    switch( $weight ) {

                        case ($weight == 0):
                            $size = 1;
                            break;
                        case ($weight > 0 && $weight <= 0.4):
                            $size = 2;
                            break;
                        case ($weight > 0.4 && $weight <= 0.6):
                            $size = 3;
                            break;
                        case ($weight > 0.6 && $weight <= 0.8):
                            $size = 4;
                            break;
                        case ($weight > 0.8 && $weight <= 1.0):
                            $size = 5;
                            break;

                    }

                    // Make the font bigger for the right number of times
                    $big_open = $big_end = "";
                    for( $i = 0; $i < $size; $i++ ) {
                            $big_open .= "<big>";
                            $big_end .= "</big>";
                    }

                    // Show the number of times the SLO has been tagged.
                    $par_count = "<small><small>(".$count.")</small></small>";

                    $cloud_string .= $big_open."<a href='".$browse_url."/".$tag_id."' target='_blank' ><b>#".$label."</b></a>".$big_end."".$par_count." &nbsp;";

                }

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr><td colspan=2><p class="cloud">'.$cloud_string.'</p></td></tr>'),
                );

            }

            elseif( $param[0] == "acceptable" && $slo_info ) {

                foreach( $slo_info as $slo_id ) {
                    $slo_text = db_result(db_query("SELECT slo_text FROM {dae_slo} WHERE id=%d", $slo_id));
                    $slo_string .= "<li><a href='".$slos_url."/".$slo_id."'>".$slo_text."</a></li>";
                }

                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">Acceptable Level (C+ to C-)</font></b></td>
                            <td align="right"><a href="'.$page_url.'">Return</a></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr><td colspan=2><ul>'.$slo_string.'</ul></td></tr>'),
                );
            }

            // If there are courses but they do not have 
            // any SLO info, indicate there is no info.
            elseif( !$param[0] ) {
 
                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">Acceptable Level (C+ to C-)</font></b></td>
                            <td align="right"></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                    '#suffix' => t('<tr><td colspan=2><ul><li>#No information available</li></ul></td></tr>')
                );

            }

        } // if( $course_info )
        
        else {
            $acceptable_flag = TRUE;
        }

          //////////////////////////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////
        // In Difficulty Level values are selected by the grades D and F
        $course_info = $slo_info = $tag_info = array();
        $cloud_string = "";

        $result = db_query("SELECT requirement_id, equivalent_id FROM {mag_student_program_form} WHERE (selection_id=%d AND mark='D') OR (selection_id=%d AND mark='F')", $selection_id, $selection_id);
        while( $row = db_fetch_array($result) ) {

            $Drequirement_ids[$row['requirement_id']] = $row['requirement_id'];

            // Add equivalent courses
            if( $row['equivalent_id'] ) {
                $course_info[] = $row['equivalent_id'];
            }

        }

        if( $Drequirement_ids ) {

            // Create the list of placeholders to accommodate a variable number of arguments.
            $Dplaceholders = implode(' OR ', array_fill(0, count($Drequirement_ids), "id=%d"));

            $result = db_query("SELECT course_id FROM {mag_program_requirement} WHERE $Dplaceholders", $Drequirement_ids);
            while( $row = db_fetch_array($result) ) {
                $course_info[] = $row['course_id'];
            }

        }

        if( $course_info ) {

            foreach( $course_info as $form_id => $course_id ) {

                $result = db_query("SELECT DISTINCT slo_id FROM {dae_course_slo} WHERE course_id=%d", $course_id);

                while( $row = db_fetch_array($result) ) {
                    $slo_info[$row['slo_id']] = $row['slo_id'];
                }
            }

            if( !$param[0] && $slo_info ) {

                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">In Difficulty Level (D, F)</font></b></td>
                            <td align="right"><a href="'.$page_url.'/indifficulty">View in difficulty outcomes</a></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                );

                foreach( $slo_info as $slo_id ) {

                    $result = db_query("SELECT DISTINCT tag_id FROM {dae_slo_tag} WHERE slo_id=%d", $slo_id);

                    while( $row = db_fetch_array($result) ) {
                        if($tag_info[$row['tag_id']]) {
                            $tag_info[$row['tag_id']] = $tag_info[$row['tag_id']] + 1;
                        }
                        else {
                            $tag_info[$row['tag_id']] = 1;
                        }
                    }
                }

                foreach( $tag_info as $tag_id => $count ) {

                    if( !$min && !$max ) {
                        $min = $max = $count;
                    }else
                        if( $count > $max ) {
                            $max = $count;
                        }elseif( $count < $min ) {
                            $min = $count;
                        }
                }

                // If max and min are equal the weight
                // value will be divided by zero.
                if( $min == $max ) {
                    $max++;
                }

                foreach( $tag_info as $tag_id => $count ) {

                    $label = db_result(db_query("SELECT tag_label FROM {dae_tag} WHERE id=%d", $tag_id));

                    // Log scale formula found online
                    $weight = (log($count)-log($min))/(log($max)-log($min));

                    switch( $weight ) {
                        case ($weight == 0):
                            $size = 1;
                            break;
                        case ($weight > 0 && $weight <= 0.4):
                            $size = 2;
                            break;
                        case ($weight > 0.4 && $weight <= 0.6):
                            $size = 3;
                            break;
                        case ($weight > 0.6 && $weight <= 0.8):
                            $size = 4;
                            break;
                        case ($weight > 0.8 && $weight <= 1.0):
                            $size = 5;
                            break;
                    }

                    // Make the font bigger for the right number of times
                    $big_open = $big_end = "";
                    for( $i = 0; $i < $size; $i++ ) {
                            $big_open .= "<big>";
                            $big_end .= "</big>";
                    }

                    // Show the number of times the SLO has been tagged.
                    $par_count="<small><small>(".$count.")</small></small>";

                    $cloud_string .= $big_open."<a href='".$browse_url."/".$tag_id."' target='_blank' ><b>#".$label."</b></a>".$big_end."".$par_count." &nbsp;";
                }

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr><td colspan=2><p class="cloud">'.$cloud_string.'</p></td></tr>'),
                );
            }

            elseif( $param[0] == "indifficulty" && $slo_info ) {

                foreach( $slo_info as $slo_id ) {
                    $slo_text = db_result(db_query("SELECT slo_text FROM {dae_slo} WHERE id=%d", $slo_id));
                    $slo_string .= "<li><a href='".$slos_url."/".$slo_id."'>".$slo_text."</a></li>";
                }

                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">In Difficulty Level (D, F)</font></b></td>
                            <td align="right"><a href="'.$page_url.'">Return</a></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<tr><td colspan=2><ul>'.$slo_string.'</ul></td></tr>'),
                );

            }

            // If there are courses but they do not have 
            // any SLO info, indicate there is no info.
            elseif( !$param[0] ) {
 
                $level = '<tr bgcolor="#C0C0C0">
                            <td style="padding:4px 4px 4px 4px;"><b><font size="3">In Difficulty Level (D, F)</font></b></td>
                            <td align="right"></td>
                          </tr>';

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($level),
                    '#suffix' => t('<tr><td colspan=2><ul><li>#No information available</li></ul></td></tr>')
                );

            }

        } //if( $course_info )
        else {
            $difficulty_flag = TRUE;
        }

        if( $excellent_flag && $very_flag && $acceptable_flag && $difficulty_flag && $program && $first_name && $last_name ) {
            drupal_set_message(t('Please check again when you have completed courses in your program.'));
        }

        // Close the Table
        $form[] = array(
            '#type' => 'item',
            '#value' => t('</table>'),
        );

        // If in a session display session logout button.
        if( $in_session ) {

            $form['end-session'] = array(
                '#type' => 'submit',
                '#value' => t('End advising session'),
            );

            // Submit the hidden session id.
            $form['$session_id'] = array( '#type' => 'value', '#value' => $session_id, );

        }

        return $form;

    } // END if( !$no_session )

    else {

        drupal_set_message(t('You must be in session to view a students program strengths.'));

        drupal_goto($base_url."/".daedalus_get_setting("advise student"));

    }

}


/**
 * Implementation of hook_submit().
 */
function daedalus_program_strengths_form_submit( $form, &$form_state ) {

    switch($form_state['values']['op']) {

        case $form_state['values']['end-session']:

            db_query("DELETE FROM {mag_advisor_session} WHERE id=%d", $form_state['values']['session-id']);

            drupal_set_message(t('Current session closed.'));

            // This generates a new session id.
            $_COOKIE[session_regenerate_id()];

            drupal_goto($base_url."/".daedalus_get_setting("advise student"));

            break;

        case $form_state['values']['dae-help-submit']:

            global $base_url;
            $page_url = daedalus_get_setting("program my strengths");

            $page_url_length = sizeof(explode('/',$page_url));

            $param[0] = arg(0+$page_url_length);

            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message(t('Help information saved.'));

                    if( $param[0]) {
                        drupal_goto($base_url."/".$page_url."/".$param[0]);
                    }else {
                    drupal_goto($base_url."/".$page_url);
                    }
            }

            break;
    }
}


//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Program->NextStep Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_program_nextstep_page() {
    return drupal_get_form("daedalus_program_nextstep_form");
};


/**
* Menu Location: Daedalus -> Programs -> NextStep
* URL Location:  daedalus/daedalus/program/nextstep
*
* Displays 
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_program_nextstep_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Determine the user name
    global $user;
    $user_id = $user->uid;

    // If the current user is a Magellan Advisor determine if there is a current advising session open.
    if( db_result(db_query("SELECT COUNT(*) FROM {role, users_roles} WHERE role.name='Magellan Advisor' AND users_roles.uid=%d AND users_roles.rid = role.rid", $user_id)) ) {

        $result = db_query("SELECT id, student_id FROM {mag_advisor_session} WHERE advisor_id=%d", $user_id);
        while( $row = db_fetch_array($result) ) {
            $session_id  = $row['id'];
            $session_sid = $row['student_id'];
        }

        // Get the session name.
        $current_session = $_COOKIE[session_name()];

        // If in session, set the user name as
        // to the selected students username.
        if( $session_id == $current_session ) {

            $in_session = TRUE;
            $user_name = db_result(db_query("SELECT user_name FROM {mag_student_identification} WHERE id=%d", $session_sid));

        }

        // Not in session, set flag to redirect the
        // advisor back to select a student to advise.
        else {
            $no_session = TRUE;
            // TODO: REMOVE, ONLY HERE WHILE ACCESS GIVEN TO "daedalus manage"
            $user_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $user_id));
        }

    }

    // User is not an advisor, select
    // the current users username.
    else {
        // Determine the user name
        $user_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $user_id));
    }

    // If the user is an advisor, the advisor must be currently be in
    // a session to view the program form information of a student. If
    // a student is viewing their program information they will never be
    // redirected because they do not have advisor privilidges, the flag
    // will never be set to true for a student.
    if( !$no_session || user_access("daedalus manage") ) {      // **** user_access("daedalus manage) TEMPORARY ONLY TO ALLOW FOR EASIER DEVELOPMENT ****** REMOVE LATER *****

        // Get current page url.
        $page_url = $help_url = daedalus_get_setting("program nextstep");

        // Get page urls.
        $course_url = $base_url."/".daedalus_get_setting("manage courses");
        $slo_url = daedalus_get_setting("manage learning outcomes");

        // Get access information
        $build_access = user_access("daedalus build");

        // Store URL Parameters in $param array
        $page_url_length = sizeof(explode('/',$page_url));
        $page_url = $base_url."/".$page_url;

        $param = array();

        $param[0] = arg(0+$page_url_length);    // "view/create", determines if the program has been selected or is being created.
        $param[1] = arg(1+$page_url_length);    // $program, the selected program.
        $param[2] = arg(2+$page_url_length);    // $year, the selected program year.

        // Get the images
        $computer_science_logo       = $base_url."/".daedalus_get_setting("computer science logo");
        $small_computer_science_logo = $base_url."/".daedalus_get_setting("computer science logo small");
        $checkmark_img               = "<img src='".$base_url."/".daedalus_get_setting("checkmark")."' title='An average of greater than or equal to B- has been obtained in the courses covering this learning outcome.' />";
        $exclamation_img             = "<img src='".$base_url."/".daedalus_get_setting("exclamation mark")."' title='An average of less than or equal to C+ has been obtained in the courses covering this learning outcome.' />";
        $course_goal_img             = "<img src='".$base_url."/".daedalus_get_setting("goal identification")."' title='This course is a part of your goal.' />";
        $slo_goal_img                = "<img src='".$base_url."/".daedalus_get_setting("goal identification")."' title='This learning outcome is a part of your goal.' />";

        // Get the user's student information.
        if( $in_session ) {

            $student = db_fetch_array(db_query("SELECT id, official_selection, first_name, last_name, goal_type, goal_id FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $selection_id = $student['official_selection'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];
            $goal_type = $student['goal_type'];
            $goal_id = $student['goal_id'];
            $student_id = $student['id'];

            $advising_text = 'Advising:';

            $official_text = 'Official Program Next Step:';

            // TODO: REMOVE LATER..................
            // Disable build access when in session
            // to allow admins to edit the form and
            // view user goal courses.
            $build_access = 0;

        }

        else {

            $student = db_fetch_array(db_query("SELECT id, official_selection, current_selection, first_name, last_name, goal_type, goal_id FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $official_selection = $student['official_selection'];
            $selection_id = $student['current_selection'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];
            $goal_type = $student['goal_type'];
            $goal_id = $student['goal_id'];
            $student_id = $student['id'];
            
            if( $selection_id == $official_selection ) {
                $official_text = 'Official Program Next Step:';
            }
            
            else {
                $official_text = 'Program Next Step:';
            }

        }

        // Get the program id
        $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $selection_id ));

        // Get the program information
        $program = db_result(db_query("SELECT program FROM {mag_program_identification} WHERE id=%d", $program_id ));

        // Get the students goal information
        if( $goal_type && $goal_id && !$build_access ) {

            $result = db_query("SELECT slo_id FROM {mag_goal_student_slo} WHERE student_id=%d ORDER BY slo_id", $student_id);
            while( $row = db_fetch_array($result) ) {
                $goal_slos[$row['slo_id']] = $row['slo_id'];
            }

            $result = db_query("SELECT course_id FROM {mag_goal_student_course} WHERE student_id=%d", $student_id);
            while( $row = db_fetch_array($result) ) {
                $goal_courses[$row['course_id']] = $row['course_id'];
            }

        }

        // Determine if the current browse
        // is Microsoft's Internet Explorer.
        if( preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT']) ) {
            $browser = "MSIE";
        }else {
            $browser = "OK";
        }

        // Display the title, if the user has selected their program display the username and program information.
        // If the user has not selected their program prompt them to do so with a link the the program form page.
        if( $program_id && $first_name && $last_name ) {

            $computer_science_logo = $base_url."/".daedalus_get_setting("computer science logo");

            drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program."<br />".$official_text."</small></i>"));

        }

        else {

            drupal_set_message(t('You must first select a program before you can view your program next steps.'), 'warning');

            drupal_goto($base_url."/".daedalus_get_setting("program form"));

        }

        $form = array();

        // Add the hidden help form. Paramaters are
        // (help url, show border, show break).
        $form = display_hidden_form($help_url, 1, 1);

        // In the Next Step:
        //      1) Find all courses in the program form for which the student has the prerequisite
        //         for (C- or above for CSCI courses, D or above for others)
        //      2) For each returned course:
        //          a) Display all of the assumed SLO and indicate which course it comes from, and
        //             the grade that the student got in this course. If the grade is above B-, put
        //             a checkmark in front of it, if, it is under, put a exclamation mark.
        //      3) Hyperlink the course to the daedalus page if the student wants to see the taught SLOs.
        //      4) If a student has a CS elective 4000+ slot, list all available course under the
        //         heading "Possible CS Elective 4000+", and so on for 3000+ and 2000+

        $program_requirements = $mark_info = $equivalent_list = $program_courses = $program_list = $completed_courses = $completed_list = $required_courses = array();

        // Note: Order by log_time is required to view the official program. An official program will have multiple entries for any given requirement. 
        // This ordering will make sure the most recent entries are filtered to the bottom of the list so when a value is over-written it will contain
        // the most recent entry. This will make no difference to any other program form as they do not have a log time.
        $result = db_query("SELECT requirement_id, equivalent_id, mark FROM {mag_student_program_form} WHERE selection_id=%d ORDER BY log_time", $selection_id);
        while( $row = db_fetch_array($result) ) {

            // This is a complete listing of the requirements in the program.
            $program_requirements[$row['requirement_id']] = $row['requirement_id'];

            // Save the mark for the given requirement
            $mark_info[$row['requirement_id']] = $row['mark'];

            // Create a list of the program forms
            // completed equivalent courses.
            if( $row['equivalent_id'] ) {
                $equivalent_list[] = $row['equivalent_id'];
            }

        }

        if( $program_requirements ) {

            $program_placeholders = implode(' OR ', array_fill(0, count($program_requirements), "id=%d"));

            $result = db_query("SELECT id, course_id, code_filter FROM {mag_program_requirement} WHERE course_id!=0 AND ($program_placeholders) ORDER BY course_id", $program_requirements);
            while( $row = db_fetch_array($result) ) {

                $program_courses[$row['course_id']]['requirement_id'] = $row['id'];
                $program_courses[$row['course_id']]['code_filter'] = $row['code_filter'];

                // Add the mark for the given requirement.
                $program_courses[$row['course_id']]['mark'] = $mark_info[$row['id']];

                // Create another copy that the php
                // function in_array() will accept.
                $program_list[] = $row['course_id'];

            }

            // Separate the courses into two lists.
            foreach( $program_courses as $cid => $course ) {

                // Completed with a given mark
                if( $course['mark'] != 'N/A' ) {

                    $completed_courses[$cid] = $course;

                    // Create another copy that the php
                    // function in_array() will accept.
                    $completed_list[] = $cid;

                }

                // All other required courses.
                else{
                    $required_courses[$cid] = $course;
                }

            }

        }

        // The required section can only be displayed if the student has completed
        // at least one course in their program. This will also prevent the PHP
        // in_array function from throwing a "Wrong datatype" error.
        if( $completed_courses ) {

            // Foreach uncompleted required course check that each of its prerequisite
            // courses have been completed in the users program. If completed add the
            // course to the list of next step required courses.
            foreach( $required_courses as $cid => $req_course ) {

                $nextstep_flag = TRUE;

                $prereq_course_display = "";

                // Each elective may have a conditional set of prerequisites
                // where one or the other or both may be completed.
                $elective_sets = array();

                $result = db_query("SELECT prereq_id, set_id FROM {dae_prereq_course} WHERE course_id=%d ORDER BY set_id", $cid);
                while( $row = db_fetch_array($result) ) {

                    $set_id = $row['set_id'];
                    $prereq_id = $row['prereq_id'];

                    // Foreach prerequisite course get the grade and determine if
                    // it is a passing grade or not. All prerequisites must have
                    // a passing grade, unless one is part of a set.
                    $prereq_mark = $program_courses[$prereq_id]['mark'];

                    $prereq_course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $prereq_id));

                    // URL to the course
                    $url = $course_url."/".str_replace(" ", "/", $prereq_course);

                    // Create a string to display the prerequisites for the required course.
                    $prereq_course_display .= "<li><a href='".$url."' target='_blank'>".$prereq_course."</a> Grade: ".$prereq_mark."</li>";

                    switch($prereq_mark) {
                        case "A+":
                        case "A":
                        case "A-":
                        case "B+":
                        case "B":
                        case "B-":
                        case "C+":
                        case "C":
                        case "C-":
                            $passing = TRUE;
                            break;
                        default:
                            $passing = FALSE;
                            break;
                    }

                    // If the set_id is 1, no need
                    // to check for a duplicate.
                    if( $set_id == 1 ) {

                        if( !in_array($prereq_id, $completed_list) || !$passing ) {
                            $nextstep_flag = FALSE;
                        }

                    }

                    // If the set_id is greater than one, this indicates that there
                    // is a set of prerequisites and that both do not need to be completed.
                    // Add the set_id to an array and indicate "no" if both are not completed,
                    // and "yes" if one or both are completed.
                    else {

                        if( (!in_array($prereq_id, $completed_list) && $elective_sets[$set_id] != "yes") || !$passing ) {
                            $elective_sets[$set_id] = "no";
                        }else {
                            $elective_sets[$set_id] = "yes";
                        }

                    }

                }

                // If the current course contained a set, check
                // to see if that set was declared as "no"
                if( $elective_sets ) {

                    foreach( $elective_sets as $id => $show ) {

                        if( $show == "no" ) {
                            $nextstep_flag = FALSE;
                        }

                    }

                }

                // If all the prerequisite courses are completed in the students program
                // form the required course including passing grades the course can then
                // be displayed in the next step list.
                if( $nextstep_flag ) { //&& $prereq_flag ) {     // Without the prereq_flag, no courses without prereqs are displayed

                    // Heading for the Required Courses
                    if( !$required_title ) {

                        $form[] = array(
                            '#type' => 'item',
                            '#title' => t('Required Courses'),
                            '#prefix' => '<font size="3">',
                            '#suffix' => '</font>',
                        );

                        $required_title++;

                    }

                    $next_course = db_fetch_array(db_query("SELECT course, mapped, course_name FROM {dae_course} WHERE id=%d", $cid));
                    $course = $next_course['course'];
                    $mapped = $next_course['mapped'];
                    $course_name = $next_course['course_name'];

                    // If the course is mapped show the assumed SLO information.
                    // If the course is not mapped show a link to the dalhousie calendar.
                    if( $mapped ) {

                        // The link to the current course;
                        $goto_course_url = $course_url."/".str_replace(" ", "/", $course);

                        // Check to see if the course is a goal course,
                        // if it is add the goal symbol and don't collapse.
                        if( $goal_courses && in_array($cid, $goal_courses) && !$build_access ) {

                            $form["course_$cid"] = array(
                                '#title' => t($course." - ".$course_name." ".$course_goal_img),
                                '#type' => 'fieldset',
                                '#collapsible' => TRUE,
                                '#collapsed' => FALSE,
                            );

                        }

                        else {

                            $form["course_$cid"] = array(
                                '#title' => t($course." - ".$course_name),
                                '#type' => 'fieldset',
                                '#collapsible' => TRUE,
                                '#collapsed' => TRUE,
                            );

                        }

                        $asslo_list = $assumed_slo = array();
                        $aslo_display = "";

                        // Get the assumed SLOs which are passed to the
                        // second parameter of the function by reference.
                        get_assumed_slos($cid, $asslo_list);

                        // Format the assumed SLOs and order by rank
                        if( $asslo_list ) {

                            $asslo_placeholders = implode(' OR ', array_fill(0, count($asslo_list), "id=%d"));

                            $result = db_query("SELECT id, slo_text FROM {dae_slo} WHERE ".$asslo_placeholders." ORDER BY slo_rank ASC, slo_text ASC", $asslo_list );
                            while( $row = db_fetch_array($result) ) {
                                $assumed_slo[$row['id']] = $row['slo_text'];
                            }

                            // Foreach assumed slo id as slo text
                            foreach( $assumed_slo as $asid => $slo_text ) {

                                $aslo_display .= "<li>";
                                $course_display = $gpa = "";
                                $total = 0;

                                // Flag to determine which image to display the checkmark
                                // or the exclamation mark. The course flag determines
                                // whether to display any image at all.
                                $course_flag = FALSE;

                                $display_image = TRUE;

                                // Add where each assumed learning outcome is taught.
                                $result = db_query("SELECT course_id FROM {dae_course_slo} WHERE slo_id=%d", $asid);
                                while( $row = db_fetch_array($result) ) {

                                    $course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $row['course_id'] ));
                                    $grade = $program_courses[$row['course_id']]['mark'];

                                    // Remove the image from the PREU courses
                                    if( stripos($course,"PREU") === 0 ) {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='Course not completed'>".$course."</a>] ";

                                        // Do not display the image for PREU courses.
                                        $display_image = FALSE;

                                    }

                                    // If the grade is not available,
                                    // do not display it.
                                    elseif( !$grade || $grade == "N/A" ) {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='Course not completed'>".$course."</a>] ";

                                        // If there is no mark, only cancel the image
                                        // display if the course is a part of the program.
                                        if( in_array( $row['course_id'], $program_list) ) {
                                            $display_image = FALSE;
                                        }

                                    }

                                    else {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='My grade: ".$grade."'>".$course."</a>] ";

                                        // If the mark is close to failing, only display the
                                        // exclamation mark if the course is a part of the program.
                                        if( in_array( $row['course_id'], $program_list) ) {

                                            switch($grade) {
                                                case "A+":
                                                    $current_grade = 4.3;
                                                    break;
                                                case "A":
                                                    $current_grade = 4.0;
                                                    break;
                                                case "A-":
                                                    $current_grade = 3.7;
                                                    break;
                                                case "B+":
                                                    $current_grade = 3.3;
                                                    break;
                                                case "B":
                                                    $current_grade = 3.0;
                                                    break;
                                                case "B-":
                                                    $current_grade = 2.7;
                                                    break;
                                                case "C+":
                                                    $current_grade = 2.3;
                                                    break;
                                                case "C":
                                                    $current_grade = 2.0;
                                                    break;
                                                case "C-":
                                                    $current_grade = 1.7;
                                                    break;
                                                case "D":
                                                    $current_grade = 1.0;
                                                    break;
                                                case "F":
                                                    $current_grade = 0;
                                                    break;
                                            }

                                            $gpa += $current_grade;
                                            $total++;

                                        }

                                    }

                                    $course_flag = TRUE;

                                }

                                if( $total > 0 ) {
                                    $gpa = $gpa/$total;
                                }else {
                                    $gpa = 0;
                                }

                                $url = $base_url."/".$slo_url."/".$asid;

                                // If build access no need
                                // to display any images.
                                if( $build_access ) {

                                    $rank = db_result(db_query("SELECT slo_rank FROM {dae_slo} WHERE id=%d", $asid));
                                    $aslo_display .= "<a href='".$url."'>".$slo_text."</a> <small><small> ".$course_display."(".$rank.")</small></small></li>";

                                }

                                elseif( $display_image && $course_flag ) {

                                    if( $gpa > 2.3 ) {

                                        // Display the goal image.
                                        if( $goal_slos && in_array($asid, $goal_slos) ) {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$checkmark_img." ".$slo_goal_img."<small><small> ".$course_display."</small></small></li>";
                                        }else {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$checkmark_img." <small><small> ".$course_display."</small></small></li>";
                                        }

                                    }

                                    else {

                                        if( $goal_slos && in_array($asid, $goal_slos) ) {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$exclamation_img." ".$slo_goal_img."<small><small> ".$course_display."</small></small></li>";
                                        }else {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$exclamation_img." <small><small> ".$course_display."</small></small></li>";
                                        }
                                    
                                    }

                                }

                                else {

                                    if( $goal_slos && in_array($asid, $goal_slos) ) {
                                        $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$slo_goal_img."<small><small> ".$course_display."</small></small></li>";
                                    }else {
                                        $aslo_display .= "<a href='".$url."'>".$slo_text."</a> <small><small> ".$course_display."</small></small></li>";
                                    }

                                }

                            }

                        }

                        if( $prereq_course_display ) {

                            $form["course_$cid"][] = array(
                                '#title' => t('Completed Prerequisite Courses'),
                                '#value' => t('<ul>'.$prereq_course_display.'</ul>'),
                                '#type' => 'item',
                                '#prefix' => '<blockquote>',
                                '#suffix' => '</blockquote>',
                            );

                        }

                        if( $aslo_display ) {

                            $form["course_$cid"][] = array(
                                '#title' => t('Assumed Student Learning Outcomes'),
                                '#value' => t('<ul>'.$aslo_display.'</ul>'),
                                '#type' => 'item',
                                '#prefix' => '<blockquote>',
                                '#suffix' => '</blockquote>',
                            );

                        }

                        else {

                            $form["course_$cid"][] = array(
                                '#title' => t('Assumed Student Learning Outcomes'),
                                '#value' => t('<ul><li><i>There are no assumed Student Learning Outcomes for this class.</i></li></ul>'),
                                '#type' => 'item',
                                '#prefix' => '<blockquote>',
                                '#suffix' => '</blockquote>',
                            );

                        }
                        
                        if( $browser == "MSIE" ) {

                            // If the current browser is Microsoft Internet Explorer the button style links
                            // will not work so use a simple textual link to open the new window.
                            $form["course_$cid"][] = array(
                                '#type' => 'item',
                                '#value' => t('<a href="'.$goto_course_url.'" target="_blank">View Course</a>'),
                                '#prefix' => '<blockquote>',
                                '#suffix' => '</blockquote>',
                            );

                        }

                        else {

                            // Every other browser handles the button style wrapped link no problem. Go figure?
                            $form["course_$cid"][] = array(
                                '#type' => 'item',
                                '#value' => t('<input type="button" value="View Course" style="width:1.5in" />'),
                                '#prefix' => '<blockquote><a href="'.$goto_course_url.'" target="_newtab">',
                                '#suffix' => '</a></blockquote>',
                            );
                        }

                    } // if( $mapped )

                    else {

                        $course_info = explode(" ", $course);
                        $code = $course_info[0];
                        $number = $course_info[1];

                        // The link to the current course;
                        $goto_calendar_url = "http://www.registrar.dal.ca/calendar/class.php?subj=".$code."&num=".$number;

                        $form["course_$cid"] = array(
                            '#title' => t($course." - ".$course_name),
                            '#type' => 'fieldset',
                            '#collapsible' => TRUE,
                            '#collapsed' => TRUE,
                        );

                        // Indicate that the course has not
                        // been mapped in daedalus as of yet.
                        $form["course_$cid"][] = array(
                            '#type' => 'item',
                            '#title' => t('Assumed Student Learning Outcomes'),
                            '#value' => t('<ul><li><i>This course is not mapped.</i></li></ul>'),
                            '#prefix' => '<blockquote>',
                            '#suffix' => '</blockquote>',
                        );

                        if( $prereq_course_display ) {

                            $form["course_$cid"][] = array(
                                '#type' => 'item',
                                '#title' => t('Completed Prerequisite Courses'),
                                '#value' => t('<ul>'.$prereq_course_display.'</ul>'),
                                '#prefix' => '<blockquote>',
                                '#suffix' => '</blockquote>',
                            );

                        }

                        if( $browser == "MSIE" ) {

                            // If the current browser is Microsoft Internet Explorer the button style links
                            // will not work so use a simple textual link to open the new window.
                            $form["course_$cid"][] = array(
                                '#type' => 'item',
                                '#value' => t('<a href="'.$goto_calendar_url.'" target="_blank">View Calendar</a>'),
                                '#prefix' => '<blockquote>',
                                '#suffix' => '</blockquote>',
                            );

                        }

                        else {

                            // Every other browser handles the button style wrapped link no problem. Go figure?
                            $form["course_$cid"][] = array(
                                '#type' => 'item',
                                '#value' => t('<input type="button" value="View Calendar" style="width:1.5in" />'),
                                '#prefix' => '<blockquote><a href="'.$goto_calendar_url.'" target="_newtab">',
                                '#suffix' => '</a></blockquote>',
                            );

                        }

                    }

                }

            }

        }

        elseif( $program && $first_name && $last_name) {
            drupal_set_message(t("Please check again when you have completed courses in your program."));
        }

        // If there are no program courses to compare against
        // the elective levels can not be displayed.
        if( $program_courses && $completed_courses ) {

             /////////////
            // 2000 LEVEL

            $filter_list = $requirement_ids = array();
            $rid_placeholders = $filter_query = "";

            $result = db_query("SELECT DISTINCT requirement_id FROM {mag_student_program_form} WHERE selection_id=%d AND requirement_order >= 2000 AND requirement_order < 3000 ORDER BY log_time", $selection_id);
            while( $row = db_fetch_array($result) ) {
                $requirement_ids[] = $row['requirement_id'];
            }

            if( $requirement_ids ) {

                $rid_placeholders = implode(' OR ', array_fill(0, count($requirement_ids), "id=%d"));

                $result = db_query("SELECT DISTINCT code_filter FROM {mag_program_requirement} WHERE $rid_placeholders", $requirement_ids);
                while( $row = db_fetch_array($result) ) {

                    $code_filter = explode_and_trim(",", $row['code_filter']);

                    foreach( $code_filter as $filter ) {

                        if( $filter ) {
                            $filter_list[$filter] = $filter;
                        }

                    }

                }

                if( $filter_list ) {

                    $filter_query = implode(' OR ', array_fill(0, count($filter_list), "course_code='%s'"));

                    // Get the results for elective courses at the 2000 level by the list of course codes
                    // in the filter query. If there is no filter query select all the possible course codes.
                    if( $filter_query ) {
                        $result = db_query("SELECT id, course, course_name, mapped FROM {dae_course} WHERE (".$filter_query.") AND course_number >= 2000 AND course_number < 3000", $filter_list);
                    }else {
                        $result = db_query("SELECT id, course, course_name, mapped FROM {dae_course} WHERE course_number >= 2000 AND course_number < 3000");
                    }

                    while( $row = db_fetch_array($result) ) {

                        // Add a 2000 level elective only if the course is not already a
                        // part of the program list or a completed equivalent course.
                        if( !in_array($row['id'], $program_list) && !in_array($row['id'], $equivalent_list) ) {

                            $elect2000[$row['id']]['course'] = $row['course'];
                            $elect2000[$row['id']]['course_name'] = $row['course_name'];
                            $elect2000[$row['id']]['mapped'] = $row['mapped'];

                        }

                    }
                    
                } // if( $filter_list )

            } // if( $requirement_ids )

            if( $elect2000 ) {

                foreach( $elect2000 as $cid => $array2000 ) {

                    $course = $array2000['course'];
                    $mapped = $array2000['mapped'];
                    $course_name = $array2000['course_name'];

                    // Each elective is required to have it's prerequisites
                    // completed if the course is going to be listed as one.
                    // It is ok if the course has no prereqs, display anyways.
                    $show_elective = TRUE;

                    // Each elective may have a conditional set of prerequisites
                    // where one or the other or both may be completed.
                    $elective_sets = array();

                    $prereq_course_display = "";

                    $result = db_query("SELECT prereq_id, set_id FROM {dae_prereq_course} WHERE course_id=%d ORDER BY set_id", $cid);
                    while( $row = db_fetch_array($result) ) {

                        $set_id = $row['set_id'];
                        $prereq_id = $row['prereq_id'];

                        $prereq_mark = $program_courses[$prereq_id]['mark'];
                        //$prereq_mark = db_result(db_query("SELECT mark FROM {mag_student_program_form} WHERE selection_id=%d AND course_id=%d", $selection_id, $prereq_id));

                        $prereq_course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $prereq_id));

                        // URL to the course
                        $url2000 = $course_url."/".str_replace(" ", "/", $prereq_course);

                        // Create a string to display the prerequisites for the required course.
                        $prereq_course_display .= "<li><a href='".$url2000."' target='_blank'>".$prereq_course."</a> Grade: ".$prereq_mark."</li>";

                        switch($prereq_mark) {
                            case "A+":
                            case "A":
                            case "A-":
                            case "B+":
                            case "B":
                            case "B-":
                            case "C+":
                            case "C":
                            case "C-":
                                $passing = TRUE;
                                break;
                            default:
                                $passing = FALSE;
                                break;
                        }

                        // If the set_id is 1, no need
                        // to check for a duplicate.
                        if( $set_id == 1 ) {

                            if( !in_array($prereq_id, $completed_list) || !$passing ) {
                                $show_elective = FALSE;
                            }

                        }

                        // If the set_id is greater than one, this indicates that there
                        // is a set of prerequisites and that both do not need to be completed.
                        // Add the set_id to an array and indicate "no" if both are not completed,
                        // and "yes" if one or both are completed.
                        else {

                            if( (!in_array($prereq_id, $completed_list) && $elective_sets[$set_id] != "yes") || !$passing ) {
                                $elective_sets[$set_id] = "no";
                            }else {
                                $elective_sets[$set_id] = "yes";
                            }

                        }

                    }

                    // If the current course contained a set, check
                    // to see if that set was declared as "no"
                    if( $elective_sets ) {

                        foreach( $elective_sets as $id => $show ) {

                            if( $show == "no" ) {
                                $show_elective = FALSE;
                            }

                        }

                    }

                    if( $show_elective ) {

                        if( !$elect2000_flag ) {

                            // Heading for the 2000 level electives
                            $form[] = array(
                                '#type' => 'item',
                                '#title' => t('Possible Electives 2000+'),
                                '#prefix' => '<br /><font size="3">',
                                '#suffix' => '</font>',
                            );

                            $elect2000_flag++;

                        }

                        // The link to the current course;
                        $goto_course_url = $course_url."/".str_replace(" ", "/", $course);

                        // Check to see if the course is a goal course
                        if( $goal_courses && in_array($cid, $goal_courses) && !$build_access) {

                            $form["course_$cid"] = array(
                                '#title' => t($course." - ".$course_name." ".$course_goal_img),
                                '#type' => 'fieldset',
                                '#collapsible' => TRUE,
                                '#collapsed' => FALSE,
                            );

                        }

                        else {

                            $form["course_$cid"] = array(
                                '#title' => t($course." - ".$course_name),
                                '#type' => 'fieldset',
                                '#collapsible' => TRUE,
                                '#collapsed' => TRUE,
                            );

                        }

                        $asslo_list = $assumed_slo = array();
                        $aslo_display = "";

                        // Get the assumed SLOs which are passed to the
                        // second parameter of the function by reference.
                        get_assumed_slos($cid, $asslo_list);

                        // Format the assumed SLOs and order by rank
                        if( $asslo_list ) {

                            $asslo_placeholders = implode(' OR ', array_fill(0, count($asslo_list), "id=%d"));

                            $result = db_query("SELECT id, slo_text FROM {dae_slo} WHERE ".$asslo_placeholders." ORDER BY slo_rank ASC, slo_text ASC", $asslo_list);
                            while($row = db_fetch_array($result) ) {
                                $assumed_slo[$row['id']] = $row['slo_text'];
                            }

                            // Foreach assumed slo id as slo text
                            foreach( $assumed_slo as $asid => $slo_text ) {

                                $aslo_display .= "<li>";
                                $course_display = $gpa = "";
                                $total = 0;

                                // Flag to determine which image to display the checkmark
                                // or the exclamation mark. The course flag determines
                                // whether to display any image at all.
                                $course_flag = FALSE;

                                $display_image = TRUE;

                                // Add where each assumed learning outcome is taught.
                                $result = db_query("SELECT course_id FROM {dae_course_slo} WHERE slo_id=%d", $asid);
                                while( $row = db_fetch_array($result) ) {

                                    $course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $row['course_id']));
                                    $grade = $program_courses[$row['course_id']]['mark'];

                                    // Remove the image from the PREU courses
                                    if( stripos($course,"PREU") === 0 ) {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='Course not completed'>".$course."</a>] ";

                                        // Do not display the image for PREU courses.
                                        $display_image = FALSE;

                                    }

                                    // If the grade is not available,
                                    // do not display it.
                                    elseif( !$grade || $grade == "N/A" ) {

                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='Course not completed'>".$course."</a>] ";

                                        if( in_array( $row['course_id'], $program_list) ) {
                                            $display_image = FALSE;
                                        }

                                    }

                                    else {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='My grade: ".$grade."'>".$course."</a>] ";

                                        if( in_array( $row['course_id'], $program_list) ) {

                                            switch($grade) {
                                                case "A+":
                                                    $current_grade = 4.3;
                                                    break;
                                                case "A":
                                                    $current_grade = 4.0;
                                                    break;
                                                case "A-":
                                                    $current_grade = 3.7;
                                                    break;
                                                case "B+":
                                                    $current_grade = 3.3;
                                                    break;
                                                case "B":
                                                    $current_grade = 3.0;
                                                    break;
                                                case "B-":
                                                    $current_grade = 2.7;
                                                    break;
                                                case "C+":
                                                    $current_grade = 2.3;
                                                    break;
                                                case "C":
                                                    $current_grade = 2.0;
                                                    break;
                                                case "C-":
                                                    $current_grade = 1.7;
                                                    break;
                                                case "D":
                                                    $current_grade = 1.0;
                                                    break;
                                                case "F":
                                                    $current_grade = 0;
                                                    break;
                                            }

                                            $gpa += $current_grade;
                                            $total++;

                                        }

                                    }

                                    $course_flag = TRUE;

                                }

                                if( $total > 0 ) {
                                    $gpa = $gpa/$total;
                                }else {
                                    $gpa = 0;
                                }

                                $url = $base_url."/".$slo_url."/".$asid;

                                // If build access no need to display
                                // any images.
                                if( $build_access ) {

                                    $rank = db_result(db_query("SELECT slo_rank FROM {dae_slo} WHERE id=%d", $asid));
                                    $aslo_display .= "<a href='".$url."'>".$slo_text."</a> <small><small> ".$course_display."(".$rank.")</small></small></li>";

                                }

                                elseif( $display_image && $course_flag ) {

                                    if( $gpa > 2.3 ) {

                                        // Display the goal image.
                                        if( $goal_slos && in_array($asid, $goal_slos) ) {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$checkmark_img." ".$slo_goal_img."<small><small> ".$course_display."</small></small></li>";
                                        }else {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$checkmark_img." <small><small> ".$course_display."</small></small></li>";
                                        }

                                    }

                                    else {

                                        if( $goal_slos && in_array($asid, $goal_slos) ) {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$exclamation_img." ".$slo_goal_img."<small><small> ".$course_display."</small></small></li>";
                                        }else {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$exclamation_img." <small><small> ".$course_display."</small></small></li>";
                                        }

                                    }

                                }

                                else {

                                    if( $goal_slos && in_array($asid, $goal_slos) ) {
                                        $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$slo_goal_img."<small><small> ".$course_display."</small></small></li>";
                                    }else {
                                        $aslo_display .= "<a href='".$url."'>".$slo_text."</a> <small><small> ".$course_display."</small></small></li>";
                                    }

                                }

                            }

                        }

                        // Display the link to the course according to its mapped value.
                        // If mapped link to the Daedalus course entry, link to the
                        // Dalhousie Calendar website if the course is not mapped.
                        if( $mapped ) {

                            if( $prereq_course_display ) {

                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#title' => t('Completed Prerequisite Courses'),
                                    '#value' => t('<ul>'.$prereq_course_display.'</ul>'),
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            if( $aslo_display ) {

                                $form["course_$cid"][] = array(
                                    '#title' => t('Assumed Student Learning Outcomes'),
                                    '#value' => t('<ul>'.$aslo_display.'</ul>'),
                                    '#type' => 'item',
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            else {

                                $form["course_$cid"][] = array(
                                    '#title' => t('Assumed Student Learning Outcomes'),
                                    '#value' => t('<ul><li><i>There are no assumed Student Learning Outcomes for this class.</i></li></ul>'),
                                    '#type' => 'item',
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            if( $browser == "MSIE" ) {

                                // If the current browser is Microsoft Internet Explorer the button style links
                                // will not work so use a simple textual link to open the new window.
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<a href="'.$goto_course_url.'" target="_blank">View Course</a>'),
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            else {

                                // Every other browser handles the button style wrapped link no problem. Go figure?
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<input type="button" value="View Course" style="width:1.5in" />'),
                                    '#prefix' => '<blockquote><a href="'.$goto_course_url.'" target="_newtab">',
                                    '#suffix' => '</a></blockquote>',
                                );

                            }

                        } // if( $mapped )

                        // Not mapped show 
                        // registrar link.
                        else {

                            $course_info = explode(" ", $course);
                            $code = $course_info[0];
                            $number = $course_info[1];

                            // The link to the current course;
                            $goto_calendar_url = "http://www.registrar.dal.ca/calendar/class.php?subj=".$code."&num=".$number;

                            // Indicate that the course has not
                            // been mapped in daedalus as of yet.
                            $form["course_$cid"][] = array(
                                '#type' => 'item',
                                '#title' => t('Assumed Student Learning Outcomes'),
                                '#value' => t('<ul><li><i>This course is not mapped.</i></li></ul>'),
                                '#prefix' => '<blockquote>',
                                '#suffix' => '</blockquote>',
                            );

                            if( $browser == "MSIE" ) {

                                // If the current browser is Microsoft Internet Explorer the button style links
                                // will not work so use a simple textual link to open the new window.
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<a href="'.$goto_calendar_url.'" target="_blank">View Calendar</a>'),
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );
                            }

                            else {

                                // Every other browser handles the button style wrapped link no problem. Go figure?
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<input type="button" value="View Calendar" style="width:1.5in" />'),
                                    '#prefix' => '<blockquote><a href="'.$goto_calendar_url.'" target="_newtab">',
                                    '#suffix' => '</a></blockquote>',
                                );

                            }

                        }

                    } // if( $show_elective )

                } // foreach( $elect2000 as $cid )

            } // if( $elect2000 )


              /////////////
             // 3000 LEVEL

            $filter_list = $requirement_ids = array();
            $rid_placeholders = $filter_query = "";

            $result = db_query("SELECT DISTINCT requirement_id FROM {mag_student_program_form} WHERE selection_id=%d AND requirement_order >= 2000 AND requirement_order < 3000 ORDER BY log_time", $selection_id);
            while( $row = db_fetch_array($result) ) {
                $requirement_ids[] = $row['requirement_id'];
            }

            if( $requirement_ids ) {

                $rid_placeholders = implode(' OR ', array_fill(0, count($requirement_ids), "id=%d"));

                $result = db_query("SELECT DISTINCT code_filter FROM {mag_program_requirement} WHERE $rid_placeholders", $requirement_ids);

                //$result = db_query("SELECT DISTINCT code_filter FROM {mag_program_requirement} WHERE course_order >= 3000 AND course_order < 4000");
                while( $row = db_fetch_array($result) ) {

                    $code_filter = explode_and_trim(",", $row['code_filter']);

                    foreach( $code_filter as $filter ) {

                        if( $filter ) {
                            $filter_list[$filter] = $filter;
                        }

                    }

                }

                if( $filter_list ) {

                    $filter_query = implode(' OR ', array_fill(0, count($filter_list), "course_code='%s'"));

                    // Get the results for elective courses at the 2000 level by the list of course codes
                    // in the filter query. If there is no filter query select all the possible course codes.
                    if( $filter_query ) {
                        $result = db_query("SELECT id, course, course_name, mapped FROM {dae_course} WHERE (".$filter_query.") AND course_number >= 3000 AND course_number < 4000", $filter_list);
                    }else {
                        $result = db_query("SELECT id, course, course_name, mapped FROM {dae_course} WHERE course_number >= 3000 AND course_number < 4000");
                    }

                    while( $row = db_fetch_array($result) ) {

                        // Add a 2000 level elective only if the course is not already a
                        // part of the program list or a completed equivalent course.
                        if( !in_array($row['id'], $program_list) && !in_array($row['id'], $equivalent_list) ) {

                            $elect3000[$row['id']]['course'] = $row['course'];
                            $elect3000[$row['id']]['course_name'] = $row['course_name'];
                            $elect3000[$row['id']]['mapped'] = $row['mapped'];

                        }

                    }

                } // if( $filter_list )

            } // if( $requirement_ids )

            if( $elect3000 ) {

                foreach( $elect3000 as $cid => $array3000 ) {

                    $course = $array3000['course'];
                    $mapped = $array3000['mapped'];
                    $course_name = $array3000['course_name'];

                    $show_elective = TRUE;

                    $elective_sets = array();

                    $prereq_course_display = "";

                    $result = db_query("SELECT prereq_id, set_id FROM {dae_prereq_course} WHERE course_id=%d ORDER BY set_id", $cid);
                    while( $row = db_fetch_array($result) ) {

                        $set_id = $row['set_id'];
                        $prereq_id = $row['prereq_id'];

                        $prereq_mark = $program_courses[$prereq_id]['mark'];

                        $prereq_course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $prereq_id));

                        // Create a string to display the prerequisites for the required course.
                        // URL to the course
                        $url = $course_url."/".str_replace(" ", "/", $prereq_course);

                        $prereq_course_display .= "<li><a href='".$url."' target='_blank'>".$prereq_course."</a> Grade: ".$prereq_mark."</li>";

                        switch($prereq_mark) {
                            case "A+":
                            case "A":
                            case "A-":
                            case "B+":
                            case "B":
                            case "B-":
                            case "C+":
                            case "C":
                            case "C-":
                                $passing = TRUE;
                                break;
                            default:
                                $passing = FALSE;
                                break;
                        }

                        if( $set_id == 1 ) {

                            if( !in_array($prereq_id, $completed_list) || !$passing ) {
                                $show_elective = FALSE;
                            }

                        }

                        else {

                            if( (!in_array($prereq_id, $completed_list) && $elective_sets[$set_id] != "yes") || !$passing ) {
                                $elective_sets[$set_id] = "no";
                            }else {
                                $elective_sets[$set_id] = "yes";
                            }

                        }

                    }

                    if( $elective_sets ) {

                        foreach( $elective_sets as $id => $show ) {

                            if( $show == "no" ) {
                                $show_elective = FALSE;
                            }

                        }

                    }

                    if( $show_elective ) {

                        if( !$elect3000_flag ) {
                            // Heading for the 3000 level electives
                            $form[] = array(
                                '#type' => 'item',
                                '#title' => t('Possible Electives 3000+'),
                                '#prefix' => '<br /><font size="3">',
                                '#suffix' => '</font>',
                            );

                            $elect3000_flag++;
                        }

                        // The link to the current course;
                        $goto_course_url = $course_url."/".str_replace(" ", "/", $course);

                        // Check to see if the course is a goal course
                        if( $goal_courses && in_array($cid, $goal_courses) && !$build_access) {

                            $form["course_$cid"] = array(
                                '#title' => t($course." - ".$course_name." ".$course_goal_img),
                                '#type' => 'fieldset',
                                '#collapsible' => TRUE,
                                '#collapsed' => FALSE,
                            );

                        }else {

                            $form["course_$cid"] = array(
                                '#title' => t($course." - ".$course_name),
                                '#type' => 'fieldset',
                                '#collapsible' => TRUE,
                                '#collapsed' => TRUE,
                            );

                        }

                        $asslo_list = $assumed_slo = array();
                        $aslo_display = "";

                        // Get the assumed SLOs which are passed to the
                        // second parameter of the function by reference.
                        get_assumed_slos($cid, $asslo_list);

                        // Format the assumed SLOs and order by rank
                        if( $asslo_list ) {

                            $asslo_placeholders = implode(' OR ', array_fill(0, count($asslo_list), "id=%d"));

                            $result = db_query("SELECT id, slo_text FROM {dae_slo} WHERE ".$asslo_placeholders." ORDER BY slo_rank ASC, slo_text ASC", $asslo_list);
                            while($row = db_fetch_array($result) ) {
                                $assumed_slo[$row['id']] = $row['slo_text'];
                            }

                            // Foreach assumed slo id as slo text
                            foreach( $assumed_slo as $asid => $slo_text ) {

                                $aslo_display .= "<li>";
                                $course_display = $gpa = "";
                                $total = 0;

                                // Flag to determine which image to display the checkmark
                                // or the exclamation mark. The course flag determines
                                // whether to display any image at all.
                                $course_flag = FALSE;

                                $display_image = TRUE;

                                // Add where each assumed learning outcome is taught.
                                $result = db_query("SELECT course_id FROM {dae_course_slo} WHERE slo_id=%d", $asid);
                                while( $row = db_fetch_array($result) ) {

                                    $course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $row['course_id']));
                                    $grade = $program_courses[$row['course_id']]['mark'];

                                    // Remove the image from the PREU courses
                                    if( stripos($course,"PREU") === 0 ) {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='Course not completed'>".$course."</a>] ";

                                        // Do not display the image for PREU courses.
                                        $display_image = FALSE;

                                    }

                                    // If the grade is not available,
                                    // do not display it.
                                    elseif( !$grade || $grade == "N/A" ) {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='Course not completed'>".$course."</a>] ";

                                        if( in_array( $row['course_id'], $program_list) ) {
                                            $display_image = FALSE;
                                        }

                                    }

                                    else {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='My grade: ".$grade."'>".$course."</a>] ";

                                        if( in_array( $row['course_id'], $program_list) ) {

                                            switch($grade) {
                                                case "A+":
                                                    $current_grade = 4.3;
                                                    break;
                                                case "A":
                                                    $current_grade = 4.0;
                                                    break;
                                                case "A-":
                                                    $current_grade = 3.7;
                                                    break;
                                                case "B+":
                                                    $current_grade = 3.3;
                                                    break;
                                                case "B":
                                                    $current_grade = 3.0;
                                                    break;
                                                case "B-":
                                                    $current_grade = 2.7;
                                                    break;
                                                case "C+":
                                                    $current_grade = 2.3;
                                                    break;
                                                case "C":
                                                    $current_grade = 2.0;
                                                    break;
                                                case "C-":
                                                    $current_grade = 1.7;
                                                    break;
                                                case "D":
                                                    $current_grade = 1.0;
                                                    break;
                                                case "F":
                                                    $current_grade = 0;
                                                    break;
                                            }

                                            $gpa += $current_grade;
                                            $total++;

                                        }

                                    }

                                    $course_flag = TRUE;

                                }

                                if( $total > 0 ) {
                                    $gpa = $gpa/$total;
                                }else {
                                    $gpa = 0;
                                }

                                $url = $base_url."/".$slo_url."/".$asid;

                                // If build access no need
                                // to display any images.
                                if( $build_access ) {

                                    $rank = db_result(db_query("SELECT slo_rank FROM {dae_slo} WHERE id=%d", $asid));
                                    $aslo_display .= "<a href='".$url."'>".$slo_text."</a> <small><small> ".$course_display."(".$rank.")</small></small></li>";

                                }

                                elseif( $display_image && $course_flag ) {

                                    if( $gpa > 2.3 ) {

                                        // Display the goal image.
                                        if( $goal_slos && in_array($asid, $goal_slos) ) {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$checkmark_img." ".$slo_goal_img."<small><small> ".$course_display."</small></small></li>";
                                        }else {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$checkmark_img." <small><small> ".$course_display."</small></small></li>";
                                        }

                                    }

                                    else {

                                        if( $goal_slos && in_array($asid, $goal_slos) ) {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$exclamation_img." ".$slo_goal_img."<small><small> ".$course_display."</small></small></li>";
                                        }else {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$exclamation_img." <small><small> ".$course_display."</small></small></li>";
                                        }

                                    }

                                }

                                else {

                                    if( $goal_slos && in_array($asid, $goal_slos) ) {
                                        $aslo_display .= "<blockquote><a href='".$url."'>".$slo_text."</a> ".$slo_goal_img."<small><small> ".$course_display."</small></small></blockquote></li>";
                                    }else {
                                        $aslo_display .= "<blockquote><a href='".$url."'>".$slo_text."</a> <small><small> ".$course_display."</small></small></blockquote></li>";
                                    }

                                }

                            }

                        }

                        // Display the link to the course according to its mapped value.
                        // If mapped link to the Daedalus course entry, link to the
                        // Dalhousie Calendar website if the course is not mapped.
                        if( $mapped ) {

                            if( $prereq_course_display ) {
                                $form["course_$cid"][] = array(
                                    '#title' => t('Completed Prerequisite Courses'),
                                    '#value' => t('<ul>'.$prereq_course_display.'</ul>'),
                                    '#type' => 'item',
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            if( $aslo_display ) {

                                $form["course_$cid"][] = array(
                                    '#title' => t('Assumed Student Learning Outcomes'),
                                    '#value' => t('<ul>'.$aslo_display.'</ul>'),
                                    '#type' => 'item',
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            else {

                                $form["course_$cid"][] = array(
                                    '#title' => t('Assumed Student Learning Outcomes'),
                                    '#value' => t('<ul><li><i>There are no assumed Student Learning Outcomes for this class.</i></li></ul>'),
                                    '#type' => 'item',
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            if( $browser == "MSIE" ) {

                                // If the current browser is Microsoft Internet Explorer the button style links
                                // will not work so use a simple textual link to open the new window.
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<a href="'.$goto_course_url.'" target="_blank">View Course</a>'),
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }else {

                                // Every other browser handles the button style wrapped link no problem. Go figure?
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<input type="button" value="View Course" style="width:1.5in" />'),
                                    '#prefix' => '<blockquote><a href="'.$goto_course_url.'" target="_newtab">',
                                    '#suffix' => '</a></blockquote>',
                                );

                            }

                        } // if( $mapped )

                        // Not mapped show
                        // registrar link.
                        else {

                            $course_info = explode(" ", $course);
                            $code = $course_info[0];
                            $number = $course_info[1];

                            // The link to the current course;
                            $goto_calendar_url = "http://www.registrar.dal.ca/calendar/class.php?subj=".$code."&num=".$number;

                            // Indicate that the course has not
                            // been mapped in daedalus as of yet.
                            $form["course_$cid"][] = array(
                                '#type' => 'item',
                                '#title' => t('Assumed Student Learning Outcomes'),
                                '#value' => t('<ul><li><i>This course is not mapped.</i></li></ul>'),
                                '#prefix' => '<blockquote>',
                                '#suffix' => '</blockquote>',
                            );

                            if( $browser == "MSIE" ) {

                                // If the current browser is Microsoft Internet Explorer the button style links
                                // will not work so use a simple textual link to open the new window.
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<a href="'.$goto_calendar_url.'" target="_blank">View Calendar</a>'),
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            else {

                                // Every other browser handles the button style wrapped link no problem. Go figure?
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<input type="button" value="View Calendar" style="width:1.5in" />'),
                                    '#prefix' => '<blockquote><a href="'.$goto_calendar_url.'" target="_newtab">',
                                    '#suffix' => '</a></blockquote>',
                                );

                            }

                        }

                    } // if( $show_elective )

                } // foreach( $elect3000 as $cid )

            } // if( $elect3000 )


              /////////////
             // 4000 LEVEL

            $filter_list = $requirement_ids = array();
            $rid_placeholders = $filter_query = "";

            $result = db_query("SELECT DISTINCT requirement_id FROM {mag_student_program_form} WHERE selection_id=%d AND requirement_order >= 4000 AND requirement_order < 5000 ORDER BY log_time", $selection_id);
            while( $row = db_fetch_array($result) ) {
                $requirement_ids[] = $row['requirement_id'];
            }

            if( $requirement_ids ) {

                $rid_placeholders = implode(' OR ', array_fill(0, count($requirement_ids), "id=%d"));

                $result = db_query("SELECT DISTINCT code_filter FROM {mag_program_requirement} WHERE $rid_placeholders", $requirement_ids);
                while( $row = db_fetch_array($result) ) {

                    $code_filter = explode_and_trim(",", $row['code_filter']);

                    foreach( $code_filter as $filter ) {

                        if( $filter ) {
                            $filter_list[$filter] = $filter;
                        }

                    }

                }

                if( $filter_list ) {

                    $filter_query = implode(' OR ', array_fill(0, count($filter_list), "course_code='%s'"));

                    // Get the results for elective courses at the 2000 level by the list of course codes
                    // in the filter query. If there is no filter query select all the possible course codes.
                    if( $filter_query ) {
                        $result = db_query("SELECT id, course, course_name, mapped FROM {dae_course} WHERE (".$filter_query.") AND course_number >= 4000 AND course_number < 5000", $filter_list);
                    }else {
                        $result = db_query("SELECT id, course, course_name, mapped FROM {dae_course} WHERE course_number > 4000 AND course_number < 5000");
                    }

                    while( $row = db_fetch_array($result) ) {

                        // Add a 2000 level elective only if the course is not already a
                        // part of the program list or a completed equivalent course.
                        if( !in_array($row['id'], $program_list) && !in_array($row['id'], $equivalent_list) ) {

                            $elect4000[$row['id']]['course'] = $row['course'];
                            $elect4000[$row['id']]['course_name'] = $row['course_name'];
                            $elect4000[$row['id']]['mapped'] = $row['mapped'];

                        }

                    }

                } // if( $filter_list )

            } // if( $requirement_ids )

            if( $elect4000 ) {

                foreach( $elect4000 as $cid => $array4000 ) {

                    $course = $array4000['course'];
                    $mapped = $array4000['mapped'];
                    $course_name = $array4000['course_name'];

                    $show_elective = TRUE;

                    $elective_sets = array();

                    $prereq_course_display = "";

                    $result = db_query("SELECT prereq_id, set_id FROM {dae_prereq_course} WHERE course_id=%d ORDER BY set_id", $cid);
                    while( $row = db_fetch_array($result) ) {

                        $set_id = $row['set_id'];
                        $prereq_id = $row['prereq_id'];

                        $prereq_mark = $program_courses[$prereq_id]['mark'];

                        $prereq_course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $prereq_id));

                        // URL to the course
                        $url = $course_url."/".str_replace(" ", "/", $prereq_course);

                        // Create a string to display the prerequisites for the required course.
                        $prereq_course_display .= "<li><a href='".$url."' target='_blank'>".$prereq_course."</a> Grade: ".$prereq_mark."</li>";

                        switch($prereq_mark) {
                            case "A+":
                            case "A":
                            case "A-":
                            case "B+":
                            case "B":
                            case "B-":
                            case "C+":
                            case "C":
                            case "C-":
                                $passing = TRUE;
                                break;
                            default:
                                $passing = FALSE;
                                break;
                        }

                        if( $set_id == 1 ) {
                            if( !in_array($prereq_id, $completed_list) || !$passing ) {
                                $show_elective = FALSE;
                            }
                        }
                        else {
                            if( (!in_array($prereq_id, $completed_list) && $elective_sets[$set_id] != "yes") || !$passing ) {
                                $elective_sets[$set_id] = "no";
                            }else {
                                $elective_sets[$set_id] = "yes";
                            }
                        }
                    }

                    if( $elective_sets ) {
                        foreach( $elective_sets as $id => $show ) {
                            if( $show == "no" ) {
                                $show_elective = FALSE;
                            }
                        }
                    }

                    if( $show_elective ) {

                        if( !$elect4000_flag ) {
                            // Heading for the 4000 level electives
                            $form[] = array(
                                '#type' => 'item',
                                '#title' => t('Possible Electives 4000+'),
                                '#prefix' => '<br /><font size="3">',
                                '#suffix' => '</font>',
                            );

                            $elect4000_flag++;
                        }

                        // The link to the current course;
                        $goto_course_url = $course_url."/".str_replace(" ", "/", $course);

                        // Check to see if the course is a goal course
                        if( $goal_courses && in_array($cid, $goal_courses) && !$build_access) {

                            $form["course_$cid"] = array(
                                '#title' => t($course." - ".$course_name." ".$course_goal_img),
                                '#type' => 'fieldset',
                                '#collapsible' => TRUE,
                                '#collapsed' => FALSE,
                            );

                        }else {

                            $form["course_$cid"] = array(
                                '#title' => t($course." - ".$course_name),
                                '#type' => 'fieldset',
                                '#collapsible' => TRUE,
                                '#collapsed' => TRUE,
                            );

                        }

                        $asslo_list = $assumed_slo = array();
                        $aslo_display = "";

                        // Get the assumed SLOs which are passed to the
                        // second parameter of the function by reference.
                        get_assumed_slos($cid, $asslo_list);

                        // Format the assumed SLOs and order by rank
                        if( $asslo_list ) {

                            $asslo_placeholders = implode(' OR ', array_fill(0, count($asslo_list), "id=%d"));

                            $result = db_query("SELECT id, slo_text FROM {dae_slo} WHERE ".$asslo_placeholders." ORDER BY slo_rank ASC, slo_text ASC", $asslo_list);
                            while($row = db_fetch_array($result) ) {
                                $assumed_slo[$row['id']] = $row['slo_text'];
                            }

                            // Foreach assumed slo id as slo text
                            foreach( $assumed_slo as $asid => $slo_text ) {

                                $aslo_display .= "<li>";
                                $course_display = $gpa = "";
                                $total = 0;

                                // Flag to determine which image to display the checkmark
                                // or the exclamation mark. The course flag determines
                                // whether to display any image at all.
                                $course_flag = FALSE;

                                $display_image = TRUE;

                                // Add where each assumed learning outcome is taught.
                                $result = db_query("SELECT course_id FROM {dae_course_slo} WHERE slo_id=%d", $asid);

                                while( $row = db_fetch_array($result) ) {

                                    $course = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $row['course_id']));
                                    $grade = $program_courses[$row['course_id']]['mark'];

                                    // Remove the image from the PREU courses
                                    if( stripos($course,"PREU") === 0 ) {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='Course not completed'>".$course."</a>] ";

                                        // Do not display the image for PREU courses.
                                        $display_image = FALSE;
                                    }

                                    // If the grade is not available,
                                    // do not display it.
                                    elseif( !$grade || $grade == "N/A" ) {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='Course not completed'>".$course."</a>] ";

                                        if( in_array( $row['course_id'], $program_list) ) {
                                            $display_image = FALSE;
                                        }

                                    }

                                    else {

                                        // URL to the course
                                        $url = $course_url."/".str_replace(" ", "/", $course);

                                        $course_display .= "[<a href='".$url."' title='My grade: ".$grade."'>".$course."</a>] ";

                                        if( in_array( $row['course_id'], $program_list) ) {

                                            switch($grade) {
                                                case "A+":
                                                    $current_grade = 4.3;
                                                    break;
                                                case "A":
                                                    $current_grade = 4.0;
                                                    break;
                                                case "A-":
                                                    $current_grade = 3.7;
                                                    break;
                                                case "B+":
                                                    $current_grade = 3.3;
                                                    break;
                                                case "B":
                                                    $current_grade = 3.0;
                                                    break;
                                                case "B-":
                                                    $current_grade = 2.7;
                                                    break;
                                                case "C+":
                                                    $current_grade = 2.3;
                                                    break;
                                                case "C":
                                                    $current_grade = 2.0;
                                                    break;
                                                case "C-":
                                                    $current_grade = 1.7;
                                                    break;
                                                case "D":
                                                    $current_grade = 1.0;
                                                    break;
                                                case "F":
                                                    $current_grade = 0;
                                                    break;
                                            }

                                            $gpa += $current_grade;
                                            $total++;

                                        }
                                    }

                                    $course_flag = TRUE;
                                }

                                if( $total > 0 ) {
                                    $gpa = $gpa/$total;
                                }else {
                                    $gpa = 0;
                                }

                                $url = $base_url."/".$slo_url."/".$asid;

                                // If build access no need
                                // to display any images.
                                if( $build_access ) {

                                    $rank = db_result(db_query("SELECT slo_rank FROM {dae_slo} WHERE id=%d", $asid));
                                    $aslo_display .= "<a href='".$url."'>".$slo_text."</a> <small><small> ".$course_display."(".$rank.")</small></small></li>";

                                }

                                elseif( $display_image && $course_flag ) {

                                    if( $gpa > 2.3 ) {

                                        // Display the goal image.
                                        if( $goal_slos && in_array($asid, $goal_slos) ) {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$checkmark_img." ".$slo_goal_img."<small><small> ".$course_display."</small></small></li>";
                                        }else {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$checkmark_img." <small><small> ".$course_display."</small></small></li>";
                                        }
                                    }

                                    else {

                                        if( $goal_slos && in_array($asid, $goal_slos) ) {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$exclamation_img." ".$slo_goal_img."<small><small> ".$course_display."</small></small></li>";
                                        }else {
                                            $aslo_display .= "<a href='".$url."'>".$slo_text."</a> ".$exclamation_img." <small><small> ".$course_display."</small></small></li>";
                                        }

                                    }

                                }

                                else {

                                    if( $goal_slos && in_array($asid, $goal_slos) ) {
                                        $aslo_display .= "<blockquote><a href='".$url."'>".$slo_text."</a> ".$slo_goal_img."<small><small> ".$course_display."</small></small></blockquote></li>";
                                    }else {
                                        $aslo_display .= "<blockquote><a href='".$url."'>".$slo_text."</a> <small><small> ".$course_display."</small></small></blockquote></li>";
                                    }

                                }

                            }

                        }

                        // Display the link to the course according to its mapped value.
                        // If mapped link to the Daedalus course entry, link to the
                        // Dalhousie Calendar website if the course is not mapped.
                        if( $mapped ) {

                            if( $prereq_course_display ) {

                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#title' => t('Completed Prerequisite Courses'),
                                    '#value' => t('<ul>'.$prereq_course_display.'</ul>'),
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            if( $aslo_display ) {

                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#title' => t('Assumed Student Learning Outcomes'),
                                    '#value' => t('<ul>'.$aslo_display.'</ul>'),
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            else {

                                $form["course_$cid"][] = array(
                                    '#title' => t('Assumed Student Learning Outcomes'),
                                    '#value' => t('<ul><li><i>There are no assumed Student Learning Outcomes for this class.</i></li></ul>'),
                                    '#type' => 'item',
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            if( $browser == "MSIE" ) {

                                // If the current browser is Microsoft Internet Explorer the button style links
                                // will not work so use a simple textual link to open the new window.
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<a href="'.$goto_course_url.'" target="_blank">View Course</a>'),
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }else {

                                // Every other browser handles the button style wrapped link no problem. Go figure?
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<input type="button" value="View Course" style="width:1.5in" />'),
                                    '#prefix' => '<blockquote><a href="'.$goto_course_url.'" target="_newtab">',
                                    '#suffix' => '</a></blockquote>',
                                );

                            }

                        } // if( $mapped )

                        // Not mapped show 
                        // registrar link.
                        else {

                            $course_info = explode(" ", $course);
                            $code = $course_info[0];
                            $number = $course_info[1];

                            // The link to the current course;
                            $goto_calendar_url = "http://www.registrar.dal.ca/calendar/class.php?subj=".$code."&num=".$number;

                            // Indicate that the course has not
                            // been mapped in daedalus as of yet.
                            $form["course_$cid"][] = array(
                                '#type' => 'item',
                                '#title' => t('Assumed Student Learning Outcomes'),
                                '#value' => t('<ul><li><i>This course is not mapped.</i></li></ul>'),
                                '#prefix' => '<blockquote>',
                                '#suffix' => '</blockquote>',
                            );

                            if( $browser == "MSIE" ) {

                                // If the current browser is Microsoft Internet Explorer the button style links
                                // will not work so use a simple textual link to open the new window.
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<a href="'.$goto_calendar_url.'" target="_blank">View Calendar</a>'),
                                    '#prefix' => '<blockquote>',
                                    '#suffix' => '</blockquote>',
                                );

                            }

                            else {

                                // Every other browser handles the button style wrapped link no problem. Go figure?
                                $form["course_$cid"][] = array(
                                    '#type' => 'item',
                                    '#value' => t('<input type="button" value="View Calendar" style="width:1.5in" />'),
                                    '#prefix' => '<blockquote><a href="'.$goto_calendar_url.'" target="_newtab">',
                                    '#suffix' => '</a></blockquote>',
                                );

                            }

                        }

                    } // if( $show_elective )

                } // foreach( $elect4000 as $cid )

            } // if( $elect4000 )

        } // if( $program_courses && $completed_courses )

        // If in a session display session logout button.
        if( $in_session ) {

            $form['end-session'] = array(
                '#type' => 'submit',
                '#value' => t('End advising session'),
            );

            // Submit the hidden session id.
            $form['$session_id'] = array( '#type' => 'value', '#value' => $session_id, );

        }

        return $form;

    } // END if( $advisor_no_session )

    else {

        drupal_set_message(t('You must be in session to view a students program next step.'));

        drupal_goto($base_url."/".daedalus_get_setting("advise student"));

    }

}


/**
 * Implementation of hook_submit().
 */
function daedalus_program_nextstep_form_submit( $form, &$form_state ) {

    switch($form_state['values']['op']) {

        case $form_state['values']['end-session']:

            db_query("DELETE FROM {mag_advisor_session} WHERE id=%d", $form_state['values']['session-id']);

            drupal_set_message(t('Current session closed.'));

            // This generates a new session id.
            $_COOKIE[session_regenerate_id()];

            drupal_goto($base_url."/".daedalus_get_setting("advise student"));

            break;

        case $form_state['values']['dae-help-submit']:

            global $base_url;
            $page_url = daedalus_get_setting("program nextstep");

            $page_url_length = sizeof(explode('/',$page_url));

            $param[0] = arg(0+$page_url_length);

            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message(t('Help information saved.'));

                    if( $param[0]) {
                        drupal_goto($base_url."/".$page_url."/".$param[0]);
                    }else {
                    drupal_goto($base_url."/".$page_url);
                    }
            }

            break;

    }

}



//------------------------------------------------------------------------------------------------------------------------------------------
// Daedalus->Program->Goal Callback Functions
//------------------------------------------------------------------------------------------------------------------------------------------
function daedalus_program_goal_page() {
    return drupal_get_form("daedalus_program_goal_form");
};


/**
* Menu Location: Daedalus -> Programs -> Goal
* URL Location:  daedalus/daedalus/program/goal
*
* Displays
*
* @global  $base_url
* @param <type> $form
* @return string
*/
function daedalus_program_goal_form( $form ) {

    // Get daedalus base url
    global $base_url;

    // Determine the user name
    global $user;
    $user_id = $user->uid;

    // If the current user is a Magellan Advisor determine if there is a current advising session open.
    if( db_result(db_query("SELECT COUNT(*) FROM {role, users_roles} WHERE role.name='Magellan Advisor' AND users_roles.uid=%d AND users_roles.rid = role.rid", $user_id)) ) {

        $result = db_query("SELECT id, student_id FROM {mag_advisor_session} WHERE advisor_id=%d", $user_id);
        while( $row = db_fetch_array($result) ) {
            $session_id  = $row['id'];
            $session_sid = $row['student_id'];
        }

        // Get the session name.
        $current_session = $_COOKIE[session_name()];

        // If in session, set the user name as
        // to the selected students username.
        if( $session_id == $current_session ) {

            $in_session = TRUE;
            $user_name = db_result(db_query("SELECT user_name FROM {mag_student_identification} WHERE id=%d", $session_sid));

        }

        // Not in session, set flag to redirect the
        // advisor back to select a student to advise.
        else {
            $no_session = TRUE;
            // TODO: REMOVE, ONLY HERE WHILE ACCESS GIVEN TO "daedalus manage"
            $user_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $user_id));//
        }

    }

    // User is not an advisor, select
    // the current users username.
    else {
        // Determine the user name
        $user_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $user_id));
    }

    // If the user is an advisor, the advisor must be currently be in
    // a session to view the program form information of a student.
    if( !$no_session || user_access("daedalus manage") ) {      // **** user_access("daedalus manage) TEMPORARY ONLY TO ALLOW FOR EASIER DEVELOPMENT ****** REMOVE LATER *****

        // Get current page url.
        $page_url = $help_url = daedalus_get_setting("program goal");

        // Get page urls.
        $course_url = $base_url."/".daedalus_get_setting("manage courses");
        $slo_url = $base_url."/".daedalus_get_setting("manage learning outcomes");

        // Get access information
        $build_access = user_access("daedalus build");

        // Store URL Parameters in $param array
        $page_url_length = sizeof(explode('/',$page_url));
        $page_url = $base_url."/".$page_url;

        $param = array();

        $param[0] = arg(0+$page_url_length);    // "view/create", determines if the program has been selected or is being created.
        $param[1] = arg(1+$page_url_length);    // $program, the selected program.
        $param[2] = arg(2+$page_url_length);    // $year, the selected program year.

        // Get the images
        $computer_science_logo       = $base_url."/".daedalus_get_setting("computer science logo");
        $small_computer_science_logo = $base_url."/".daedalus_get_setting("computer science logo small");

        // Get the user's student information.
        if( $in_session ) {

            $student = db_fetch_array(db_query("SELECT id, official_selection, first_name, last_name, goal_type, goal_id FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $selection_id = $student['official_selection'];
            $student_id = $student['id'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];
            $student_id = $student['id'];
            $goal_type = $student['goal_type'];
            $goal_id = $student['goal_id'];

        }

        else {

            $student = db_fetch_array(db_query("SELECT id, current_selection, first_name, last_name, goal_type, goal_id FROM {mag_student_identification} WHERE user_name='%s'", $user_name ));

            $selection_id = $student['current_selection'];
            $student_id = $student['id'];
            $first_name = $student['first_name'];
            $last_name = $student['last_name'];
            $student_id = $student['id'];
            $goal_type = $student['goal_type'];
            $goal_id = $student['goal_id'];

        }

        // Get the program id
        $program_id = db_result(db_query("SELECT program_id FROM {mag_student_program_selection} WHERE id=%d", $selection_id ));

        // Get the program information
        $program = db_result(db_query("SELECT program FROM {mag_program_identification} WHERE id=%d", $program_id ));

        // Display the title, if the user has selected their program display the username and program information.
        // If the user has not selected their program prompt them to do so with a link the the program form page.
        if( $program_id && $first_name && $last_name ) {

            if( $in_session ) {
                $advising_text = "Advising:";
            }

            drupal_set_title(t("<b>".$advising_text." ".$first_name." ".$last_name."</b> <a class='show-help'><img src='".$computer_science_logo."' align='right' alt='?' /></a> <br /><i><small>".$program."<br />Program Goal:</small></i>"));

        }
        else {

            drupal_set_message(t('You must first select a program before you can create a program goal.'), 'warning');

            drupal_goto($base_url."/".daedalus_get_setting("program form"));

        }

        $form = array();

        // Add the hidden help form. Paramaters are
        // (help url, show border, show break).
        $form = display_hidden_form($help_url, 1, 1);

        // If the student has not selected a goal and
        // there are no URL parameters present.
        if( !$param[0] && !$goal_type ) {

            // Make sure the user has been created
            // and the program has been selected.
            if( $program_id && $first_name && $last_name ) {

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<h3>Select goal by career</h3>'),
                );

                $result = db_query("SELECT * FROM {mag_goal_label} ORDER BY goal_label");
                while( $row = db_fetch_array($result) ) {

                    $slo_count = db_result(db_query("SELECT COUNT(*) FROM {mag_goal_sets} WHERE label_id=%d", $row['id']));

                    $label_url = $page_url."/label/".$row['id'];

                    $label_string .= "<a href='".$label_url."'>#".$row['goal_label']."</a> <small><small>(".$slo_count.")</small></small>  ";
                }

                if( !$label_string ) {
                    $label_string = "<ul><li><i>No matches found</i></li></ul>";
                }

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t($label_string),
                    '#suffix' => '<br />',
                );

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<h3>Select goal by course</h3>'),
                );

                // Border size, Column size, and table string
                $columns = daedalus_get_setting("column iteration");

                $course_codes = array();

                // Select the list of course codes
                $result = db_query("SELECT DISTINCT course_code FROM {dae_course} WHERE course_code != 'PREU'");
                while($row = db_fetch_array($result)) {
                    $course_codes[$row['course_code']] = array();
                }

                // Select the list of alias course codes
                $result = db_query("SELECT DISTINCT alias_code FROM {dae_course_alias} WHERE alias_code != 'PREU'");
                while($row = db_fetch_array($result)) {
                    $course_codes[$row['alias_code']] = array();
                }

                // Sort the code values.
                ksort($course_codes);

                // Foreach course code => empty array of courses.
                foreach( $course_codes as $code => $array ) {

                    // Get all courses with the current code.
                    $result = db_query("SELECT course_code, course, course_name, mapped FROM {dae_course} WHERE course_code='%s' ORDER BY course_number", $code);
                    while($row = db_fetch_array($result)) {

                        // Only add courses to the list
                        // that have been mapped.
                        if( $row['mapped'] ) {
                            $course_codes[$row['course_code']][] = $row['course'] . " - " . $row['course_name'];
                        }
                    }
                }

                // Foreach course code => empty array of courses.
                foreach( $course_codes as $code => $array ) {

                    // Get all alias courses with the current code.
                    $result = db_query("SELECT parent_id, alias_code, alias_course FROM {dae_course_alias} WHERE alias_code='%s' ORDER BY alias_number", $code);
                    while($row = db_fetch_array($result)) {

                        $course_info = db_fetch_array(db_query("SELECT course_name, mapped FROM {dae_course} WHERE id=%d", $row['parent_id']));

                        $alias_name = $course_info['course_name'];
                        $mapped = $course_info['mapped'];

                        // Only add courses to the list
                        // that have been mapped.
                        if( $row['mapped'] ) {
                            $course_codes[$row['alias_code']][] = $row['alias_course'] . " - ".$alias_name;
                        }
                    }
                }

                // Order the courses for a single column.
                if( $columns == 1 || $columns > 4 ) {

                    foreach( $course_codes as $code => $array ) {

                        // Sort the list of courses
                        sort($array);

                        // For each course code (dept) save the
                        // freshly sorted course list.
                        $course_codes[$code] = $array;
                    }
                }

                // Order the courses for 2TWO2 columns.
                if( $columns == 2 ) {

                    foreach( $course_codes as $code => $array ) {

                        // Sort the list of courses
                        sort($array);
                        $course_total = count($array);

                        if( $course_total >= 2 ) {

                            $column_division = round(count($array)/$columns);
                            $column_remainder = ($column_division * 2) - $course_total;
                            $new_order = array();

                            $first_array = $second_array = array();

                            if( $column_remainder == 0 ) {
                                $division1 = $column_division;
                                $division2 = ($column_division * 2);
                            }elseif( $column_remainder == 1 ) {
                                $division1 = $column_division;
                                $division2 = ($column_division * 2) - 1;
                            }elseif( $column_remainder == -1 ) {
                                $division1 = $column_division + 1;
                                $division2 = ($column_division * 2);
                            }
                            for( $i=0; $i < $course_total; $i++ ) {

                                if( $i < $division1 ) {
                                    $first_array[] = $array[$i];
                                }

                                else {
                                    $second_array[] = $array[$i];
                                }

                            }

                            $inc1 = $inc2 = 0;

                            for( $i=0; $i < $course_total; $i++ ) {

                                if( ($i % 2) == 0 ) {
                                    $new_order[] = $first_array[$inc1++];
                                }
                                elseif( ($i % 2) == 1 ) {
                                    $new_order[] = $second_array[$inc2++];
                                }
                            }

                            $course_codes[$code] = $new_order;
                        }

                        // else if the amount of courses is too small to
                        // divide into columns, just copy the ordered array.
                        else {
                            $course_codes[$code] =  $array;
                        }
                    }
                }

                // Order the courses for 3THREE3 columns.
                if( $columns == 3 ) {

                    foreach( $course_codes as $code => $array ) {

                        // Sort the list of courses
                        sort($array);
                        $course_total = count($array);

                        if( $course_total >= 3 ) {

                            $column_division = round(count($array)/$columns);
                            $column_remainder = ($column_division * 3) - $course_total;
                            $new_order = array();

                            $first_array = $second_array = $third_array = array();

                            if( $column_remainder == 0 ) {
                                $division1 = $column_division;
                                $division2 = ($column_division * 2);
                                $division3 = ($column_division * 3);
                            }elseif( $column_remainder == 1 ) {
                                $division1 = $column_division;
                                $division2 = ($column_division * 2);
                                $division3 = ($column_division * 3) - 1;
                            }elseif( $column_remainder == 2 ) {
                                $division1 = $column_division;
                                $division2 = ($column_division * 2) - 1;
                                $division3 = ($column_division * 3) - 1;
                            }elseif( $column_remainder == -1 ) {
                                $division1 = $column_division + 1;
                                $division2 = ($column_division * 2) + 1;
                                $division3 = ($column_division * 3);
                            }
                            for( $i=0; $i < $course_total; $i++ ) {

                                if( $i < $division1 ) {
                                    $first_array[] = $array[$i];
                                }

                                elseif( $i >= $division1 && $i < $division2 ) {
                                    $second_array[] = $array[$i];
                                }

                                else {
                                    $third_array[] = $array[$i];
                                }

                            }

                            $inc1 = $inc2 = $inc3 = 0;

                            for( $i=0; $i < $course_total; $i++ ) {

                                if( ($i % 3) == 0 ) {
                                    $new_order[] = $first_array[$inc1++];
                                }
                                elseif( ($i % 3) == 1 ) {
                                    $new_order[] = $second_array[$inc2++];
                                }
                                elseif( ($i % 3) == 2 ) {
                                    $new_order[] = $third_array[$inc3++];
                                }
                            }

                            $course_codes[$code] = $new_order;
                        }

                        // else if the amount of courses is too small to
                        // divide into columns, just copy the ordered array.
                        else {
                            $course_codes[$code] =  $array;
                        }
                    }
                }

                // Order the courses for 4FOUR4 columns.
                if( $columns == 4 ) {

                    foreach( $course_codes as $code => $array ) {

                        // Sort the list of courses
                        sort($array);
                        $course_total = count($array);

                        if( $course_total >= 4 ) {

                            $column_division = round(count($array)/$columns);
                            $column_remainder = ($column_division * 4) - $course_total;
                            $new_order = array();

                            $first_array = $second_array = $third_array = $fourth_array = array();

                            if( $column_remainder == 0 ) {
                                $division1 = $column_division;
                                $division2 = ($column_division * 2);
                                $division3 = ($column_division * 3);
                                $division4 = ($column_division * 4);
                            }elseif( $column_remainder == 1 ) {
                                $division1 = $column_division;
                                $division2 = ($column_division * 2);
                                $division3 = ($column_division * 3);
                                $division4 = ($column_division * 4) - 1;
                            }elseif( $column_remainder == 2 ) {
                                $division1 = $column_division;
                                $division2 = ($column_division * 2);
                                $division3 = ($column_division * 3) - 1;
                                $division4 = ($column_division * 4) - 1;
                            }elseif( $column_remainder == 3 ) {
                                $division1 = $column_division;
                                $division2 = ($column_division * 2) - 1;
                                $division3 = ($column_division * 3) - 1;
                                $division4 = ($column_division * 4) - 1;
                            }elseif( $column_remainder == -1 ) {
                                $division1 = $column_division + 1;
                                $division2 = ($column_division * 2) + 1;
                                $division3 = ($column_division * 3) + 1;
                                $division4 = ($column_division * 4);
                            }
                            for( $i=0; $i < $course_total; $i++ ) {

                                if( $i < $division1 ) {
                                    $first_array[] = $array[$i];
                                }

                                elseif( $i >= $division1 && $i < $division2 ) {
                                    $second_array[] = $array[$i];
                                }

                                elseif( $i >= $division2 && $i < $division3 ) {
                                    $third_array[] = $array[$i];
                                }

                                else {
                                    $fourth_array[] = $array[$i];
                                }

                            }

                            $inc1 = $inc2 = $inc3 = $inc4 = 0;

                            for( $i=0; $i < $course_total; $i++ ) {

                                if( ($i % 4) == 0 ) {
                                    $new_order[] = $first_array[$inc1++];
                                }
                                elseif( ($i % 4) == 1 ) {
                                    $new_order[] = $second_array[$inc2++];
                                }
                                elseif( ($i % 4) == 2 ) {
                                    $new_order[] = $third_array[$inc3++];
                                }
                                elseif( ($i % 4) == 3 ) {
                                    $new_order[] = $fourth_array[$inc4++];
                                }
                            }

                            $course_codes[$code] = $new_order;
                        }

                        // else if the amount of courses is too small to
                        // divide into columns, just copy the ordered array.
                        else {
                            $course_codes[$code] =  $array;
                        }
                    }
                }

                // The string can be set to cut off after a certian amount
                // of digits. This finds the value from the database
                $preview_length = daedalus_get_setting("course name preview length");

                // Display all the course information in the table.
                foreach( $course_codes as $code => $course_list ) {

                    if( count($course_list) > 0 ) {

                        // Start out by making one row with just the title of the department being viewed,
                        // and the code. So this gets the name of the department currently being printed.
                        $name = db_result(db_query("SELECT department_name FROM {dae_valid_course_codes} WHERE course_code='%s'",$code));

                        // This is what will be displayed, for formatting and such
                        $code_header = $code." - ".$name;

                        // Only enter space after first department.
                        if( $flag++ ) {
                            $space = "<tr><td>&nbsp;</td></tr>";
                        }

                        // Put it into the string
                        $table .= $space."<tr style='border-top-style:solid; border-width:2px; border-color:#C0C0C0;'><td colspan=".$columns."><b><big>".$code_header."</big></b></td></tr>";

                        $i=0; // Row counter

                        foreach( $course_list as $iter => $course ) {

                            // The course list contains the course code, number and name.
                            // Explode this value to get the number from the 2nd element.
                            $temp = explode(" ",$course);
                            $number = $temp[1];

                            $crosslisted = db_result(db_query("SELECT COUNT(*) FROM {dae_course_alias} WHERE alias_code='%s' AND alias_number=%d", $code, $number));

                            if( $crosslisted ) {

                                // Get the cross listed courses parent id
                                $course_id = db_result(db_query("SELECT parent_id FROM {dae_course_alias} WHERE alias_code='%s' AND alias_number=%d", $code, $number));

                            }else {

                                // Get the course id.
                                $course_id = db_result(db_query("SELECT id FROM {dae_course} WHERE course_code='%s' AND course_number='%s'", $code, $number));

                            }

                            $course_url = $page_url."/course/".$course_id;

                            // Find new Row position
                            if( $i % $columns == 0 ) {
                                if( $i != 0 ) {
                                    $table .= "</tr>";
                                }
                                $table .= "<tr>";
                            }

                            $i++;

                            // The course variable may need to be cut off depending on length. To make sure the
                            // full string isn't lost make a copy for hovering. Display(...) to replace excess.
                            $display_course = $course;

                            if( $preview_length > 0 ) {
                                if( strlen($display_course) > $preview_length ) {
                                    $display_course = substr($course,0,$preview_length-3)."...";
                                }
                            }

                            // Append what should be displayed in the table to the string.
                            $table .= "<td><a href='".$course_url."' title='".$course."'>".$display_course."</a></td>";

                        }

                        // Close final row
                        $table .= "</tr>";

                    }
                }

                if( !user_access("daedalus build courses") && !$table ) {
                    $table = "<ul><li><i>No matches found</i></li></ul>";
                }

                //and then display the table and return
                $form['display-courses'] = array(
                    '#type' => 'item',
                    '#value' => t("<table>".$table."</table>"),
                );

            }

        } // if( $goal_type)

        // Confirm the selected course goal
        elseif( $param[0] == "course" ) {

            $course_id = $param[1];

            $course_info = db_fetch_array(db_query("SELECT course, course_name FROM {dae_course} WHERE id=%d", $course_id));

            $course = $course_info['course'];
            $course_name = $course_info['course_name'];

            $form[] = array(
                '#type' => 'item',
                '#value' => t('<h3>Confirm course goal: '.$course." - ".$course_name.'</h3>'),
            );

            $form['confirm-course'] = array(
                '#type' => 'submit',
                '#value' => t('Confirm'),
            );

            $form['cancel'] = array(
                '#type' => 'submit',
                '#value' => t('Cancel'),
            );

            // Submit hidden form data
            $form['course-id']  = array( '#type' => 'value', '#value' => $course_id, );
            $form['student-id'] = array( '#type' => 'value', '#value' => $student_id, );

        } // elseif( $param[0] == "course")

        // Confirm the selected label goal.
        elseif( $param[0] == "label" ) {

            $label_id = $param[1];

            $goal_label = db_result(db_query("SELECT goal_label FROM {mag_goal_label} WHERE id=%d", $label_id));

            $form[] = array(
                '#type' => 'item',
                '#value' => t('<h3>Confirm career goal: '.$goal_label.'</h3>'),
            );

            $form['confirm-label'] = array(
                '#type' => 'submit',
                '#value' => t('Confirm'),
            );

            $form['cancel'] = array(
                '#type' => 'submit',
                '#value' => t('Cancel'),
            );

            // Submit hidden form data
            $form['label-id']   = array( '#type' => 'value', '#value' => $label_id, );
            $form['student-id'] = array( '#type' => 'value', '#value' => $student_id, );

        } // elseif( $param[0] == "label")

        // Confirm the deletion of the users current goal.
        elseif( $param[0] == "confirm_switch" ) {

            if( $goal_type == "course" ) {
                $course_info = db_fetch_array(db_query("SELECT course, course_name FROM {dae_course} WHERE id=%d", $goal_id));

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<h3>Delete Course Goal:  <i>'.$course_info['course'].' - '.$course_info['course_name'].'</i></h3><br />'),
                );
            }
            elseif( $goal_type == "label" ) {
                $career = db_result(db_query("SELECT goal_label FROM {mag_goal_label} WHERE id=%d", $goal_id));

                $form[] = array(
                    '#type' => 'item',
                    '#value' => t('<h3>Delete Career Goal:  <i>'.$career.'</i></h3><br />'),
                );
            }

            $form['delete-forward'] = array(
                '#type' => 'submit',
                '#value' => t("Delete goal."),
            );

            $form['delete-reverse'] = array(
                '#type' => 'submit',
                '#value' => t("On second thought, I better not. Take me back."),
            );

            // Submit hidden information to reduce database accesses
            $form['student-id'] = array( '#type' => 'value', '#value' => $student_id, );

        } // elseif( $param[0] == "confirm_switch" )

        // Calculate the SLO information, then the course info.
        // Create the course map from the course info, display.
        else {

            // Determine if the current browse
            // is Microsoft's Internet Explorer.
            if( preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT']) ) {
                $browser = "MSIE";
            }else {
                $browser = "OK";
            }

            $recursed_slos = $goal_slos = $course_ids = array();

            // Determine selection by goal type
            if( $goal_type == "label" ) {
                $result = db_query("SELECT slo_id FROM {mag_goal_sets} WHERE label_id=%d", $goal_id);
            }
            elseif( $goal_type == "course") {
                $result = db_query("SELECT slo_id FROM {dae_course_slo} WHERE course_id=%d", $goal_id);
            }

            while( $row = db_fetch_array($result) ) {

                // Recurse all the SLOs
                get_prereq_slos_recursively($row['slo_id'], $recursed_slos);

                // Make a list of the original goal
                // SLOs to add to the recursed SLOs.
                $goal_slos[] = $row['slo_id'];
            }

            // Make sure there were
            // SLOs to recurse upon.
            if( $recursed_slos ) {

                // Add the label SLO's to the complete list of
                // learning outcomes that are recursed upon.
                $recursed_slos = array_merge($recursed_slos, $goal_slos);

                // Remove Duplicates
                $recursed_slos = array_unique($recursed_slos);

                sort($recursed_slos);

                // Create the query string placeholder to select the
                // courses according to the recursed SLO values.
                $rec_placeholders = implode(' OR ', array_fill(0, count($recursed_slos), "slo_id=%d"));

                // Implement the course query
                $result = db_query("SELECT DISTINCT course_id FROM {dae_course_slo} WHERE $rec_placeholders ORDER BY course_id", $recursed_slos);
                while( $row = db_fetch_array($result) ) {
                    $course_ids[$row['course_id']] = $row['course_id'];
                }

                // Create the list of placeholders to accommodate a variable number of arguments.
                $slo_placeholders = implode(',', array_fill(0, count($recursed_slos), "(%d, %d)"));

                $query_values = array();

                // Create the query values, a student
                // id followed by the slo id value.
                foreach( $recursed_slos as $sloid ) {
                    $query_values[] = $student_id;
                    $query_values[] = $sloid;
                }

                // Delete
                db_query("DELETE FROM {mag_goal_student_slo} WHERE student_id=%d", $student_id);

                // Insert
                db_query("INSERT INTO {mag_goal_student_slo} (student_id, slo_id) VALUES $slo_placeholders", $query_values);

            }else {

                drupal_set_message(t('There are no learning outcomes associated with your goal.'), 'warning');

                $form['change-goal'] = array(
                    '#type' => 'submit',
                    '#value' => t('Change Goal'),
                    '#prefix' => t('<br />'),
                );

                if( $in_session ) {

                    $form['end-session'] = array(
                        '#type' => 'submit',
                        '#value' => t('End advising session'),
                    );

                    // Submit the hidden session id.
                    $form['$session_id'] = array( '#type' => 'value', '#value' => $session_id, );

                }

            }

              //////////////////////////////////////////////////////////////////////
             //////////////////////////////////////////////////////////////////////
            // Create Course Map - There must be courses to create the course map

            // Determine if the graphviz directory is properly installed
            // and get the working directory and path for the dot program.
            $installed = graphviz_installed($pwd, $graphviz_path);

            if( $course_ids && $installed ) {

                $map_output  = "digraph Constellation {\n";
                $map_output .= "size=\"10,13\"\n";

                $completed_courses = $slo_check = array();

                // Determine which courses have been completed by the user
                $result = db_query("SELECT mark, requirement_id FROM {mag_student_program_form} WHERE mark!='N/A' AND selection_id=%d", $selection_id);
                while( $row = db_fetch_array($result) ) {

                    switch( $row['mark'] ) {
                            case "A+":
                            case "A":
                            case "A-":
                            case "B+":
                            case "B":
                            case "B-":
                            case "C+":
                            case "C":
                            case "C-":

                                // Select the course id from the requirement.
                                $completed_id = db_result(db_query("SELECT course_id FROM {mag_program_requirement} WHERE id=%d", $row['requirement_id']));

                                $completed_courses[$completed_id] = $completed_id;

                            break;
                        }
                }

                // Create the query to select courses with prerequisites.
                $goal_query = "SELECT * FROM {dae_course} WHERE ";

                $goal_query .= implode(' OR ', array_fill(0, count($course_ids), "id=%d"));

                $goal_query .= " ORDER BY course";

                // Get the the courses with prerequisites
                $result = db_query($goal_query, $course_ids);

                while( $row = db_fetch_array($result) ) {

                    $goal_course[$row['id']]['id'] = $row['id'];
                    $goal_course[$row['id']]['course'] = $row['course'];
                    $goal_course[$row['id']]['mapped'] = $row['mapped'];
                    $goal_course[$row['id']]['course_code'] = $row['course_code'];
                    $goal_course[$row['id']]['course_number'] = $row['course_number'];
                    $goal_course[$row['id']]['course_name'] = $row['course_name'];

                }

                // Modify the list of goal courses, remove any course down the chain
                // where all of its goal SLOs have been covered by another course...
                // This step must be completed after the courses have been arranged
                // by their course value, which orders courses by dept then code....
                foreach( $goal_course as $id => $gc_array ) {

                    $unique_slos = FALSE;

                    $result = db_query("SELECT slo_id FROM {dae_course_slo} WHERE course_id=%d ORDER BY slo_id", $id);
                    while( $row = db_fetch_array($result) ) {

                        if( in_array($row['slo_id'], $recursed_slos) ) {

                            if( !in_array($row['slo_id'], $slo_check) ) {
                                $unique_slos = TRUE;
                            }

                            $slo_check[$row['slo_id']] = $row['slo_id'];
                        }
                    }

                    // If the are no unique SLOs for the current
                    // course, remove it from the list of goal courses.
                    if( !$unique_slos ) {
                        unset($goal_course[$id]);
                    }

                    // Hardcoded to remove the PREU courses
                    // from the list of goal courses.
                    elseif( strtoupper($gc_array['course_code']) == "PREU" ) {
                        unset($goal_course[$id]);
                    }

                    // Remove any course from the goal map
                    // that is not mapped within Daedalus.
                    elseif( !$gc_array['mapped'] ) {
                        unset($goal_course[$id]);
                    }
                }

                // Copy the goal courses to the simpler course_ids array
                // because I was having a hellofatime unsetting the GD values.
                // This array enables the use of the php in_array function.
                $course_ids = $prereq_ids = array();

                foreach( $goal_course as $id => $goal_array ) {
                    $course_ids[] = $id;
                }

                // Create the list of placeholders to accommodate a variable number of arguments.
                $placeholders = implode(',', array_fill(0, count($course_ids), "(%d, %d)"));

                $query_values = array();

                // Create the query values, a student
                // id followed by the slo id value.
                foreach( $course_ids as $cid ) {
                    $query_values[] = $student_id;
                    $query_values[] = $cid;
                }

                // Delete
                db_query("DELETE FROM {mag_goal_student_course} WHERE student_id=%d", $student_id);

                // Insert
                if( $course_ids ) {
                    db_query("INSERT INTO {mag_goal_student_course} (student_id, course_id) VALUES $placeholders", $query_values);
                }

                // Display the goldenrod course goal
                // if the goal type is a course.
                if( $goal_type == "course" ) {
                    $selected_course = $goal_id;
                }

                foreach( $goal_course as $gc ) {

                    // There is a selected course for the goal, color
                    // the course so that it stands out in the map. Do
                    // the same for completed. Format the URL according
                    // to the mapped value.
                    if( $gc['mapped'] || $build_access ) {

                        $current_url = $course_url."/".$gc['course_code']."/".$gc['course_number'];

                        if( $selected_course && $selected_course == $gc['id'] ) {
                            $map_output .= " course".$gc['id']." [shape=box, fillcolor=\"goldenrod\", style=\"rounded,filled\", label=\"".$gc['course']."\", URL=\"".$current_url."\", fontsize=9];\n";
                        }
                        elseif( in_array($gc['id'], $completed_courses) ) {
                            $map_output .= " course".$gc['id']." [shape=box, style=\"rounded,filled\", label=\"".$gc['course']."\", URL=\"".$current_url."\", fontsize=9];\n";
                        }
                        else {
                            $map_output .= " course".$gc['id']." [shape=box, style=\"rounded\", label=\"".$gc['course']."\", URL=\"".$current_url."\", fontsize=9];\n";
                        }

                    }else {

                        $current_url = "http://www.registrar.dal.ca/calendar/class.php?subj=".$gc['course_code']."&num=".$gc['course_number'];

                        if( $selected_course && $selected_course == $gc['id'] ) {
                            $map_output .= " course".$gc['id']." [shape=box, fillcolor=\"goldenrod\", style=\"rounded,filled\", label=\"".$gc['course']."\", URL=\"".$current_url."\", fontsize=9];\n";
                        }
                        elseif( in_array($gc['id'], $completed_courses) ) {
                            $map_output .= " course".$gc['id']." [shape=box, style=\"rounded,filled\", label=\"".$gc['course']."\", URL=\"".$current_url."\", fontsize=9];\n";
                        }
                        else {
                            $map_output .= " course".$gc['id']." [shape=box, style=\"rounded\", label=\"".$gc['course']."\", URL=\"".$current_url."\", fontsize=9];\n";
                        }

                    }

                    // Get all of the prerequisite course information
                    $result = db_query("SELECT prereq_id FROM {dae_prereq_course} WHERE course_id=%d ORDER BY prereq_id, course_id", $gc);
                    while( $row = db_fetch_array($result) ) {

                        if( !in_array($row['prereq_id'], $course_ids)) {

                            //$prereq_title = db_result(db_query("SELECT course FROM {dae_course} WHERE id=%d", $row['prereq_id']));
                            $prereq_data = db_fetch_array(db_query("SELECT course, mapped FROM {dae_course} WHERE id=%d", $row['prereq_id']));

                            $prereq_title = $prereq_data['course'];
                            $prereq_map   = $prereq_data['mapped'];

                            // Get the course info for the URL
                            $prereq_info = explode(" ", $prereq_title);
                            $pre_code = $prereq_info[0];
                            $pre_numb = $prereq_info[1];

                            if( $prereq_map || $build_access ) {

                                $current_url = $course_url."/".$pre_code."/".$pre_numb;

                                if( in_array($row['prereq_id'], $completed_courses) ) {
                                    $map_output .= " course".$row['prereq_id']." [shape=ellipse, style=\"rounded,filled\", label=\"".$prereq_title."\", URL=\"".$current_url."\", fontsize=9];\n";
                                }else {
                                    $map_output .= " course".$row['prereq_id']." [shape=ellipse, style=\"rounded\", label=\"".$prereq_title."\", URL=\"".$current_url."\", fontsize=9];\n";
                                }

                            }else {

                                $current_url = "http://www.registrar.dal.ca/calendar/class.php?subj=".$pre_code."&num=".$pre_numb;

                                if( in_array($row['prereq_id'], $completed_courses) ) {
                                    $map_output .= " course".$row['prereq_id']." [shape=ellipse, style=\"filled\", label=\"".$prereq_title."\", URL=\"".$current_url."\", fontsize=9];\n";
                                }else {
                                    $map_output .= " course".$row['prereq_id']." [shape=ellipse, label=\"".$prereq_title."\", URL=\"".$current_url."\", fontsize=9];\n";
                                }

                            }

                            // Show prereqs links with a small line
                            $map_output .= " course".$row['prereq_id']." -> course".$gc['id']." [style=\"setlinewidth(2)\"];\n";

                        }else {
                            $map_output .= " course".$row['prereq_id']." -> course".$gc['id']." [style=\"setlinewidth(3)\"];\n";
                        }

                        // Create list of prereq ids to check to see
                        // if any prereq is a postreq to a goal course.
                        $prereq_ids[$row['prereq_id']] = $row['prereq_id'];
                    }
                }

                // For each prerequiste to a goal course find it's prerequisite
                // courses. For each of these prereqs add a link only if the prereq
                // is a goal course also. This will help unclutter the goal map.
                foreach( $prereq_ids as $pre_id ) {

                    if( !in_array($pre_id, $course_ids) ) {

                        // Get all of the prerequisite course information
                        $result = db_query("SELECT prereq_id, set_id FROM {dae_prereq_course} WHERE course_id=%d ORDER BY prereq_id, course_id", $pre_id);
                        while( $row = db_fetch_array($result) ) {

                            if( in_array($row['prereq_id'], $course_ids) ) {
                                $map_output .= " course".$row['prereq_id']." -> course".$pre_id." [style=\"setlinewidth(3)\"];\n";
                            }

                            elseif( in_array($row['prereq_id'], $prereq_ids) ) {
                                $map_output .= " course".$row['prereq_id']." -> course".$pre_id." [style=\"setlinewidth(2)\"];\n";
                            }

                            // Added this code to show prereqs to unrequired prereqs (3/29/2011)
                            else {

                                $preprereq_data = db_fetch_array(db_query("SELECT course, mapped FROM {dae_course} WHERE id=%d", $row['prereq_id']));

                                $preprereq_title = $preprereq_data['course'];
                                $preprereq_map   = $preprereq_data['mapped'];

                                // Get the course info for the URL
                                $preprereq_info = explode(" ", $preprereq_title);
                                $prepre_code = $preprereq_info[0];
                                $prepre_numb = $preprereq_info[1];

                                if( $preprereq_map || $build_access ) {

                                    $current_url = $course_url."/".$prepre_code."/".$prepre_numb;

                                    $map_output .= " course".$row['prereq_id']." [shape=point, height=0.12, width=0.12, style=\"rounded\", label=\"".$preprereq_title."\", URL=\"".$current_url."\", fontsize=1];\n";

                                }else {

                                    $current_url = "http://www.registrar.dal.ca/calendar/class.php?subj=".$prepre_code."&num=".$prepre_numb;

                                    $map_output .= " course".$row['prereq_id']." [shape=point, height=0.12, width=0.12, style=\"rounded\", label=\"".$preprereq_title."\", URL=\"".$current_url."\", fontsize=1];\n";

                                }

                                if( $row['set_id'] > 1 ) {
                                    $map_output .= " course".$row['prereq_id']." -> course".$pre_id." [style=\"setlinewidth(1), dashed\"];\n";
                                }else {
                                    $map_output .= " course".$row['prereq_id']." -> course".$pre_id." [style=\"setlinewidth(1)\"];\n";
                                }
                            }
                        }
                    }
                }

                $map_output .= "}";

                // Remove old user maps
                system('rm '.$pwd.'/graphviz/user'.$student_id.'*');

                // Get the iteration to append to the file.
                // This prevents the browser from displaying
                // the cached file because the location changes.
                $iteration = daedalus_get_setting("graphviz iteration");

                // Update the iteration value
                db_query("UPDATE {dae_settings} SET VALUE=%d WHERE setting='%s'", $iteration+1, "graphviz iteration");

                // The file name
                $user_map = "user".$student_id."_goal_map".$iteration;

                // Write the .png output to file
                $myFile = $pwd."/graphviz/".$user_map.".dot";
                $fileHandle = fopen($myFile, 'w+') or die("can't open file");
                fwrite($fileHandle, $map_output);
                fclose($fileHandle);

                // Execute dot to create the map output.
                system($graphviz_path.'dot '.$pwd.'/graphviz/'.$user_map.'.dot -Tcmapx -o '.$pwd.'/graphviz/'.$user_map.'.map');

                // Execute dot to create the gif output.
                system($graphviz_path.'dot '.$pwd.'/graphviz/'.$user_map.'.dot -Tgif -o '.$pwd.'/graphviz/'.$user_map.'.gif');

                daedalus_parse_imagemap( $pwd.'/graphviz/'.$user_map.'.map' );

                // Store the newly creatd map into a variable
                // This is appended to the HTML to make the
                // gif image file linkable.
                $myFile = $pwd."/graphviz/".$user_map.".map";
                $file = fopen($myFile, "rb") or exit("Unable to open file!");

                while(!feof($file)) {
                    $map_file .= fgets($file);
                }

                fclose($file);

                  //////////////////////////////////////////////////////////////////////
                 //////////////////////////////////////////////////////////////////////
                // DISPLAY THE INFORMATION FOR THE USER

                if( $goal_type == "course" ) {

                    $course_info = db_fetch_array(db_query("SELECT course, course_name FROM {dae_course} WHERE id=%d", $goal_id));

                    $map_title = "My Course Goal:  <i>" . $course_info['course'] . " - " . $course_info['course_name'] . "</i>";

                }
                elseif( $goal_type == "label" ) {

                    $career = db_result(db_query("SELECT goal_label FROM {mag_goal_label} WHERE id=%d", $goal_id));

                    $map_title = "My Career Goal:  <i>" . $career . "</i>";

                }

                $form["goal_map_$cid"] = array(
                    '#title' => t($map_title),
                    '#type' => 'fieldset',
                    '#collapsible' => TRUE,
                    '#collapsed' => FALSE,
                );

                $user_map_img = "<img id='Constellation' src='".$base_url."/graphviz/".$user_map.".gif' usemap='#Constellation' />";

                // The goal map legend image url.
                $goal_legend = "<img src='".$base_url."/".daedalus_get_setting("goal legend")."' align='top' />";

                // Display the legend with the user map. Add the map file to
                // the prefix. This is required to make the image map functional.
                $form["goal_map_$cid"][] = array(
                    '#type' => 'item',
                    '#prefix' => t($map_file."<br /><blockquote>".$goal_legend." ".$user_map_img),
                    '#suffix' => t("</blockquote>"),
                );

                // Iterate the list of goal courses
                foreach( $goal_course as $id => $gc_array ) {

                    // The link to the current course;
                    $goto_course_url = $course_url."/".str_replace(" ", "/", $gc_array['course']);

                    $cid = $gc_array['id'];

                    $form["course_$cid"] = array(
                        '#title' => t($gc_array['course']." - ".$gc_array['course_name']),
                        '#type' => 'fieldset',
                        '#collapsible' => TRUE,
                        '#collapsed' => TRUE,
                    );

                    $slo_list = "";

                    $result = db_query("SELECT slo_id FROM {dae_course_slo} WHERE course_id=%d", $cid);
                    while( $row = db_fetch_array($result) ) {

                        $slo_info = db_fetch_array(db_query("SELECT id, slo_text FROM {dae_slo} WHERE id=%d", $row['slo_id']));

                        $slo_id   = $slo_info['id'];
                        $slo_text = $slo_info['slo_text'];

                        // The SLO must be in the list created
                        // earlier to be relvant to the course.
                        if( in_array($slo_id, $recursed_slos) ) {
                            $slo_list .= "<li><a href='".$slo_url."/".$slo_id."' target='_blank'>".$slo_text."</a><li>";
                        }

                    }

                    $form["course_$cid"][] = array(
                        '#title' => t('Goal Specific Student Learning Outcomes'),
                        '#type' => 'item',
                        '#value' => t("<ul>".$slo_list."</ul>"),
                        '#prefix' => '<blockquote>',
                        '#suffix' => '</blockquote>',
                    );


                    if( $browser == "MSIE" ) {

                        // If the current browser is Microsoft Internet Explorer the button style links
                        // will not work so use a simple textual link to open the new window.
                        $form["course_$cid"][] = array(
                            '#type' => 'item',
                            '#value' => t('<a href="'.$goto_course_url.'" target="_blank">View Course</a>'),
                            '#prefix' => '<blockquote>',
                            '#suffix' => '</blockquote>',
                        );

                    }else {

                        // Every other browser handles the button style wrapped link no problem. Go figure?
                        $form["course_$cid"][] = array(
                            '#type' => 'item',
                            '#value' => t('<input type="button" value="View Course" style="width:1.5in" />'),
                            '#prefix' => '<blockquote><a href="'.$goto_course_url.'" target="_newtab">',
                            '#suffix' => '</a></blockquote>',
                        );
                    }

                }

                $form['change-goal'] = array(
                    '#type' => 'submit',
                    '#value' => t('Change Goal'),
                    '#prefix' => t('<br />'),
                );

                if( $in_session ) {

                    $form['end-session'] = array(
                        '#type' => 'submit',
                        '#value' => t('End advising session'),
                    );

                    // Submit the hidden session id.
                    $form['$session_id'] = array( '#type' => 'value', '#value' => $session_id, );

                }

            } // if( $course_ids )

            elseif( !$installed ) {
                drupal_set_message(t('There is a problem with the graphviz installation. Please contact an administrator.'), 'warning');
            }

        }

        return $form;

    } // END if( $advisor_no_session )

    else {

        drupal_set_message(t('You must be in session to view a students program goals.'));

        drupal_goto($base_url."/".daedalus_get_setting("advise student"));

    }

}


/**
 * Implementation of hook_submit().
 */
function daedalus_program_goal_form_submit( $form, &$form_state ) {

    global $base_url;
    $page_url = daedalus_get_setting("program goal");

    $student_id = $form_state['values']['student-id'];

    switch($form_state['values']['op']) {

        case $form_state['values']['end-session']:

            db_query("DELETE FROM {mag_advisor_session} WHERE id=%d", $form_state['values']['session-id']);

            drupal_set_message(t('Current session closed.'));

            // This generates a new session id.
            $_COOKIE[session_regenerate_id()];

            drupal_goto($base_url."/".daedalus_get_setting("advise student"));

            break;

        case $form_state['values']['cancel']:

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['change-goal']:

            drupal_goto($base_url."/".$page_url."/confirm_switch");

            break;

        case $form_state['values']['delete-forward']:

            // Remove course info
            db_query("DELETE FROM {mag_goal_student_course} WHERE student_id=%d", $student_id);

            // Remove SLO info
            db_query("DELETE FROM {mag_goal_student_slo} WHERE student_id=%d", $student_id);

            // Update the user info
            db_query("UPDATE {mag_student_identification} SET goal_type='', goal_id='' WHERE id=%d", $student_id);

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['delete-reverse']:

            drupal_goto($base_url."/".$page_url);

            break;

        case $form_state['values']['confirm-course']:

            $course_id = $form_state['values']['course-id'];

            db_query("UPDATE {mag_student_identification} SET goal_type='course', goal_id=%d WHERE id=%d", $course_id, $student_id);
 
            drupal_goto($base_url."/".$page_url."/submit_goal/");

            break;

        case $form_state['values']['confirm-label']:

            $label_id = $form_state['values']['label-id'];

            db_query("UPDATE {mag_student_identification} SET goal_type='label', goal_id=%d WHERE id=%d", $label_id, $student_id);

            drupal_goto($base_url."/".$page_url."/submit_goal/");

            break;

        case $form_state['values']['dae-help-submit']:

            $page_url_length = sizeof(explode('/',$page_url));

            $param[0] = arg(0+$page_url_length);

            $help_text = $form_state['values']['dae-help'];

            if( $help_text ) {

                if( db_result(db_query("SELECT COUNT(*) FROM {dae_page_help} WHERE page_url='%s'", $page_url)) ) {
                    db_query("UPDATE {dae_page_help} SET help_text='%s' WHERE page_url='%s'", $help_text, $page_url);
                }else {
                    db_query("INSERT INTO {dae_page_help} (page_url, help_text) VALUES ('%s', '%s')", $page_url, $help_text);
                }

                drupal_set_message(t('Help information saved.'));

                    if( $param[0]) {
                        drupal_goto($base_url."/".$page_url."/".$param[0]);
                    }else {
                    drupal_goto($base_url."/".$page_url);
                    }
            }

            break;

    }

}